/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import fr.ird.t3.entities.T3DAOHelper;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.util.List;

/**
 * T3 Migration callback.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0.1
 */
public class T3MigrationCallback
        extends TopiaMigrationCallbackByClassNG {

    public T3MigrationCallback() {
        super(new MigrationCallBackForVersionResolverByServiceLoader());
    }

    @Override
    public Version getApplicationVersion() {
        return VersionUtil.valueOf(T3DAOHelper.getModelVersion());
    }

    @Override
    public boolean askUser(Version version, List<Version> versions) {
        return true;
    }

}
