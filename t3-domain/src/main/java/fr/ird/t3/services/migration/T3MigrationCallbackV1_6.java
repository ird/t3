package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2013 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.util.List;

/**
 * Migration for version {@code 1.5}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.5
 */
public class T3MigrationCallbackV1_6 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return VersionUtil.valueOf("1.6");
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // See http://forge.codelutin.com/issues/2298
        updateSpeciesThresholdNumberLevel3(queries);

    }

    protected void updateSpeciesThresholdNumberLevel3(List<String> queries) {

        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 25, thresholdNumberLevel3ObjectSchoolType = 25, topiaVersion = topiaVersion + 1;");
        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 150, thresholdNumberLevel3ObjectSchoolType = 150 WHERE faoId = 'YFT';");
        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 150, thresholdNumberLevel3ObjectSchoolType = 150 WHERE faoId = 'SKJ';");
        queries.add("UPDATE Species SET thresholdNumberLevel3ObjectSchoolType = 180 WHERE faoId = 'BET';");

    }

}
