/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.collect.Lists;
import fr.ird.t3.services.ioc.Injector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Service to inject stuff using T3 IOC engine.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see Injector
 * @since 1.0
 */
public class IOCService extends T3ServiceSupport implements T3ServiceInitializable, T3ServiceSingleton {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IOCService.class);

    protected Collection<Injector<?, ?>> injectors;

    @Override
    public void init(T3ServiceContext serviceContext) {
        for (Injector<?, ?> injector : getInjectors()) {
            injector.init(serviceContext);
        }
    }

    public void injectExcept(Object bean,
                             Class<?>... excludedInjectors) throws Exception {

        // obtain the list of available injectors
        Collection<Injector<?, ?>> injectorsToUse;

        if (excludedInjectors.length == 0) {

            // use all injectors
            injectorsToUse = getInjectors();
        } else {
            injectorsToUse = Lists.newArrayList(getInjectors());
            List<Class<?>> annotations = Arrays.asList(excludedInjectors);
            Iterator<Injector<?, ?>> itr = injectorsToUse.iterator();
            while (itr.hasNext()) {
                Injector<?, ?> injector = itr.next();
                if (annotations.contains(injector.getAnnotationType())) {
                    itr.remove();
                }
            }
        }

        // get all fields for the given type
        injectForType(bean, bean.getClass(), injectorsToUse);
    }

    public void injectOnly(Object bean,
                           Class<?>... onlyInjectors) throws Exception {

        // obtain the list of available injectors
        Collection<Injector<?, ?>> injectorsToUse =
                Lists.newArrayList(getInjectors());
        List<Class<?>> annotations = Arrays.asList(onlyInjectors);
        Iterator<Injector<?, ?>> itr = injectorsToUse.iterator();
        while (itr.hasNext()) {
            Injector<?, ?> injector = itr.next();
            if (!annotations.contains(injector.getAnnotationType())) {
                itr.remove();
            }
        }

        // get all fields for the given type
        injectForType(bean, bean.getClass(), injectorsToUse);
    }

    protected void injectForType(Object bean,
                                 Class<?> beanType,
                                 Collection<Injector<?, ?>> injectors) throws Exception {
        Field[] fields = beanType.getDeclaredFields();
        for (Field field : fields) {

            if (Modifier.isFinal(field.getModifiers())) {

                // nothing to affect to a final field
                continue;
            }

            if (Modifier.isStatic(field.getModifiers())) {

                // nothing to affect to a static field
                continue;
            }

            Injector injector = getInjector(field, injectors);
            if (injector != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Will use injector " + injector + " for " + field);
                }
                injector.processField(field, bean);
            }
        }
        Class<?> superclass = beanType.getSuperclass();
        if (superclass != null && superclass.isAssignableFrom(beanType)) {
            injectForType(bean, superclass, injectors);
        }
    }

    protected Injector<?, ?> getInjector(Field field,
                                         Collection<Injector<?, ?>> injectors) {

        Injector<?, ?> result = null;
        for (Injector<?, ?> injector : injectors) {
            Class<? extends Annotation> annotationType =
                    injector.getAnnotationType();
            if (field.isAnnotationPresent(annotationType)) {
                result = injector;
                break;
            }
        }

        return result;
    }

    protected Collection<Injector<?, ?>> getInjectors() {
        if (injectors == null) {
            injectors = Lists.newArrayList();
            for (Injector<?, ?> injector :
                    ServiceLoader.load(Injector.class)) {

                injectors.add(injector);
            }
        }
        return injectors;
    }
}
