package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created on 25/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public abstract class T3MigrationCallbackSupport extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3MigrationCallbackSupport.class);

    protected final Version version;

    protected T3MigrationCallbackSupport(Version version) {
        this.version = version;
    }

    @Override
    public final Version getVersion() {
        return version;
    }

    protected void addScript(String rank, String prefix, List<String> queries) {

        String[] migrationQueries = loadScript(rank, prefix);
        for (String migrationQuery : migrationQueries) {
            migrationQuery = migrationQuery.trim();
            if (!migrationQuery.startsWith("--")) {
                queries.add(migrationQuery);
            }
        }

    }

    private String[] loadScript(String rank, String migrationScript) {

        String scriptPath = "/db/migration/V" + version.getValidName() + "_" + rank + "_" + migrationScript + ".sql";

        if (log.isInfoEnabled()) {
            log.info("Will will load migration script: " + scriptPath);
        }
        try (InputStream stream = getClass().getResourceAsStream(scriptPath)) {
            Preconditions.checkNotNull(stream, "Script " + scriptPath + " non trouvé dans le class-path.");
            String content = IOUtils.toString(stream, Charsets.UTF_8);
            return content.split("\n");

        } catch (IOException e) {
            throw new TopiaRuntimeException("Could not load migration script: " + migrationScript, e);
        }

    }

}