/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Configuration;
import org.nuiton.topia.TopiaContext;

import java.util.Date;
import java.util.Locale;

/**
 * Default implementation of the service context.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class DefaultT3ServiceContext implements T3ServiceContext {

    private TopiaContext transaction;

    private TopiaContext internalTransaction;

    private Locale locale;

    private final T3Configuration applicationConfiguration;

    private final T3ServiceFactory serviceFactory;

    public static DefaultT3ServiceContext newContext(
            DefaultT3ServiceContext serviceContext,
            TopiaContext internalTransaction,
            TopiaContext transaction) {
        return newContext(serviceContext.getLocale(),
                          internalTransaction,
                          transaction,
                          serviceContext.getApplicationConfiguration(),
                          serviceContext.getServiceFactory()
        );
    }

    public static DefaultT3ServiceContext newContext(
            Locale locale,
            TopiaContext internalTransaction,
            TopiaContext transaction,
            T3Configuration applicationConfiguration,
            T3ServiceFactory serviceFactory) {
        return new DefaultT3ServiceContext(locale,
                                           internalTransaction,
                                           transaction,
                                           applicationConfiguration,
                                           serviceFactory);
    }

    protected DefaultT3ServiceContext(Locale locale,
                                      TopiaContext internalTransaction,
                                      TopiaContext transaction,
                                      T3Configuration applicationConfiguration,
                                      T3ServiceFactory serviceFactory) {
        this.locale = locale;
        this.internalTransaction = internalTransaction;
        this.transaction = transaction;
        this.applicationConfiguration = applicationConfiguration;
        this.serviceFactory = serviceFactory;
    }

    @Override
    public TopiaContext getInternalTransaction() {
        return internalTransaction;
    }

    @Override
    public TopiaContext getTransaction() {
        return transaction;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public T3Configuration getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setInternalTransaction(TopiaContext internalTransaction) {
        this.internalTransaction = internalTransaction;
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        this.transaction = transaction;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }
}
