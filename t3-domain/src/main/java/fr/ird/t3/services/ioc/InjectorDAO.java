/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;

/**
 * Fires the {@link InjectDAO} annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectDAO
 * @since 1.0
 */
public class InjectorDAO extends AbstractInjector<InjectDAO, TopiaTransactionAware> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InjectorDAO.class);

    public InjectorDAO() {
        super(InjectDAO.class);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      TopiaTransactionAware bean,
                                      InjectDAO annotation) throws Exception {
        // get entity type
        Class<? extends TopiaEntity> entityType = annotation.entityType();

        // get dao
        TopiaDAO<?> dao = getDAO(bean, entityType);

        if (log.isInfoEnabled()) {
            log.info("Will set DAO [" + dao + "] to field " + field);
        }
        return dao;
    }

}
