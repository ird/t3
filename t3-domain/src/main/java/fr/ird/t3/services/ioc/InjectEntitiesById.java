/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Inject a list of entities using a collection of topia ids.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectorEntitiesById
 * @since 1.0
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface InjectEntitiesById {


    /**
     * Obtains the id of the parameter of the business action which contains
     * set of ids of the entities to load and inject.
     *
     * @return the id of the parameter containing ids of entities
     */
    String path() default "";

    /**
     * Obtains the type of entities to load.
     *
     * @return the type of entities to load
     */
    Class<? extends TopiaEntity> entityType();
}
