/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3ServiceInitializable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Contract of an injector that fires a injector annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see IOCService
 * @since 1.0
 */
public interface Injector<A extends Annotation, B> extends T3ServiceInitializable {

    /** @return the annotation managed by this injector. */
    Class<A> getAnnotationType();

    /**
     * Given a {@code field} and a {@code bean} containing this field, do the
     * injection.
     *
     * @param field the field to set
     * @param bean  the bean containing the field
     * @throws Exception if any problem while doing the injection
     */
    void processField(Field field, B bean) throws Exception;
}
