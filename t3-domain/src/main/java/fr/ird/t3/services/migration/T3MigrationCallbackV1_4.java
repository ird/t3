package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2013 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.util.List;

/**
 * Migration for version {@code 1.4}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.4
 */
public class T3MigrationCallbackV1_4 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return VersionUtil.valueOf("1.4");
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // Add more faoid on some species (http://forge.codelutin.com/issues/1185)
        addMissingFaoIds(queries);

        // Add more lengthWeightconversion some species (http://forge.codelutin.com/issues/1185)
        addMissingLengthWeightConversions(queries);

        // Remove ExtrapolatedAllSetSpeciesCatWeight entity (http://forge.codelutin.com/issues/2027)
        removeExtrapolatedAllSetSpeciesCatWeight(queries);

        // A natural id on ExtrapolatedAllSetSpeciesFrequency entity (http://forge.codelutin.com/issues/2044)
        addUniqueContraints(queries);

        // Add new vessels (http://forge.codelutin.com/issues/1979)
        addNewVessels(queries);
    }

    private void addNewVessels(List<String> queries) {
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358543#0.2979721129875378';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358543#0.3906068976243432';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358543#0.288815696470218';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358543#0.864128160166968';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.7561123148900394';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.09792007282064641';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.7304879767409045';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.836288278983629';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.5750379746821634';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360331358544#0.1767963573130531';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678972#0.8443155926826021';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678973#0.2748185673764696';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678973#0.49964044235753113';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678973#0.9794926145191589';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678973#0.4921325222516365';");
        queries.add("delete from vessel where topiaid='fr.ird.t3.entities.reference.Vessel#1360332678973#0.4082699510307807';");

        queries.add("INSERT INTO PUBLIC.VESSEL(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, KEELCODE, YEARSERVICE, FLAGCHANGEDATE, LENGTH, POWER, CAPACITY, SEARCHMAXIMUM, LIBELLE, COMMENT, FLAGCOUNTRY, VESSELTYPE, VESSELSIZECATEGORY, FLEETCOUNTRY) VALUES\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358543#0.2979721129875378', 0, TIMESTAMP '2013-02-08 14:50:00.001', 861, 618, 2012, NULL, 90.00, 5163.0, 1470.0, 17.5, 'DOLOMIEU', 'Sister-ship du FRANCHE TERRE et MANAPANY', \n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358543#0.3906068976243432', 0, TIMESTAMP '2013-02-08 14:50:00.001', 862, 504, 1974, NULL, 54.50, 2200.0, 750.0, 12.0, 'AGNES 2', 'Changement de nom en mai 2012', \n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358543#0.288815696470218', 0, TIMESTAMP '2013-02-08 14:50:00.001', 863, 619, 1996, NULL, 25.56, 624.0, 0, 0, 'AMATXU', NULL, \n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528730#0.1379381127148035', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358543#0.864128160166968', 0, TIMESTAMP '2013-02-08 14:50:00.001', 864, 620, 1996, NULL, 26.00, 316.0, 0.0, 0.0, 'CANALECHEBARRIA', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528730#0.1379381127148035', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.7561123148900394', 0, TIMESTAMP '2013-02-08 14:50:00.001', 865, 621, 1991, NULL, 36.50, 870.0, 0.0, 0.0, 'FURABOLOS', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8804897590207591', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.09792007282064641', 0, TIMESTAMP '2013-02-08 14:50:00.001', 866, 622, 1997, NULL, 25.26, 316.0, 0.0, 0.0, 'AITXETXU', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528730#0.1379381127148035', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.7304879767409045', 0, TIMESTAMP '2013-02-08 14:50:00.001', 867, 623, 0, NULL, 36.25, 0.0, 0.0, 0.0, 'OCEAN EXPLORER', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.836288278983629', 0, TIMESTAMP '2013-02-08 14:50:00.001', 868, 624, 0, NULL, 36.25, 0.0, 0.0, 0.0, 'OCEAN SCOUT 1', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.5750379746821634', 0, TIMESTAMP '2013-02-08 14:50:00.001', 869, 625, 1975, NULL, 40.60, 1200.0, 0.0, 0.0, 'BONEA', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8804897590207591', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360331358544#0.1767963573130531', 0, TIMESTAMP '2013-02-08 14:50:00.001', 879, 626, 1965, NULL, 30.00, 700.0, 0.0, 0.0, 'MARIA ROSARIO DE FATIMA', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8804897590207591', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678972#0.8443155926826021', 0, TIMESTAMP '2013-02-08 14:50:00.001', 871, 627, 0, NULL, 36.25, 0.0, 0.0, 0.0, 'URPECO UNO', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678973#0.2748185673764696', 0, TIMESTAMP '2013-02-08 14:50:00.001', 872, 246, 1977, '2012-07-01', 78.82, 6000.0, 2272.0, 0.0, 'XIXILI', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.7353550641367269', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.46323564513794135', 'fr.ird.t3.entities.reference.Country#1297580528739#0.7353550641367269'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678973#0.49964044235753113', 0, TIMESTAMP '2013-02-08 14:50:00.001', 873, 628, 2012, NULL, 90.00, 5163.0, 1470.0, 17.5, 'BELOUVE', 'Sister-ship du FRANCHE TERRE et MANAPANY', \n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678973#0.9794926145191589', 0, TIMESTAMP '2013-02-08 14:50:00.001', 874, 396, 1989, NULL, 77.30, 4690.0, 1650.0, 0.0, 'PLAYA DE NOJA', 'Octobre 2012 changement de nom', \n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678973#0.4921325222516365', 0, TIMESTAMP '2013-02-08 14:50:00.001', 875, 629, 1987, NULL, 30.60, 675.0, 0.0, 0.0, 'MONTECLARO', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528740#0.35990795991992364', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8804897590207591', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463'),\n" +
                    "('fr.ird.t3.entities.reference.Vessel#1360332678973#0.4082699510307807', 0, TIMESTAMP '2013-02-08 14:50:00.001', 876, 337, 1974, NULL, 57.43, 1800.0, 0.0, 0.0, 'MARINE 711', NULL,\n" +
                    "'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', \n" +
                    "'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396');");
        queries.add("update vessel set vesselvirtual= false where vesselvirtual is null;");
    }

    private void addUniqueContraints(List<String> queries) {
        queries.add("ALTER TABLE extrapolatedallsetspeciesfrequency ADD CONSTRAINT extrapolatedallsetspeciesfrequency_unique UNIQUE (activity,species,lflengthclass);");
    }

    private void addMissingFaoIds(List<String> queries) {

        queries.add("UPDATE species SET faoid = 'BIP' WHERE code = 16;");
        queries.add("UPDATE species SET faoid = 'FRZ' WHERE code = 18;");
        queries.add("UPDATE species SET faoid = 'BOP' WHERE code = 19;");
        queries.add("UPDATE species SET faoid = 'WAH' WHERE code = 20;");
        queries.add("UPDATE species SET faoid = 'SSM' WHERE code = 21;");
        queries.add("UPDATE species SET faoid = 'KGM' WHERE code = 22;");
        queries.add("UPDATE species SET faoid = 'MAW' WHERE code = 23;");
        queries.add("UPDATE species SET faoid = 'CER' WHERE code = 24;");
        queries.add("UPDATE species SET faoid = 'COM' WHERE code = 25;");
        queries.add("UPDATE species SET faoid = 'GUT' WHERE code = 26;");
        queries.add("UPDATE species SET faoid = 'STS' WHERE code = 27;");
        queries.add("UPDATE species SET faoid = 'BRS' WHERE code = 28;");
        queries.add("UPDATE species SET faoid = '1BUM' WHERE code = 34;");
        queries.add("UPDATE species SET faoid = '1RAV' WHERE code = 43;");

    }

    private void addMissingLengthWeightConversions(List<String> queries) {

        queries.add(
                "INSERT INTO PUBLIC.LENGTHWEIGHTCONVERSION(TOPIAID, TOPIAVERSION, SEXE, BEGINDATE, SPECIES, OCEAN, TOPIACREATEDATE, COEFFICIENTS, TOLENGTHRELATION, TOWEIGHTRELATION, ENDDATE) VALUES\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187464#0.35237659955136724', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.10329800580627058', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.462', 'a=3.5E-6:b=3.158', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187536#0.3704075476668607', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.10329800580627058', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.536', 'a=3.5E-6:b=3.158', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187545#0.6964781259130424', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.2032631853627409', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.545', 'a=0.5:b=0.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187555#0.6554877034117935', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528891#0.2626954826359922', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.555', 'a=4.4E-6:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187564#0.5940198964598981', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.7387428235028691', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.564', 'a=4.6E-6:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187596#0.35863352079913013', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.15829471459979372', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.596', 'a=1.54E-5:b=3.08', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187607#0.4846295489557012', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.1885405502623827', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.607', 'a=6.5E-6:b=2.96', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187626#0.7594022071936318', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.4823294475226977', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.626', 'a=7.6E-6:b=3.249', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187648#0.2400798950274411', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.4823294475226977', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.648', 'a=7.6E-6:b=3.249', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187664#0.3393779244292533', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.7195876762319068', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.664', 'a=2.8E-7:b=4.13514', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187670#0.0857862823097687', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.7195876762319068', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.67', 'a=2.8E-7:b=4.13514', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187685#0.44391333921412557', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528891#0.42371878801807505', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.685', 'a=1.61E-5:b=2.72', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187690#0.32419279490993924', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.7235429225787087', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.69', 'a=3.0E-5:b=2.908', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187700#0.7882646490239464', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.2905388174926805', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.7', 'a=4.4E-6:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187706#0.24351968176505834', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.5141201542305377', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.706', 'a=1.43E-5:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187712#0.034150463888226845', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7797586779656372', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.712', 'a=50.0:b=0.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187720#0.6766276263895612', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7797586779656372', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.72', 'a=50.0:b=0.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187726#0.011798763639261689', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.5685612495908416', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.726', 'a=4.4E-6:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187733#0.8153598640282579', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.5685612495908416', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.732', 'a=4.4E-6:b=3.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187740#0.9869530786381929', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528891#0.7255260110145089', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.74', 'a=0.5:b=0.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187753#0.11730343971743751', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.2565749169300484', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.753', 'a=1.65E-5:b=3.062', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187759#0.911882675730391', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.9734051109859334', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.759', 'a=7.8E-6:b=3.21', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187764#0.15278281209546685', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528892#0.9734051109859334', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.764', 'a=7.8E-6:b=3.21', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187770#0.6146774138456206', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.9598281772783329', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.77', 'a=3.2E-6:b=3.201', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187775#0.9480850486796076', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528890#0.9598281772783329', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2013-02-19 08:33:07.775', 'a=3.2E-6:b=3.201', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),\n" +
                "('fr.ird.t3.entities.reference.LengthWeightConversion#1361259187781#0.16426955369199603', 0, 0, TIMESTAMP '1969-12-31 00:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528891#0.9291243253311353', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2013-02-19 08:33:07.78', 'a=0.5:b=0.0', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL);");
    }

    private void removeExtrapolatedAllSetSpeciesCatWeight(List<String> queries) {
        queries.add("DROP TABLE extrapolatedAllSetSpeciesCatWeight;");
    }

}