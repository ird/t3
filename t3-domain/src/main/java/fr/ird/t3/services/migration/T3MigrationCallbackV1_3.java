/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.util.List;

/**
 * Migration for version {@code 1.3}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class T3MigrationCallbackV1_3
        extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return VersionUtil.valueOf("1.3");
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // Add two columns on species (http://forge.codelutin.com/issues/1151)
        addSpeciesColumns(queries);

        // Change some int types to float or Float (http://forge.codelutin.com/issues/1160)
        changeIntToFloat(queries);
    }

    private void addSpeciesColumns(List<String> queries) {
        queries.add("ALTER TABLE species ADD COLUMN faoId VARCHAR(255) NULL UNIQUE;");
        queries.add("ALTER TABLE species ADD COLUMN wormsId integer NULL UNIQUE;");
    }

    private void changeIntToFloat(List<String> queries) {
        queries.add("ALTER TABLE samplespeciesFrequency ALTER COLUMN numberextrapolated TYPE real;");
        queries.add("ALTER TABLE setspeciesFrequency ALTER COLUMN number TYPE real;");
        queries.add("ALTER TABLE ExtrapolatedAllSetSpeciesFrequency ALTER COLUMN number TYPE real;");
    }
}
