/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.util.List;

/**
 * Migration for version {@code 1.2}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class T3MigrationCallbackV1_2
        extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return VersionUtil.valueOf("1.2");
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // add missing weight category treatments (see http://forge.codelutin.com/issues/1048)
        addMissingWeightCategoryTreatment(queries);

        // add N1ResultState + remove sampleStandardised on Trip (http://forge.codelutin.com/issues/1036)
        adaptTripN1Flags(queries);

        // rename trip column (http://forge.codelutin.com/issues/1028)
        renameTripColumns(queries);

        // add new fileds in Vessel (http://forge.codelutin.com/issues/1120)
        addFieldsInVessel(queries);

        // add missing references (http://forge.codelutin.com/issues/1114
        // and http://forge.codelutin.com/issues/1116)
        addMissingReferences(queries);

        // add completionStatus flag on trip (http://forge.codelutin.com/issues/1071)
        addTripCompletionStatus(queries);

        // add N0 other trip flags (http://forge.codelutin.com/issues/1137)
        adaptTripN0Flags(queries);

        // remove oboslete fields from activity (http://forge.codelutin.com/issues/1142)
        removeActivityColumns(queries);

        // rename extraploatedallsetspeciesfrequency entity (http://forge.codelutin.com/issues/1141)
        renameExtraploatedallsetspeciesfrequencyEntity(queries);

        // add  and rename fields on activities (http://forge.codelutin.com/issues/1143)
        addActivityNewFlagsForN2AndN3(queries);

        // remove level2|3 computed field on trip (http://forge.codelutin.com/issues/1145)
        removeTripColumns(queries);

        addNewIndexs(queries);
    }

    private void addNewIndexs(List<String> queries) {
        queries.add("CREATE INDEX idx_Activity_setSpeciesFrequency ON setSpeciesFrequency(activity)");
        queries.add("CREATE INDEX idx_Activity_setSpeciesCatWeight ON setSpeciesCatWeight(activity)");
        queries.add(
                "CREATE INDEX idx_Activity_extrapolatedAllSetSpeciesFrequency ON extrapolatedAllSetSpeciesFrequency(activity)");
        queries.add(
                "CREATE INDEX idx_Activity_extrapolatedAllSetSpeciesCatWeight ON extrapolatedAllSetSpeciesCatWeight(activity)");
    }

    private void addActivityNewFlagsForN2AndN3(List<String> queries) {
        queries.add("ALTER TABLE activity ADD COLUMN useMeanStratumCompositionN2 boolean;");
        queries.add("ALTER TABLE activity ADD COLUMN useMeanStratumCompositionN3 boolean;");
        queries.add("ALTER TABLE activity RENAME COLUMN level2Stratum TO stratumLevelN2;");
        queries.add("ALTER TABLE activity RENAME COLUMN level3Stratum TO stratumLevelN3;");
    }

    private void removeActivityColumns(List<String> queries) {
        queries.add("ALTER TABLE activity DROP COLUMN weightedWeightMinus10;");
        queries.add("ALTER TABLE activity DROP COLUMN weightedWeightPlus10;");
    }

    private void removeTripColumns(List<String> queries) {
        queries.add("ALTER TABLE trip DROP COLUMN level2computed;");
        queries.add("ALTER TABLE trip DROP COLUMN level3computed;");
    }

    private void addTripCompletionStatus(List<String> queries) {
        queries.add("ALTER TABLE trip ADD COLUMN completionstatus integer;");
    }

    private void renameExtraploatedallsetspeciesfrequencyEntity(List<String> queries) {
        queries.add("ALTER TABLE extraploatedallsetspeciesfrequency RENAME TO extrapolatedallsetspeciesfrequency;");
    }

    private void addMissingReferences(List<String> queries) {

        // new harbour (http://forge.codelutin.com/issues/1114)
        queries.add(
                "INSERT INTO PUBLIC.HARBOUR(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, LATITUDE, LONGITUDE, COMMENT, LOCODE) VALUES\n" +
                "('fr.ird.t3.entities.reference.Harbour#1334600123513#0.12332082367823705', 0, TIMESTAMP '2011-02-13 08:02:06.373', 38, 'TAKORADI', 0.0, 0.0, NULL, 'GHTKD');");

        // new vessels (http://forge.codelutin.com/issues/1116)
        queries.add(
                "INSERT INTO PUBLIC.VESSEL(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, KEELCODE, YEARSERVICE, FLAGCHANGEDATE, LENGTH, POWER, CAPACITY, SEARCHMAXIMUM, LIBELLE, COMMENT, FLAGCOUNTRY, VESSELTYPE, VESSELSIZECATEGORY, FLEETCOUNTRY) VALUES\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.4151449426787046', 0, TIMESTAMP '2012-04-16 18:00:00.500', 835, 611, 2010, to_date('15/12/2010', 'DD/MM/YYYY'), 90.00, 5163, 1470, 0, 'BERNICA', 'Sister-ship du FRANCHE TERRE et MANAPANY', 'fr.ird.t3.entities.reference.Country#1297580528743#0.9082708203163934', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.1934035812816559', 0, TIMESTAMP '2012-04-16 18:00:00.500', 836, 236, 1978, to_date('01/09/1994', 'DD/MM/YYYY'), 62.74, 3652, 980,  0, 'JACQUES CARTIER', 'Vendu aux islandais','fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521',  'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8372725392088513', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.7268333559607761', 0, TIMESTAMP '2012-04-16 18:00:00.500', 837, 333, 0, NULL, 0, 0,  0, 0, 'ACE 1', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.4648270562204567', 0, TIMESTAMP '2012-04-16 18:00:00.500', 838, 341, 0,  NULL, 0, 0,   0, 0, 'VICTORY', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.4523292327386541', 0, TIMESTAMP '2012-04-16 18:00:00.500', 839, 469, 0,  NULL, 0, 0,  0, 0, 'TRUST 79', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.6329353182977022', 0, TIMESTAMP '2012-04-16 18:00:00.500', 840, 278, 0,  NULL, 0, 0,  0, 0, 'CAP SAINT PAUL', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8372725392088513', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.8800106003178237', 0, TIMESTAMP '2012-04-16 18:00:00.500', 841, 612, 0,  NULL, 0, 0,  0, 0, 'PANOFI DICOVERER', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.22393563578191245', 0, TIMESTAMP '2012-04-16 18:00:00.500', 842, 613, 0,  NULL, 0, 0,  0, 0, 'PANOFI PATH FINDER ', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.9160465233030383', 0, TIMESTAMP '2012-04-16 18:00:00.500', 843, 614, 0,  NULL, 0, 0,  0, 0, 'PANOFI FOR RUNNER', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.7990273811433629', 0, TIMESTAMP '2012-04-16 18:00:00.500', 844, 615, 0,  NULL, 0, 0,  0, 0, 'OWUM OPE SIKA', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.2125833196598348', 0, TIMESTAMP '2012-04-16 18:00:00.500', 845, 466, 1992, to_date('02/07/2010', 'DD/MM/YYYY'), 79.80, 4957, 1742, 13, 'PREMIER', NULL, 'fr.ird.t3.entities.reference.Country#1297580528739#0.7353550641367269', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528739#0.7353550641367269'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.30318778696350435', 0, TIMESTAMP '2012-04-16 18:00:00.500', 846, 344, 0,  NULL, 0, 0,  0, 0, 'JITO 3', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.5863372725625371', 0, TIMESTAMP '2012-04-16 18:00:00.500', 847, 465, 1973,  NULL, 57.71, 1650,  0, 0, 'JITO 5', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694343#0.5658194439547352', 0, TIMESTAMP '2012-04-16 18:00:00.500', 848, 465, 1973,  NULL, 57.71, 1650,  0, 0, 'ELI', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.11684464496192548', 0, TIMESTAMP '2012-04-16 18:00:00.500', 849, 616, 0,  NULL, 0, 0,  0, 0, 'SINFIN CUATRO', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624',  'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.8071573964251233', 0, TIMESTAMP '2012-04-16 18:00:00.500', 850, 616, 0,  NULL, 0, 0,  0, 0, 'RICO UNO', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.06330363205399081', 0, TIMESTAMP '2012-04-16 18:00:00.500', 851, 569, 1991, to_date('01/01/2011', 'DD/MM/YYYY'), 79, 4800, 1100, 0, 'CAP COZ', NULL, 'fr.ird.t3.entities.reference.Country#1297580528742#0.8988427274945849', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.9105423145573949', 0, TIMESTAMP '2012-04-16 18:00:00.500', 852, 570, 1991, to_date('01/01/2011', 'DD/MM/YYYY'), 79, 4800, 1100,  0, 'CAP VERGA', NULL, 'fr.ird.t3.entities.reference.Country#1297580528742#0.8988427274945849', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.7833644913778908', 0, TIMESTAMP '2012-04-16 18:00:00.500', 853, 573, 1991, to_date('01/01/2011', 'DD/MM/YYYY'), 79, 4800, 1100,  0, 'CAP FINISTERE', NULL, 'fr.ird.t3.entities.reference.Country#1297580528742#0.8988427274945849', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.6056952030400613', 0, TIMESTAMP '2012-04-16 18:00:00.500', 854, 541, 2000, to_date('01/01/2011', 'DD/MM/YYYY'), 72.5, 3600, 650, 0, 'CAP D''AMBRE', NULL, 'fr.ird.t3.entities.reference.Country#1297580528742#0.8988427274945849', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8372725392088513', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.9390808792231422', 0, TIMESTAMP '2012-04-16 18:00:00.500', 855, 218, 1973,  NULL, 51.00, 2200, 675,  0, 'ADJOA AMISABA', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.15498570376103094', 0, TIMESTAMP '2012-04-16 18:00:00.500', 856, 344, 0,  NULL, 0, 0,  0, 0, 'MAKOKOS', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.15034546562226914', 0, TIMESTAMP '2012-04-16 18:00:00.500', 857, 616, 0,  NULL, 0, 0,  0, 0, 'RICO SIETE', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.30450596327100854', 0, TIMESTAMP '2012-04-16 18:00:00.500', 858, 437, 1967,  NULL, 0, 0,  0, 0, 'YOUNG BOK', NULL, 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396'), \n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.8234438156342408', 0, TIMESTAMP '2012-04-16 18:00:00.500', 859, 294, 1982,  NULL, 55.43, 2800, 800, 10, 'SOLEVANT', 'Propriété de l''armement ivoiro-coréen SOLEVANT', 'fr.ird.t3.entities.reference.Country#1297580528739#0.24216094850648695', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 'fr.ird.t3.entities.reference.Country#1297580528739#0.7353550641367269'),\n" +
                "('fr.ird.t3.entities.reference.Vessel#1334588694344#0.14379951957189474', 0, TIMESTAMP '2012-04-16 18:00:00.500', 860, 256, 1976, to_date('01/01/2012', 'DD/MM/YYYY'), 76.76, 4000, 1300,  0, 'ALBACORA 6', NULL, 'fr.ird.t3.entities.reference.Country#1297580528743#0.6699533207736184', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463')\n" +
                ";");

        // add new vessel for samplesOnly trips http://forge.codelutin.com/issues/1123
        queries.add(
                "INSERT INTO PUBLIC.VESSEL(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, KEELCODE, YEARSERVICE, FLAGCHANGEDATE, LENGTH, POWER, CAPACITY, SEARCHMAXIMUM, LIBELLE, COMMENT, FLAGCOUNTRY, VESSELTYPE, VESSELSIZECATEGORY, FLEETCOUNTRY) VALUES" +
                "('fr.ird.t3.entities.reference.Vessel#1334665675179#0.06307977391745634', 0, TIMESTAMP '2012-04-17 15:00:00.000', 1000, 1000, 0, NULL, 0, 0, 0, 0, 'UNKNOWN SPANISH', 'Used by anonymized Spanish samples AVDTH databases', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463')," +
                "('fr.ird.t3.entities.reference.Vessel#1334668174918#0.8897059213772033', 0, TIMESTAMP '2012-04-17 15:00:00.000', 1001, 1001, 0, NULL, 0, 0, 0, 0, 'UNKNOWN GHANIAN', 'Used by anonymized Ghanian samples AVDTH databases', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396')," +
                "('fr.ird.t3.entities.reference.Vessel#1334668174917#0.1082426724943295', 0, TIMESTAMP '2012-04-17 15:00:00.000', 1002, 1002, 0, NULL, 0, 0, 0, 0, 'UNKNOWN FRENCH', 'Used by anonymized French samples AVDTH databases', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 'fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845');");

    }

    private void renameTripColumns(List<String> queries) {
        queries.add("ALTER TABLE trip RENAME COLUMN computedTimeAtSeaNO TO computedTimeAtSeaN0;");
    }

    private void addMissingWeightCategoryTreatment(List<String> queries) {
        // ocean atlantique (missing BI)
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.44386731073429053', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.505', '-10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5237318856431237', 0, 10, 30, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', '10-30 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807192', 0, 30, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', '+30 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807193', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', 'Indéfini');");

        // ocean indien (missing BI)
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.6434841998774145', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.507', '-10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221246', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', '+10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221247', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', 'Indéfini');");

        // ocean pacifique (missing BO/BL/BI)
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830506#0.4807760142660965', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.506', '-10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660062', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.507', '+10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660063', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.507', 'Indéfini');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.6434841998774135', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.507', '-10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221255', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.508', '+10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221256', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.508', 'Indéfini');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.6434841998774145', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.507', '-10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221246', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', '+10 kg');");
        queries.add(
                "INSERT INTO weightcategorytreatment VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221247', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', 'Indéfini');");
    }

    private void adaptTripN1Flags(List<String> queries) {
        queries.add("ALTER TABLE trip ADD COLUMN extrapolateSampleCountedAndMeasured boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN standardizeSampleMeasures boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN computeWeightOfCategoriesForSet boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN redistributeSampleNumberToSet boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN extrapolateSampleWeightToSet boolean default false;");
        queries.add("ALTER TABLE trip DROP COLUMN sampleStandardised;");
        queries.add("ALTER TABLE trip ADD COLUMN samplesOnly boolean default false;");
    }

    private void adaptTripN0Flags(List<String> queries) {
        queries.add("ALTER TABLE trip ADD COLUMN wellPlanWeightCategoriesComputed boolean default false;");
    }

    private void addFieldsInVessel(List<String> queries) {
        queries.add("ALTER TABLE vessel ADD COLUMN communautaryId VARCHAR(255);");
        queries.add("ALTER TABLE vessel ADD COLUMN nationalId VARCHAR(255);");
    }
}
