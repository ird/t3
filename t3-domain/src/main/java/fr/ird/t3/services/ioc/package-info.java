/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;
/**
 * This package contains a little IOC engine.
 *
 * <h1>Annotation to use</h1>
 *
 * We define here some annotations to inject some data in some bean fields :
 *
 * <ul>
 *     <li>{@link fr.ird.t3.services.ioc.InjectDAO} to inject a DAO</li>
 *     <li>{@link fr.ird.t3.services.ioc.InjectFromDAO} to inject from amethod invocation on a DAO</li>
 *     <li>{@link fr.ird.t3.services.ioc.InjectEntitiesById} to inject some entities using a collection of ids into a object</li>
 *     <li>{@link fr.ird.t3.services.ioc.InjectEntityById} to inject a entity using a simple id into a object</li>
 *     <li>{@link fr.ird.t3.services.ioc.InjectDecoratedBeans} to inject a map of decorated beans for an existing list (or a unique) entities or IdAbles</li>
 * </ul>
 *
 * To use injector, place some of thoses annoations on your fields.
 *
 * <h2>Injectors</h2>
 * An injector must implements the contract {@link fr.ird.t3.business.ioc.Injector}
 * and is linked to a annotation via the method {@link fr.ird.t3.business.ioc.Injector#getAnnotationType()}.
 *
 *
 * <h2>How to do do some injections</h2>
 * See the {@link fr.ird.t3.services.IOCService}.
 *
 * <h2>To define a new injector</h2>
 *
 * Defines a implementation of a {@link fr.ird.t3.business.ioc.Injector},
 * and a new annotation (the injector is linked to it).
 *
 * Register the injector into the {@code META-INF/services/fr.ird.t3.business.ioc.Injector} files
 *
 * And that's all, the injector engine will find out your new injector and will use it on fields
 * annotated with your new annotation :)
 *
 */

