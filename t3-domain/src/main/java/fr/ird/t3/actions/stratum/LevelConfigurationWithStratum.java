/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;

import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Define the gloabal configuration of a level 2 or 3 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class LevelConfigurationWithStratum implements T3ActionConfiguration {

    private static final long serialVersionUID = 1L;

    /** Id of selected ocean. */
    protected String oceanId;

    /** Id of selected type of Zone. */
    protected String zoneTypeId;

    /** Id of selected version of Zone. */
    protected String zoneVersionId;

    /** Selected fleet id (for catch filtering). */
    protected String catchFleetId;

    /** Number of months to define a stratus. */
    protected int timeStep;

    /** Start date for treatment. */
    protected T3Date beginDate;

    /** End date for treatment. */
    protected T3Date endDate;

    /** min begin date to use. */
    protected T3Date minDate;

    /** max end date to use. */
    protected T3Date maxDate;

    /** Maximum weight ratio (catch / sample) usable on a stratum. */
    protected float stratumWeightRatio;

    /** Selected species ids to fix. */
    protected Set<String> speciesIds;

    /** Selected fleet ids (for sample filtering). */
    protected Set<String> sampleFleetIds;

    /** Selected flag ids (for sample filtering). */
    protected Set<String> sampleFlagIds;

    /** Usable oceans. */
    protected List<Ocean> oceans;

    /** Usable species. */
    protected List<Species> species;

    /** Usable catch fleets. */
    protected List<Country> catchFleets;

    /** Usable sample flags. */
    protected List<Country> sampleFleets;

    /** Usable sample fleets. */
    protected List<Country> sampleFlags;

    /** Usable zone's types. */
    protected List<ZoneStratumAwareMeta> zoneTypes;

    /** Usable zone's versions. */
    protected List<ZoneVersion> zoneVersions;

    /**
     * flag to validate the step one of configuration.
     * <p/>
     * The first step is the general configuration.
     */
    protected boolean validStep1;

    /**
     * Flag to validate the step two of configuration.
     * <p/>
     * The second step is the sample selection configuration.
     */
    protected boolean validStep2;

    protected Locale locale;

    public Set<String> getSpeciesIds() {
        if (speciesIds == null) {
            speciesIds = Sets.newHashSet();
        }
        return speciesIds;
    }

    public void setSpeciesIds(Set<String> speciesIds) {
        this.speciesIds = speciesIds;
    }

    public String getOceanId() {
        return oceanId;
    }

    public void setOceanId(String oceanId) {
        this.oceanId = oceanId;
    }

    public String getZoneTypeId() {
        return zoneTypeId;
    }

    public void setZoneTypeId(String zoneTypeId) {
        this.zoneTypeId = zoneTypeId;
    }

    public String getZoneVersionId() {
        return zoneVersionId;
    }

    public void setZoneVersionId(String zoneVersionId) {
        this.zoneVersionId = zoneVersionId;
    }

    public int getTimeStep() {
        return timeStep;
    }

    public void setTimeStep(int timeStep) {
        this.timeStep = timeStep;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(T3Date beginDate) {
        this.beginDate = beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public void setEndDate(T3Date endDate) {
        this.endDate = endDate;
    }

    public String getCatchFleetId() {
        return catchFleetId;
    }

    public void setCatchFleetId(String catchFleetId) {
        this.catchFleetId = catchFleetId;
    }

    public Set<String> getSampleFleetIds() {
        if (sampleFleetIds == null) {
            sampleFleetIds = Sets.newHashSet();
        }
        return sampleFleetIds;
    }

    public void setSampleFleetIds(Set<String> sampleFleetIds) {
        this.sampleFleetIds = sampleFleetIds;
    }

    public Set<String> getSampleFlagIds() {
        if (sampleFlagIds == null) {
            sampleFlagIds = Sets.newHashSet();
        }
        return sampleFlagIds;
    }

    public void setSampleFlagIds(Set<String> sampleFlagIds) {
        this.sampleFlagIds = sampleFlagIds;
    }

    public float getStratumWeightRatio() {
        return stratumWeightRatio;
    }

    public void setStratumWeightRatio(float stratumWeightRatio) {
        this.stratumWeightRatio = stratumWeightRatio;
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public void setOceans(List<Ocean> oceans) {
        this.oceans = oceans;
    }

    public List<Species> getSpecies() {
        return species;
    }

    public void setSpecies(List<Species> species) {
        this.species = species;
    }

    public List<Country> getCatchFleets() {
        return catchFleets;
    }

    public void setCatchFleets(List<Country> catchFleets) {
        this.catchFleets = catchFleets;
    }

    public List<Country> getSampleFleets() {
        return sampleFleets;
    }

    public void setSampleFleets(List<Country> sampleFleets) {
        this.sampleFleets = sampleFleets;
    }

    public List<Country> getSampleFlags() {
        return sampleFlags;
    }

    public void setSampleFlags(List<Country> sampleFlags) {
        this.sampleFlags = sampleFlags;
    }

    public List<ZoneStratumAwareMeta> getZoneTypes() {
        return zoneTypes;
    }

    public void setZoneTypes(List<ZoneStratumAwareMeta> zoneTypes) {
        this.zoneTypes = zoneTypes;
    }

    public List<ZoneVersion> getZoneVersions() {
        if (zoneVersions == null) {
            zoneVersions = Lists.newArrayList();
        }
        return zoneVersions;
    }

    public void setZoneVersions(List<ZoneVersion> zoneVersions) {
        this.zoneVersions = zoneVersions;
    }

    public boolean isValidStep1() {
        return validStep1;
    }

    public void setValidStep1(boolean validStep1) {
        this.validStep1 = validStep1;
    }

    public boolean isValidStep2() {
        return validStep2;
    }

    public void setValidStep2(boolean validStep2) {
        this.validStep2 = validStep2;
    }

    public T3Date getMinDate() {
        return minDate;
    }

    public void setMinDate(T3Date minDate) {
        this.minDate = minDate;
    }

    public T3Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(T3Date maxDate) {
        this.maxDate = maxDate;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
