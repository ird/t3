/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3ServiceSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Base business action.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class T3Action<C extends T3ActionConfiguration> extends T3ServiceSupport implements T3Messager, TopiaTransactionAware, LocaleProvider {

    /** Logger. */
    private final Log log = LogFactory.getLog(getClass());

    /** Action locale (used for i18n). */
    protected Locale locale;

    /**
     * A state setted after the {@link #executeAction()}
     * invocation to know if a commit is required.
     */
    protected boolean needCommit;

    public T3ActionContext<C> getActionContext() {
        return (T3ActionContext<C>) serviceContext;
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
    }

    public boolean isNeedCommit() {
        return needCommit;
    }

    public final void run() throws Exception {

        long t0 = System.nanoTime();
        getActionContext().setProgression(0);

        try {

            prepareAction();

            deletePreviousData();

            // execute action for all selected data
            needCommit = executeAction();

            finalizeAction();

        } finally {

            getActionContext().setTotalTime(System.nanoTime() - t0);
        }
    }

    /**
     * Fill the action context.
     * <p/>
     * By default, just copy incoming parameters in the action context.
     *
     * @throws Exception if any db pb while acquiring data
     */
    protected void prepareAction() throws Exception {

        locale = getLocale();

        // inject fields in action
        newService(IOCService.class).injectExcept(this);
    }

    /**
     * Delete data computed from a previous treatment.
     *
     * @since 1.2
     */
    protected abstract void deletePreviousData() throws TopiaException;

    protected void finalizeAction() throws TopiaException, IOException {

        // if one commit required, then commit transaction
        if (isNeedCommit()) {
            getTransaction().commitTransaction();
        }
    }

    protected abstract boolean executeAction() throws Exception;

    public C getConfiguration() {
        return getActionContext().getConfiguration();
    }

    public String getActionName() {
        return getActionContext().getActionName();
    }

    @Override
    public void addInfoMessage(String message) {
        getActionContext().addInfoMessage(message);
    }

    @Override
    public void addWarningMessage(String message) {
        getActionContext().addWarningMessage(message);
    }

    @Override
    public void addErrorMessage(String message) {
        getActionContext().addErrorMessage(message);
    }

    @Override
    public List<String> getInfoMessages() {
        return getActionContext().getInfoMessages();
    }

    @Override
    public List<String> getWarnMessages() {
        return getActionContext().getWarnMessages();
    }

    @Override
    public List<String> getErrorMessages() {
        return getActionContext().getErrorMessages();
    }

    public int getProgression() {
        return getActionContext().getProgression();
    }

    public void setProgression(float progression) {
        getActionContext().setProgression(progression);
    }

    public void incrementsProgression() {
        getActionContext().incrementsProgression();
        if (log.isDebugEnabled()) {
            log.debug("After increments : " + getProgression());
        }
    }

    public int getNbSteps() {
        return getActionContext().getNbSteps();
    }

    public void setNbSteps(int nbSteps) {
        getActionContext().setNbSteps(nbSteps);
        if (log.isInfoEnabled()) {
            log.info("Nb steps : " + getNbSteps() + " (step increment : " +
                     getActionContext().getStepIncrement() + ")");
        }
    }

    public void putResult(String key, Object value) {
        getActionContext().putResult(key, value);
    }

    public <V> V getResult(String paramKey, Class<V> valueType) {
        return getActionContext().getResult(paramKey, valueType);
    }

    public <V> Set<V> getResultAsSet(String paramKey, Class<V> valueType) {
        return getActionContext().getResultAsSet(paramKey, valueType);
    }

    public <V> List<V> getResultAsList(String paramKey, Class<V> valueType) {
        return getActionContext().getResultAsList(paramKey, valueType);
    }

    public <K, V> Map<K, V> getResultAsMap(String paramKey) {
        return getActionContext().getResultAsMap(paramKey);
    }

    public <K, V> Multimap<K, V> getResultAsMultimap(String paramKey) {
        return getActionContext().getResultAsMultimap(paramKey);
    }

    public String getRenderTemplateName() {
        String path = "/ftl." + getClass().getName();
        path = path.replaceAll("\\.", "/") + ".ftl";
        return path;
    }

    public String decorate(Object bean) {
        String s = decorate(bean, null);
        return s;
    }

    protected DecoratorService decoratorService;

    public String decorate(Object bean, String context) {
        String s = getDecoratorService().decorate(getLocale(), bean, context);
        return s;
    }

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = newService(DecoratorService.class);
        }
        return decoratorService;
    }

}
