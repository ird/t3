/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import fr.ird.t3.entities.data.Activity;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;

import java.util.Map;
import java.util.Set;

/**
 * To loader a catch stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see CatchStratum
 * @since 1.3
 */
public abstract class CatchStratumLoader<C extends LevelConfigurationWithStratum> implements TopiaTransactionAware {

    protected TopiaContext transaction;

    /**
     * To obtain all activities of the catch stratum.
     *
     * @param configuration stratum configuration
     * @return the activites to use for the catch stratum
     * @throws TopiaException if any pb while quering db
     */
    public abstract Map<Activity, Integer> loadData(StratumConfiguration<C> configuration) throws TopiaException;

    @Override
    public TopiaContext getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        this.transaction = transaction;
    }
}
