/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import org.nuiton.util.config.ApplicationConfigProvider;
import org.nuiton.util.config.ConfigActionDef;
import org.nuiton.util.config.ConfigOptionDef;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Application config provider (for site generation).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class T3ApplicationConfigProvider implements ApplicationConfigProvider {

    @Override
    public String getName() {
        return "t3";
    }

    @Override
    public String getDescription(Locale locale) {
        return l_(locale, "t3.application.config");
    }

    @Override
    public ConfigOptionDef[] getOptions() {
        return T3ConfigurationOption.values();
    }

    @Override
    public ConfigActionDef[] getActions() {
        return new ConfigActionDef[0];
    }
}
