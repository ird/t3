/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityMeta;
import fr.ird.t3.entities.T3EntityEnum;

import java.lang.reflect.InvocationHandler;

/**
 * Les méta données d'une entité récupéré depuis une base access.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.5
 */
public class T3AccessEntityMeta extends AbstractAccessEntityMeta<T3EntityEnum> {

    protected T3AccessEntityMeta(T3AccessEntityMeta source) {
        super(source);
    }

    protected T3AccessEntityMeta(T3EntityEnum type,
                                 String tableName,
                                 String[] pkeys,
                                 Object[] association,
                                 Object[] reverseProperties,
                                 Object... properties) {
        super(T3AccessEntity.class,
              type,
              tableName,
              pkeys,
              association,
              reverseProperties,
              properties);
    }

    @Override
    public String getTopiaNaturalId() {
        return "code";
    }

    @Override
    protected InvocationHandler newHandler(int rowId, Object[] pkey) throws Exception {
        return new T3AccessEntityProxyHandler(this, rowId, pkey);
    }
}
