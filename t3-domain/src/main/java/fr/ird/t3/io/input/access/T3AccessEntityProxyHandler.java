/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityProxyHandler;
import fr.ird.msaccess.type.IntToCoordonne;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.io.input.access.type.IntToBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.EntityOperator;

/**
 * Implementation of the {@link AbstractAccessEntityProxyHandler} for the
 * T3 project.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3AccessEntityProxyHandler extends AbstractAccessEntityProxyHandler<T3EntityEnum> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3AccessEntityProxyHandler.class);

    @SuppressWarnings({"unchecked"})
    public T3AccessEntityProxyHandler(T3AccessEntityMeta meta,
                                      int rowId,
                                      Object[] pkey) throws Exception {
        super(meta, rowId, pkey);
    }

    @Override
    protected EntityOperator<TopiaEntity> getOperator(TopiaEntityEnum type) {
        Class<? extends TopiaEntity> contract = type.getContract();
        return (EntityOperator<TopiaEntity>) T3DAOHelper.getOperator(contract);
    }

    @Override
    protected Object getPropertyValue(Class<?> type,
                                      String propertyName,
                                      Object value) {

        if (type.equals(Float.class)) {
            Object newValue = Float.valueOf(value + "");
            return newValue;
        }

        if (type.equals(Integer.class)) {

            // on repasse toujours par un float car dans la base
            // access certain champs sont Float alors
            // que dans ObServe c'est du Integer
            Object newValue = Float.valueOf(value + "").intValue();
            return newValue;
        }

        if (type.equals(IntToCoordonne.class)) {
            IntToCoordonne c = new IntToCoordonne(
                    Integer.valueOf(value + ""));
            Object newValue = c.getFloatValue();
            if (log.isDebugEnabled()) {
                log.debug(propertyName + " " + c.getIntValue() +
                          " --> " + newValue);
            }
            return newValue;
        }

        if (type.equals(IntToBoolean.class)) {

            // on recupere un integer que l'on transformera en boolean
            IntToBoolean bValue = new IntToBoolean(
                    Float.valueOf(value + "").intValue());
            if (log.isDebugEnabled()) {
                log.debug("Will set [" + propertyName + ":" +
                          bValue.getIntValue() + "-" +
                          bValue.getBooleanvalue() + "] to " + entity);
            }
            Object newValue = bValue.getBooleanvalue();
            return newValue;
        }
        return value;
    }

    @Override
    protected Object getPropertyValueFromMetaType(TopiaEntityEnum metaType,
                                                  String propertyName,
                                                  Object value) {
        return value;
    }

}
