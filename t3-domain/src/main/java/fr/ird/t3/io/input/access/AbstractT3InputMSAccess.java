/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3EntityMap;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.io.input.MissingForeignKey;
import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.T3InputConfiguration;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The implementation of the {@link T3Input} for AVDTH databases.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractT3InputMSAccess implements T3Input {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractT3InputMSAccess.class);

    public abstract T3AccessDataSource newDataSource(File inputFile);

    public abstract T3DataEntityVisitor newDataVisitor(T3AccessDataSource dataSource,
                                                       ReferenceEntityMap safeReferences);

    protected T3AccessDataSource dataSource;

    protected ReferenceEntityMap safeReferences;

    protected Map<Trip, List<MissingForeignKey>> missingFK;

    protected String[] errors;

    protected String[] warnings;

    protected T3InputConfiguration configuration;

    protected Map<Integer, Vessel> vessels;


    @Override
    public List<String> getPkeyNames(T3EntityEnum type) {
        T3AccessEntityMeta meta = dataSource.getMeta(type);
        return meta.getPkeys();
    }

    @Override
    public T3InputConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void init(T3InputConfiguration configuration) throws Exception {

        this.configuration = configuration;

        // open data source
        dataSource = newDataSource(configuration.getInputFile());

        Set<String> t3DataTypes = Sets.newHashSet();

        for (T3EntityEnum t3EntityEnum : getDataTypes()) {
            t3DataTypes.add(t3EntityEnum.getContract().getSimpleName());
        }
//        for (Class<? extends TopiaEntity> t3DataType :
//                T3EntityHelper.getT3ImportableDataTypes()) {
//            t3DataTypes.add(t3DataType.getSimpleName());
//        }
        T3AccessHitModel hitModel = new T3AccessHitModel();

        hitModel.addPropertyChangeListener(new LoadDbPropertyChangeListener(t3DataTypes));

        // analyse data source
        dataSource.analyzeDb(hitModel);

        if (dataSource.hasError()) {
            errors = dataSource.getErrors();
        }

        if (dataSource.hasWarning()) {
            errors = dataSource.getWarnings();
        }
    }

    @Override
    public String[] getAnalyzeErrors() {
        return errors;
    }

    @Override
    public String[] getAnalyzeWarnings() {
        return warnings;
    }

//    @Override
//    public ReferenceEntityMap loadUnsafeReferences() throws Exception {
//
//        if (dataSource == null) {
//            throw new IllegalStateException(
//                    "Input was not initialized via the init method.");
//        }
//
//        // init unsafe reference universe
//        ReferenceEntityMap result = new ReferenceEntityMap();
//
//        // load unsafe references
//        T3ReferentielEntityVisitor visitor =
//                new T3ReferentielEntityVisitor(dataSource, result);
//
//        try {
//            // load unsafe references
//            for (T3EntityEnum type : REFERENCE_TYPES) {
//                T3MSAccessHelper.loadReferentiel(visitor,
//                                                 dataSource,
//                                                 result,
//                                                 type,
//                                                 true);
//            }
//        } finally {
//            visitor.clear();
//        }
//        return result;
//    }

    @Override
    public Map<Integer, Vessel> getNewVessels() throws Exception {

        if (dataSource == null) {
            throw new IllegalStateException(
                    "Input was not initialized via the init method.");
        }

        if (vessels == null) {
            // init unsafe reference universe
            ReferenceEntityMap map = new ReferenceEntityMap();

            // load unsafe references
            T3ReferentielEntityVisitor visitor =
                    new T3ReferentielEntityVisitor(dataSource, safeReferences);
            visitor.setStrictCheck(false);


            try {
                // load unsafe references
                T3MSAccessHelper.loadReferentiel(visitor,
                                                 dataSource,
                                                 map,
                                                 T3EntityEnum.Vessel,
                                                 true);
            } finally {
                visitor.clear();
            }
            List<Vessel> vesselList = map.get(Vessel.class);
            RemoveProxyEntityVisitor visitor2 = new RemoveProxyEntityVisitor();
            try {
                List<Vessel> result = T3MSAccessHelper.removeProxies(visitor2,
                                                                     vesselList);

                // remove from it all the one existing in incoming db
                List<Vessel> existingVessels = safeReferences.get(Vessel.class);
                List<Integer> existingVesselCodes = Lists.transform(existingVessels, T3Functions.TO_REFERENTIEL_CODE);
                Iterator<Vessel> iterator = result.iterator();
                while (iterator.hasNext()) {
                    Vessel next = iterator.next();
                    if (existingVesselCodes.contains(next.getCode())) {

                        // existing vessel, do not keep it
                        iterator.remove();
                    }
                }
                // index by code
                vessels = Maps.uniqueIndex(result,
                                           T3Functions.TO_REFERENTIEL_CODE);
            } finally {
                visitor2.clear();
            }
        }
        return vessels;
    }

    @Override
    public void setSafeReferences(ReferenceEntityMap safeReferences) {
        this.safeReferences = safeReferences;
    }

    @Override
    public Map<Trip, T3EntityMap> loadTrips(LoadingTripHitModel hitModel) throws Exception {

        if (dataSource == null) {
            throw new IllegalStateException(
                    "Input was not initialized via the init method.");
        }

        if (safeReferences == null) {
            throw new IllegalStateException(
                    "No safe references was initialized by method " +
                    "setSafeReferences.");
        }

        boolean samplesOnly = getConfiguration().isSamplesOnly();
        boolean canCreateVessel = getConfiguration().isCanCreateVessel();

        // load trip data

        T3EntityEnum constant = T3EntityEnum.valueOf(Trip.class);
        T3AccessEntityMeta meta = dataSource.getMeta(constant);
        List<Trip> entities = dataSource.loadEntities(meta);
        Map<Trip, T3EntityMap> result = Maps.newLinkedHashMap();
        missingFK = Maps.newHashMap();

        T3DataEntityVisitor dataVisitor =
                newDataVisitor(dataSource, safeReferences);

        try {

            for (Trip trip : entities) {

                T3AccessEntity a = (T3AccessEntity) trip;

                Integer vesselCode = Integer.valueOf(a.getPkey()[0] + "");

                Trip loadedTrip = loadTrip(vesselCode,
                                           trip,
                                           dataVisitor,
                                           samplesOnly,
                                           canCreateVessel);

                hitModel.addTripLoaded();

                // get the touched references for this trip
                T3EntityMap entitiesTouched =
                        dataVisitor.getEntitiesTouched();

                List<MissingForeignKey> missingFKforTrip =
                        dataVisitor.getMissingForeignKeys();

                if (CollectionUtils.isNotEmpty(missingFKforTrip)) {
                    missingFK.put(loadedTrip, missingFKforTrip);
                }

                result.put(loadedTrip, entitiesTouched);

                if (log.isDebugEnabled()) {
                    log.debug("Touchs " + entitiesTouched.size() +
                              " types of entities for the trip " +
                              Arrays.toString(a.getPkey()));
                    for (Map.Entry<T3EntityEnum, List<? extends TopiaEntity>> entry :
                            entitiesTouched.entrySet()) {
                        log.debug(" [" + entry.getKey() + "] : " +
                                  entry.getValue().size());
                    }
                }
                dataVisitor.resetTripStates();
            }
        } finally {

            // always clean the visitor at the end (contains a lot of data...)
            dataVisitor.clear();
        }

        return result;
    }

    @Override
    public void destroy() {
        if (safeReferences != null) {
            safeReferences = null;
        }
        if (dataSource != null) {
            dataSource.destroy();
            dataSource = null;
        }
    }

    @Override
    public int getNbTrips() {

        if (dataSource == null) {
            throw new IllegalStateException(
                    "Input was not initialized via the init method.");
        }

        T3AccessEntityMeta meta = dataSource.getMeta(T3EntityEnum.Trip);
        int result = dataSource.getTableData(meta).length;
        return result;
    }

    public T3AccessDataSource getDataSource() {
        return dataSource;
    }

    @Override
    public List<MissingForeignKey> getMissingForeignKeys(Trip trip) {
        if (missingFK == null) {
            return null;
        }
        List<MissingForeignKey> missingForeignKeys = missingFK.get(trip);
        return missingForeignKeys;
    }

    protected Trip loadTrip(int vesselCode,
                            Trip trip,
                            T3DataEntityVisitor dataVisitor,
                            boolean samplesOnly,
                            boolean canCreateVessel) throws Exception {

        RemoveProxyEntityVisitor visitor = new RemoveProxyEntityVisitor();
        try {
            if (log.isDebugEnabled()) {
                log.debug("Will visit deeply entity " + trip);
            }

            // fill the trip
            dataVisitor.doVisit(trip, true);

            // remove proxy on data
            Trip loadedTrip = visitor.doVisit(trip);

            // set samplesOnly flag
            loadedTrip.setSamplesOnly(samplesOnly);

            if (trip.getVessel() == null && canCreateVessel) {

                // use new created vessel
                Vessel vessel = getNewVessels().get(vesselCode);
                loadedTrip.setVessel(vessel);
                List<Vessel> vesselToucheds =
                        dataVisitor.getEntitiesTouched().get(Vessel.class);
                vesselToucheds.clear();
                vesselToucheds.add(vessel);
            }
            return loadedTrip;

        } catch (TopiaException e) {
            throw new IllegalStateException("Could not visit data " + trip, e);
        } finally {
            visitor.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

    private static class LoadDbPropertyChangeListener implements PropertyChangeListener {

        private final Set<String> t3DataTypes;

        public LoadDbPropertyChangeListener(Set<String> t3DataTypes) {
            this.t3DataTypes = t3DataTypes;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();
            if (log.isInfoEnabled()) {
                if (t3DataTypes.contains(propertyName)) {
                    log.info("Detected data entity " + propertyName + " with " + evt.getNewValue() + " entries");
                } else {
                    log.info("Detected ref  entity " + propertyName + " with " + evt.getNewValue() + " entries");
                }
            }
        }
    }
}
