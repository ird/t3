/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input;

import org.nuiton.util.Version;

import java.io.File;
import java.io.Serializable;

/**
 * The contract to define a new pilot of {@link T3Input}.
 * <p/>
 * The unicity of a provider is done on the field {@link #getId()}
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface T3InputProvider extends Serializable {

    /**
     * Obtain the unique id of the implementation.
     *
     * @return the id of the implementation
     */
    String getId();

    /**
     * Obtain the invariant name of the implementation.
     *
     * @return the name of the implementation
     */
    String getName();

    /**
     * Obtain the invariant version of the implementation.
     *
     * @return the version of the implementation.
     * @since 1.0
     */
    Version getVersion();

    /**
     * Obtain the invariant type of data source (says access, csv,...)
     *
     * @return the type of data source
     */
    String getInputType();

    /**
     * Obtain the description of the provider.
     * <p/>
     * this data will be displayed in GUI and should show the name, version
     * and input type.
     *
     * @return the description of the provider
     */
    String getLibelle();

    /**
     * To instanciate a new {@link T3Input} for a given {@code inputFile} to
     * read.
     *
     * @param inputFile the input file to read
     * @return the new instance of the pilot
     */
    T3Input newInstance(File inputFile);

}
