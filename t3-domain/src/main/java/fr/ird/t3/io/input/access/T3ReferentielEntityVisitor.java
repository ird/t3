/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityMeta;
import fr.ird.t3.entities.ReferenceEntityMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.Serializable;
import java.util.Arrays;

/**
 * To visit and fill properly an reference entity.
 * <p/>
 * If visitor has the flag {@link #deepVisit} is set to {@code false}, then
 * only the pkey will be filled, otherwise all fields or compositions are filled.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3ReferentielEntityVisitor extends AbstractT3EntityVisitor {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3ReferentielEntityVisitor.class);

    public T3ReferentielEntityVisitor(T3AccessDataSource dataSource,
                                      ReferenceEntityMap referentiel) {
        super(dataSource, referentiel);
    }

    @Override
    public void onVisitSimpleProperty(String propertyName,
                                      Class<?> type,
                                      T3AccessEntity entity, T3AccessEntityMeta meta) {
        // just set the property into the entity

        if (!deepVisit) {
            if (!propertyName.equals(meta.getTopiaNaturalId())) {

                // not a deep visit and not a pkey property name, skip visit
                return;
            }
        }

        Serializable newValue = getProperty(propertyName, meta, row);

        if (newValue != null) {
            if (log.isDebugEnabled()) {
                String colName = meta.getPropertyColumnName(propertyName);
                log.debug("get property [" + propertyName + "] (type:" +
                          type.getName() + ") (dbcol:" + colName + ") = " +
                          newValue);
            }
            entity.setProperty(propertyName, newValue);
        }
    }

    @Override
    public void onVisitComposition(String propertyName,
                                   Class<?> type,
                                   T3AccessEntity entity,
                                   T3AccessEntityMeta meta) {

        if (!deepVisit) {
            return;
        }

        // find the reference entity to add as composition
        Serializable newValue = getProperty(propertyName, meta, row);
        if (newValue == null) {

            // rien a faire si la composition est nulle
            return;
        }

        AbstractAccessEntityMeta.PropertyMapping mapping =
                meta.getPropertyMapping(propertyName);
        Class<?> compositionType = mapping.getType();

        T3AccessEntityMeta compoMeta;

        T3AccessEntityMeta[] compoMetas =
                dataSource.getMetaForType(compositionType);

        if (compoMetas.length == 0) {
            if (log.isWarnEnabled()) {
                log.warn("Skip composition [" + meta.getType() + " - " +
                         propertyName + "], meta of type [" + compositionType +
                         "] not found...");
            }
            return;
        }

        if (compoMetas.length > 1) {

            // on n'autorise pas d'avoir plusieurs méta à traiter
            // il s'agit d'une reférence sur un référentiel
            throw new IllegalStateException(
                    "Found more than one meta for referentiel type [" +
                    compositionType + "] : " + Arrays.toString(compoMetas));
        }

        compoMeta = compoMetas[0];

        TopiaEntity compoEntity = getReferenceEntity(compoMeta, newValue);

        if (compoEntity == null) {

            // could not find the correct composition entity
            throw new IllegalStateException(
                    "Could not find reference entity of type [" +
                    compoMeta.getType() + "] with pkey [" + newValue + "]");
        }

//        // find the correct entity
//        T3AccessEntity toSave = (T3AccessEntity) compoEntity;
//
//        if (log.isDebugEnabled()) {
//            log.debug("Will add composition [" + propertyName +
//                      ":" + toSave + "] to " + entity);
//        }
        // then affect it to this entity for the given property
        entity.setProperty(propertyName, compoEntity);
    }

    @Override
    public void onVisitAssociation(String propertyName,
                                   Class<?> type,
                                   T3AccessEntity entity,
                                   T3AccessEntityMeta meta) {
        // no used for reference entity
        throw new IllegalStateException(
                "Can not visit an association for a reference entity " +
                entity);
    }

    @Override
    public void onVisitReverseAssociation(String propertyName,
                                          T3AccessEntity entity,
                                          T3AccessEntityMeta meta) {
        // no used for reference entity
        throw new IllegalStateException(
                "Can not visit a reverse association for a reference entity " +
                entity);
    }

}
