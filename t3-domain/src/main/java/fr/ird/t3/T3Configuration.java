/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Version;
import org.nuiton.util.config.ApplicationConfig;
import org.nuiton.util.config.ArgumentsParserException;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * T3 configuration
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3Configuration {

    /** Logger. */
    protected static final Log log = LogFactory.getLog(T3Configuration.class);

    private final ApplicationConfig config;

    public T3Configuration(Properties defaultProps) {

        config = new ApplicationConfig();
        config.setConfigFileName("t3-config");

        if (log.isInfoEnabled()) {
            log.info(this + " is initializing...");
        }

        if (defaultProps != null) {
            for (Map.Entry<Object, Object> entry : defaultProps.entrySet()) {

                config.setDefaultOption(
                        String.valueOf(entry.getKey()),
                        String.valueOf(entry.getValue())
                );
            }
        }
        config.loadDefaultOptions(T3ConfigurationOption.values());
    }

    public T3Configuration() {

        this(null);
    }

    /**
     * To initialise the configuration.
     * <p/>
     * Will mainly parse configuration options and creates required directories.
     */
    public void init() {

        // parse configuration (with no parameters)
        parse();

        createDirectory(T3ConfigurationOption.DATA_DIRECTORY,
                        "data directory");

        createDirectory(T3ConfigurationOption.USER_LOG_DIRECTORY,
                        "user logs directory");

        createDirectory(T3ConfigurationOption.TREATMENT_WORKING_DIRECTORY,
                        "treatment workdir directory");
    }

    protected void parse() {
        try {
            config.parse();
        } catch (ArgumentsParserException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not parse configuration", e);
            }
        }
    }

    public File getTreatmentWorkingDirectory(String name,
                                             boolean create) {
        File file = getTreatmentWorkingDirectory();
        file = new File(file, name);
        if (create) {

            if (!file.exists()) {
                try {
                    if (log.isInfoEnabled()) {
                        log.info("Will create treatment directory " + file);
                    }
                    FileUtil.createDirectoryIfNecessary(file);
                } catch (IOException e) {
                    throw new IllegalStateException(
                            "Could not create treatment workdir directory " + file, e);
                }
            }
        }
        return file;
    }

    public String getApplicationVersion() {
        String v = config.getOption("application.version");
        return v;
    }

    public Version getT3DataVersion() {
        Version v = config.getOptionAsVersion(T3ConfigurationOption.T3_DATA_VERSION.key);
        return v;
    }

    public URL getApplicationSite() {
        URL url = config.getOptionAsURL("application.site.url");
        return url;
    }

    public File getDataDirectory() {
        File file = config.getOptionAsFile(T3ConfigurationOption.DATA_DIRECTORY.key);
        return file;
    }

    public File getInternalDbDirectory() {
        File file = config.getOptionAsFile(T3ConfigurationOption.INTERNAL_DB_DIRECTORY.key);
        return file;
    }

    public File getParameterProfilePath() {
        File file = config.getOptionAsFile(T3ConfigurationOption.PARAMETER_PROFILE_DIRECTORY.key);
        return file;
    }

    public File getTreatmentWorkingDirectory() {
        File file = config.getOptionAsFile(T3ConfigurationOption.TREATMENT_WORKING_DIRECTORY.key);
        return file;
    }

    public File getUserLogDirectory() {
        File file = config.getOptionAsFile(T3ConfigurationOption.USER_LOG_DIRECTORY.key);
        return file;
    }

    public Float getWeightedSetWeight() {
        Float result = config.getOptionAsFloat(T3ConfigurationOption.WEIGHTED_SET_WEIGHT.key);
        return result;
    }

    public Float getStratumWeightRatio() {
        Float result = config.getOptionAsFloat(T3ConfigurationOption.STRATUM_WEIGHT_RATIO.key);
        return result;
    }

    public float getRF1MinimumRate() {
        Float result = config.getOptionAsFloat(T3ConfigurationOption.RF1_MINIMUM_RATE.key);
        return result;
    }

    public float getRF1MaximumRate() {
        Float result = config.getOptionAsFloat(T3ConfigurationOption.RF1_MAXIMUM_RATE.key);
        return result;
    }

    public int getRFTotMax() {
        Integer result = config.getOptionAsInt(T3ConfigurationOption.RF_TOT_MAX.key);
        return result;
    }

    public int getRFMinus10Max() {
        Integer result = config.getOptionAsInt(T3ConfigurationOption.RF_MINUS10_MAX.key);
        return result;
    }

    public int getRFPlus10Max() {
        Integer result = config.getOptionAsInt(T3ConfigurationOption.RF_PLUS10_MAX.key);
        return result;
    }

    public int getRFMinus10MinNumber() {
        Integer result = config.getOptionAsInt(T3ConfigurationOption.RF_MINUS10_MIN_NUMBER.key);
        return result;
    }

    public int getRFPlus10MinNumber() {
        Integer result = config.getOptionAsInt(T3ConfigurationOption.RF_PLUS10_MIN_NUMBER.key);
        return result;
    }

    public List<Integer> getLevel2DefaultSpecies() {
        ApplicationConfig.OptionList optionAsList = config.getOptionAsList(T3ConfigurationOption.LEVEL2_DEFAULT_SPECIES.key);
        List<Integer> result = optionAsList.getOptionAsInt();
        return result;
    }

    public List<Integer> getLevel3DefaultSpecies() {
        ApplicationConfig.OptionList optionAsList = config.getOptionAsList(T3ConfigurationOption.LEVEL3_DEFAULT_SPECIES.key);
        List<Integer> result = optionAsList.getOptionAsInt();
        return result;
    }

    public boolean isUpdateSchema() {
        Boolean result = config.getOptionAsBoolean("updateSchema");
        return result != null && result;
    }

    /**
     * Creates a directory given the configuration given key.
     *
     * @param key  the configuration option key which contains the location of
     *             the directory to create
     * @param name a name used for logs
     */
    protected void createDirectory(T3ConfigurationOption key, String name) {

        File directory = config.getOptionAsFile(key.getKey());

        Preconditions.checkNotNull(
                directory,
                "Could not find " + name + " (key " +
                key +
                "in your configuration file named t3-config)"
        );
        if (log.isInfoEnabled()) {
            log.info(key + " = " + directory);
        }
        try {
            FileUtil.createDirectoryIfNecessary(directory);
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Could not create " + name + "in " + directory);
        }
    }

}
