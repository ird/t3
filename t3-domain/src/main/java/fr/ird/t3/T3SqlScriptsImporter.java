package fr.ird.t3;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.T3ScriptHelper;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.Version;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * Pour importer tous les scripts contenus dans un répertoire.
 *
 * Created on 30/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class T3SqlScriptsImporter {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3SqlScriptsImporter.class);

    protected final File scriptsDirectory;

    protected ImmutableSet<File> scriptsFile;

    public static final Predicate<File> SKIP_SPECIES_LENGTH_STEP_FILE = new Predicate<File>() {
        @Override
        public boolean apply(File input) {
            return !input.getName().toLowerCase().contains("specieslengthstep.sql");
        }
    };

    public static void importReferential(File buildRootDirectory, T3ServiceContext serviceContext, Predicate<File> filePredicate) throws IOException, TopiaException {

        Version t3DataVersion = serviceContext.getApplicationConfiguration().getT3DataVersion();
        File scriptsDirectory = Paths.get(buildRootDirectory.getAbsolutePath())
                .resolve("target")
                .resolve("t3-data-referential-" + t3DataVersion.toString())
                .toFile();

        Preconditions.checkState(scriptsDirectory.exists(), "Could not find referential script directory: " + scriptsDirectory);
        if (log.isInfoEnabled()) {
            log.info("Referential script directory: " + scriptsDirectory);
        }

        if (log.isInfoEnabled()) {
            log.info("Will load referentiel in h2 database ");
        }

        T3SqlScriptsImporter importer = new T3SqlScriptsImporter(scriptsDirectory);
        importer.prepare();
        importer.importScripts(serviceContext, filePredicate);

    }

    public T3SqlScriptsImporter(File scriptsDirectory) {
        this.scriptsDirectory = scriptsDirectory;
    }

    public void prepare() {

        List<String> scriptsName = Lists.newArrayList(scriptsDirectory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".sql");
            }
        }));
        Collections.sort(scriptsName);
        ImmutableSet.Builder<File> filesBuilder = ImmutableSet.builder();

        for (String ddlScriptName : scriptsName) {
            filesBuilder.add(new File(scriptsDirectory, ddlScriptName));
        }
        scriptsFile = filesBuilder.build();
    }

    public ImmutableSet<File> getScriptsFile() {
        return scriptsFile;
    }

    public void importScripts(T3ServiceContext serviceContext, Predicate<File> filePredicate) throws IOException, TopiaException {

        for (File scriptFile : scriptsFile) {
            if (log.isInfoEnabled()) {
                log.info(" o Loading sql script ...(" + scriptFile.getName() + ")");
            }
            if (filePredicate.apply(scriptFile)) {
                loadScript(serviceContext, scriptFile);
            }
        }

    }

    protected void loadScript(T3ServiceContext serviceContext, File script) throws TopiaException, IOException {

        TopiaContext tx = serviceContext.getTransaction().beginTransaction();

        try {
            T3ScriptHelper.loadScript(tx, script);
        } finally {
            tx.closeContext();
        }
    }

}
