package fr.ird.t3;

/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2013 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.csv.Common;
import org.nuiton.util.csv.ValueParserFormatter;
import org.nuiton.util.csv.ext.AbstractImportExportModel;

import java.text.ParseException;
import java.util.Map;

/**
 * Useful class around csv import / export.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.4
 */
public class T3CsvUtil extends Common {

    protected T3CsvUtil() {
        // no instance
    }

    public static abstract class AbstractT3ImportExportModel<M> extends AbstractImportExportModel<M> {

        public AbstractT3ImportExportModel(char separator) {
            super(separator);
        }

        public <E extends TopiaEntity> void newForeignKeyColumn(String headerName, String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
            newMandatoryColumn(headerName, propertyName, newForeignKeyValue(entityType, foreignKeyName, universe));
        }

        public <E extends TopiaEntity> void newForeignKeyColumn(String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
            newMandatoryColumn(propertyName, propertyName, newForeignKeyValue(entityType, foreignKeyName, universe));
        }

        public <E extends TopiaEntity> ForeignKeyValue<E> newForeignKeyValue(Class<E> type, String propertyName, Map<String, E> universe) {
            return new ForeignKeyValue<E>(type, propertyName, universe);
        }

    }

    /**
     * @param <E>
     * @author tchemit <chemit@codelutin.com>
     * @since 1.0
     */
    public static class ForeignKeyValue<E extends TopiaEntity> implements ValueParserFormatter<E> {

        protected final String propertyName;

        protected final Class<E> entityType;

        protected final Map<String, E> universe;

        public ForeignKeyValue(Class<E> entityType,
                               String propertyName,
                               Map<String, E> universe) {
            this.entityType = entityType;
            this.propertyName = propertyName;
            this.universe = universe;
        }

        @Override
        public E parse(String value) throws ParseException {
            E result = null;
            if (StringUtils.isNotBlank(value)) {

                // get entity from universe
                result = universe.get(value);

                if (result == null) {

                    // can not find entity this is a big problem for us...
                    throw new RuntimeException(
                            "Could not find entity of type " +
                            entityType.getSimpleName() + " with '" +
                            propertyName + "' = " + value);
                }
            }
            return result;
        }

        @Override
        public String format(E e) {
            String value = "";
            if (e != null) {
                value = e.getTopiaId();
            }
            return value;
        }
    }
}
