/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;

/**
 * To deal with {@link ComputedDataAware} entities.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class ComputedDataHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputedDataHelper.class);

    protected ComputedDataHelper() {
        // no instance of this helper
    }

    public static <E extends ComputedDataAware & T3DataEntity> void deleteComputedDatas(E entity) {
        if (log.isDebugEnabled()) {
            log.debug("delete comuted data from " + entity.getTopiaId());
        }
        entity.deleteComputedDataLevel0();
        entity.deleteComputedDataLevel1();
        entity.deleteComputedDataLevel2();
        entity.deleteComputedDataLevel3();
    }

    public static <E extends ComputedDataAware> void deleteComputedDataLevel0(Collection<E> entities) {
        if (CollectionUtils.isNotEmpty(entities)) {
            for (E entity : entities) {
                entity.deleteComputedDataLevel0();
            }
        }
    }

    public static <E extends ComputedDataAware> void deleteComputedDataLevel1(Collection<E> entities) {
        if (CollectionUtils.isNotEmpty(entities)) {
            for (E entity : entities) {
                entity.deleteComputedDataLevel1();
            }
        }
    }

    public static <E extends ComputedDataAware> void deleteComputedDataLevel2(Collection<E> entities) {
        if (CollectionUtils.isNotEmpty(entities)) {
            for (E entity : entities) {
                entity.deleteComputedDataLevel2();
            }
        }
    }

    public static <E extends ComputedDataAware> void deleteComputedDataLevel3(Collection<E> entities) {
        if (CollectionUtils.isNotEmpty(entities)) {
            for (E entity : entities) {
                entity.deleteComputedDataLevel3();
            }
        }
    }
}
