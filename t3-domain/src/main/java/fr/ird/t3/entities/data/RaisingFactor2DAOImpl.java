/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RaisingFactor2DAOImpl<E extends RaisingFactor2> extends RaisingFactor2DAOAbstract<E> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RaisingFactor2DAOImpl.class);

    protected final Comparator<RaisingFactor2> raisingFactor2Comparator =
            new Comparator<RaisingFactor2>() {
                @Override
                public int compare(RaisingFactor2 o1, RaisingFactor2 o2) {
                    Date d1 = o1.getMonth();
                    Date d2 = o2.getMonth();
                    return d1.compareTo(d2);
                }
            };

    public RaisingFactor2 findByTrip(Trip trip) throws TopiaException {
        Country country = trip.getVessel().getFleetCountry();
        VesselSimpleType vesselSimpleType = trip.getVessel().getVesselType().getVesselSimpleType();
        Harbour landingHarbour = trip.getLandingHarbour();
        List<E> allRF = findAllByProperties(
                RaisingFactor2.PROPERTY_COUNTRY, country,
                RaisingFactor2.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType,
                RaisingFactor2.PROPERTY_HARBOUR, landingHarbour
        );

        if (CollectionUtils.isEmpty(allRF)) {

            if (log.isWarnEnabled()) {
                log.warn("Could not find any raising factor 2 for trip with " +
                         "country [" + country.getLibelle() +
                         "], vesselSimpleType [" + vesselSimpleType.getLibelle() +
                         "] and landingHarbour [" +
                         landingHarbour.getLibelle() + "]");
            }
            return null;
        }

        // sort found raising factor from the month
        Collections.sort(allRF, raisingFactor2Comparator);

        Date landingDate = trip.getLandingDate();

        // get the month of landing date
        T3Date beforeDate = T3Date.newDate(landingDate);

        // search for the last
        RaisingFactor2 lastBefore = null;
        for (E e : allRF) {

            // obtain the month of the date
            T3Date month = T3Date.newDate(e.getMonth());

            if (month.equals(beforeDate) ||
                month.before(beforeDate)) {

                // new last brefore is this one
                lastBefore = e;
            }
        }
        return lastBefore;
    }
}
