/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.type;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.nuiton.i18n.I18n.n_;

public class T3Date implements Comparable<T3Date>, Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MONTH = "month";

    public static final String PROPERTY_YEAR = "year";

    protected int month;

    protected int year;

    public static T3Date newDate(int month, int year) {
        T3Date result = new T3Date();
        result.setMonth(month);
        result.setYear(year);
        return result;
    }

    public static T3Date newDate(Date date) {
        T3Date result = new T3Date();
        result.fromDate(date);
        return result;
    }

    /**
     * Obtain all the possible start date of a stratum given the begin date, end
     * date and the time step.
     *
     * @param beginDate the first date where a stratum can begin
     * @param endDate   the ending date of the stratum
     * @param timeStep  number of months for each time period ina stratum
     * @return the set of start dates for each possible stratum
     */
    public static Set<T3Date> getStartDates(T3Date beginDate,
                                            T3Date endDate,
                                            int timeStep) {
        Set<T3Date> result = Sets.newLinkedHashSet();
        T3Date startdate = beginDate;
        while (startdate.before(endDate)) {
            result.add(startdate);
            startdate = startdate.incrementsMonths(timeStep);
        }
        return result;
    }

    static {

        n_("t3.common.t3Date");
        n_("t3.common.month");
        n_("t3.common.year");
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date toBeginDate() {
        Date result = getBeginDate();
        return result;
    }

    public Date getBeginDate() {
        Calendar c = getBeginCalendar();
        Date result = c.getTime();
        return result;
    }

    public Date toEndDate() {
        Date result = getEndDate();
        return result;
    }

    public Date getEndDate() {
        Calendar c = getEndCalendar();
        Date result = c.getTime();
        return result;
    }

    public java.sql.Date toBeginSqlDate() {
        java.sql.Date result = getBeginSqlDate();
        return result;
    }

    public java.sql.Date getBeginSqlDate() {
        Calendar c = getBeginCalendar();
        java.sql.Date result = new java.sql.Date(c.getTimeInMillis());
        return result;
    }

    /**
     * Obtains the ending date of this month.
     * <p/>
     * this means, we take the next month, and just remove a milisecond, so we
     * will include all days of the current month.
     *
     * @return the sql date corresponding to an ending date of this month.
     */
    public java.sql.Date toEndSqlDate() {
        java.sql.Date result = getEndSqlDate();
        return result;
    }

    public java.sql.Date getEndSqlDate() {
        Calendar c = getEndCalendar();
        java.sql.Date result = new java.sql.Date(c.getTimeInMillis());
        return result;
    }

    public void fromDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        // we start month at one
        month = c.get(Calendar.MONTH) + 1;
        year = c.get(Calendar.YEAR);
    }

    public void fromT3Date(T3Date date) {

        month = date.getMonth();
        year = date.getYear();
    }

    public T3Date incrementsMonths(int nbMonth) {
        Preconditions.checkArgument(
                nbMonth > 0,
                "nbMonth must be a strict positif integer but was : " + nbMonth);
        int nmonth = getMonth();
        int nyear = getYear();
        int nbMonthCount = nbMonth;
        while (nbMonthCount > 0) {
            if (nmonth == 12) {
                nmonth = 0;
                nyear++;
            }
            nmonth++;
            nbMonthCount--;
        }
        T3Date newDate = newDate(nmonth, nyear);
        return newDate;
    }

    public T3Date decrementsMonths(int nbMonth) {
        Preconditions.checkArgument(
                nbMonth > 0,
                "nbMonth must be a strict positif integer but was : " + nbMonth);

        int nmonth = getMonth();
        int nyear = getYear();
        int nbMonthCount = nbMonth;
        while (nbMonthCount > 0) {
            if (nmonth == 1) {
                nyear--;
                nmonth = 12;
            } else {
                nmonth--;
            }
            nbMonthCount--;
        }
        T3Date newDate = newDate(nmonth, nyear);
        return newDate;
    }

    @Override
    public String toString() {
        return (month < 10 ? "0" : "") + month + "-" + year;
    }

    @Override
    public int compareTo(T3Date date) {
        int result = year - date.year;
        if (result != 0) {
            return result;
        }
        result = month - date.month;
        return result;
    }

    public boolean before(T3Date date) {
        int result = compareTo(date);
        return result < 0;
    }

    public boolean beforeOrEquals(T3Date date) {
        int result = compareTo(date);
        return result <= 0;
    }

    public boolean after(T3Date date) {
        int result = compareTo(date);
        return result > 0;
    }

    public boolean afterOrEquals(T3Date date) {
        int result = compareTo(date);
        return result >= 0;
    }

    @Override
    public boolean equals(Object date) {
        if (!(date instanceof T3Date)) {
            return false;
        }
        if (date == this) {
            return true;
        }
        T3Date that = (T3Date) date;
        int result = year - that.year;
        if (result != 0) {
            return false;
        }
        result = month - that.month;
        return result == 0;
    }

    @Override
    public int hashCode() {
        return year + 17 * month;
    }

    public boolean isModuloMonths(T3Date date2, int nbMonth) {
        boolean result;
        int nbMonths = getNbMonths();
        int nbMonths2 = date2.getNbMonths();
        int delta;
        if (nbMonths2 > nbMonths) {
            delta = nbMonths2 - nbMonths + 1;
        } else {
            delta = nbMonths - nbMonths2 + 1;
        }
        result = delta % nbMonth == 0;
        return result;
    }

    protected int getNbMonths() {
        return 12 * year + month - 1;
    }

    protected Calendar getBeginCalendar() {
        Calendar c = Calendar.getInstance();
        setDayFields(c);
        removeTimeFields(c);
        return c;
    }

    protected Calendar getEndCalendar() {
        Calendar c = Calendar.getInstance();
        setDayFields(c);
        removeTimeFields(c);
        // add a month and then remove a milisecond to obtain all days of the
        // current month
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.MILLISECOND, -1);
        return c;
    }

    protected void removeTimeFields(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

    protected void setDayFields(Calendar c) {
        // we start month at 1 and Calendar at 0
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.YEAR, year);
        c.set(Calendar.DAY_OF_MONTH, 1);
    }
}
