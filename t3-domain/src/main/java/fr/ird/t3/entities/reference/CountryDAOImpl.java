/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Set;

/**
 * {@link Country} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class CountryDAOImpl<E extends Country> extends CountryDAOAbstract<E> {

    /**
     * Obtains all countries used as fleet for any trip in the database.
     *
     * @return the set of fleet countries used by any trip in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllFleetUsedInTrip() throws TopiaException {

        String hql = "SELECT DISTINCT(c) " +
                     "FROM CountryImpl c, TripImpl t " +
                     "WHERE t.vessel.fleetCountry = c.id";

//        TopiaQuery query = createQuery("c")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_FLEET_COUNTRY + " = c.id");

        return T3EntityHelper.querytoSet(hql, this);
    }

    /**
     * Obtains all countries used as flag for any trip in the database.
     *
     * @return the set of flag countries used by any trip in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllFlagUsedInTrip() throws TopiaException {

//        TopiaQuery query = createQuery("c")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_FLAG_COUNTRY + " = c.id");

        String hql = "SELECT DISTINCT(c) " +
                     "FROM CountryImpl c, TripImpl t " +
                     "WHERE t.vessel.flagCountry = c.id";

        return T3EntityHelper.querytoSet(hql, this);
    }

    /**
     * Obtains all countries used as fleet for any catches in the database.
     * <p/>
     * In fact we search on each boat from any trip having at least one catch
     *
     * @return the set of fleet countries used by any catches in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllFleetUsedInCatch() throws TopiaException {

//        TopiaQuery query = createQuery("c")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_FLEET_COUNTRY + " = c.id");
//        FIXME : trouvé pk cela est aussi long...
//                .addWhere("size (t." + Trip.PROPERTY_ACTIVITY + "." + Activity.PROPERTY_CORRECTED_ELEMENTARY_CATCH + ") >0");

        String hql = "SELECT DISTINCT(c) " +
                     "FROM CountryImpl c, TripImpl t " +
                     "WHERE t.vessel.fleetCountry = c.id";
//        FIXME : trouvé pk cela est aussi long...
//                AND size (t.activity.correctedElementaryCatch) >0";

        return T3EntityHelper.querytoSet(hql, this);
    }

    /**
     * Obtains all countries used as fleet for any samples in the database.
     * <p/>
     * In fact we search on each boat from any trip having at least one sample
     *
     * @return the set of fleet countries used by any samples in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllFleetUsedInSample(String oceanId) throws TopiaException {
//        TopiaQuery query = createQuery("c")
//                .addFrom(Trip.class, "t")
//                .addLeftJoin("t." + Trip.PROPERTY_ACTIVITY, "a", false)
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_FLEET_COUNTRY + " = c.id")
//                .addWhere("a." + Activity.PROPERTY_OCEAN + ".id = :oceanId")
////        FIXME : trouvé pk cela est aussi long...
////                .addWhere("(SELECT count(*) FROM " + SetSpeciesFrequencyImpl.class.getSimpleName() + " s WHERE s." + SetSpeciesFrequency.PROPERTY_ACTIVITY + " = a.id) > 0")
//                .addParam("oceanId", oceanId);

        String hql = "SELECT DISTINCT(c) " +
                     "FROM CountryImpl c, TripImpl t LEFT JOIN t.activity as a " +
                     "WHERE t.vessel.fleetCountry = c.id AND a.ocean.id = :oceanId";
//        FIXME : trouvé pk cela est aussi long...
//                .addWhere("(SELECT count(*) FROM " + SetSpeciesFrequencyImpl.class.getSimpleName() + " s WHERE s." + SetSpeciesFrequency.PROPERTY_ACTIVITY + " = a.id) > 0")

        return T3EntityHelper.querytoSet(hql, this, "oceanId", oceanId);
    }

    /**
     * Obtains all countries used as flag for any samples in the database.
     * <p/>
     * In fact we search on each boat from any trip having at least one sample
     *
     * @return the set of flag countries used by any samples in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllFlagUsedInSample(String oceanId) throws TopiaException {
//        TopiaQuery query = createQuery("c")
//                .addFrom(Trip.class, "t")
//                .addLeftJoin("t." + Trip.PROPERTY_ACTIVITY, "a", false)
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_FLAG_COUNTRY + " = c.id")
//                .addWhere("a." + Activity.PROPERTY_OCEAN + ".id = :oceanId")
////        FIXME : trouvé pk cela est aussi long...
////                .addWhere("(SELECT count(*) FROM " + SetSpeciesFrequencyImpl.class.getSimpleName() + " s WHERE s." + SetSpeciesFrequency.PROPERTY_ACTIVITY + " = a.id) >0")
//                .addParam("oceanId", oceanId);

        String hql = "SELECT DISTINCT(c) " +
                     "FROM CountryImpl c, TripImpl t LEFT JOIN t.activity as a " +
                     "WHERE t.vessel.flagCountry = c.id AND a.ocean.id = :oceanId";
//        FIXME : trouvé pk cela est aussi long...
//                .addWhere("(SELECT count(*) FROM " + SetSpeciesFrequencyImpl.class.getSimpleName() + " s WHERE s." + SetSpeciesFrequency.PROPERTY_ACTIVITY + " = a.id) >0")

        return T3EntityHelper.querytoSet(hql, this, "oceanId", oceanId);
    }

    public static Set<Country> getAllFleetCountries(Collection<Trip> trips) {

        Set<Country> result = Sets.newHashSet(
                Collections2.transform(trips,
                                       T3Functions.TRIP_TO_FLEET_COUNTRY)
        );
        return result;
    }

    public static Set<Country> getAllFlagCountries(Collection<Trip> trips) {

        Set<Country> result = Sets.newHashSet(
                Collections2.transform(trips,
                                       T3Functions.TRIP_TO_FLEET_COUNTRY)
        );
        return result;
    }
}
