/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.topia.TopiaException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * {@link Vessel} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class VesselDAOImpl<E extends Vessel> extends VesselDAOAbstract<E> {

    /**
     * Obtains all vessels used by all trips in the database.
     *
     * @return the set of used vessel in trips in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllUsedInTrip() throws TopiaException {

//        TopiaQuery query = createQuery("v")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + " = v.id");

        String hql = "SELECT DISTINCT(v) FROM VesselImpl v, TripImpl t WHERE t.vessel = v.id";

        return T3EntityHelper.querytoSet(hql, this);
    }

    /**
     * Obtain the set of possible vessels usable from the configuration to
     * obtain catches of the catch stratum.
     * <p/>
     * Vessels must verifiy two conditions :
     * <ul>
     * <li>Be of a senneur vessel simple type</li>
     * <li>Be in the selected fleet (from the configuration)</li>
     * </ul>
     *
     * @param country level 2 configuration
     * @return set of possible vessels usable for activity (so trips) of the
     *         catch stratum
     * @throws TopiaException if any problem while loading data
     */
    public Set<E> getPossibleCatchVessels(Country country) throws TopiaException {

        List<E> vessels = findAllByProperty(
                Vessel.PROPERTY_VESSEL_TYPE + "." +
                VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + "." +
                VesselSimpleType.PROPERTY_CODE, 1);

        Predicate<Vessel> predicate =
                T3Predicates.vesselUsingFleetCountry(Arrays.asList(country));

        Set<E> result = Sets.newHashSet(
                Collections2.filter(vessels, predicate));
        return result;
    }

    /**
     * Obtain the set of possible vessels usable from the configuration to
     * obtain catches of the catch stratum.
     * <p/>
     * Vessels must verifiy two conditions :
     * <ul>
     * <li>Be of a senneur vessel simple type</li>
     * <li>Be in the selected fleet (from the configuration)</li>
     * </ul>
     *
     * @param sampleFleets level 2 configuration
     * @param sampleFlags  the database transaction to load data
     * @return set of possible vessels usable for activity (so trips) of the
     *         catch stratum
     * @throws TopiaException if any problem while loading data
     */
    public Set<E> getPossibleSampleVessels(Collection<Country> sampleFleets,
                                           Collection<Country> sampleFlags) throws TopiaException {

        // takes only senneurs vessels
        List<E> vessels = findAllByProperty(
                Vessel.PROPERTY_VESSEL_TYPE + "." +
                VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + "." +
                VesselSimpleType.PROPERTY_CODE, 1);

        Predicate<Vessel> predicate =
                Predicates.and(
                        T3Predicates.vesselUsingFleetCountry(sampleFleets),
                        T3Predicates.vesselUsingFlagCountry(sampleFlags)
                );

        Set<E> result = Sets.newHashSet(
                Collections2.filter(vessels, predicate));
        return result;
    }

    public static <E extends Vessel> void retainsVesselSimpleTypes(
            Collection<E> vessels,
            Collection<VesselSimpleType> vesselSimpleTypes) {

        Iterables.removeIf(vessels,
                           Predicates.not(T3Predicates.vesselUsingVesselType(vesselSimpleTypes)));
    }

    public static <E extends Vessel> void retainsFleetCountries(Collection<E> vessels,
                                                                Collection<Country> fleetCountries) {
        Iterables.removeIf(vessels,
                           Predicates.not(T3Predicates.vesselUsingFleetCountry(fleetCountries)));
    }

    public static <E extends Vessel> void retainsFlagCountries(Collection<E> vessels,
                                                               Collection<Country> flagCountries) {
        Iterables.removeIf(vessels,
                           Predicates.not(T3Predicates.vesselUsingFlagCountry(flagCountries)));
    }

    public static Set<Vessel> getAllVessels(Collection<Trip> allTrips) {

        Set<Vessel> result = Sets.newHashSet(
                Collections2.transform(allTrips, T3Functions.TRIP_BY_VESSEL)
        );
        return result;
    }

}
