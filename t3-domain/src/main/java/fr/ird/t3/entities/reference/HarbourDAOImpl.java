/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import fr.ird.t3.entities.T3EntityHelper;
import org.nuiton.topia.TopiaException;

import java.util.Set;

/**
 * {@link Harbour} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class HarbourDAOImpl<E extends Harbour> extends HarbourDAOAbstract<E> {

    /**
     * Obtains all landing harbour used for any trip in the database.
     *
     * @return the set of landing harbour used by any trip in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllUsedInLandingTrip() throws TopiaException {

//        TopiaQuery query = createQuery("h")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_LANDING_HARBOUR + " = h.id");

        String hql = "SELECT DISTINCT(h) FROM HarbourImpl h, TripImpl t WHERE t.landingHarbour = h.id";

        return T3EntityHelper.querytoSet(hql, this);
    }
}