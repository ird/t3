/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.util.List;
import java.util.Set;

/**
 * Abstract implementation of {@link ZoneStratumAware}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractZoneStratumAwareMeta implements ZoneStratumAwareMeta {

    private static final long serialVersionUID = 1L;

    /** Persistant type of the zone. */
    protected final Class<? extends ZoneStratumAware> type;

    /** Persistant table name of the zone. */
    protected final String tableName;

    /** I18n libelle of the type of zone. */
    protected final String i18nKey;

    protected AbstractZoneStratumAwareMeta(String tableName,
                                           Class<? extends ZoneStratumAware> type,
                                           String i18nKey) {
        this.tableName = tableName;
        this.type = type;
        this.i18nKey = i18nKey;
    }

    @Override
    public List<ZoneVersion> getAllZoneVersions(TopiaContext tx) throws TopiaException {
        ZoneStratumAwareDAOImpl<?> dao = getDAO(tx);
        List<ZoneVersion> result = dao.findAllVersion();
        return result;
    }

    @Override
    public Multimap<SchoolType, ZoneStratumAware> getZones(Ocean ocean,
                                                           Set<SchoolType> schoolTypes,
                                                           ZoneVersion zoneVersion,
                                                           TopiaContext tx) throws TopiaException {

        ZoneStratumAwareDAOImpl<ZoneStratumAware> dao = getDAO(tx);
        Multimap<SchoolType, ZoneStratumAware> result =
                ArrayListMultimap.create();

        for (SchoolType schoolType : schoolTypes) {

            List<ZoneStratumAware> zones =
                    dao.findAllByOceanSchoolTypeAndZoneVersion(
                            ocean,
                            schoolType,
                            zoneVersion
                    );
            result.putAll(schoolType, zones);
        }
        return result;
    }

    @Override
    public ZoneVersion getZoneVersion(String zoneVersionId,
                                      TopiaContext tx) throws TopiaException {
        ZoneStratumAwareDAOImpl<ZoneStratumAware> dao = getDAO(tx);
        ZoneVersion zoneVersion = dao.findVersionByVersionId(zoneVersionId);
        return zoneVersion;
    }

    public Class<? extends ZoneStratumAware> getType() {
        return type;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public String getId() {
        return getTableName();
    }

    public String getI18nKey() {
        return i18nKey;
    }

    protected ZoneStratumAwareDAOImpl<ZoneStratumAware> getDAO(TopiaContext tx) throws TopiaException {
        ZoneStratumAwareDAOImpl<ZoneStratumAware> result = T3DAOHelper.getDAO(tx, type);
        return result;
    }
}
