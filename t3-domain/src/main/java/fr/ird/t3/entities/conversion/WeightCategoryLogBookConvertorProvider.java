/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentDAO;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.util.Collection;

/**
 * To obtain a  ll available wieght convertors.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class WeightCategoryLogBookConvertorProvider {

    protected static final String OCEAN =
            WeightCategoryTreatment.PROPERTY_OCEAN + "." + Ocean.PROPERTY_CODE;

    protected static final String SCHOOL_TYPE =
            WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE + "." +
            SchoolType.PROPERTY_CODE;

    protected final Collection<WeightCategoryLogBookConvertor> convertors;

    public static final int OA = 1;

    public static final int OI = 2;

    public static final int OP = 5;

    public static final int BO = 1;

    public static final int BL = 2;

    public static final int BI = 3;

    public static WeightCategoryLogBookConvertorProvider newInstance(TopiaContext transaction) throws TopiaException {

        WeightCategoryTreatmentDAO dao =
                T3DAOHelper.getWeightCategoryTreatmentDAO(transaction);

        Collection<WeightCategoryLogBookConvertor> converters =
                Lists.newArrayList();

        // BO = 1, BL = 2, BI = 3
        // OA = 1, OI = 2, OP = 5

        prepareForOA(dao, converters);
        prepareForOI(dao, converters);
        prepareForOP(dao, converters);

        WeightCategoryLogBookConvertorProvider helper =
                new WeightCategoryLogBookConvertorProvider(converters);

        return helper;
    }

    private static void prepareForOA(WeightCategoryTreatmentDAO dao,
                                     Collection<WeightCategoryLogBookConvertor> converters) throws TopiaException {
        {
            // BO Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OABO(
                            getCategory(dao, OA, BO, null, null),
                            getCategory(dao, OA, BO, 0, 10),
                            getCategory(dao, OA, BO, 10, null)
                    );
            converters.add(convertor);
        }
        {
            // BL Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OABL(
                            getCategory(dao, OA, BL, null, null),
                            getCategory(dao, OA, BL, 0, 10),
                            getCategory(dao, OA, BL, 10, 30),
                            getCategory(dao, OA, BL, 30, null)
                    );
            converters.add(convertor);
        }
        {
            // BI Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OABI(
                            getCategory(dao, OA, BI, null, null),
                            getCategory(dao, OA, BI, 0, 10),
                            getCategory(dao, OA, BI, 10, 30),
                            getCategory(dao, OA, BI, 30, null)
                    );
            converters.add(convertor);
        }

    }

    private static void prepareForOI(WeightCategoryTreatmentDAO dao,
                                     Collection<WeightCategoryLogBookConvertor> converters) throws TopiaException {

        {
            // BO Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OIBO(
                            getCategory(dao, OI, BO, null, null),
                            getCategory(dao, OI, BO, 0, 10),
                            getCategory(dao, OI, BO, 10, null)
                    );
            converters.add(convertor);
        }
        {
            // BL Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OIBL(
                            getCategory(dao, OI, BL, null, null),
                            getCategory(dao, OI, BL, 0, 10),
                            getCategory(dao, OI, BL, 10, null)
                    );
            converters.add(convertor);
        }
        {
            // BI Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OIBI(
                            getCategory(dao, OI, BI, null, null),
                            getCategory(dao, OI, BI, 0, 10),
                            getCategory(dao, OI, BI, 10, null)
                    );
            converters.add(convertor);
        }
    }

    private static void prepareForOP(WeightCategoryTreatmentDAO dao,
                                     Collection<WeightCategoryLogBookConvertor> converters) throws TopiaException {

        {
            // BO Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OPBO(
                            getCategory(dao, OP, BO, null, null),
                            getCategory(dao, OP, BO, 0, 10),
                            getCategory(dao, OP, BO, 10, null)
                    );
            converters.add(convertor);
        }
        {
            // BL Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OPBL(
                            getCategory(dao, OP, BL, null, null),
                            getCategory(dao, OP, BL, 0, 10),
                            getCategory(dao, OP, BL, 10, null)
                    );
            converters.add(convertor);
        }
        {
            // BI Convertor
            WeightCategoryLogBookConvertor convertor =
                    new WeightCategoryLogBookConvertorFOR_OPBI(
                            getCategory(dao, OP, BI, null, null),
                            getCategory(dao, OP, BI, 0, 10),
                            getCategory(dao, OP, BI, 10, null)
                    );
            converters.add(convertor);
        }
    }

    protected WeightCategoryLogBookConvertorProvider(Collection<WeightCategoryLogBookConvertor> convertors) {
        this.convertors = convertors;
    }

    public WeightCategoryLogBookConvertor getConvertor(Ocean ocean,
                                                       SchoolType schoolType) {
        WeightCategoryLogBookConvertor result = null;
        for (WeightCategoryLogBookConvertor converter : convertors) {
            if (converter.accept(ocean, schoolType)) {
                result = converter;
                break;
            }
        }
        return result;
    }

    protected static WeightCategoryTreatment getCategory(WeightCategoryTreatmentDAO dao,
                                                         int oceanCode,
                                                         int schoolTypeCode,
                                                         Integer min,
                                                         Integer max) throws TopiaException {

        WeightCategoryTreatment result = dao.findByOceanSchoolTypeAndBound(
                oceanCode,
                schoolTypeCode,
                min,
                max
        );

        Preconditions.checkNotNull(
                result,
                "Could not find weight treatment category with " +
                "parameters : [ocean:" + oceanCode + ", schoolType:" +
                schoolTypeCode + ", min:" + min + ", max:" + max + "]"
        );
        return result;
    }
}
