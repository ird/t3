/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.nuiton.topia.TopiaException;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * {@link WeightCategoryTreatment} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class WeightCategoryTreatmentDAOImpl<E extends WeightCategoryTreatment> extends WeightCategoryTreatmentDAOAbstract<E> {

    protected static final String OCEAN = WeightCategoryTreatment.PROPERTY_OCEAN + "." + Ocean.PROPERTY_CODE;

    protected static final String SCHOOL_TYPE = WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE + "." + SchoolType.PROPERTY_CODE;

    public static Comparator<WeightCategoryTreatment> newComparator() {
        return new WeightCategoryTreatmentComparator();
    }

    public void sort(List<E> weightCategoryTreatment) {
        Collections.sort(weightCategoryTreatment, newComparator());
    }

    public static WeightCategoryTreatment getWeightCategoryTreatment(
            Map<WeightCategoryTreatment, Integer> weightCategories,
            int lfLengthClass) {
        WeightCategoryTreatment result = null;
        for (Map.Entry<WeightCategoryTreatment, Integer> e :
                weightCategories.entrySet()) {
            if (lfLengthClass < e.getValue()) {

                // got the good category
                result = e.getKey();
                break;
            }
        }

        if (result == null) {
            throw new IllegalStateException(
                    "Could not find a weightCategoryTreatment for given " +
                    "lfLengthClass " + lfLengthClass);
        }
        return result;
    }

    public static void sortWeightCategoryTreatments(List<WeightCategoryTreatment> list) {
        Collections.sort(list, new WeightCategoryTreatmentComparator());
    }

    public WeightCategoryTreatment findByOceanSchoolTypeAndBound(int oceanCode,
                                                                 int schoolTypeCode,
                                                                 Integer min,
                                                                 Integer max) throws TopiaException {
//        TopiaQuery query = createQuery("c").
//                addEquals("c." + OCEAN, oceanCode).
//                addEquals("c." + SCHOOL_TYPE, schoolTypeCode);

        String hql = "SELECT c FROM WeightCategoryTreatmentImpl c " +
                     "WHERE c.ocean.code = :oceanCode " +
                     "AND c.schoolType.code = :schoolTypeCode";

        List<Object> params = Lists.<Object>newArrayList(
                "oceanCode", oceanCode,
                "schoolTypeCode", schoolTypeCode);

        if (min == null) {
//            query.addNull("c." + WeightCategoryTreatment.PROPERTY_MIN);
            hql += " AND c.min IS NULL";
        } else {
//            query.addEquals("c." + WeightCategoryTreatment.PROPERTY_MIN, min);
            hql += " AND c.min = :min";
            params.add("min");
            params.add(min);
        }
        if (max == null) {
//            query.addNull("c." + WeightCategoryTreatment.PROPERTY_MAX);
            hql += " AND c.max IS NULL";
        } else {
//            query.addEquals("c." + WeightCategoryTreatment.PROPERTY_MAX, max);
            hql += " AND c.max = :max";
            params.add("max");
            params.add(max);
        }

//        WeightCategoryTreatment result = query.executeToEntity(
//                getContext(),
//                WeightCategoryTreatment.class
//        );
        WeightCategoryTreatment result = findByQuery(hql, params.toArray());
        return result;
    }

    /**
     * Obtain for each given school type in the given ocean (by his id) all
     * available weright category.
     *
     * @param ocean       the filtering ocean
     * @param schoolTypes all the school type
     * @return the universe of weight categories found for each school type (in
     *         the given ocean too)
     * @throws TopiaException if any problem while loading data form database
     */
    public Multimap<SchoolType, E> getWeightCategories(Ocean ocean,
                                                       Set<SchoolType> schoolTypes) throws TopiaException {

        Multimap<SchoolType, E> result = ArrayListMultimap.create();

        for (SchoolType schoolType : schoolTypes) {

            List<E> categories = findAllByOceanAndSchoolType(ocean, schoolType);
            result.putAll(schoolType, categories);
        }
        return result;
    }

    /**
     * Obtain for given school type in the given ocean, all available weight
     * categories.
     *
     * @param ocean      the filtering ocean
     * @param schoolType the filtering school type
     * @return the universe of weight categories found for each school type (in
     *         the given ocean too)
     * @throws TopiaException if any problem while loading data form database
     */
    public List<E> findAllByOceanAndSchoolType(Ocean ocean,
                                               SchoolType schoolType) throws TopiaException {

        List<E> result = findAllByProperties(
                WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType,
                WeightCategoryTreatment.PROPERTY_OCEAN, ocean
        );
        return result;
    }

    public Map<E, Integer> getWeightCategoryTreatmentLengthClass(
            Ocean ocean,
            LengthWeightConversion conversion) throws TopiaException {

        // get all categories for the given ocean
        List<E> categories = findAllByOcean(ocean);

        Map<E, Integer> result = Maps.newTreeMap(newComparator());

        for (E category : categories) {

            Integer lengthClass;
            Integer max = category.getMax();
            if (max == null) {

                // this means that the category has not upper bound,
                // just use Integer.MAX_VALUE (this will enougth I think)
                lengthClass = Integer.MAX_VALUE;

            } else {

                // convert max weight to max lf length class
                lengthClass = conversion.computeLFLengthClassFromWeight(max);
            }

            // keep for the category the max length class computed
            result.put(category, lengthClass);
        }

        return result;
    }

    public static class WeightCategoryTreatmentComparator implements Comparator<WeightCategoryTreatment>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(WeightCategoryTreatment o1, WeightCategoryTreatment o2) {

            if (o1.getMin() == null && o1.getMax() == null) {

                // equals to o2 if only if o2.min = o2.max = null

                if (o2.getMin() == null && o2.getMax() == null) {
                    return 0;
                }
                return 1;
            }

            if (o2.getMin() == null && o2.getMax() == null) {

                // equals to o1 if only if o1.min = o1.max = null

                if (o1.getMin() == null && o1.getMax() == null) {
                    return 0;
                }
                return -1;
            }

            if (o1.getMin() == null) {
                return o2.getMin() == null ? 0 : -1;
            }
            if (o2.getMin() == null) {
                return 1;
            }
            return o1.getMin().compareTo(o2.getMin());
        }

        private WeightCategoryTreatmentComparator() {
        }
    }
}
