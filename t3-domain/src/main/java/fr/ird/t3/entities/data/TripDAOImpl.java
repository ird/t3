/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.entities.type.T3Point;
import fr.ird.t3.entities.type.T3PointImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaSQLQuery;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Client implementation of the Trip dao.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class TripDAOImpl<E extends Trip> extends TripDAOAbstract<E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TripDAOImpl.class);

    /**
     * Obtains all trips usable for a level 1 configuration, says :
     * <p/>
     * <ul>
     * <li>Only for senneur vessel simple type</li>
     * <li>samplesOnly = true or completionstatus >0</li>
     * </ul>
     *
     * @return list of all trips usable in level 1
     * @throws TopiaException if could not query db
     * @since 1.2
     */
    public List<E> findAllForLevel1() throws TopiaException {
//        TopiaQuery query = createQuery("t")
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." + Vessel.PROPERTY_VESSEL_TYPE + "." +
//                          VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + "." +
//                          VesselSimpleType.PROPERTY_CODE, TopiaQuery.Op.EQ, 1)
//                .addWhere("t." + Trip.PROPERTY_SAMPLES_ONLY +
//                          " = true OR (t." + Trip.PROPERTY_COMPLETION_STATUS +
//                          " IS NOT NULL AND t." +
//                          Trip.PROPERTY_COMPLETION_STATUS + " >0)");
//        List<E> result = findAllByQuery(query);

        String hql = "SELECT t FROM TripImpl t WHERE t.vessel.vesselType.vesselSimpleType.code = 1 " +
                     "AND (t.samplesOnly = true OR (t.completionStatus IS NOT NULL AND t.completionStatus >0))";
        List<E> result = findAllByQuery(hql);
        sortTrips(result);
        return result;
    }

    public List<E> findAllByIds(List<String> allTripIds) throws TopiaException {
        List<E> result;
        if (CollectionUtils.isEmpty(allTripIds)) {
            result = Lists.newArrayList();
        } else {
//            TopiaQuery query = createQuery("t")
//                    .addWhere("t.id", TopiaQuery.Op.IN, allTripIds);

            String hql = "SELECT t FROM TripImpl t WHERE t.id IN :ids";
            result = findAllByQuery(hql, "ids", allTripIds);
            sortTrips(result);
        }
        return result;
    }

    public List<String> findAllIdsByOcean(Ocean ocean) throws TopiaException {

//        TopiaQuery query;
        String hql;
        List<Object> params;

        if (ocean == null) {

            // get all trips with no activity
//            query = createQuery("t")
//                    .setSelect("t.id")
//                    .addWhere("size(t." + Trip.PROPERTY_ACTIVITY + ") = 0");
            hql = "SELECT t.id FROM TripImpl t WHERE size(t.activity) = 0";
            params = Collections.emptyList();
        } else {
//            query = new TopiaQuery(Activity.class, "a")
//                    .setSelect("distinct(a.trip.id)")
//                    .addWhere("a.ocean", TopiaQuery.Op.EQ, ocean);
            hql = "SELECT DISTINCT(a.trip.id) FROM ActivityImpl a WHERE a.ocean = :ocean";
            params = Lists.<Object>newArrayList("ocean", ocean);
        }
        List<String> result = findAllByQuery(String.class, hql, params.toArray());
        return result;
    }

    public Multimap<Ocean, String> findAllIdsByOcean() throws TopiaException {

        Set<Ocean> oceans =
                T3DAOHelper.getOceanDAO(getContext()).findAllUsedInActivity();

        Multimap<Ocean, String> result = ArrayListMultimap.create();
        for (Ocean ocean : oceans) {
            List<String> allByOcean = findAllIdsByOcean(ocean);
            if (CollectionUtils.isNotEmpty(allByOcean)) {
                result.putAll(ocean, allByOcean);
            }
        }
        // add trips with no ocean (http://forge.codelutin.com/issues/1134)
        List<String> tripsWithNoOcean = findAllIdsByOcean(null);
        if (CollectionUtils.isNotEmpty(tripsWithNoOcean)) {
            result.putAll(null, tripsWithNoOcean);
        }
        return result;
    }

    public T3Point getBarycenterForActivitiesOfADay(E trip, Date day) throws TopiaException {

        TopiaSQLQuery<T3Point> query = new GetT3PointQuery<E>(trip, day);
        T3Point point = query.findSingleResult(getContext());
        return point;
    }

    /**
     * Obtain the list of trips between the inclusive landing dates.
     *
     * @param beginDate              the min bound of landing date
     * @param endDate                the max bound of landing date
     * @param samplesOnly            flag to use {@code Trip.samplesOnly} value if not null
     * @param rejectCompletionStatus flag to use {@code Trip.rejectComputationStatus} value if not null
     * @return the list of trips with landing date between given bound.
     * @throws TopiaException if any pb while loading data
     */
    public List<E> findAllBetweenLandingDate(T3Date beginDate,
                                             T3Date endDate,
                                             Boolean samplesOnly,
                                             boolean rejectCompletionStatus) throws TopiaException {
        String hql = "SELECT t FROM TripImpl t WHERE t.landingDate BETWEEN :beginDate AND :endDate";
        List<Object> params = Lists.<Object>newArrayList(
                "beginDate", beginDate.toBeginSqlDate(),
                "endDate", endDate.toEndSqlDate());
//        TopiaQuery query = createQuery().
//                addBetween(Trip.PROPERTY_LANDING_DATE,
//                           beginDate.toBeginSqlDate(),
//                           endDate.toEndSqlDate());
        if (samplesOnly != null) {
            hql += " AND t.samplesOnly = :samplesOnly";
            params.add("samplesOnly");
            params.add(samplesOnly);
//            query.addWhere(Trip.PROPERTY_SAMPLES_ONLY, TopiaQuery.Op.EQ, samplesOnly);
        }
        if (rejectCompletionStatus) {

            // does not accept trip with completionStatus = 0
//            query.addWhere(Trip.PROPERTY_COMPLETION_STATUS, TopiaQuery.Op.NEQ, 0);
            hql += " AND t.completionStatus != 0";
        }

//        List<E> result = query.
//                executeToEntityList(getContext(), getEntityClass());
        List<E> result = findAllByQuery(hql, params.toArray());
        return result;
    }

    public List<Integer> findAllYearsUsedInTrip() throws TopiaException {
        T3Date minDate = getFirstLandingDate();
        T3Date maxDate = getLastLandingDate();
        int minYear = minDate.getYear();
        int maxYear = maxDate.getYear();
        Set<Integer> years = Sets.newHashSet();
        for (int i = minYear; i <= maxYear; i++) {
            years.add(i);
        }
        List<Integer> result = Lists.newArrayList(years);
        Collections.sort(result);
        return result;
    }

    public T3Date getFirstLandingDate() throws TopiaException {

        String hql = " SELECT min(t.landingDate) FROM TripImpl t";
        Date date = findByQuery(Date.class, hql);
//        Date date = (Date) createQuery().
//                executeToObject(getContext(),
//                                "min(" + Trip.PROPERTY_LANDING_DATE + ")");

        T3Date result = null;

        if (date != null) {
            result = T3Date.newDate(date);
        }
        return result;
    }

    public T3Date getLastLandingDate() throws TopiaException {

        String hql = " SELECT max(t.landingDate) FROM TripImpl t";
        Date date = findByQuery(Date.class, hql);

//        Date date = (Date) createQuery().
//                executeToObject(getContext(),
//                                "max(" + Trip.PROPERTY_LANDING_DATE + ")");

        T3Date result = null;

        if (date != null) {
            result = T3Date.newDate(date);
        }
        return result;
    }

    public List<List<E>> splitCompleteTrip(List<E> trips) {
        List<List<E>> result = Lists.newArrayList();
        List<E> completeTrip = Lists.newArrayList();
        for (E trip : trips) {

            // add this trip to in building complete trip list
            completeTrip.add(trip);

            // compute if this trip finish a complete trip
            boolean tripWasComplete =
                    T3Predicates.TRIP_ENDS_A_COMPLETE_TRIP.apply(trip);

            if (tripWasComplete) {

                // found a complete trip
                if (log.isInfoEnabled()) {
                    log.info("Found a complete trip with " +
                             completeTrip.size() + " trip(s).");
                }
                if (log.isDebugEnabled()) {
                    for (Trip t : completeTrip) {
                        log.debug("complete trip part - " + t.getLandingDate());
                    }
                }

                // add this complete trip
                result.add(completeTrip);

                // reset new complete trip list
                completeTrip = Lists.newArrayList();
            }
        }
        return result;
    }

    public List<CompleteTrip> toCompleteTrip(List<E> trips) {
        List<CompleteTrip> result = Lists.newArrayList();
        List<Trip> completeTrip = Lists.newArrayList();
        for (E trip : trips) {

            // add this trip to in building complete trip list
            completeTrip.add(trip);

            // compute if this trip finish a complete trip
            boolean tripWasComplete =
                    T3Predicates.TRIP_ENDS_A_COMPLETE_TRIP.apply(trip);

            if (tripWasComplete) {

                // found a complete trip
                if (log.isDebugEnabled()) {
                    log.debug("Found a complete trip with " +
                              completeTrip.size() + " trip(s).");
                    for (Trip t : completeTrip) {
                        log.debug("complete trip part - " + t.getLandingDate());
                    }
                }

                // add this complete trip
                result.add(new CompleteTrip(completeTrip));

                // reset new complete trip list
                completeTrip = Lists.newArrayList();
            }
        }
        return result;
    }

    public static <E extends Trip> Collection<E> getAllTripsWithNoDataComputed(Collection<E> trips) {
        Collection<E> result = Collections2.filter(
                trips, T3Predicates.TRIP_WITH_NO_DATA_COMPUTED);
        return result;
    }

    public static <E extends Trip> Collection<E> getAllTripsWithAllDataComputed(Collection<E> trips) {
        Collection<E> result = Collections2.filter(
                trips, T3Predicates.TRIP_WITH_ALL_DATA_COMPUTED);
        return result;
    }

    public static <E extends Trip> Collection<E> getAllTripsWithSomeDataComputed(Collection<E> trips) {
        Collection<E> result = Collections2.filter(
                trips, T3Predicates.TRIP_WITH_SOME_DATA_COMPUTED);
        return result;
    }

    public static <E extends Trip> Set<Integer> getAllTripsDepartureYear(Collection<E> trips) {

        Set<Integer> result = Sets.newHashSet(
                Collections2.transform(
                        trips, T3Functions.TRIP_TO_DEPARTURE_YEAR
                )
        );

        return result;
    }

    public static <E extends Trip> List<TripDTO> toDTO(List<E> t3Trips) {

        List<TripDTO> trips;

        if (CollectionUtils.isEmpty(t3Trips)) {

            trips = Lists.newArrayList();
        } else {

            trips = Lists.transform(t3Trips, T3Functions.TRIP_TO_DTO);

        }
        return trips;
    }

    public static Multimap<T3Date, CompleteTrip> splitTripsByMonth(List<CompleteTrip> trips) {
        Multimap<T3Date, CompleteTrip> result = LinkedHashMultimap.create();
        for (CompleteTrip trip : trips) {

            // get trip landing month
            T3Date tripMonth = T3Functions.TRIP_TO_LANDING_DATE.apply(trip.getLandingTrip());

            result.put(tripMonth, trip);
        }
        return result;
    }

    public static void retainsVessels(Collection<Trip> trips,
                                      Collection<Vessel> vessels) {
        Predicate<Trip> predicate = T3Predicates.tripUsingVessel(vessels);
        Iterables.removeIf(trips, Predicates.not(predicate));
    }

    public static void retainsOceans(Collection<Trip> trips,
                                     Collection<Ocean> oceans) {
        Predicate<Trip> predicate = T3Predicates.tripUsingOcean(oceans);
        Iterables.removeIf(trips, Predicates.not(predicate));
    }

    public static void retainsLandingHarbours(Collection<Trip> trips,
                                              Collection<Harbour> harbours) {

        Predicate<Trip> predicate = T3Predicates.tripUsingHarbour(harbours);
        Iterables.removeIf(trips, Predicates.not(predicate));
    }

    public static void retainsDepartureYears(Collection<Trip> trips,
                                             Collection<Integer> years) {
        Predicate<Trip> predicate = T3Predicates.tripUsingDepartureYear(years);
        Iterables.removeIf(trips, Predicates.not(predicate));
    }

    public static boolean isTripsAllWithLogBook(Iterable<Trip> trips) {
        boolean allwithLogBook =
                Iterables.all(trips, T3Predicates.TRIP_WITH_LOG_BOOK);
        return allwithLogBook;
    }

    /**
     * Sort the given trips given their landing date.
     *
     * @param list the trips to sort
     */
    public static <E extends Trip> void sortTrips(List<E> list) {
        Collections.sort(list, new TripComparator<Trip>());
    }

    /**
     * Given some trips, group them by their owing vessel.
     *
     * @param trips the trips to scan
     * @return the trips grouped by their owing vessel
     */
    public static ListMultimap<Vessel, Trip> groupByVessel(List<Trip> trips) {
        ListMultimap<Vessel, Trip> result =
                Multimaps.index(trips, T3Functions.TRIP_BY_VESSEL);
        return result;
    }

    /**
     * Gets from the given trips theire activities bound date.
     *
     * @param trips the trips to scan
     * @return the bound pair of min and max date of activities of the given trips
     */
    public static MutablePair<Date, Date> getActivityBoundDate(Collection<Trip> trips) {

        Date firstDate = null;
        Date lastDate = null;
        for (Trip trip : trips) {

            if (!trip.isActivityEmpty()) {
                for (Activity activity : trip.getActivity()) {
                    Date activityDate = activity.getDate();
                    if (firstDate == null || activityDate.before(firstDate)) {
                        firstDate = activityDate;
                    }
                    if (lastDate == null || activityDate.after(lastDate)) {
                        lastDate = activityDate;
                    }
                }
            }
        }
        MutablePair<Date, Date> result = MutablePair.of(firstDate, lastDate);
        return result;
    }

    private static class TripComparator<E extends Trip> implements Comparator<E>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(E o1, E o2) {
            return o1.getLandingDate().compareTo(o2.getLandingDate());
        }

    }

    private static class GetT3PointQuery<E extends Trip> extends TopiaSQLQuery<T3Point> {
        private final E trip;

        private final Date day;

        public GetT3PointQuery(E trip, Date day) {
            this.trip = trip;
            this.day = day;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT ST_X(record.point), ST_Y(record.point) FROM " +
                    "(" +
                    "   SELECT ST_Centroid(ST_Collect(a.the_geom)) as point" +
                    "   FROM activity a " +
                    "   WHERE a.trip = ? AND a.date::date = ?" +
                    ") AS record;");
            ps.setString(1, trip.getTopiaId());
            ps.setDate(2, new java.sql.Date(day.getTime()));
            return ps;
        }

        @Override
        protected T3Point prepareResult(ResultSet set) throws SQLException {
            float x = set.getFloat(1);
            float y = set.getFloat(2);
            T3Point point = new T3PointImpl(x, y);
            return point;
        }
    }
}
