/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import fr.ird.t3.entities.reference.Species;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SampleWellImpl extends SampleWellAbstract {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SampleWellImpl.class);

    @Override
    public void deleteComputedData() {
        if (log.isDebugEnabled()) {
            log.debug("delete comuted data from " + getTopiaId());
        }
        ComputedDataHelper.deleteComputedDatas(this);
    }

    @Override
    public void deleteComputedDataLevel0() {
        // nothing to delete
    }

    @Override
    public void deleteComputedDataLevel1() {

        setRfMinus10(null);
        setRfPlus10(null);
        setRfTot(null);
        setPropWeightedWeight(null);
        setWeightedWeightMinus10(null);
        setWeightedWeightPlus10(null);
        setTotalSampleWeight(null);

        clearSampleSetSpeciesFrequency();
        clearSampleSetSpeciesCatWeight();
    }

    @Override
    public void deleteComputedDataLevel2() {
        // nothing to delete
    }

    @Override
    public void deleteComputedDataLevel3() {
        // nothing to delete
    }

    @Override
    public float getTotalSampleSetSpeciesFrequencyNumber(Species species) {

        float nb = 0;
        for (SampleSetSpeciesFrequency standardiseSampleSpecies : getSampleSetSpeciesFrequency()) {
            if (species.equals(standardiseSampleSpecies.getSpecies())) {
                nb += standardiseSampleSpecies.getNumber();
            }
        }
        return nb;
    }

}
