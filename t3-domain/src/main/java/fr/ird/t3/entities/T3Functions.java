/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Function;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityAware;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDTO;
import fr.ird.t3.entities.data.WeightCategorySampleAware;
import fr.ird.t3.entities.data.WeightCategoryTreatmentAware;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesLengthStep;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.lang3.time.DateUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Calendar;
import java.util.Date;

/**
 * Usefull functions on entites.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1
 */
public class T3Functions {


    public static final Function<TopiaEntity, String> TO_TOPIA_ID = new Function<TopiaEntity, String>() {
        @Override
        public String apply(TopiaEntity input) {
            return input.getTopiaId();
        }
    };

    public static final Function<T3ReferenceEntity, Integer> TO_REFERENTIEL_CODE = new Function<T3ReferenceEntity, Integer>() {
        @Override
        public Integer apply(T3ReferenceEntity input) {
            return input.getCode();
        }
    };

    public static final Function<Trip, Vessel> TRIP_BY_VESSEL = new Function<Trip, Vessel>() {
        @Override
        public Vessel apply(Trip input) {
            return input.getVessel();
        }
    };

    public static final Function<ActivityAware, Activity> ACTIVITY_AWARE_BY_ACTIVITY = new Function<ActivityAware, Activity>() {
        @Override
        public Activity apply(ActivityAware input) {
            return input.getActivity();
        }
    };

    public static final Function<Activity, Long> ACTIVITY_BY_LONG_DAY = new Function<Activity, Long>() {
        @Override
        public Long apply(Activity input) {
            Date d = roundDateToDay(input.getDate());
            long dayId = d.getTime();
            return dayId;
        }

        Date roundDateToDay(Date date) {
            Calendar calendar = DateUtils.toCalendar(date);
            int year = calendar.get(Calendar.YEAR);
            int day = calendar.get(Calendar.DAY_OF_YEAR);
            calendar.setTimeInMillis(0);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.DAY_OF_YEAR, day);
            Date result = calendar.getTime();
            return result;
        }
    };

    public static final Function<SpeciesAware, Species> SPECIES_AWARE_BY_SPECIES = new Function<SpeciesAware, Species>() {
        @Override
        public Species apply(SpeciesAware input) {
            return input.getSpecies();
        }
    };

    public static final Function<SpeciesLengthStep, Integer> SPECIES_LENGTH_STEP_BY_LD1_CLASS = new Function<SpeciesLengthStep, Integer>() {
        @Override
        public Integer apply(SpeciesLengthStep input) {
            return input.getLd1Class();
        }
    };

    public static final Function<Trip, Country> TRIP_TO_FLEET_COUNTRY = new Function<Trip, Country>() {
        @Override
        public Country apply(Trip input) {
            return input.getVessel().getFleetCountry();
        }
    };

    public static final Function<Trip, T3Date> TRIP_TO_LANDING_DATE = new Function<Trip, T3Date>() {
        @Override
        public T3Date apply(Trip input) {
            return T3Date.newDate(input.getLandingDate());
        }
    };

    public static final Function<Trip, Integer> TRIP_TO_LANDING_YEAR = new Function<Trip, Integer>() {

        Calendar calendar = Calendar.getInstance();

        @Override
        public Integer apply(Trip input) {
            Date activityDate = input.getLandingDate();
            calendar.setTime(activityDate);
            return calendar.get(Calendar.YEAR);
        }
    };

    public static final Function<Trip, Integer> TRIP_TO_DEPARTURE_YEAR = new Function<Trip, Integer>() {

        Calendar cal = Calendar.getInstance();

        @Override
        public Integer apply(Trip input) {
            Date departureDate = input.getDepartureDate();
            cal.setTime(departureDate);
            int year = cal.get(Calendar.YEAR);
            return year;
        }
    };

    public static final Function<Trip, TripDTO> TRIP_TO_DTO = new Function<Trip, TripDTO>() {
        @Override
        public TripDTO apply(Trip input) {
            return input.toDTO();
        }
    };

    public static final Function<SetSpeciesFrequency, Integer> SET_SPECIES_FREQUENCY_BY_LF_LENGTH_CLASS = new Function<SetSpeciesFrequency, Integer>() {
        @Override
        public Integer apply(SetSpeciesFrequency input) {
            return input.getLfLengthClass();
        }
    };

    public static final Function<SetSpeciesFrequency, Float> SET_SPECIES_FREQUENCY_BY_COUNT = new Function<SetSpeciesFrequency, Float>() {
        @Override
        public Float apply(SetSpeciesFrequency input) {
            return input.getNumber();
        }
    };

    public static final Function<ExtrapolatedAllSetSpeciesFrequency, Float> EXTRAPOLATED_ALL_SET_SPECIES_FREQUENCY_BY_COUNT = new Function<ExtrapolatedAllSetSpeciesFrequency, Float>() {
        @Override
        public Float apply(ExtrapolatedAllSetSpeciesFrequency input) {
            return input.getNumber();
        }
    };

    public static final Function<WeightCategoryTreatmentAware, WeightCategoryTreatment> BY_WEIGHT_CATEGORY_TREATMENT = new Function<WeightCategoryTreatmentAware, WeightCategoryTreatment>() {
        @Override
        public WeightCategoryTreatment apply(WeightCategoryTreatmentAware input) {
            return input.getWeightCategoryTreatment();
        }
    };

    public static final Function<WeightCategorySampleAware, WeightCategorySample> BY_WEIGHT_CATEGORY_SAMPLE = new Function<WeightCategorySampleAware, WeightCategorySample>() {
        @Override
        public WeightCategorySample apply(WeightCategorySampleAware input) {
            return input.getWeightCategorySample();
        }
    };

    public static final Function<CorrectedElementaryCatch, Float> CORRECTED_ELEMENTARY_CATCH_TO_CATCH_WEIGHT = new Function<CorrectedElementaryCatch, Float>() {
        @Override
        public Float apply(CorrectedElementaryCatch input) {
            return input.getCatchWeight();
        }
    };

    public static final Function<CorrectedElementaryCatch, Float> CORRECTED_ELEMENTARY_CATCH_TO_CORRECTED_CATCH_WEIGHT = new Function<CorrectedElementaryCatch, Float>() {
        @Override
        public Float apply(CorrectedElementaryCatch input) {
            return input.getCorrectedCatchWeight();
        }
    };

    public static final Function<SetSpeciesCatWeight, Float> SET_SPECIES_CAT_WEIGHT_TO_WEIGHT = new Function<SetSpeciesCatWeight, Float>() {
        @Override
        public Float apply(SetSpeciesCatWeight input) {
            return input.getWeight();
        }
    };

    public static final Function<StandardiseSampleSpeciesFrequency, Float> STANDARDISE_SAMPLE_SPECIES_FREQUENCY_NUMBER = new Function<StandardiseSampleSpeciesFrequency, Float>() {
        @Override
        public Float apply(StandardiseSampleSpeciesFrequency input) {
            return input.getNumber();
        }
    };
}
