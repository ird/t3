/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.models.LengthCompositionAggregateModel;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.util.decorator.Decorator;

import java.util.Map;

/**
 * Some usefull suppliers.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class T3Suppliers {

    public static final Supplier<Float> FLOAT_DEFAULT_VALUE = new Supplier<Float>() {
        @Override
        public Float get() {
            return 0.0f;
        }
    };

    public static final Supplier<CountAndWeight> COUNT_AND_WEIGHT_DEFAULT_VALUE = new Supplier<CountAndWeight>() {
        @Override
        public CountAndWeight get() {
            return new CountAndWeight();
        }
    };

    public static final Supplier<Integer> INTEGER_DEFAULT_VALUE = new Supplier<Integer>() {
        @Override
        public Integer get() {
            return 0;
        }
    };

    public static final Supplier<Map<Species, Float>> MAP_SPECIES_FLOAT_SUPPLIER = new Supplier<Map<Species, Float>>() {
        @Override
        public Map<Species, Float> get() {
            return Maps.newHashMap();
        }
    };

    public static final Supplier<LengthCompositionAggregateModel> AGGREGATE_LENGTH_COMPOSITION_MODEL_SUPPLIER = new Supplier<LengthCompositionAggregateModel>() {
        @Override
        public LengthCompositionAggregateModel get() {
            return new LengthCompositionAggregateModel();
        }
    };

    public static Supplier<String> newActivityDecorateSupplier(final T3ServiceContext serviceContext,
                                                               final Activity a,
                                                               final String text) {
        return new Supplier<String>() {
            @Override
            public String get() {
                DecoratorService decoratorService =
                        serviceContext.newService(DecoratorService.class);

                Decorator<Trip> tripDecorator = decoratorService.getDecorator(serviceContext.getLocale(), Trip.class, DecoratorService.WITH_ID);
                Decorator<Activity> activityDecorator = decoratorService.getDecorator(serviceContext.getLocale(), Activity.class, DecoratorService.WITH_ID);
                String tStr = tripDecorator.toString(a.getTrip());
                String aStr = activityDecorator.toString(a);
                return String.format(text, tStr, aStr);
            }
        };
    }
}
