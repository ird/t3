/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * To define a type of zone usable in level 2 and 3.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface ZoneStratumAwareMeta extends Serializable, Idable {

    /**
     * Obtain all zones for the given {@code schoolType}, {@code ocean} and
     * {@code zoneVersion}.
     *
     * @param ocean       the filtered ocean
     * @param schoolTypes the filtered school types
     * @param zoneVersion the filtered zone version
     * @param tx          the database transaction to load data
     * @return the universe of zones given each school type
     * @throws TopiaException if any problem while loading data form database
     */
    Multimap<SchoolType, ZoneStratumAware> getZones(Ocean ocean,
                                                    Set<SchoolType> schoolTypes,
                                                    ZoneVersion zoneVersion,
                                                    TopiaContext tx) throws TopiaException;

    /**
     * Obtains the {@link ZoneVersion} given his {@code zoneVersionId}.
     *
     * @param zoneVersionId the id of the zone version.
     * @param tx            the database transaction to load data
     * @return the zone version
     * @throws TopiaException if any problem while loading data form database
     */
    ZoneVersion getZoneVersion(String zoneVersionId,
                               TopiaContext tx) throws TopiaException;

    /**
     * Obtains all zone versions for this type of zone.
     *
     * @param tx the database transaction to load data
     * @return all zone versions found
     * @throws TopiaException if any problem while loading data form database
     */
    List<ZoneVersion> getAllZoneVersions(TopiaContext tx) throws TopiaException;

    /**
     * Obtains the type of zone, this class reflects the persistent entity.
     *
     * @return the type of the zone
     */
    Class<? extends ZoneStratumAware> getType();

    /**
     * Obtains the name of persistent table.
     *
     * @return the name of the persistent table
     */
    String getTableName();

    /**
     * Obtains the i18n libelle key for this type of zone.
     *
     * @return the i18n libelle key of this type of zone
     */
    String getI18nKey();
}
