package fr.ird.t3.entities.data;

/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.reference.Species;

import java.util.Collection;
import java.util.Set;

public class SampleSetSpeciesFrequencyDAOImpl<E extends SampleSetSpeciesFrequency> extends SampleSetSpeciesFrequencyDAOAbstract<E> {

    public static <E extends SampleSetSpeciesFrequency> void collectLengthClasses(Multimap<Species, E> values,
                                                                                  Multimap<Species, Integer> lengthClasses) {
        for (Species species : values.keySet()) {

            Collection<E> frequencies = values.get(species);

            Set<Integer> classes = Sets.newHashSet();
            for (E frequency : frequencies) {
                classes.add(frequency.getLfLengthClass());
            }
            lengthClasses.putAll(species, classes);
        }
    }
}