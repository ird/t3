/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSpecies;
import fr.ird.t3.entities.data.SpeciesAware;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class SpeciesDAOImpl<E extends Species> extends SpeciesDAOAbstract<E> {

    /**
     * Obtains all countries used as fleet for any catches in the database.
     * <p/>
     * In fact we search on each boat from any trip having at least one catch
     *
     * @return the set of fleet countries used by any catches in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllSpeciesUsedInCatch() throws TopiaException {

//        TopiaQuery query = createQuery("s")
//                .addFrom(CorrectedElementaryCatch.class, "c")
//                .addDistinct()
//                .addWhere("c." + CorrectedElementaryCatch.PROPERTY_SPECIES + " = s.id");

        String hql = "SELECT DISTINCT(s) FROM SpeciesImpl s, CorrectedElementaryCatchImpl c WHERE c.species = s.id";

        return T3EntityHelper.querytoSet(hql, this);
    }

    public static <E extends SpeciesAware> Multimap<Species, E> groupBySpecies(Collection<E> specieAwares) {

        Multimap<Species, E> result;

        if (CollectionUtils.isEmpty(specieAwares)) {
            result = ArrayListMultimap.create();
        } else {
            result = Multimaps.index(specieAwares,
                                     T3Functions.SPECIES_AWARE_BY_SPECIES);
        }
        return result;
    }

    public static <S extends SpeciesAware> void retainSpecies(
            Collection<S> setSpecieFrequencies,
            Collection<String> specieIds) {
        Iterator<S> itr = setSpecieFrequencies.iterator();
        while (itr.hasNext()) {
            SpeciesAware speciesAware = itr.next();
            Species species = speciesAware.getSpecies();
            if (!specieIds.contains(species.getTopiaId())) {
                itr.remove();
            }
        }
    }

    public static Set<Species> getAllSpecies(Collection<? extends SpeciesAware> speciesAwares) {
        Set<Species> result = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(speciesAwares)) {
            for (SpeciesAware elementaryCatch : speciesAwares) {
                result.add(elementaryCatch.getSpecies());
            }
        }
        return result;
    }

    public static Set<Species> getAllSpeciesFromSampleSpecies(Collection<Sample> samples) {

        Set<Species> result = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(samples)) {

            // there is some samples

            for (Sample sample : samples) {

                if (!sample.isSampleSpeciesEmpty()) {

                    // there is some sample result

                    for (SampleSpecies sampleSpecies :
                            sample.getSampleSpecies()) {
                        result.add(sampleSpecies.getSpecies());
                    }
                }
            }
        }
        return result;
    }
}
