/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.user;


import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.List;

public class T3UserDAOImpl<E extends T3User> extends T3UserDAOAbstract<E> {

    public static <E extends T3User> List<T3UserDTO> toDTO(Collection<E> t3Users) {
        List<T3UserDTO> users = Lists.newArrayList();

        if (CollectionUtils.isNotEmpty(t3Users)) {

            for (E t3User : t3Users) {
                users.add(t3User.toDTO());
            }
        }
        return users;
    }
}
