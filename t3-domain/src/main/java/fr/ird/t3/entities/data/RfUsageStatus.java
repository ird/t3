package fr.ird.t3.entities.data;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Rf±10 usage status in {@link SampleWell}.
 *
 * Created on 01/02/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 * @see SampleWell#getRfMinus10UsageStatus()
 * @see SampleWell#getRfPlus10UsageStatus() ()
 */
public enum RfUsageStatus {
    ACCEPTED,
    REJECTED_BY_CONFIGURATION,
    REJECTED_RF_NOT_DEFINED,
    REJECTED_RF_IS_ZERO,
    REJECTED_RF_TOO_HIGH,
    REJECTED_NOT_ENOUGH_SAMPLE_COUNT
}
