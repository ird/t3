/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;

public class ZoneDAOImpl<E extends Zone> extends ZoneDAOAbstract<E> {

    public List<ZoneVersion> findAllVersion() throws TopiaException {
        String hql = "SELECT distinct(versionId), versionLibelle, versionStartDate, versionEndDate " +
                     "FROM " + getTopiaEntityEnum().getImplementationFQN();
//        TopiaQuery query = new TopiaQuery();
//        query.setFrom(getEntityClass());
//        query.setSelect("distinct(versionId), versionLibelle, versionStartDate, versionEndDate");
        List<Object[]> queryList = findAllByQuery(Object[].class, hql);
        List<ZoneVersion> result = Lists.newArrayList();
        for (Object[] o : queryList) {
            ZoneVersion z = new ZoneVersionImpl(
                    (String) o[0],
                    (String) o[1],
                    (Date) o[2],
                    (Date) o[3]
            );
            result.add(z);
        }
        return result;
    }

    public ZoneVersion findVersionByVersionId(String versionId) throws TopiaException {
//        TopiaQuery query = new TopiaQuery();
//        query.setFrom(getEntityClass());
//        query.setSelect("distinct(versionId), versionLibelle, versionStartDate, versionEndDate");
//        query.addEquals(Zone.PROPERTY_VERSION_ID, versionId);
        String hql = "SELECT distinct(versionId), versionLibelle, versionStartDate, versionEndDate" +
                     " FROM " + getTopiaEntityEnum().getImplementationFQN() +
                     " WHERE versionId = :versionId";
//
        List<Object[]> queryList = findAllByQuery(Object[].class, hql, "versionId", versionId);
        ZoneVersion result = null;
        if (CollectionUtils.isNotEmpty(queryList)) {
            Object[] l = queryList.get(0);
            result = new ZoneVersionImpl(
                    (String) l[0],
                    (String) l[1],
                    (Date) l[2],
                    (Date) l[3]
            );
        }
        return result;
    }
}
