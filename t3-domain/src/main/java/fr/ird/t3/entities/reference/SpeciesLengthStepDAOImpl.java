/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.entities.T3Functions;
import org.nuiton.topia.TopiaException;

import java.util.List;

public class SpeciesLengthStepDAOImpl<E extends SpeciesLengthStep> extends SpeciesLengthStepDAOAbstract<E> {

    public Multimap<Integer, E> findAllByOceanAndSpeciesGroupByLd1Class(Ocean ocean,
                                                                        Species species) throws TopiaException {
        List<E> proportions = findAllByProperties(
                SpeciesLengthStep.PROPERTY_SPECIES, species,
                SpeciesLengthStep.PROPERTY_OCEAN, ocean
        );
        Multimap<Integer, E> result = Multimaps.index(
                proportions, T3Functions.SPECIES_LENGTH_STEP_BY_LD1_CLASS
        );
        return result;
    }
}
