/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import com.google.common.collect.Sets;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryImpl;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourImpl;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselImpl;

import java.util.Collection;
import java.util.Set;

import static org.nuiton.i18n.I18n.n_;

/**
 * Default implmentation of {@link Trip}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class TripImpl extends TripAbstract {

    private static final long serialVersionUID = 1L;

    static {
        n_("t3.common.elementaryCatchTotalWeight");
        n_("t3.common.elementaryLandingTotalWeight");
        n_("t3.common.sampleTotalLD1Number");
        n_("t3.common.sampleTotalLFNumber");
        n_("t3.common.sampleTotalNumber");
    }

    private transient Boolean level2Computed;

    private transient Boolean level3Computed;

    @Override
    public boolean isSomeDataComputed() {
        return !isNoDataComputed() && !isAllDataComputed();
    }

    @Override
    public boolean isNoDataComputed() {
        return !isRf1Computed();
    }

    @Override
    public boolean isAllDataComputed() {
        return isLevel2Computed() && isLevel3Computed();
    }

    public boolean isLevel2Computed() {
        if (level2Computed == null) {
            level2Computed = false;
            if (!isActivityEmpty()) {
                for (Activity a : getActivity()) {
                    Boolean b = a.getUseMeanStratumCompositionN2();
                    if (b != null) {
                        level2Computed = true;
                        break;
                    }
                }
            }

        }
        return level2Computed;
    }

    public boolean isLevel3Computed() {
        if (level3Computed == null) {
            level3Computed = false;
            if (!isActivityEmpty()) {
                for (Activity a : getActivity()) {
                    Boolean b = a.getUseMeanStratumCompositionN3();
                    if (b != null) {
                        level3Computed = true;
                        break;
                    }
                }
            }
        }
        return level3Computed;
    }

    @Override
    public String getVesselLabel() {
        String result;
        if (vessel == null) {

            // no vessel
            result = "No vessel";
        } else {
            result = vessel.getCode() + " - " + vessel.getLibelle();
        }
        return result;
    }

    /** @return the sum of all activities set duration in hours. */
    @Override
    public float getTotalSetsDuration() {
        float result = 0;
        if (!isActivityEmpty()) {
            for (Activity activity : getActivity()) {

                // get the setDuration
                Float setTime = activity.getSetDuration();

                result += setTime == null ? 0f : setTime;
            }
        }
        return result / 60;
    }

    @Override
    public float getElementaryCatchTotalWeight(Collection<Species> species) {
        float result = 0;
        if (!isActivityEmpty()) {
            for (Activity activity : getActivity()) {
                result += activity.getElementaryCatchTotalWeight(species);
            }
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeightRf1(Collection<Species> species) {
        float result = 0;
        if (getRf1() != null && !isActivityEmpty()) {
            for (Activity activity : getActivity()) {

                if (activity.isElementaryCatchEmpty()) {

                    // no catches...
                    continue;
                }

                for (ElementaryCatch catchN0 : activity.getElementaryCatch()) {
                    if (species.contains(catchN0.getWeightCategoryLogBook().getSpecies())) {
                        result += catchN0.getCatchWeightRf1();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeightRf2(Collection<Species> species) {
        float result = 0;
        if (getRf1() != null && !isActivityEmpty()) {
            for (Activity activity : getActivity()) {

                if (activity.isElementaryCatchEmpty()) {

                    // no catches...
                    continue;
                }

                for (ElementaryCatch catchN0 : activity.getElementaryCatch()) {
                    if (species.contains(catchN0.getWeightCategoryLogBook().getSpecies())) {
                        result += catchN0.getCatchWeightRf2();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public float getElementaryLandingTotalWeight(Collection<Species> species) {
        float result = 0;
        if (!isElementaryLandingEmpty()) {
            for (ElementaryLanding elementaryLanding : getElementaryLanding()) {
                if (species.contains(elementaryLanding.getWeightCategoryLanding().getSpecies())) {
                    result += elementaryLanding.getWeight();
                }
            }
        }
        return result;
    }


    @Override
    public Set<Species> getElementaryCatchSpecies() {
        Set<Species> result = Sets.newHashSet();
        if (!isActivityEmpty()) {
            for (Activity activity : getActivity()) {
                if (!activity.isElementaryCatchEmpty()) {
                    for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                        result.add(aCatch.getWeightCategoryLogBook().getSpecies());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Set<Species> getElementaryLandingSpecies() {
        Set<Species> result = Sets.newHashSet();
        if (!isElementaryLandingEmpty()) {
            for (ElementaryLanding landing : getElementaryLanding()) {
                result.add(landing.getWeightCategoryLanding().getSpecies());
            }
        }
        return result;
    }

    @Override
    public Set<Ocean> getAllOceans() {
        Set<Ocean> result = Sets.newHashSet();
        if (!isActivityEmpty()) {
            for (Activity activity : getActivity()) {
                result.add(activity.getOcean());
            }
        }
        return result;
    }

    @Override
    public void applyRf1(Float rf1, Collection<Species> species) {

        setRf1(rf1);

        if (isActivityEmpty()) {

            // no activity, so nothing to do!
            return;
        }

        if (rf1 == null) {

            // let's apply a null for all catches

            for (Activity activity : getActivity()) {
                if (!activity.isElementaryCatchEmpty()) {

                    for (ElementaryCatch aCatch :
                            activity.getElementaryCatch()) {
                        aCatch.setCatchWeightRf1(null);
                    }
                }
            }

            return;
        }

        if (rf1 == 1.0f) {

            // special case, push back the original value for every catch
            // withou any filter on species

            for (Activity activity : getActivity()) {
                if (!activity.isElementaryCatchEmpty()) {

                    for (ElementaryCatch aCatch :
                            activity.getElementaryCatch()) {
                        float value = aCatch.getCatchWeight();
                        aCatch.setCatchWeightRf1(value);
                    }
                }
            }
            return;
        }

        // complete case where we distinguish species
        // for the one given, use the rf1 gven, for the others use a rf1=1

        for (Activity activity : getActivity()) {
            if (!activity.isElementaryCatchEmpty()) {

                for (ElementaryCatch aCatch :
                        activity.getElementaryCatch()) {
                    float value = aCatch.getCatchWeight();
                    if (species.contains(aCatch.getWeightCategoryLogBook().getSpecies())) {
                        value *= rf1;
                    }
                    aCatch.setCatchWeightRf1(value);
                }
            }
        }
    }

    @Override
    public void applyRf2(Float rf2) {
        setRf2(rf2);
        boolean withRF1 = getRf1() != null;
        if (!isActivityEmpty()) {

            for (Activity activity : getActivity()) {
                if (!activity.isElementaryCatchEmpty()) {

                    for (ElementaryCatch aCatch :
                            activity.getElementaryCatch()) {

                        Float catchWeight = withRF1 ?
                                            aCatch.getCatchWeightRf1() :
                                            aCatch.getCatchWeight();

                        float value = catchWeight * rf2;
                        aCatch.setCatchWeightRf2(value);
                    }
                }
            }
        }
    }

    @Override
    public long getSampleTotalLD1Number() {
        long result = 0;
        if (!isSampleEmpty()) {
            for (Sample aSample : getSample()) {
                if (!aSample.isSampleSpeciesEmpty()) {
                    for (SampleSpecies sampleSpecies : aSample.getSampleSpecies()) {
                        if (sampleSpecies.isLd1LengthClass()) {
                            result += sampleSpecies.getTotalSampleSpeciesFrequencyNumber();
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public long getSampleTotalLFNumber() {
        long result = 0;
        if (!isSampleEmpty()) {
            for (Sample aSample : getSample()) {
                if (!aSample.isSampleSpeciesEmpty()) {
                    for (SampleSpecies sampleSpecies : aSample.getSampleSpecies()) {
                        if (!sampleSpecies.isLd1LengthClass()) {
                            result += sampleSpecies.getTotalSampleSpeciesFrequencyNumber();
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public TripDTO toDTO() {
        TripDTO dto = new TripDTOImpl();
        dto.setId(getTopiaId());
        dto.setDepartureDate(getDepartureDate());
        Harbour h;
        h = new HarbourImpl();
        h.setTopiaId(getDepartureHarbour().getTopiaId());
        h.setCode(getDepartureHarbour().getCode());
        h.setLibelle(getDepartureHarbour().getLibelle());
        dto.setDepartureHarbour(h);

        dto.setLandingDate(getLandingDate());
        h = new HarbourImpl();
        h.setTopiaId(getLandingHarbour().getTopiaId());
        h.setCode(getLandingHarbour().getCode());
        h.setLibelle(getLandingHarbour().getLibelle());
        dto.setLandingHarbour(h);

        Vessel v = new VesselImpl();
        v.setTopiaId(getVessel().getTopiaId());
        v.setCode(getVessel().getCode());
        v.setLibelle(getVessel().getLibelle());

        Country fleet = new CountryImpl();
        v.setFleetCountry(fleet);
        fleet.setTopiaId(getVessel().getFleetCountry().getTopiaId());
        fleet.setCode(getVessel().getFleetCountry().getCode());
        fleet.setLibelle(getVessel().getFleetCountry().getLibelle());

        Country flag = new CountryImpl();
        v.setFlagCountry(flag);
        flag.setTopiaId(getVessel().getFlagCountry().getTopiaId());
        flag.setCode(getVessel().getFlagCountry().getCode());
        flag.setLibelle(getVessel().getFlagCountry().getLibelle());

        dto.setVessel(v);
        return dto;
    }

    @Override
    public void deleteComputedData() {
        ComputedDataHelper.deleteComputedDatas(this);
    }

    @Override
    public void deleteComputedDataLevel0() {

        // level 0 flags
        setRf1Computed(false);
        setRf2Computed(false);
        setCatchesWeightCategorieConverted(false);
        setSetDurationAndPositiveCountComputed(false);
        setEffortComputed(false);
        setWellPlanWeightCategoriesComputed(false);

        // level 0 data
        setCompletionStatus(null);
        setRf1(null);
        setRf2(null);
        setComputedFishingTimeN0(null);
        setComputedSearchTimeN0(null);
        setComputedTimeAtSeaN0(null);

        ComputedDataHelper.deleteComputedDataLevel0(getActivity());
        ComputedDataHelper.deleteComputedDataLevel0(getSample());
        ComputedDataHelper.deleteComputedDataLevel0(getWell());
    }

    @Override
    public void deleteComputedDataLevel1() {

        // level 1 flags
        setExtrapolateSampleCountedAndMeasured(false);
        setStandardizeSampleMeasures(false);
        setComputeWeightOfCategoriesForSet(false);
        setRedistributeSampleNumberToSet(false);
        setExtrapolateSampleWeightToSet(false);
        setConvertSetSpeciesFrequencyToWeight(false);
        setConvertSampleSetSpeciesFrequencyToWeight(false);

        ComputedDataHelper.deleteComputedDataLevel1(getActivity());
        ComputedDataHelper.deleteComputedDataLevel1(getSample());
        ComputedDataHelper.deleteComputedDataLevel1(getWell());
    }

    @Override
    public void deleteComputedDataLevel2() {
        // level 2 flags

        ComputedDataHelper.deleteComputedDataLevel2(getActivity());
        ComputedDataHelper.deleteComputedDataLevel2(getSample());
        ComputedDataHelper.deleteComputedDataLevel2(getWell());
    }

    @Override
    public void deleteComputedDataLevel3() {
        // level 3 flags

        ComputedDataHelper.deleteComputedDataLevel3(getActivity());
        ComputedDataHelper.deleteComputedDataLevel3(getSample());
        ComputedDataHelper.deleteComputedDataLevel3(getWell());
    }

}
