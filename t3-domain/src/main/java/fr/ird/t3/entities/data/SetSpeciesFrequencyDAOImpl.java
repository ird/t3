/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.reference.Species;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SetSpeciesFrequencyDAOImpl<E extends SetSpeciesFrequency> extends SetSpeciesFrequencyDAOAbstract<E> {

    public Map<Integer, E> findAllByActivityAndSpeciesOrderdyByLengthClass(Activity activity, Species species) throws TopiaException {
        Collection<E> setSpeciesFrequency =
                (Collection<E>) activity.getSetSpeciesFrequency();
        Iterable<E> forSpecies = Iterables.filter(
                setSpeciesFrequency,
                T3Predicates.newSpeciesFilter(species));

        Map<Integer, E> result = Maps.newTreeMap();

        result.putAll(Maps.uniqueIndex(
                forSpecies,
                T3Functions.SET_SPECIES_FREQUENCY_BY_LF_LENGTH_CLASS));
        return result;
    }

    public static <E extends SetSpeciesFrequency> void collectLengthClasses(Multimap<Species, E> values,
                                                                            Multimap<Species, Integer> lengthClasses) {
        for (Species species : values.keySet()) {

            Collection<E> frequencies = values.get(species);

            Set<Integer> classes = Sets.newHashSet();
            for (E frequency : frequencies) {
                classes.add(frequency.getLfLengthClass());
            }
            lengthClasses.putAll(species, classes);
        }
    }

    public static Map<Integer, Float> collectSampleCount(Collection<SetSpeciesFrequency> newDatas) {

        Map<Integer, Float> result = Maps.newHashMap();

        for (SetSpeciesFrequency newData : newDatas) {

            Integer lengthClass = newData.getLfLengthClass();

            Float newCount = result.get(lengthClass);
            if (newCount == null) {
                newCount = 0f;
            }
            newCount += newData.getNumber();

            result.put(lengthClass, newCount);
        }
        return result;
    }

    public static float collectSimpleSampleCount(Collection<SetSpeciesFrequency> newDatas) {

        float result = 0f;

        for (SetSpeciesFrequency newData : newDatas) {

            Integer lengthClass = newData.getLfLengthClass();

            result += newData.getNumber();
        }
        return result;
    }
}
