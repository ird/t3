/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.type;


public class T3PointImpl extends T3Point {

    private static final long serialVersionUID = 1L;

    public T3PointImpl() {
    }

    public T3PointImpl(float x, float y) {
        setX(x);
        setY(y);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof T3Point)) {
            return false;
        }

        T3Point t3Point = (T3Point) o;

        if (Float.compare(t3Point.x, x) != 0) {
            return false;
        }
        return Float.compare(t3Point.y, y) == 0;

    }

    @Override
    public int hashCode() {
        int result = x != +0.0f ? Float.floatToIntBits(x) : 0;
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + " <x:" + x + ", y:" + y + ">";
    }
}
