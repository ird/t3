package fr.ird.t3.entities.reference;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created on 25/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class WeightCategories {

    public static boolean isMinus10Category(WeightCategorySample weightCategorySample) {
        Integer min = weightCategorySample.getMin();
        Integer max = weightCategorySample.getMax();
        return min != null && min == 0 && max != null && max == 10;
    }

    public static boolean isPlus10Category(WeightCategorySample weightCategorySample) {
        Integer min = weightCategorySample.getMin();
        Integer max = weightCategorySample.getMax();
        return min != null && min == 10 && max == null;
    }

    public static <E extends WeightCategory> void sort(List<E> weightCategoryTreatment) {
        Collections.sort(weightCategoryTreatment, newComparator());
    }

    public static Comparator<WeightCategory> newComparator() {
        return new WeightCategoryComparator();
    }

    public static class WeightCategoryComparator implements Comparator<WeightCategory>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(WeightCategory o1, WeightCategory o2) {

            if (o1.getMin() == null && o1.getMax() == null) {

                // equals to o2 if only if o2.min = o2.max = null

                if (o2.getMin() == null && o2.getMax() == null) {
                    return 0;
                }
                return 1;
            }

            if (o2.getMin() == null && o2.getMax() == null) {

                // equals to o1 if only if o1.min = o1.max = null

                if (o1.getMin() == null && o1.getMax() == null) {
                    return 0;
                }
                return -1;
            }

            if (o1.getMin() == null) {
                return o2.getMin() == null ? 0 : -1;
            }
            if (o2.getMin() == null) {
                return 1;
            }
            return o1.getMin().compareTo(o2.getMin());
        }

        private WeightCategoryComparator() {
        }
    }
}
