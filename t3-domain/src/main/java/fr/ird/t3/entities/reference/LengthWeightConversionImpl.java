/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.LengthWeightConversionHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LengthWeightConversionImpl extends LengthWeightConversionAbstract {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(LengthWeightConversionImpl.class);

    private static final long serialVersionUID = 1L;


    private static final Pattern COEFFICIENTS_PATTERN =
            Pattern.compile("(.+)=(.+)");

    /** variable poids à utiliser dans la relation taille */
    public static final String VARIABLE_POIDS = "P";

    /** variable taille à utiliser dans la relation poids */
    public static final String VARIABLE_TAILLE = "L";

    @Override
    public int getCode() {
        // not used
        return 0;
    }

    @Override
    public String getLibelle() {
        // not used
        return null;
    }

    @Override
    public float computeWeight(float longueur) {
        return LengthWeightConversionHelper.computeWeight(this, longueur);
    }

    @Override
    public float computeWeightFromLFLengthClass(int lengthClass) {
        Integer lfLengthClassStep = species.getLfLengthClassStep();
        float longueur = lengthClass + lfLengthClassStep / 2f;
        return LengthWeightConversionHelper.computeWeight(this, longueur);
    }

    @Override
    public float computeLength(float poids) {
        return LengthWeightConversionHelper.computeLength(this, poids);
    }

    @Override
    public int computeLFLengthClassFromWeight(float poids) {
        float length = computeLength(poids);
        int lengthClass = (int) length;
        Integer lfLengthClassStep = species.getLfLengthClassStep();
        lengthClass = lengthClass - lengthClass % lfLengthClassStep;
        return lengthClass;
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {

        Map<String, Double> result = Maps.newTreeMap();
        String coefficients = getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher =
                        COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                if (log.isDebugEnabled()) {
                    log.debug("constant to test = " + coefficientDef);
                }
                if (matcher.matches()) {

                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        if (log.isDebugEnabled()) {
                            log.debug("detets coefficient " + key + '=' +
                                      val);
                        }
                    } catch (NumberFormatException e) {
                        // pas pu recupere le nombre...
                        if (log.isWarnEnabled()) {
                            log.warn("could not parse double " + val +
                                     " for coefficient " + key);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * For a given species, ocean, sexe and date, get the first length class
     * which corresponds to a weight of 10Kg or more.
     *
     * @return the first length class which when converts to weight excees 10Kg
     */
    @Override
    public int getSpeciePlus10KGLengthClass() {

        return getSpecieHighestLengthClass(10);
//        int lenghtClassStep = species.getLfLengthClassStep();
//
//        int lenghtClass = 0;
//
//        float weight = 0;
//
//        while (weight < 10) {
//
//            // convert the length into weight
//            weight = computeWeightFromLFLengthClass(lenghtClass);
//
//            if (weight < 10) {
//
//                // can try next length class
//                lenghtClass += lenghtClassStep;
//            }
//        }
//        return lenghtClass;
    }

    @Override
    public int getSpecieHighestLengthClass(Integer maxWeight) {

        int result;

        if (maxWeight == null) {

            // no max bound, can not compute it
            result = Integer.MAX_VALUE;
        } else {
            int lenghtClassStep = species.getLfLengthClassStep();

            result = 0;

            float weight = 0;

            while (weight < maxWeight) {

                // convert the length into weight
                weight = computeWeightFromLFLengthClass(result);

                if (weight < maxWeight) {

                    // can try next length class
                    result += lenghtClassStep;
                }
            }
        }

        return result;
    }


}
