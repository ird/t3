/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Deal a cache of {@link LengthWeightConversion}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthWeightConversionHelper {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(LengthWeightConversionHelper.class);

    protected final LengthWeightConversionDAO dao;

    protected final Map<String, LengthWeightConversion> lengthWeightConversions;

    protected final Map<String, Integer> lengthClassMap;

    private final ConversionContext conversionContext;

    protected <W extends WeightCategory> W getNextCategory(LengthWeightConversion conversion, int lengthClass, Iterator<W> itr) throws TopiaException {
        W result = null;
        if (itr.hasNext()) {

            result = itr.next();

            if (result.getOldCategoryCode() == 9) {

                // does not accept undefined categories
                result = getNextCategory(conversion, lengthClass, itr);
            } else {

                // get max lengthClass
                int nextLengthClass = getSpecieHighestLengthClass(conversion, result);

                if (nextLengthClass < lengthClass) {

                    // try with next category, this one is too small
                    result = getNextCategory(conversion, lengthClass, itr);
                }
            }
        }
        return result;
    }

    public <W extends WeightCategory> Map<Integer, W> getWeightCategoriesDistribution(
            LengthWeightConversion conversion,
            List<W> weightCategories,
            List<Integer> lengthClasses) throws TopiaException {

        Map<Integer, W> result = Maps.newTreeMap();

        W currentWeightCatetory = null;
        int currentMaxLengthClass = -1;

        Iterator<W> itr = weightCategories.iterator();
        for (Integer lengthClass : lengthClasses) {

            if (lengthClass > currentMaxLengthClass) {

                // need another weightCategory
                currentWeightCatetory = null;
            }

            if (currentWeightCatetory == null) {

                // get the correct weight category
                currentWeightCatetory = getNextCategory(conversion, lengthClass, itr);

                currentMaxLengthClass = getSpecieHighestLengthClass(conversion,
                                                                    currentWeightCatetory);
            }
            result.put(lengthClass, currentWeightCatetory);
        }
        return result;
    }

    private static class ConversionContext {

        private final Ocean ocean;

        private final int sex;

        private final Date date;

        private final String key;

        private ConversionContext(Ocean ocean, int sex, Date date) {
            this.ocean = ocean;
            this.sex = sex;
            this.date = date;
            this.key = "-" + ocean.getCode() + "-" + sex + "-" +
                       T3Date.newDate(date);
        }

        public Ocean getOcean() {
            return ocean;
        }

        public int getSex() {
            return sex;
        }

        public Date getDate() {
            return date;
        }

        public String getKey() {
            return key;
        }
    }

    LengthWeightConversionHelper(LengthWeightConversionDAO dao,
                                 Ocean ocean, int sex, Date date) {
        this.dao = dao;
        this.conversionContext = new ConversionContext(ocean, sex, date);
        lengthWeightConversions = Maps.newTreeMap();
        lengthClassMap = Maps.newTreeMap();
    }

    LengthWeightConversionHelper(LengthWeightConversionDAO dao) {
        this.dao = dao;
        this.conversionContext = null;
        lengthWeightConversions = Maps.newTreeMap();
        lengthClassMap = Maps.newTreeMap();
    }

    /**
     * Get the convertor for the given parameters from the cache.
     * <p/>
     * If not found, then load it from db.
     *
     * @param species species to use
     * @param ocean   ocean to use
     * @param sex     sex to use
     * @param date    min date to use
     * @return convertor found
     * @throws TopiaException if any db while querying db
     */
    public LengthWeightConversion getConversions(Species species,
                                                 Ocean ocean,
                                                 int sex,
                                                 Date date) throws TopiaException {
        LengthWeightConversion result;
        String key = species.getCode() + "-" + ocean.getCode() +
                     "-" + sex + "-" + T3Date.newDate(date);
        result = lengthWeightConversions.get(key);

        if (result == null && !lengthWeightConversions.containsKey(key)) {

            // load it from db

            result = dao.findLengthWeightConversion(
                    species,
                    ocean,
                    0,
                    date
            );

            // store it once for all in cache
            lengthWeightConversions.put(key, result);

            if (log.isInfoEnabled()) {
                log.info("Cache lengthWeightConversion [" +
                         lengthWeightConversions.size() + "] for " + key);
            }
        }
        return result;
    }

    /**
     * Get the convertor for the given parameters from the cache.
     * <p/>
     * If not found, then load it from db.
     *
     * @param species species to use
     * @return convertor found
     * @throws TopiaException if any db while querying db
     */
    public LengthWeightConversion getConversions(Species species) throws TopiaException {

        Preconditions.checkNotNull(conversionContext,
                                   "No conversion context, must specify " +
                                   "all parameters to obtain a convertor.");
        LengthWeightConversion result;
        String key = species.getCode() + conversionContext.getKey();
        result = lengthWeightConversions.get(key);

        if (result == null && !lengthWeightConversions.containsKey(key)) {

            // load it from db

            result = dao.findLengthWeightConversion(
                    species,
                    conversionContext.getOcean(),
                    conversionContext.getSex(),
                    conversionContext.getDate()
            );

            // store it once for all in cache
            lengthWeightConversions.put(key, result);

            if (log.isInfoEnabled()) {
                log.info("Cache lengthWeightConversion [" +
                         lengthWeightConversions.size() + "] for " + key);
            }
        }
        return result;
    }

    public Integer getSpecieHighestLengthClass(Species species,
                                               WeightCategory weightCategory) throws TopiaException {

        LengthWeightConversion conversions = getConversions(species);
        Integer result;
        if (conversions == null) {
            result = null;
        } else {
            result = getSpecieHighestLengthClass(conversions, weightCategory);
        }
        return result;
    }

    public int getSpecieHighestLengthClass(LengthWeightConversion conversion,
                                           WeightCategory weightCategory) throws TopiaException {

        String key = conversion.getTopiaId() + "-" + weightCategory.getTopiaId();
        Integer result = lengthClassMap.get(key);
        if (result == null) {

            // load it once for all
            result = conversion.getSpecieHighestLengthClass(weightCategory.getMax());

            // sotre it once for all
            lengthClassMap.put(key, result);

            if (log.isInfoEnabled()) {
                log.info("Cache lengthClassMap [" +
                         lengthClassMap.size() +
                         "] for " + key);
            }
        }
        return result;
    }

}
