/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategory;
import fr.ird.t3.services.DecoratorService;
import org.nuiton.util.decorator.Decorator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Helper on models of this package.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class WeightCompositionModelHelper {

    public static String decorateModel(DecoratorService decoratorService,
                                       String title,
                                       WeightCompositionAggregateModel model) {

        StringBuilder inResume = new StringBuilder();

        Locale locale = decoratorService.getLocale();

        String speciesLibelle = l_(locale, "t3.common.species");
        String weightLibelle = l_(locale, "t3.common.weight.forAll");

        int speciesColumnLength = speciesLibelle.length();
        int weightColumnLength = 15;
        int rateColumnLength = 15;

        Decorator<Species> decorator =
                decoratorService.getDecorator(locale, Species.class, null);

        for (Species species : model.getTotalModel().getSpecies()) {
            speciesColumnLength = Math.max(speciesColumnLength,
                                           decorator.toString(species).length());
        }
        Set<WeightCategory> weightCategories = model.getWeightCategories();

        int maxLength = speciesColumnLength +
                        weightColumnLength +
                        rateColumnLength + 6;
        int maxLengthOrig = maxLength;
        for (WeightCategory weightCategory : weightCategories) {
            maxLength = Math.max(maxLength,
                                 l_(locale, "t3.common.weightCategory",
                                    decoratorService.decorate(
                                            locale, weightCategory, null)).length());
        }
        if (maxLength > maxLengthOrig) {
            speciesColumnLength += maxLength - maxLengthOrig;
        }
        CompositionTableModel header = new CompositionTableModel();
        String lineFormat = "| %1$-" + speciesColumnLength + "s | %2$-" +
                            weightColumnLength + "s - %3$-" + rateColumnLength
                            + "s |\n";

        header.setLineFormat(lineFormat);
        header.setHeader(
                String.format("| %1$-" + speciesColumnLength + "s | %2$-" +
                              (weightColumnLength + rateColumnLength + 3) + "s |\n",
                              speciesLibelle,
                              weightLibelle));

        header.setCategoryFormat("| %1$-" + (maxLength) + "s |\n");
        header.setSeparatorFormat(Strings.padEnd("|", maxLength + 3, '-') + "|\n");
        header.setTopSeparatorFormat(Strings.padEnd("=", maxLength + 4, '=') + "\n");
        header.setBottomSeparatorFormat(Strings.padEnd("=", maxLength + 4, '=') + "\n");

        inResume.append(title).append('\n');
        inResume.append(header.getTopSeparatorFormat());
        inResume.append(header.getHeader());
        inResume.append(header.getSeparatorFormat());
        for (WeightCategory weightCategory : weightCategories) {
            WeightCompositionModelHelper.decorateModel(
                    header,
                    decoratorService,
                    model,
                    weightCategory,
                    inResume
            );
        }
        WeightCompositionModelHelper.decorateModel(
                header,
                decoratorService,
                model,
                null,
                inResume
        );
        return inResume.toString();
    }

    public static String decorateModel(DecoratorService decoratorService,
                                       String title,
                                       WeightCompositionAggregateModel model,
                                       WeightCompositionAggregateModel subModel) {

        StringBuilder inResume = new StringBuilder();

        Locale locale = decoratorService.getLocale();

        String speciesLibelle = l_(locale, "t3.common.species");
        String weightLibelle = l_(locale, "t3.common.weight.forAll");
        String weightFixLibelle = l_(locale, "t3.common.weight.forFix");

        int speciesColumnLength = speciesLibelle.length();
        int weightColumnLength = 15;
        int rateColumnLength = 15;
        int weightFixColumnLength = 15;
        int rateFixColumnLength = 15;

        Decorator<Species> decorator =
                decoratorService.getDecorator(locale, Species.class, null);

        for (Species species : model.getTotalModel().getSpecies()) {
            speciesColumnLength = Math.max(speciesColumnLength,
                                           decorator.toString(species).length());
        }
        CompositionTableModel header = new CompositionTableModel();
        String lineFormat = "| %1$-" + speciesColumnLength + "s | %2$-" +
                            weightColumnLength + "s - %3$-" + rateColumnLength
                            + "s | %4$-" + weightFixColumnLength + "s - %5$-" +
                            rateFixColumnLength + "s|\n";

        header.setLineFormat(lineFormat);
        header.setHeader(
                String.format("| %1$-" + speciesColumnLength + "s | %2$-" +
                              (weightColumnLength + rateColumnLength + 3) + "s | %3$-" +
                              (weightFixColumnLength + rateFixColumnLength + 3) + "s|\n",
                              speciesLibelle,
                              weightLibelle,
                              weightFixLibelle));

        int categoryLength = speciesColumnLength +
                             weightColumnLength +
                             rateColumnLength +
                             weightFixColumnLength +
                             rateFixColumnLength + 11;

        header.setCategoryFormat("| %1$-" + categoryLength + "s |\n");
        header.setSeparatorFormat(Strings.padEnd("|", categoryLength + 3, '-') + "|\n");
        header.setTopSeparatorFormat(Strings.padEnd("-", categoryLength + 3, '-') + "-\n");
        header.setBottomSeparatorFormat(Strings.padEnd("-", categoryLength + 3, '-') + "-\n");

        inResume.append(title).append('\n');
        inResume.append(header.getTopSeparatorFormat());
        inResume.append(header.getHeader());
        inResume.append(header.getSeparatorFormat());
        for (WeightCategory weightCategory : model.getWeightCategories()) {
            WeightCompositionModelHelper.decorateModel(
                    header,
                    decoratorService,
                    model,
                    subModel,
                    weightCategory,
                    inResume
            );
        }
        WeightCompositionModelHelper.decorateModel(
                header,
                decoratorService,
                model,
                subModel,
                null,
                inResume
        );
        return inResume.toString();
    }

    private static void decorateModel(CompositionTableModel header,
                                      DecoratorService decoratorService,
                                      WeightCompositionAggregateModel model,
                                      WeightCompositionAggregateModel subModel,
                                      WeightCategory weightCategory,
                                      StringBuilder inResume) {

        WeightCompositionModel m;
        WeightCompositionModel subM;


        String categoryLibelle;

        Locale locale = decoratorService.getLocale();

        if (weightCategory == null) {

            m = model.getTotalModel();
            subM = subModel.getTotalModel();
            categoryLibelle = l_(locale, "t3.common.forAllWeightCategories");
        } else {
            m = model.getModel(weightCategory);
            subM = subModel.getModel(weightCategory);

            categoryLibelle =
                    l_(locale, "t3.common.weightCategory",
                       decoratorService.decorate(
                               locale, weightCategory, null));
        }

        if (!m.isEmpty()) {

            Decorator<Species> decorator = decoratorService.getDecorator(
                    locale, Species.class, null);

            inResume.append(String.format(header.getCategoryFormat(), categoryLibelle));
            inResume.append(header.getSeparatorFormat());

            Set<Species> speciesSubSet = subM.getSpecies();

            String lineFormat = header.getLineFormat();

            float rate = 0f;
            float subRate = 0f;

            List<Species> allSpecies = Lists.newArrayList(m.getSpecies());
            Collections.sort(allSpecies, SPECIES_COMPARATOR);


            for (Species species : allSpecies) {

                // show first species from both models

                if (!speciesSubSet.contains(species)) {

                    continue;
                }

                float weightRate1 = m.getWeightRate(species);
                rate += weightRate1;

                // species is in sub model

                float subWeightRate1 = subM.getWeightRate(species);
                subRate += subWeightRate1;

                inResume.append(String.format(
                        lineFormat,
                        decorator.toString(species),
                        m.getWeight(species),
                        (100 * weightRate1) + "%",
                        subM.getWeight(species),
                        (100 * subWeightRate1) + "%"));
            }

            for (Species species : allSpecies) {

                // show others species

                if (speciesSubSet.contains(species)) {

                    continue;
                }

                float weightRate1 = m.getWeightRate(species);
                rate += weightRate1;

                inResume.append(String.format(
                        lineFormat,
                        decorator.toString(species),
                        m.getWeight(species),
                        (100 * weightRate1) + "%", " - ", " - "));
            }
            inResume.append(String.format(lineFormat,
                                          "Total", m.getTotalWeight(),
                                          (100 * rate) + "%",
                                          subM.getTotalWeight(),
                                          (100 * subRate) + "%"));

            if (weightCategory == null) {
                inResume.append(header.getBottomSeparatorFormat());
            } else {
                inResume.append(header.getSeparatorFormat());
            }
        }
    }

    private static void decorateModel(CompositionTableModel header,
                                      DecoratorService decoratorService,
                                      WeightCompositionAggregateModel model,
                                      WeightCategory weightCategory,
                                      StringBuilder inResume) {

        WeightCompositionModel m;


        String categoryLibelle;

        Locale locale = decoratorService.getLocale();

        if (weightCategory == null) {

            m = model.getTotalModel();
            categoryLibelle = l_(locale, "t3.common.forAllWeightCategories");
        } else {
            m = model.getModel(weightCategory);

            categoryLibelle =
                    l_(locale, "t3.common.weightCategory",
                       decoratorService.decorate(
                               locale, weightCategory, null));
        }

        if (!m.isEmpty()) {

            Decorator<Species> decorator = decoratorService.getDecorator(
                    locale, Species.class, null);

            inResume.append(String.format(header.getCategoryFormat(), categoryLibelle));
            inResume.append(header.getSeparatorFormat());

            String lineFormat = header.getLineFormat();

            List<Species> allSpecies = Lists.newArrayList(m.getSpecies());
            Collections.sort(allSpecies, SPECIES_COMPARATOR);


            for (Species species : allSpecies) {

                float weightRate1 = m.getWeightRate(species);

                inResume.append(String.format(
                        lineFormat,
                        decorator.toString(species),
                        m.getWeight(species),
                        (100 * weightRate1) + "%"));
            }

            if (weightCategory == null) {
                inResume.append(header.getBottomSeparatorFormat());
            } else {
                inResume.append(header.getSeparatorFormat());
            }
        }
    }

    public static final Comparator<Species> SPECIES_COMPARATOR = new Comparator<Species>() {
        @Override
        public int compare(Species o1, Species o2) {
            return o1.getCode() - o2.getCode();
        }
    };

    protected WeightCompositionModelHelper() {
    }
}
