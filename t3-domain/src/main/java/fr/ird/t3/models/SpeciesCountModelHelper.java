/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import org.nuiton.util.decorator.Decorator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Helper on {@link SpeciesCountModel} of this package.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class SpeciesCountModelHelper {

    /**
     * Get the nicely formatted table for the givne model.
     *
     * @param decoratorService decorator service used to decorate entites
     * @param title            title of the table
     * @param model            model to render
     * @param modelNames       order of model names to render
     * @return the localized table
     */
    public static String decorateModel(DecoratorService decoratorService,
                                       String title,
                                       SpeciesCountAggregateModel model,
                                       String... modelNames) {

        StringBuilder inResume = new StringBuilder();

        Locale locale = decoratorService.getLocale();

        String speciesLibelle = l_(locale, "t3.common.lengthClass");
        String totalCountForSpeciesLibelle = l_(locale, "t3.common.totalCount.forSpecies");
        String totalCountLibelle = l_(locale, "t3.common.totalCount");

        List<String> modelNamesOrder = Lists.newArrayList();

        Set<String> existingModelNames = model.getAvailableModelNames();
        for (String modelName : modelNames) {
            if (existingModelNames.contains(modelName)) {
                modelNamesOrder.add(modelName);
            }
        }
        List<Species> availableSpecies = Lists.newArrayList(model.getAvailableSpecies());
        Collections.sort(availableSpecies, SPECIES_COMPARATOR);

        int speciesColumnLength = Math.max(speciesLibelle.length(),
                                           totalCountForSpeciesLibelle.length());

        speciesColumnLength = Math.max(speciesColumnLength,
                                       totalCountLibelle.length());

        Decorator<Species> decorator =
                decoratorService.getDecorator(locale, Species.class, null);

        for (Species species : availableSpecies) {
            speciesColumnLength = Math.max(speciesColumnLength,
                                           decorator.toString(species).length());
        }

        String lineFormat = "| %1$-" + speciesColumnLength + "s |";

        int index = 2;
        int totalLength = speciesColumnLength;

        List<String> headerParams = Lists.newArrayList(speciesLibelle);

        for (String modelName : modelNamesOrder) {
            String modelNameTitle = l_(locale, modelName);
            int length = modelNameTitle.length();
            lineFormat += " %" + index + "$-" + length + "s |";
            totalLength += 3 + length;
            index++;
            headerParams.add(modelNameTitle);
        }

        CompositionTableModel header = new CompositionTableModel();
        lineFormat += "\n";

        header.setLineFormat(lineFormat);
        header.setHeader(
                String.format(
                        lineFormat,
                        headerParams.toArray(new String[headerParams.size()])));

        header.setCategoryFormat("| %1$-" + (totalLength) + "s |\n");

        header.setSeparatorFormat(Strings.padEnd("|", totalLength + 3, '-') + "|\n");
        header.setSeparatorFormat2(Strings.padEnd("|", totalLength + 3, '=') + "|\n");
        header.setTopSeparatorFormat(Strings.padEnd("=", totalLength + 4, '=') + "\n");
        header.setBottomSeparatorFormat(Strings.padEnd("=", totalLength + 4, '=') + "\n");

        inResume.append(title).append('\n');
        inResume.append(header.getTopSeparatorFormat());
        inResume.append(header.getHeader());

        for (Species species : availableSpecies) {

            inResume.append(header.getSeparatorFormat2());

            // add species header
            inResume.append(String.format(header.getCategoryFormat(),
                                          decorator.toString(species)));

            SpeciesCountModel speciesCountModel = model.getModel(species);

            List<Integer> lengthClasses =
                    speciesCountModel.getAvailableLengthClasses();

            inResume.append(header.getSeparatorFormat());
            for (Integer lengthClass : lengthClasses) {

                List<String> params = Lists.newArrayList();

                params.add(String.valueOf(lengthClass));

                for (String modelName : modelNames) {
                    Map<Integer, Float> countModel = speciesCountModel.getModel(modelName);
                    Float count = countModel.get(lengthClass);
                    params.add(count == null ? "-" : String.valueOf(count));
                }

//                inResume.append(header.getSeparatorFormat());
                inResume.append(
                        String.format(
                                lineFormat,
                                params.toArray(new String[params.size()])));

            }

            // add a total row
            List<String> params = Lists.newArrayList();

            params.add(totalCountForSpeciesLibelle);

            for (String modelName : modelNames) {
                Float totalCount = speciesCountModel.getTotalCount(modelName);
                params.add(totalCount == null ? "-" : String.valueOf(totalCount));
            }

            inResume.append(header.getSeparatorFormat());
            inResume.append(
                    String.format(
                            lineFormat,
                            params.toArray(new String[params.size()])));


        }

        // total count (for any species)
        List<String> params = Lists.newArrayList();
        params.add(totalCountLibelle);
        for (String modelName : modelNames) {
            Float totalCount = model.getTotalCount(modelName);
            params.add(totalCount == null ? "-" : String.valueOf(totalCount));
        }
        inResume.append(header.getBottomSeparatorFormat());
        inResume.append(
                String.format(
                        lineFormat,
                        params.toArray(new String[params.size()])));

        inResume.append(header.getBottomSeparatorFormat());
        return inResume.toString();
    }

    public static final Comparator<Species> SPECIES_COMPARATOR = new Comparator<Species>() {
        @Override
        public int compare(Species o1, Species o2) {
            return o1.getCode() - o2.getCode();
        }
    };
}
