/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategory;
import org.apache.commons.collections.MapUtils;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Model of a weight composition model.
 * <p/>
 * This contains for some species, weight of it and also
 * his rate amoung all species weights.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class WeightCompositionModel implements Closeable {

    /** The weight cateogry involved. */
    protected final WeightCategory weightCategory;

    /** Weight for each species. */
    protected final Map<Species, Float> weight;

    /** Rate for each species. */
    protected final Map<Species, Float> weightRate;

    /** Total weight for all species. */
    protected float totalWeight;

    public WeightCompositionModel() {
        this(null);
    }

    public WeightCompositionModel(WeightCategory weightCategory) {
        this.weightCategory = weightCategory;
        weight = Maps.newHashMap();
        weightRate = Maps.newHashMap();
    }

    public WeightCompositionModel(WeightCategory weightCategory,
                                  Map<Species, Float> weights) {
        this(weightCategory);
        addWeights(weights);
    }

    public boolean isEmpty() {
        return weight.isEmpty();
    }

    public void addWeights(Map<Species, Float> weights) {

        for (Map.Entry<Species, Float> e : weights.entrySet()) {
            Species species = e.getKey();
            Float speciesWeight = e.getValue();
            if (speciesWeight == 0) {

                // do not add empty species
                continue;
            }
            Float aFloat = weight.get(species);
            if (aFloat == null) {
                aFloat = speciesWeight;

            } else {
                aFloat += speciesWeight;
            }
            weight.put(species, aFloat);
        }

        // rebuild totalWeight
        totalWeight = 0;
        for (Float aFloat : weight.values()) {
            totalWeight += aFloat;
        }

        boolean nullTotalWeight = totalWeight == 0;
        if (nullTotalWeight) {

            // to be sure to have no NaN
            totalWeight = 1;
        }

        // rebuild weightRate
        weightRate.clear();
        for (Map.Entry<Species, Float> e : weight.entrySet()) {
            Species species = e.getKey();
            Float w = e.getValue();
            weightRate.put(species, w / totalWeight);
        }

        if (nullTotalWeight) {

            // push back total weight to 0
            totalWeight = 0;
        }
    }

    public void addWeights(WeightCompositionModel model) {

        addWeights(model.weight);
    }

    public WeightCompositionModel extractForSpecies(Collection<Species> species) {
        Map<Species, Float> newWeights = Maps.newHashMap();
        for (Species specy : species) {
            float weight1;
            if (weight.containsKey(specy)) {
                weight1 = getWeight(specy);
            } else {
                weight1 = 0;
            }
            newWeights.put(specy, weight1);
        }

        WeightCompositionModel result;
        if (MapUtils.isEmpty(newWeights)) {
            result = null;
        } else {
            result = new WeightCompositionModel(weightCategory, newWeights);
        }
        return result;
    }

    public WeightCategory getWeightCategory() {
        return weightCategory;
    }

    public float getWeightRate(Species species) {
        float result = weightRate.get(species);
        return result;
    }

    public float getWeight(Species species) {
        float result = weight.get(species);
        return result;
    }

    public float getTotalWeight() {
        return totalWeight;
    }

    public Set<Species> getSpecies() {
        return weight.keySet();
    }

    @Override
    public void close() throws IOException {
        weight.clear();
        weightRate.clear();
    }
}
