/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.data.SpeciesFrequencyAware;
import fr.ird.t3.entities.reference.Species;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To keep count of fishes for some species.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class SpeciesCountModel implements Closeable {

    /**
     * Species used in that model.
     *
     * @since 1.3.1
     */
    protected final Species species;

    /**
     * All counts for this species per modelName (the key of map)
     * then by length classe.
     *
     * @since 1.3.1
     */
    protected final Map<String, Map<Integer, Float>> count;

    public SpeciesCountModel(Species species) {
        this.species = species;
        count = Maps.newHashMap();
    }

    public Species getSpecies() {
        return species;
    }

    public Set<String> getModelNames() {
        return count.keySet();
    }

    public Map<Integer, Float> getModel(String modelName) {
        Map<Integer, Float> result = count.get(modelName);
        if (result == null) {
            result = Maps.newTreeMap();
            count.put(modelName, result);
        }
        return result;
    }

    public <E extends SpeciesFrequencyAware> void addValues(String modelName,
                                                            Collection<E> data) {

        // get model
        Map<Integer, Float> model = getModel(modelName);

        for (E aData : data) {

            int lfLengthClass = aData.getLfLengthClass();

            Float value = aData.getNumber();

            Float oldValue = model.get(lfLengthClass);
            if (oldValue == null) {
                oldValue = 0f;
            }
            model.put(lfLengthClass, oldValue + value);
        }
    }

    public void addValue(String modelName, Integer lengthClass, Float value) {

        Map<Integer, Float> model = getModel(modelName);

        Float oldValue = model.get(lengthClass);
        if (oldValue == null) {
            oldValue = 0f;
        }
        model.put(lengthClass, oldValue + value);

    }

    public void addValues(SpeciesCountModel model) {

        Set<String> modelNames = model.getModelNames();
        for (String modelName : modelNames) {

            Map<Integer, Float> incomingModel = model.getModel(modelName);
            Map<Integer, Float> myModel = getModel(modelName);
            for (Map.Entry<Integer, Float> entry : incomingModel.entrySet()) {
                Integer lengthClass = entry.getKey();
                Float valueToAdd = entry.getValue();
                Float oldValue = myModel.get(lengthClass);
                if (oldValue == null) {
                    oldValue = 0f;
                }
                myModel.put(lengthClass, oldValue + valueToAdd);
            }
        }
    }

    @Override
    public void close() throws IOException {
        count.clear();
    }

    public List<Integer> getAvailableLengthClasses() {
        Set<Integer> result = Sets.newHashSet();
        for (Map<Integer, Float> integerFloatMap : count.values()) {
            result.addAll(integerFloatMap.keySet());
        }
        List<Integer> result2 = Lists.newArrayList(result);
        Collections.sort(result2);
        return result2;
    }

    public Float getTotalCount(String modelName) {
        Float result = 0f;
        Map<Integer, Float> model = getModel(modelName);
        for (Float aFloat : model.values()) {
            if (aFloat != null) {
                result += aFloat;
            }
        }
        return result;
    }
}
