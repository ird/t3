---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE Vessel ADD COLUMN speedMaximum REAL;
ALTER TABLE Vessel ADD COLUMN validityStartDate DATE;
ALTER TABLE Vessel ADD COLUMN validityEndDate DATE;
ALTER TABLE Vessel ADD COLUMN faoCode VARCHAR(256);
ALTER TABLE Vessel ADD COLUMN ctoiCode VARCHAR(256);
ALTER TABLE Vessel ADD COLUMN iattcCode VARCHAR(256);