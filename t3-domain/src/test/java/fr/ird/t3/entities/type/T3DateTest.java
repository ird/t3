/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.type;

import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * Tests the {@link T3Date}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3DateTest {

    @Test
    public void newDate() {

        T3Date expected, actual;

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2011);
        cal.set(Calendar.MONTH, 0);
        actual = T3Date.newDate(cal.getTime());

        expected = T3Date.newDate(1, 2011);
        Assert.assertEquals(expected, actual);

        Date actualDate;
        T3Date actual2;
        actualDate = actual.toBeginDate();
        actual2 = T3Date.newDate(actualDate);
        Assert.assertEquals(expected, actual2);

        cal.set(Calendar.YEAR, 2011);
        cal.set(Calendar.MONTH, 11);
        actual = T3Date.newDate(cal.getTime());

        expected = T3Date.newDate(12, 2011);
        Assert.assertEquals(expected, actual);

        actualDate = actual.toBeginDate();
        actual2 = T3Date.newDate(actualDate);
        Assert.assertEquals(expected, actual2);
    }

    @Test
    public void incrementsMonths() {

        T3Date expected, actual, current;

        current = T3Date.newDate(1, 2011);

        actual = current.incrementsMonths(1);
        expected = T3Date.newDate(2, 2011);
        Assert.assertEquals(expected, actual);

        actual = current.incrementsMonths(10);
        expected = T3Date.newDate(11, 2011);
        Assert.assertEquals(expected, actual);

        actual = current.incrementsMonths(11);
        expected = T3Date.newDate(12, 2011);
        Assert.assertEquals(expected, actual);

        actual = current.incrementsMonths(12);
        expected = T3Date.newDate(1, 2012);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void decrementsMonths() {

        T3Date expected, actual, current;

        current = T3Date.newDate(1, 2011);

        actual = current.decrementsMonths(1);
        expected = T3Date.newDate(12, 2010);
        Assert.assertEquals(expected, actual);

        actual = current.decrementsMonths(10);
        expected = T3Date.newDate(3, 2010);
        Assert.assertEquals(expected, actual);

        actual = current.decrementsMonths(11);
        expected = T3Date.newDate(2, 2010);
        Assert.assertEquals(expected, actual);

        actual = current.decrementsMonths(12);
        expected = T3Date.newDate(1, 2010);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void isModuloMonths() {

        T3Date date1, date2;

        date1 = T3Date.newDate(1, 2001);
        date2 = T3Date.newDate(1, 2001);

        // same date = 1 month
        assertIsModuloMonths(date1, date2, 2, false);

        // 2 months
        date2 = T3Date.newDate(2, 2001);
        assertIsModuloMonths(date1, date2, 2, true);

        // 3 months
        date2 = T3Date.newDate(3, 2001);
        assertIsModuloMonths(date1, date2, 2, false);

        // 4 months
        date2 = T3Date.newDate(4, 2001);
        assertIsModuloMonths(date1, date2, 2, true);

        // 6 months
        date2 = T3Date.newDate(6, 2001);
        assertIsModuloMonths(date1, date2, 2, true);

        // 12 months
        date2 = T3Date.newDate(12, 2001);
        assertIsModuloMonths(date1, date2, 2, true);

        // 14 months
        date2 = T3Date.newDate(2, 2002);
        assertIsModuloMonths(date1, date2, 2, true);

        // 3 months
        date2 = T3Date.newDate(3, 2001);
        assertIsModuloMonths(date1, date2, 3, true);

        // 6 months
        date2 = T3Date.newDate(6, 2001);
        assertIsModuloMonths(date1, date2, 3, true);

        // 9 months
        date2 = T3Date.newDate(9, 2001);
        assertIsModuloMonths(date1, date2, 3, true);

        // 12 months
        date2 = T3Date.newDate(12, 2001);
        assertIsModuloMonths(date1, date2, 3, true);

        // 13 months
        date2 = T3Date.newDate(1, 2002);
        assertIsModuloMonths(date1, date2, 3, false);
    }

    protected void assertIsModuloMonths(T3Date date1,
                                        T3Date date2,
                                        int nbStep,
                                        boolean excepted) {
        boolean actual;

        actual = date1.isModuloMonths(date2, nbStep);
        Assert.assertEquals(excepted, actual);

        actual = date2.isModuloMonths(date1, nbStep);
        Assert.assertEquals(excepted, actual);
    }
}
