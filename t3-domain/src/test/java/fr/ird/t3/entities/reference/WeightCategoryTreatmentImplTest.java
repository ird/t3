/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3ScriptHelper;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * To test not generated methods of {@link WeightCategoryTreatmentImpl}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class WeightCategoryTreatmentImplTest extends AbstractDatabaseTest {

    @Test
    public void testGetOldCategoryCode() throws Exception {
        TopiaContext tx = beginTransaction();
        T3ScriptHelper.loadReferentiel(tx, "/db/WeightCategoryTreatmentImplTest.sql");
        WeightCategoryTreatmentDAO dao = T3DAOHelper.getWeightCategoryTreatmentDAO(tx);
        List<WeightCategoryTreatment> all = dao.findAll();
        for (WeightCategoryTreatment weightCategoryTreatment : all) {
            weightCategoryTreatment.getOldCategoryCode();
        }
    }

    @Test
    public void testWeightCategoryTreatmentComparator() throws Exception {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLibelle("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLibelle("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLibelle("unbound");
        catUnbound.setMin(10);
        catUnbound.setMax(null);

        List<WeightCategoryTreatment> list = Lists.newArrayList();
        list.add(catUnbound);
        list.add(catInconnu);
        list.add(catBound);

        WeightCategoryTreatmentDAO.sortWeightCategoryTreatments(list);

        Assert.assertEquals(list.get(0), catBound);
        Assert.assertEquals(list.get(1), catUnbound);
        Assert.assertEquals(list.get(2), catInconnu);
    }

    @Test
    public void testWeightCategoryTreatmentComparator2() throws Exception {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLibelle("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLibelle("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catBound2 = new WeightCategoryTreatmentImpl();
        catBound2.setLibelle("bound2");
        catBound2.setMin(10);
        catBound2.setMax(30);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLibelle("unbound");
        catUnbound.setMin(30);
        catUnbound.setMax(null);

        List<WeightCategoryTreatment> list = Lists.newArrayList();
        list.add(catBound2);
        list.add(catUnbound);
        list.add(catInconnu);
        list.add(catBound);

        Comparator<WeightCategoryTreatment> comparator =
                WeightCategoryTreatmentDAO.newComparator();

        int compare;

        compare = comparator.compare(catBound, catBound2);
        Assert.assertTrue(compare < 0);

        compare = comparator.compare(catBound, catUnbound);
        Assert.assertTrue(compare < 0);

        compare = comparator.compare(catBound2, catUnbound);
        Assert.assertTrue(compare < 0);

        WeightCategoryTreatmentDAO.sortWeightCategoryTreatments(list);

        Assert.assertEquals(list.get(0), catBound);
        Assert.assertEquals(list.get(1), catBound2);
        Assert.assertEquals(list.get(2), catUnbound);
        Assert.assertEquals(list.get(3), catInconnu);
    }

    @Test
    public void getWeightCategoryTreatment() throws Exception {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLibelle("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLibelle("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLibelle("unbound");
        catUnbound.setMin(10);
        catUnbound.setMax(null);

        Map<WeightCategoryTreatment, Integer> weightCategories =
                Maps.newLinkedHashMap();

        weightCategories.put(catBound, 20);
        weightCategories.put(catUnbound, Integer.MAX_VALUE);
        weightCategories.put(catInconnu, Integer.MAX_VALUE);

        WeightCategoryTreatment actual, expected;

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 0);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 19);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 20);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 100);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getWeightCategoryTreatment2() throws Exception {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLibelle("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLibelle("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catBound2 = new WeightCategoryTreatmentImpl();
        catBound.setLibelle("bound2");
        catBound.setMin(10);
        catBound.setMax(30);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLibelle("unbound");
        catUnbound.setMin(30);
        catUnbound.setMax(null);

        Map<WeightCategoryTreatment, Integer> weightCategories =
                Maps.newLinkedHashMap();

        weightCategories.put(catBound, 20);
        weightCategories.put(catBound2, 40);
        weightCategories.put(catUnbound, Integer.MAX_VALUE);
        weightCategories.put(catInconnu, Integer.MAX_VALUE);

        WeightCategoryTreatment actual, expected;

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 0);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 19);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 20);
        expected = catBound2;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 39);
        expected = catBound2;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 40);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentDAO.getWeightCategoryTreatment(weightCategories, 100);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);
    }
}
