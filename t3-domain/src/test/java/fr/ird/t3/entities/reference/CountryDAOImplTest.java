/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Set;

/**
 * Test the user dao {@link CountryDAOImpl}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class CountryDAOImplTest extends AbstractDatabaseTest {

    @Test
    public void findAllFleetUsedInTrip() throws Exception {

        TopiaContext tx = beginTransaction();

        CountryDAO dao = T3DAOHelper.getCountryDAO(tx);

        Country country = dao.create(Country.PROPERTY_CODE, 0);
        Country country2 = dao.create(Country.PROPERTY_CODE, 1);
        Vessel vessel = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country, Vessel.PROPERTY_CODE, 0);
        Vessel vessel2 = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country2, Vessel.PROPERTY_CODE, 1);
        Trip trip = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel);

        Set<Country> result = dao.findAllFleetUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFleetUsedInCatch() throws Exception {

        TopiaContext tx = beginTransaction();

        CountryDAO dao = T3DAOHelper.getCountryDAO(tx);

        Country country = dao.create(Country.PROPERTY_CODE, 0);
        Country country2 = dao.create(Country.PROPERTY_CODE, 1);
        Vessel vessel = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country, Vessel.PROPERTY_CODE, 0);
        Vessel vessel2 = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country2, Vessel.PROPERTY_CODE, 1);
        Trip trip = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel);

        // create another trip without any activity (so withou also any corrected catches)
        Trip trip2 = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel2);

        Activity activity = T3DAOHelper.getActivityDAO(tx).create(
                Activity.PROPERTY_TRIP, trip
        );
        trip.addActivity(activity);

        CorrectedElementaryCatch catche =
                T3DAOHelper.getCorrectedElementaryCatchDAO(tx).create();
        activity.addCorrectedElementaryCatch(catche);

        Set<Country> result = dao.findAllFleetUsedInCatch();
        Assert.assertNotNull(result);
        //FIXME Should be one when we will fix the request...
//        Assert.assertEquals(1, result.size());
        Assert.assertEquals(2, result.size());
//        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFleetUsedInSample() throws Exception {

        TopiaContext tx = beginTransaction();

        Ocean ocean = T3DAOHelper.getOceanDAO(tx).create();

        CountryDAO dao = T3DAOHelper.getCountryDAO(tx);

        Country country = dao.create(Country.PROPERTY_CODE, 0);
        Country country2 = dao.create(Country.PROPERTY_CODE, 1);
        Vessel vessel = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country, Vessel.PROPERTY_CODE, 0);
        Vessel vessel2 = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLEET_COUNTRY, country2, Vessel.PROPERTY_CODE, 1);
        Trip trip = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel);

        // create another trip without any activity (so withou also any corrected catches)
        Trip trip2 = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel2);

        Activity activity = T3DAOHelper.getActivityDAO(tx).create(
                Activity.PROPERTY_TRIP, trip,
                Activity.PROPERTY_OCEAN, ocean
        );
        trip.addActivity(activity);

        Set<Country> result;

        result = dao.findAllFleetUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);

        //FIXME Should be zero when we will fix the request...
//        Assert.assertEquals(0, result.size());
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());

//        SetSpeciesFrequency f =
//                T3DAOHelper.getSetSpeciesFrequencyDAO(tx).create(SetSpeciesFrequency.PROPERTY_ACTIVITY, activity);

        result = dao.findAllFleetUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFlagUsedInSample() throws Exception {

        TopiaContext tx = beginTransaction();

        Ocean ocean = T3DAOHelper.getOceanDAO(tx).create();

        CountryDAO dao = T3DAOHelper.getCountryDAO(tx);

        Country country = dao.create(Country.PROPERTY_CODE, 0);
        Country country2 = dao.create(Country.PROPERTY_CODE, 1);
        Vessel vessel = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLAG_COUNTRY, country, Vessel.PROPERTY_CODE, 0);
        Vessel vessel2 = T3DAOHelper.getVesselDAO(tx).create(Vessel.PROPERTY_FLAG_COUNTRY, country2, Vessel.PROPERTY_CODE, 1);
        Trip trip = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel);

        // create another trip without any activity (so withou also any corrected catches)
        Trip trip2 = T3DAOHelper.getTripDAO(tx).create(Trip.PROPERTY_VESSEL, vessel2);

        Activity activity = T3DAOHelper.getActivityDAO(tx).create(
                Activity.PROPERTY_TRIP, trip,
                Activity.PROPERTY_OCEAN, ocean
        );
        trip.addActivity(activity);

        Set<Country> result;

        result = dao.findAllFlagUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);
        //FIXME Should be zero when we will fix the request...
//        Assert.assertEquals(0, result.size());
        Assert.assertEquals(1, result.size());

//        SetSpeciesFrequency f =
//                T3DAOHelper.getSetSpeciesFrequencyDAO(tx).create(SetSpeciesFrequency.PROPERTY_ACTIVITY, activity);

        result = dao.findAllFlagUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());
    }

}
