---
-- #%L
-- T3 :: Domain
-- $Id$
-- $HeadURL$
-- %%
-- Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- 5 +/- SELECT COUNT(*) FROM PUBLIC.OCEAN;
INSERT INTO PUBLIC.OCEAN(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
('fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 0, TIMESTAMP '2011-02-13 08:02:06.344', 1, 'Atlantique'),
('fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 0, TIMESTAMP '2011-02-13 08:02:06.353', 2, 'Indien'),
('fr.ird.t3.entities.reference.Ocean#1297580528924#0.45013994503441146', 0, TIMESTAMP '2011-02-13 08:02:06.353', 3, 'Pacifique Ouest'),
('fr.ird.t3.entities.reference.Ocean#1297580528924#0.8628908745736912', 0, TIMESTAMP '2011-02-13 08:02:06.353', 4, 'Pacifique Est'),
('fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 0, TIMESTAMP '2011-02-13 08:02:06.353', 5, 'Pacifique ');
UPDATE PUBLIC.ocean SET status = TRUE;

-- 59 +/- SELECT COUNT(*) FROM PUBLIC.SPECIES;
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
('fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', 0, TIMESTAMP '2011-02-13 08:02:06.254', 1, 'YFT', 'Albacore', 'Thunnus albacares', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', 0, TIMESTAMP '2011-02-13 08:02:06.265', 2, 'SKJ', 'Listao', 'Katsuwonus pelamis', 0.1, 1, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', 0, TIMESTAMP '2011-02-13 08:02:06.265', 3, 'BET', 'Patudo', 'Thunnus obesus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', 0, TIMESTAMP '2011-02-13 08:02:06.265', 4, 'ALB', 'Germon', 'Thunnus alalunga', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.4053316565120936', 0, TIMESTAMP '2011-02-13 08:02:06.265', 5, 'LTA', 'Thonine', 'Euthynnus alleteratus', 0.1, 1, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.5568385538955283', 0, TIMESTAMP '2011-02-13 08:02:06.265', 6, 'FRI', 'Auxide', 'Auxis thasard', 0.1, 1, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.7797586779656372', 0, TIMESTAMP '2011-02-13 08:02:06.265', 7, 'SHX', 'Requins', 'Lamnidae & Carcharidae', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.8288119155884791', 0, TIMESTAMP '2011-02-13 08:02:06.265', 8, 'DSC', 'Thonidés rejetés', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', 0, TIMESTAMP '2011-02-13 08:02:06.265', 9, 'MIX', 'Espèces mélangées de thonidés', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.7235429225787087', 0, TIMESTAMP '2011-02-13 08:02:06.265', 10, 'KAW', 'Thonine orientale', 'Euthynnus affinis', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.5141201542305377', 0, TIMESTAMP '2011-02-13 08:02:06.265', 11, 'LOT', 'Thon mignon', 'Thunnus tonggol', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.15829471459979372', 0, TIMESTAMP '2011-02-13 08:02:06.265', 12, 'BLF', 'Thon à nageoires noires', 'Thunnus atlanticus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.42115687710460803', 0, TIMESTAMP '2011-02-13 08:02:06.266', 13, 'BFT', 'Thon rouge', 'Thunnus thynnus thynnus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.18430303815137705', 0, TIMESTAMP '2011-02-13 08:02:06.266', 14, 'SBF', 'Thon rouge du sud', 'Thunnus maccoyii', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.2032631853627409', 0, TIMESTAMP '2011-02-13 08:02:06.266', 15, 'BON', 'Bonite à dos rayé', 'Sarda sarda', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.37853239561355567', 0, TIMESTAMP '2011-02-13 08:02:06.266', 16, 'BIP', 'Bonito oriental', 'Sarda orientalis', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.4823294475226977', 0, TIMESTAMP '2011-02-13 08:02:06.267', 17, 'BLT', 'Bonitou', 'Auxis rochei', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.7195876762319068', 0, TIMESTAMP '2011-02-13 08:02:06.267', 18, 'FRZ', 'Auxides & Bonitou', 'Auxis spp.', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.05492166218055494', 0, TIMESTAMP '2011-02-13 08:02:06.267', 19, 'BOP', 'Palomette', 'Orcynopsis unicolor', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528890#0.9598281772783329', 0, TIMESTAMP '2011-02-13 08:02:06.267', 20, 'WAH', 'Thazard bâtard', 'Acanthocybium solandri', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.8321988986415713', 0, TIMESTAMP '2011-02-13 08:02:06.267', 21, 'SSM', 'Thazard atlantique, Maquereau espagnol', 'Scomberomorus maculatus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.3702981130755375', 0, TIMESTAMP '2011-02-13 08:02:06.267', 22, 'KGM', 'Thazard serra', 'Scomberomorus cavalla', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.9291243253311353', 0, TIMESTAMP '2011-02-13 08:02:06.267', 23, 'MAW', 'Thazard blanc', 'Scomberomorus tritor', 1.0, 2, 300, 300);
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
('fr.ird.t3.entities.reference.Species#1297580528891#0.6973713085931087', 0, TIMESTAMP '2011-02-13 08:02:06.267', 24, 'CER', 'Thazard franc', 'Scomberomorus regalis', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.06598964017890285', 0, TIMESTAMP '2011-02-13 08:02:06.267', 25, 'COM', 'Thazard rayé (Indo-Pacifique)', 'Scomberomorus commerson', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.277861084682149', 0, TIMESTAMP '2011-02-13 08:02:06.267', 26, 'GUT', 'Thazard ponctué (Indo-Pacifique)', 'Scomberomorus guttatus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.8733458797590166', 0, TIMESTAMP '2011-02-13 08:02:06.267', 27, 'STS', 'Thazard cirrus', 'Scomberomorus lineolatus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.07624446469154489', 0, TIMESTAMP '2011-02-13 08:02:06.267', 28, 'BRS', 'Thazard tacheté du sud', 'Scomberomorus brasiliensis', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.7255260110145089', 0, TIMESTAMP '2011-02-13 08:02:06.267', 29, 'KGX', 'Thazards non classés', 'Scomberomorus spp.', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.2626954826359922', 0, TIMESTAMP '2011-02-13 08:02:06.267', 30, 'SAI', 'Voilier Atlantique', 'Istiophorus albicans', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528891#0.42371878801807505', 0, TIMESTAMP '2011-02-13 08:02:06.267', 31, 'SFA', 'Voilier Indo-Pacifique', 'Istiophorus platypterus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.1885405502623827', 0, TIMESTAMP '2011-02-13 08:02:06.267', 32, 'BLM', 'Makaire noir', 'Makaira indica', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.10329800580627058', 0, TIMESTAMP '2011-02-13 08:02:06.267', 33, 'BUM', 'Makaire bleu Atlantique', 'Makaira nigricans', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.8468938192204287', 0, TIMESTAMP '2011-02-13 08:02:06.267', 34, 'BLZ', 'Makaire bleu Indo-Pacifique', 'Makaira mazara', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.7387428235028691', 0, TIMESTAMP '2011-02-13 08:02:06.268', 35, 'WHM', 'Makaire blanc', 'Tetrapturus albidus', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.2565749169300484', 0, TIMESTAMP '2011-02-13 08:02:06.268', 36, 'MLS', 'Marlin rayé', 'Tetrapturus audax', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.2905388174926805', 0, TIMESTAMP '2011-02-13 08:02:06.268', 37, 'SPF', 'Makaire bécune & Marlin de Méditerranée', 'Tetrapurus pfluegeri & T. belone', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.5685612495908416', 0, TIMESTAMP '2011-02-13 08:02:06.268', 38, 'BIL', 'Poissons à rostre non classés', 'Istiophoridae spp.', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.9734051109859334', 0, TIMESTAMP '2011-02-13 08:02:06.268', 39, 'SWO', 'Espadon', 'Xiphias gladius', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.3497160306862599', 0, TIMESTAMP '2011-02-13 08:02:06.268', 40, 'BGT', 'Grands Thonidés', 'Thunini', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528892#0.8581060481567976', 0, TIMESTAMP '2011-02-13 08:02:06.268', 41, 'TUN', 'Thons et bonites non classés', 'Thunini & Sardini ', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.4145401332713441', 0, TIMESTAMP '2011-02-13 08:02:06.268', 43, 'RAV', 'Ravil', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.8103466931799156', 0, TIMESTAMP '2011-02-13 08:02:06.268', 99, 'OTH', 'Espèces inconnues ou autres', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.5318768995033861', 0, TIMESTAMP '2011-02-13 08:02:06.268', 701, 'xxx', 'Appât Sardine de France', '?', 1.0, 2, 300, 300);
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
('fr.ird.t3.entities.reference.Species#1297580528893#0.4508776879595856', 0, TIMESTAMP '2011-02-13 08:02:06.268', 702, 'xxx', 'Appât Sardine ', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.5816657386910375', 0, TIMESTAMP '2011-02-13 08:02:06.268', 703, 'xxx', 'Appât Chinchard', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.16090069597553502', 0, TIMESTAMP '2011-02-13 08:02:06.268', 704, 'xxx', 'Appât Pyronos', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.5305774036220411', 0, TIMESTAMP '2011-02-13 08:02:06.268', 705, 'xxx', 'Appât Anchois', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.2958994878325616', 0, TIMESTAMP '2011-02-13 08:02:06.268', 706, 'xxx', 'Appât Sardine', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.9539593096416237', 0, TIMESTAMP '2011-02-13 08:02:06.268', 707, 'xxx', 'Appât Bogas', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.691484221909629', 0, TIMESTAMP '2011-02-13 08:02:06.268', 708, 'xxx', 'Appât Sardine', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.866664221472501', 0, TIMESTAMP '2011-02-13 08:02:06.268', 709, 'xxx', 'Appât Inconnu', '?', 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528893#0.18704485470829113', 0, TIMESTAMP '2011-02-13 08:02:06.268', 801, 'Dyf', 'Albacore rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.6090603164066191', 0, TIMESTAMP '2011-02-13 08:02:06.268', 802, 'Dsk', 'Listao rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.5515829087627862', 0, TIMESTAMP '2011-02-13 08:02:06.268', 803, 'Dbe', 'Patudo rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.6319671377494842', 0, TIMESTAMP '2011-02-13 08:02:06.269', 804, 'Dal', 'Germon rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.8908576247454943', 0, TIMESTAMP '2011-02-13 08:02:06.269', 805, 'Dlt', 'Thonine rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.5783125750061949', 0, TIMESTAMP '2011-02-13 08:02:06.269', 806, 'Dfr', 'Auxide rejeté', NULL, 1.0, 2, 300, 300),
('fr.ird.t3.entities.reference.Species#1297580528894#0.15942007915259582', 0, TIMESTAMP '2011-02-13 08:02:06.269', 811, 'Dlo', 'Thon mignon rejeté', NULL, 1.0, 2, 300, 300);

UPDATE PUBLIC.SPECIES SET status = TRUE;

-- 3 +/- SELECT COUNT(*) FROM PUBLIC.SCHOOLTYPE;
INSERT INTO PUBLIC.SCHOOLTYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, LIBELLE4, THRESHOLDNUMBERLEVEL2) VALUES
('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', 0, TIMESTAMP '2011-02-13 08:02:06.309', 1, 'Banc sous objet', 'BO', 4500),
('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', 0, TIMESTAMP '2011-02-13 08:02:06.319', 2, 'Banc libre', 'BL', 3000),
('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', 0, TIMESTAMP '2011-02-13 08:02:06.319', 3, 'Indéterminé', 'IND', NULL);
UPDATE PUBLIC.SCHOOLTYPE SET status = TRUE;

-- 13 +/- SELECT COUNT(*) FROM PUBLIC.WEIGHTCATEGORYTREATMENT;
INSERT INTO PUBLIC.WEIGHTCATEGORYTREATMENT(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830500#0.6310700924396421', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.5', '-10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400381', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.505', '+10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400382', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.505', 'Indéfini'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.44386731073429053', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.505', '-10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5237318856431237', 0, 10, 30, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', '10-30 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807192', 0, 30, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', '+30 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807193', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', 'Indéfini'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.4807760142660965', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.506', '-10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660062', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.507', '+10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660063', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.507', 'Indéfini'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.6434841998774135', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.507', '-10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221255', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.508', '+10 kg'),
('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221256', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.508', 'Indéfini');
UPDATE PUBLIC.weightcategorytreatment SET status = TRUE;

-- 12 +/- SELECT COUNT(*) FROM PUBLIC.LENGTHWEIGHTCONVERSION;
INSERT INTO PUBLIC.LENGTHWEIGHTCONVERSION(TOPIAID, TOPIAVERSION, SEXE, BEGINDATE, SPECIES, OCEAN, TOPIACREATEDATE, COEFFICIENTS, TOLENGTHRELATION, TOWEIGHTRELATION, ENDDATE) VALUES
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149460#0.8887187640790983', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.456', 'a=2.153E-5:b=2.976', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149484#0.9082108398982728', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.484', 'a=7.480E-6:b=3.2526', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149485#0.6422899849847009', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.485', 'a=2.396E-5:b=2.9774', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149488#0.7974927985760988', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.488', 'a=1.3718E-5:b=3.0973', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149488#0.7059361871400016', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.4053316565120936', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.488', 'a=1.377E-5:b=3.035', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149489#0.5654424276202628', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5568385538955283', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', TIMESTAMP '2011-03-17 20:29:09.489', 'a=2.8E-7:b=4.13514', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149489#0.8196297229301321', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.489', 'a=5.313E-5:b=2.75366:c=1.5849E-5:d=3.046:LSeuil=64:PSeuil=5', 'P < PSeuil?(Math.pow(P/a, 1/b)):(Math.pow(P/c, 1/d))', 'L < LSeuil?(a * Math.pow(L, b)):(c * Math.pow(L, d))', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149490#0.5981747781630338', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.49', 'a=7.48E-6:b=3.2526', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149491#0.7513130306600405', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.491', 'a=2.7E-5:b=2.95100', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149491#0.04113800209528151', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.491', 'a=6.303E-6:b=3.2825', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149492#0.8363738129717692', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.4053316565120936', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.492', 'a=1.377E-5:b=3.035', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL),
('fr.ird.t3.entities.reference.LengthWeightConversion#1300390149493#0.9126797959243788', 0, 0, TIMESTAMP '1969-12-31 12:00:00.0', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5568385538955283', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', TIMESTAMP '2011-03-17 20:29:09.493', 'a=2.8E-7:b=4.13514', 'Math.pow(P/a, 1/b)', 'a * Math.pow(L, b)', NULL);
UPDATE PUBLIC.lengthweightconversion SET status = TRUE;