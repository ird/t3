<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  T3
  %%
  Copyright (C) 2008 - 2011 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses />.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>codelutinpom</artifactId>
    <version>6</version>
  </parent>

  <groupId>fr.ird</groupId>
  <artifactId>t3</artifactId>
  <version>2.0-SNAPSHOT</version>

  <modules>
    <module>t3-domain</module>
    <module>t3-actions</module>
    <module>t3-input-avdthv33</module>
    <module>t3-input-avdthv35</module>    
    <module>t3-input-avdthv36</module>        
    <module>t3-output-balbayav32</module>
    <module>t3-web</module>
  </modules>

  <name>T3</name>

  <description>Projet T3+</description>
  <inceptionYear>2010</inceptionYear>
  <url>http://t3.codelutin.com</url>

  <organization>
    <name>IRD</name>
    <url>http://www.ird.fr/</url>
  </organization>

  <licenses>
    <license>
      <name>Affero General Public License (AGPL)</name>
      <url>http://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>chemit</id>
      <name>Tony Chemit</name>
      <email>chemit at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <roles>
        <role>lead</role>
        <role>developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>

  </developers>

  <!--scm>
    <url>https://svn.mpl.ird.fr/osiris/t3/trunk</url>
    <connection>scm:svn:https://svn.mpl.ird.fr/osiris/t3/trunk</connection>
    <developerConnection>
      scm:svn:https://svn.mpl.ird.fr/osiris/t3/trunk
    </developerConnection>
  </scm-->
  <scm>
    <url>https://gitlab.nuiton.org/codelutin/t3</url>
    <connection>scm:git:git@gitlab.nuiton.org:codelutin/t3.git</connection>
    <developerConnection>scm:git:git@gitlab.nuiton.org:codelutin/t3.git</developerConnection>
  </scm>

  <distributionManagement>
    <site>
      <id>${site.server}</id>
      <url>${site.url}</url>
    </site>
  </distributionManagement>

  <packaging>pom</packaging>

  <properties>

    <!--  Super pom properties  -->
    <projectId>t3</projectId>

    <t3-data.version>1.1</t3-data.version>

    <!-- libraries version -->

    <eugenePluginVersion>2.6.2</eugenePluginVersion>
    <topiaVersion>2.8.1.3</topiaVersion>
    <nuitonUtilsVersion>2.6.12</nuitonUtilsVersion>
    <nuitonI18nVersion>2.5</nuitonI18nVersion>
    <nuitonWebVersion>1.12-beta-2</nuitonWebVersion>
    <msaccessImporterVersion>1.4.1</msaccessImporterVersion>
    <struts2Version>2.3.14</struts2Version>
    <jqueryPluginVersion>3.5.1</jqueryPluginVersion>
    <h2Version>1.3.171</h2Version>
    <postgresqlVersion>9.1-901-1.jdbc4</postgresqlVersion>
    <hibernateVersion>4.2.0.Final</hibernateVersion>
    <slf4jVersion>1.7.5</slf4jVersion>

    <!-- license header configuration -->
    <license.organizationName>
      IRD, Codelutin, Tony Chemit
    </license.organizationName>
    <license.licenseName>agpl_v3</license.licenseName>

    <!-- i18n configuration -->
    <i18n.bundles>fr_FR,en_GB</i18n.bundles>
    <i18n.silent>true</i18n.silent>

    <t3.bundleOutputName>t3-i18n</t3.bundleOutputName>

    <!-- Site configuration -->
    <locales>fr,en</locales>
    <siteSourcesType>rst</siteSourcesType>

    <commonsCollectionsVersion>3.2.1</commonsCollectionsVersion>
    <commonsLang3Version>3.1</commonsLang3Version>
    <commonsLoggingVersion>1.1.2</commonsLoggingVersion>
    <commonsIoVersion>2.4</commonsIoVersion>
    <commonsBeanutilsVersion>1.8.3</commonsBeanutilsVersion>
    <commonsJxpathVersion>1.3</commonsJxpathVersion>
    <commonsPrimitivesVersion>1.0</commonsPrimitivesVersion>

    <guavaVersion>14.0.1</guavaVersion>
    <junitVersion>4.11</junitVersion>
    <log4jVersion>1.2.17</log4jVersion>

  </properties>

  <dependencyManagement>
    <dependencies>

      <dependency>
        <groupId>fr.ird</groupId>
        <artifactId>msaccess-importer</artifactId>
        <version>${msaccessImporterVersion}</version>
        <scope>compile</scope>
      </dependency>

      <!-- librairies des lutins-->

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-utils</artifactId>
        <version>${nuitonUtilsVersion}</version>
        <scope>compile</scope>
        <exclusions>
          <exclusion>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-vfs2</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-config</artifactId>
        <version>${nuitonUtilsVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-csv</artifactId>
        <version>${nuitonUtilsVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-validator</artifactId>
        <version>${nuitonUtilsVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton.web</groupId>
        <artifactId>nuiton-web</artifactId>
        <version>${nuitonWebVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton.web</groupId>
        <artifactId>nuiton-struts2</artifactId>
        <version>${nuitonWebVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>nuiton-i18n</artifactId>
        <version>${nuitonI18nVersion}</version>
        <scope>compile</scope>
      </dependency>

      <!-- librairie topia -->
      <dependency>
        <groupId>org.nuiton.topia</groupId>
        <artifactId>topia-persistence</artifactId>
        <version>${topiaVersion}</version>
        <exclusions>
          <exclusion>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-ehcache</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.nuiton.topia</groupId>
        <artifactId>topia-persistence</artifactId>
        <version>${topiaVersion}</version>
        <scope>test</scope>
        <classifier>tests</classifier>
      </dependency>

      <dependency>
        <groupId>org.nuiton.topia</groupId>
        <artifactId>topia-service-migration</artifactId>
        <version>${topiaVersion}</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>${hibernateVersion}</version>
      </dependency>

      <!-- base postgres -->

      <dependency>
        <groupId>postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <version>${postgresqlVersion}</version>
      </dependency>

      <!-- base h2 -->
      <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <version>${h2Version}</version>
      </dependency>

      <!-- Struts 2 -->

      <dependency>
        <groupId>org.apache.struts</groupId>
        <artifactId>struts2-core</artifactId>
        <version>${struts2Version}</version>
      </dependency>

      <dependency>
        <groupId>com.jgeppert.struts2.jquery</groupId>
        <artifactId>struts2-jquery-plugin</artifactId>
        <version>${jqueryPluginVersion}</version>
      </dependency>

      <dependency>
        <groupId>com.jgeppert.struts2.jquery</groupId>
        <artifactId>struts2-jquery-grid-plugin</artifactId>
        <version>${jqueryPluginVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.struts</groupId>
        <artifactId>struts2-json-plugin</artifactId>
        <version>${struts2Version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.struts</groupId>
        <artifactId>struts2-sitemesh-plugin</artifactId>
        <version>${struts2Version}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.struts.xwork</groupId>
        <artifactId>xwork-core</artifactId>
        <version>${struts2Version}</version>
        <exclusions>
          <exclusion>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.freemarker</groupId>
        <artifactId>freemarker</artifactId>
        <version>2.3.19</version>
      </dependency>

      <!-- librairies web -->

      <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
        <scope>provided</scope>
      </dependency>

      <!-- loggin libraries -->

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-jcl</artifactId>
        <version>${slf4jVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4jVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>jcl-over-slf4j</artifactId>
        <version>${slf4jVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>1.9.0</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>commons-logging</groupId>
        <artifactId>commons-logging</artifactId>
        <version>${commonsLoggingVersion}</version>
      </dependency>

      <dependency>
        <groupId>commons-primitives</groupId>
        <artifactId>commons-primitives</artifactId>
        <version>${commonsPrimitivesVersion}</version>
      </dependency>

      <dependency>
        <groupId>commons-collections</groupId>
        <artifactId>commons-collections</artifactId>
        <version>${commonsCollectionsVersion}</version>
      </dependency>

      <dependency>
        <groupId>commons-beanutils</groupId>
        <artifactId>commons-beanutils</artifactId>
        <version>${commonsBeanutilsVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>${commonsLang3Version}</version>
      </dependency>

      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>${commonsIoVersion}</version>
      </dependency>

      <dependency>
        <groupId>commons-jxpath</groupId>
        <artifactId>commons-jxpath</artifactId>
        <version>${commonsJxpathVersion}</version>
      </dependency>

      <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava</artifactId>
        <version>${guavaVersion}</version>
      </dependency>

      <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${log4jVersion}</version>
      </dependency>

      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>${junitVersion}</version>
        <scope>test</scope>
      </dependency>

    </dependencies>

  </dependencyManagement>

  <repositories>
    <repository>
      <id>codelutin-public</id>
      <name>Codelutin public repository 4All</name>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <url>http://nexus.nuiton.org/nexus/content/groups/public</url>
    </repository>
  </repositories>

  <build>

    <extensions>
      <!-- Enabling the use of scpexe with maven 3.0 -->
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-ssh-external</artifactId>
        <version>2.2</version>
      </extension>
    </extensions>

    <plugins>

      <plugin>

        <artifactId>maven-antrun-plugin</artifactId>
        <executions>

          <!-- on recopie le changelog.txt vers le site pour faire la release note -->
          <execution>
            <id>copy model to site</id>
            <phase>pre-site</phase>
            <inherited>false</inherited>
            <configuration>
              <!-- TODO chemit 2011-02-24 Change to 'target' when using ant-run 1.6 in mavenpom4labs... -->
              <tasks>
                <echo message="Copy model to site" />
                <copy verbose="${maven.verbose}" failonerror="false" overwrite="true" todir="${project.reporting.outputDirectory}/model">
                  <fileset dir="${basedir}/doc/modelisation/t3">
                    <include name="**/*.png" />
                  </fileset>
                  <fileset dir="${basedir}/t3-domain/src/main/xmi">
                    <include name="t3-persistence.zargo" />
                  </fileset>
                </copy>
              </tasks>
            </configuration>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>

    <pluginManagement>
      <plugins>

        <!-- plugin site -->
        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>org.nuiton.jrst</groupId>
              <artifactId>doxia-module-jrst</artifactId>
              <version>${jrstPluginVersion}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.nuiton.eugene</groupId>
          <artifactId>eugene-maven-plugin</artifactId>
          <version>${eugenePluginVersion}</version>
        </plugin>

        <plugin>
          <groupId>org.nuiton.i18n</groupId>
          <artifactId>i18n-maven-plugin</artifactId>
          <version>${nuitonI18nVersion}</version>
        </plugin>

        <plugin>
          <groupId>org.nuiton</groupId>
          <artifactId>nuiton-utils-maven-report-plugin</artifactId>
          <version>${nuitonUtilsVersion}</version>
        </plugin>

        <plugin>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <version>${projectInfoReportsPluginVersion}</version>
        </plugin>

      </plugins>
    </pluginManagement>

  </build>

  <reporting>

    <!-- par defaut pas de report pour les sites -->
    <excludeDefaults>true</excludeDefaults>
  </reporting>

  <profiles>

    <profile>
      <id>extra-modules</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <modules>
        <module>t3-installer</module>
      </modules>

    </profile>

    <profile>
      <id>central-safe</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <properties>

        <!-- deploy releases on other-releases repository -->
        <release.repository>${other.release.repository}</release.repository>

      </properties>

      <build>
        <defaultGoal>validate</defaultGoal>
        <plugins>
          <plugin>
            <groupId>org.nuiton</groupId>
            <artifactId>helper-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>check-central-safe</id>
                <inherited>true</inherited>
                <goals>
                  <goal>check-auto-container</goal>
                </goals>
                <configuration>
                  <addMavenCentral>true</addMavenCentral>
                  <failIfNotSafe>true</failIfNotSafe>
                  <repositories>
                    <central-releases>
                      http://nexus.nuiton.org/nexus/content/repositories/central-releases
                    </central-releases>
                    <other-releases>
                      http://nexus.nuiton.org/nexus/content/repositories/other-releases
                    </other-releases>
                  </repositories>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>

          <plugin>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>${projectInfoReportsPluginVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>project-team</report>
                  <report>mailing-list</report>
                  <report>cim</report>
                  <report>issue-tracking</report>
                  <report>license</report>
                  <report>scm</report>
                  <report>dependencies</report>
                  <report>dependency-convergence</report>
                  <report>plugin-management</report>
                  <report>plugins</report>
                  <report>dependency-management</report>
                  <report>summary</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>

          <plugin>
            <groupId>org.nuiton</groupId>
            <artifactId>nuiton-utils-maven-report-plugin</artifactId>
            <version>${nuitonUtilsVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>aggregate-config-report</report>
                </reports>
              </reportSet>
            </reportSets>
            <configuration>
              <i18nBundleName>${t3.bundleOutputName}</i18nBundleName>
            </configuration>
          </plugin>

        </plugins>
      </reporting>

    </profile>

  </profiles>

</project>
