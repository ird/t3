/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.type.T3Date;
import org.nuiton.topia.TopiaException;

import java.util.Set;

/**
 * Level 2 Sample stratum loader for atlantic ocean.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class L2SampleStratumLoaderAtlantic extends L2SampleStratumLoader {

    public L2SampleStratumLoaderAtlantic(L2SampleStratum sampleStratum) {
        super(sampleStratum);
    }

    @Override
    protected Set<String> findActivityIds(int level) throws TopiaException {

        StratumConfiguration<Level2Configuration> configuration =
                getSampleStratum().getConfiguration();

        String schoolTypeId = configuration.getSchoolType().getTopiaId();
        String zoneId = configuration.getZone().getTopiaId();
        T3Date beginDate = configuration.getBeginDate();
        T3Date endDate = configuration.getEndDate();
        int currentYear = beginDate.getYear();
        int timeStep = getTimeStep();

        Set<String> activityIds;

        switch (level) {

            case 1:
                // same stratum as catch stratum
                activityIds = findActivityIds(schoolTypeId,
                                              beginDate,
                                              endDate,
                                              zoneId);
                break;

            case 2:
                // same stratum previous year
                activityIds = findActivityIds(
                        schoolTypeId,
                        beginDate.decrementsMonths(12),
                        endDate.decrementsMonths(12),
                        zoneId);
                break;

            case 3:
                // same stratum previous time period
                activityIds = findActivityIds(
                        schoolTypeId,
                        beginDate.decrementsMonths(timeStep),
                        endDate.decrementsMonths(timeStep),
                        zoneId);
                break;

            case 4:
                // same stratum next time period
                activityIds = findActivityIds(
                        schoolTypeId,
                        beginDate.incrementsMonths(timeStep),
                        endDate.incrementsMonths(timeStep),
                        zoneId);
                break;

            case 5:
                // same stratum for all current year
                activityIds = findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear),
                        T3Date.newDate(12, currentYear),
                        zoneId);
                break;

            case 6:
                // same stratum for all previous year
                activityIds = findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear - 1),
                        T3Date.newDate(12, currentYear - 1),
                        zoneId);
                break;

            case 7:
                // same stratum for all zones in all current year
                activityIds = findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear),
                        T3Date.newDate(12, currentYear),
                        getSampleStratum().getConfiguration().getZoneIds());
                break;

            default:

                // null means nothing more to do
                activityIds = null;
        }

        return activityIds;
    }
}
