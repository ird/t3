/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.stratum.SampleStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Base level 2 sample stratum loader.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public abstract class L3SampleStratumLoader extends SampleStratumLoader<Level3Configuration, Level3Action, L3SampleStratum> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(L3SampleStratumLoader.class);

    /**
     * The minimum sample total weight to obtain the correct substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration
     * and catch stratum total weight.
     *
     * @since 1.4
     */
    private final float minimumSampleTotalWeight;

    /**
     * The minimum sample count required to obtain the correct
     * substitution level for each species to fix.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration.
     */
    private final Map<String, Integer> minimumSampleCount;

    @InjectDAO(entityType = Activity.class)
    protected ActivityDAO activityDAO;

    public L3SampleStratumLoader(L3SampleStratum sampleStratum) {
        super(sampleStratum);

        Level3Configuration configuration =
                sampleStratum.getLevelConfiguration();

        // compute minimumSampleTotalWeight

        float catchStratumTotalWeight = sampleStratum.getCatchStratumTotalWeight();
        float maximumWeightRatio = configuration.getStratumWeightRatio();

        minimumSampleTotalWeight = catchStratumTotalWeight / maximumWeightRatio;

        minimumSampleCount =
                sampleStratum.getConfiguration().getStratumMinimumCountBySpecie();
    }

    @Override
    protected Set<String> findActivityIds(String schoolTypeId,
                                          T3Date beginDate,
                                          T3Date endDate,
                                          String... zoneIds) throws TopiaException {

        Set<String> result = Sets.newHashSet();

        StratumConfiguration<Level3Configuration> configuration = getSampleStratum().getConfiguration();
        for (String zoneId : zoneIds) {

            // on commence par récupérer les activités :
            // - dans la zone ET
            // - avec le bon type de banc
            // - dans la bonne période de temps

            List<String> activityIds = activityDAO.findAllActivityIdsForSampleStratum(
                    configuration.getZoneTableName(),
                    zoneId,
                    schoolTypeId,
                    beginDate,
                    endDate
            );

            result.addAll(activityIds);
        }
        return result;
    }

    @Override
    protected Set<Activity> filterActivities(Set<String> activityIds) throws TopiaException {

        Set<Activity> result = Sets.newHashSet();

        StratumConfiguration<Level3Configuration> configuration = getSampleStratum().getConfiguration();
        Set<Vessel> possibleVessels =
                configuration.getPossibleSampleVessels();

        for (String activityId : activityIds) {
            Activity activity = configuration.getActivity(activityId);

            // get his trip
            Trip trip = activity.getTrip();
            if (!possibleVessels.contains(trip.getVessel())) {

                // not a matching boat
                continue;
            }

            // recheck activity have some samples.
            Preconditions.checkState(
                    !activity.isSetSpeciesCatWeightEmpty(),
                    "Can not accept an activity (" + activity.getTopiaId() +
                    ") with no sample");

            result.add(activity);
        }
        return result;
    }

    @Override
    public boolean isStratumOk() {

        L3SampleStratum stratum = getSampleStratum();

        // test if we have enough sample weight
        float sampleStratumTotalWeight = stratum.getSampleStratumTotalWeight();
        boolean minimumSampleTotalWeightOk =
                sampleStratumTotalWeight >= minimumSampleTotalWeight;
        if (!minimumSampleTotalWeightOk) {

            // need more sample weight
            if (log.isInfoEnabled()) {
                log.info("Missing some weight (got: " + sampleStratumTotalWeight + ", but required minimum: " + minimumSampleTotalWeight + ").");
            }
        }

        // get all species to fix
        Collection<Species> species = Sets.newHashSet(stratum.getSpeciesToFix());

        // retains only the one found in sample
        species.retainAll(stratum.getSampleSpecies());

        // test if we have enough sample count for each species to fix
        boolean minimumSampleCountOk = true;
        for (Species aSpecies : species) {

            int minCount = minimumSampleCount.get(aSpecies.getTopiaId());
            float sampleCount = stratum.getSpeciesTotalCount(aSpecies);

            if (sampleCount < minCount) {

                minimumSampleCountOk = false;
                // need more fishes count
                if (log.isInfoEnabled()) {
                    log.info("Missing fishes count for species " +
                             aSpecies.getLibelle() + " (got: " + sampleCount + ", but required minimum: " + minCount + " ).");
                }
            }
        }

        // at least one test must be ok
        // see http://forge.codelutin.com/issues/3229
        boolean result = minimumSampleTotalWeightOk || minimumSampleCountOk;
        return result;
    }
}