/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ird.t3.actions.stratum.CatchStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.MapUtils;
import org.nuiton.topia.TopiaException;

import java.util.Map;
import java.util.Set;

/**
 * To load a {@link L3CatchStratum} for a given stratum configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class L3CatchStratumLoader extends CatchStratumLoader<Level3Configuration> {

    @InjectDAO(entityType = Activity.class)
    protected ActivityDAO activityDAO;

    @Override
    public Map<Activity, Integer> loadData(StratumConfiguration<Level3Configuration> configuration) throws TopiaException {
        // on commence par récupérer les activités :
        // - dans la zone
        // - avec le bon type de banc
        // - dans la bonne période de temps
        // - expertFlag <> 0
        // - avec des captures
        // - dont la marée n'est pas samplesOnly
        // - dont la marée est complète

        Map<String, Integer> activityIds = activityDAO.findAllActivityIdsForCatchStratum(
                configuration.getZoneTableName(),
                configuration.getZone().getTopiaId(),
                configuration.getSchoolType().getTopiaId(),
                configuration.getBeginDate(),
                configuration.getEndDate()
        );

        // ensuite pour chaque activité on récupère sa marée et on conserve
        // l'activité uniquement ssi :
        // - maree.bateau de type senneur OK
        // - maree.bateau dans la bonne flotte OK

        Map<Activity, Integer> result = filterActivities(configuration, activityIds);

        return result;
    }

    protected Map<Activity, Integer> filterActivities(
            StratumConfiguration<Level3Configuration> configuration,
            Map<String, Integer> activityIds) throws TopiaException {

        Set<Vessel> possibleVessels = configuration.getPossibleCatchVessels();

        Map<Activity, Integer> result = Maps.newHashMap();
        if (MapUtils.isNotEmpty(activityIds)) {
            for (Map.Entry<String, Integer> e : activityIds.entrySet()) {

                String activityId = e.getKey();

                // get activity
                Activity activity = configuration.getActivity(activityId);

                // get his trip
                Trip trip = activity.getTrip();
                if (!possibleVessels.contains(trip.getVessel())) {

                    // not a matching boat
                    continue;
                }

                // recheck activity have some catches.
                Preconditions.checkState(
                        !activity.isCorrectedElementaryCatchEmpty(),
                        "Can not accept an activity (" + activity.getTopiaId() +
                        ") with no catch"
                );

                result.put(activity, e.getValue());
            }
        }
        return result;
    }
}
