/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import static org.nuiton.i18n.I18n.n_;

/**
 * Configuration of a compute RF1 action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see ComputeRF1Action
 * @since 1.0
 */
public class ComputeRF1Configuration extends AbstractLevel0Configuration {

    static {
        n_("t3.common.rf1.minimumRate.acceptable");
        n_("t3.common.rf1.maximumRate.acceptable");
    }

    private static final long serialVersionUID = 1L;

    /** Minimum rf1 rate acceptable. */
    protected float minimumRate;

    /** Maximum rf1 rate acceptable. */
    protected float maximumRate;

    public ComputeRF1Configuration() {
        super(Level0Step.COMPUTE_RF1);
    }

    public float getMinimumRate() {
        return minimumRate;
    }

    public void setMinimumRate(float minimumRate) {
        this.minimumRate = minimumRate;
    }

    public float getMaximumRate() {
        return maximumRate;
    }

    public void setMaximumRate(float maximumRate) {
        this.maximumRate = maximumRate;
    }
}
