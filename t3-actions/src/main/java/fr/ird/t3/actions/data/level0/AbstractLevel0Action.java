/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.List;
import java.util.Set;

/**
 * Abstract level 0 action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractLevel0Action<C extends AbstractLevel0Configuration> extends T3Action<C> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractLevel0Action.class);

    public static final String RESULT_TRIP_LIST_DONE = "tripListDone";

    public static final String RESULT_TRIP_LIST_REDONE = "tripListReDone";

    @InjectDAO(entityType = Trip.class)
    protected TripDAO tripDAO;

    @InjectEntitiesById(entityType = Country.class)
    protected List<Country> fleets;

    @InjectEntitiesById(entityType = VesselSimpleType.class)
    protected List<VesselSimpleType> vesselSimpleTypes;

    protected List<Trip> trips;

    protected Set<Vessel> vessels;

    protected int nbTrips;

    protected int nbVessels;

    protected final Level0Step step;

    protected final Set<Level0Step> higherSteps;

    public AbstractLevel0Action(Level0Step step) {
        this.step = step;
        higherSteps = Level0Step.allAfter(step);
    }

    public Level0Step getStep() {
        return step;
    }

    public final void setTrips(List<Trip> trips) {
        this.trips = trips;
        nbTrips = trips.size();
    }

    public final int getNbTrips() {
        return nbTrips;
    }

    public final int getNbVessels() {
        return nbVessels;
    }

    public void setVessels(Set<Vessel> vessels) {
        this.vessels = vessels;
        nbVessels = vessels.size();
    }

//    protected List<Trip> getUsableTrips() throws TopiaException {
//        return getUsableTrips(null);
//    }

    protected List<Trip> getUsableTrips(List<Harbour> landingHarbours,
                                        boolean rejectComputationStatus) throws TopiaException {

        C configuration = getConfiguration();

        List<Trip> tripList = tripDAO.findAllBetweenLandingDate(
                configuration.getBeginDate(),
                configuration.getEndDate(),
                false,
                rejectComputationStatus
        );

        if (log.isInfoEnabled()) {
            log.info("All trips between " + configuration.getBeginDate() +
                     " and " + configuration.getEndDate() + " : " + tripList.size());
        }

        // get all vessels used in trips
        Set<Vessel> vesselSet = VesselDAO.getAllVessels(tripList);
        setVessels(vesselSet);

        if (log.isInfoEnabled()) {
            log.info("All vessels used by any trip : " + getNbVessels());
        }

        // keep only given vessel simple types
        VesselDAO.retainsVesselSimpleTypes(vesselSet, vesselSimpleTypes);

        if (log.isInfoEnabled()) {
            log.info("Usable vessels (retains vessel simple types) : " + getNbVessels());
        }
        // keep only given fleets
        VesselDAO.retainsFleetCountries(vesselSet, fleets);

        if (log.isInfoEnabled()) {
            log.info("Usable vessels (retains fleets) : " + getNbVessels());
        }
        // keep trips only for given vessels
        TripDAO.retainsVessels(tripList, vesselSet);

        if (log.isInfoEnabled()) {
            log.info("Usable trips (retains vessels) : " + tripList.size());
        }

        if (landingHarbours != null) {


            // keep trips only for given landing harbours
            TripDAO.retainsLandingHarbours(tripList, landingHarbours);

            if (log.isInfoEnabled()) {
                log.info("Usable trips (retains landing harbours) : " +
                         tripList.size());
            }
        }

        // sort trips on landing date (once for all)
        TripDAO.sortTrips(tripList);

        return tripList;
    }

    protected void markTripAsTreated(Trip trip) {

        // let's validate this step
        step.setTripState(trip, true);

        // and invalidate all other following steps
        Level0Step.invalidate(trip, higherSteps);
    }

}
