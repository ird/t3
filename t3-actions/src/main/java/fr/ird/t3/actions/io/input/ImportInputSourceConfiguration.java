/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;

import java.io.File;
import java.util.Locale;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Configuration of a import data action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ImportInputSourceConfiguration implements InputSourceConfiguration {

    private static final long serialVersionUID = 1L;

    public static ImportInputSourceConfiguration newConfiguration(
            InputSourceConfiguration analyzeConfiguration) {
        ImportInputSourceConfiguration result =
                new ImportInputSourceConfiguration();
        result.setInputFile(analyzeConfiguration.getInputFile());
        result.setInputProvider(analyzeConfiguration.getInputProvider());
        result.setUseWells(analyzeConfiguration.isUseWells());
        result.setSamplesOnly(analyzeConfiguration.isSamplesOnly());
        result.setCanCreateVessel(analyzeConfiguration.isCanCreateVessel());
        result.setCreateVirtualVessel(analyzeConfiguration.isCanCreateVessel());
        return result;
    }

    protected boolean useWells;

    protected boolean samplesOnly;

    protected boolean canCreateVessel;

    protected boolean createVirtualVessel;

    protected T3InputProvider inputProvider;

    protected File inputFile;

    protected Set<Trip> tripsToImport;

    protected Set<Trip> tripsToDelete;

    @Override
    public boolean isUseWells() {
        return useWells;
    }

    @Override
    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    @Override
    public boolean isSamplesOnly() {
        return samplesOnly;
    }

    @Override
    public void setSamplesOnly(boolean samplesOnly) {
        this.samplesOnly = samplesOnly;
    }

    @Override
    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    @Override
    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    @Override
    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    @Override
    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    @Override
    public T3InputProvider getInputProvider() {
        return inputProvider;
    }

    @Override
    public void setInputProvider(T3InputProvider inputProvider) {
        this.inputProvider = inputProvider;
    }

    @Override
    public File getInputFile() {
        return inputFile;
    }

    @Override
    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    @Override
    public String getName(Locale locale) {
        return l_(locale, "t3.input.ImportInputSource");
    }

    public Set<Trip> getTripsToImport() {
        return tripsToImport;
    }

    public void setTripsToImport(Set<Trip> tripsToImport) {
        this.tripsToImport = tripsToImport;
    }

    public Set<Trip> getTripsToDelete() {
        return tripsToDelete;
    }

    public void setTripsToDelete(Set<Trip> tripsToDelete) {
        this.tripsToDelete = tripsToDelete;
    }
}
