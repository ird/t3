/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.decorator.Decorator;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Configuration of any action of level 1.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level1Configuration implements T3ActionConfiguration {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Level1Configuration.class);

    protected List<SampleQuality> sampleQualities;

    protected List<SampleType> sampleTypes;

    protected List<Country> fleets;

    protected List<Ocean> oceans;

    /** Ids of sample qualities to use. */
    protected List<String> sampleQualityIds;

    /** Ids of sample types to use. */
    protected List<String> sampleTypeIds;

    /** Ids of fleet countries to use. */
    protected List<String> fleetIds;

    /** Ids of trips to use. */
    protected Multimap<String, String> sampleIdsByTripId;

    /** Id of selected ocean. */
    protected String oceanId;

    /** begin date to use. */
    protected T3Date beginDate;

    /** end date to use. */
    protected T3Date endDate;

    /** min begin date to use. */
    protected T3Date minDate;

    /** max end date to use. */
    protected T3Date maxDate;

    /** Max rfTot acceptable. */
    protected int rfTotMax;

    /** Max rfMinus10 acceptable. */
    protected int rfMinus10Max;

    /** Max rfPlus10 acceptable. */
    protected int rfPlus10Max;

    /** Min number of fish for rfMinus10 to be acceptable. */
    protected int rfMinus10MinNumber;

    /** Min number of fish for rfPlus10 to be acceptable. */
    protected int rfPlus10MinNumber;

    /** Already executed step for this configuration. */
    protected Set<Level1Step> executedSteps;

    /**
     * To use rfMins10 and rfPlus10 in {@link ExtrapolateSampleWeightToSetAction}.
     *
     * By default, use it.
     *
     * @since 1.6.1
     */
    protected boolean useRfMins10AndRfPlus10 = true;

    /** Current executing step. */
    private Level1Step step;

    /**
     * For not selected samples, reason of none selection.
     *
     * @since 1.2
     */
    private Map<String, String> notTreatedSampleReason = Maps.newTreeMap();

    public Set<Level1Step> getExecutedSteps() {
        if (executedSteps == null) {
            executedSteps = EnumSet.noneOf(Level1Step.class);
        }
        return executedSteps;
    }

    public void addExecutedStep(Level1Step step) {
        getExecutedSteps().add(step);
    }

    public String getOceanId() {
        return oceanId;
    }

    public void setOceanId(String oceanId) {
        this.oceanId = oceanId;
    }

    public Multimap<String, String> getSampleIdsByTripId() {
        return sampleIdsByTripId;
    }

    public void setSampleIdsByTripId(Multimap<String, String> sampleIdsByTripId) {
        this.sampleIdsByTripId = sampleIdsByTripId;
    }

    public List<String> getSampleQualityIds() {
        return sampleQualityIds;
    }

    public void setSampleQualityIds(List<String> sampleQualityIds) {
        this.sampleQualityIds = sampleQualityIds;
    }

    public List<String> getSampleTypeIds() {
        return sampleTypeIds;
    }

    public void setSampleTypeIds(List<String> sampleTypeIds) {
        this.sampleTypeIds = sampleTypeIds;
    }

    public List<String> getFleetIds() {
        return fleetIds;
    }

    public void setFleetIds(List<String> fleetIds) {
        this.fleetIds = fleetIds;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(T3Date beginDate) {
        this.beginDate = beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public void setEndDate(T3Date endDate) {
        this.endDate = endDate;
    }

    public int getRfTotMax() {
        return rfTotMax;
    }

    public void setRfTotMax(int rfTotMax) {
        this.rfTotMax = rfTotMax;
    }

    public int getRfMinus10Max() {
        return rfMinus10Max;
    }

    public void setRfMinus10Max(int rfMinus10Max) {
        this.rfMinus10Max = rfMinus10Max;
    }

    public int getRfPlus10Max() {
        return rfPlus10Max;
    }

    public void setRfPlus10Max(int rfPlus10Max) {
        this.rfPlus10Max = rfPlus10Max;
    }

    public int getRfMinus10MinNumber() {
        return rfMinus10MinNumber;
    }

    public void setRfMinus10MinNumber(int rfMinus10MinNumber) {
        this.rfMinus10MinNumber = rfMinus10MinNumber;
    }

    public int getRfPlus10MinNumber() {
        return rfPlus10MinNumber;
    }

    public void setRfPlus10MinNumber(int rfPlus10MinNumber) {
        this.rfPlus10MinNumber = rfPlus10MinNumber;
    }

    public List<SampleQuality> getSampleQualities() {
        return sampleQualities;
    }

    public void setSampleQualities(List<SampleQuality> sampleQualities) {
        this.sampleQualities = sampleQualities;
    }

    public List<SampleType> getSampleTypes() {
        return sampleTypes;
    }

    public void setSampleTypes(List<SampleType> sampleTypes) {
        this.sampleTypes = sampleTypes;
    }

    public List<Country> getFleets() {
        return fleets;
    }

    public void setFleets(List<Country> fleets) {
        this.fleets = fleets;
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public void setOceans(List<Ocean> oceans) {
        this.oceans = oceans;
    }

    public Map<String, String> getNotTreatedSampleReason() {
        return notTreatedSampleReason;
    }

    public String getNotTreatedSampleReason(Sample sample) {
        String id = sample.getTopiaId();
        String result = notTreatedSampleReason.get(id);
        return result;
    }

    public boolean isUseRfMins10AndRfPlus10() {
        return useRfMins10AndRfPlus10;
    }

    public void setUseRfMins10AndRfPlus10(boolean useRfMins10AndRfPlus10) {
        this.useRfMins10AndRfPlus10 = useRfMins10AndRfPlus10;
    }

    public Multimap<String, String> getMatchingTrips(
            Collection<Trip> trips,
            Locale locale,
            DecoratorService decoratorService) {

        // clean rejected reasons
        notTreatedSampleReason.clear();

        Multimap<String, String> result = TreeMultimap.create();

        if (oceanId == null) {

            // no matching trips (so no matching trip
            return result;
        }
        Preconditions.checkNotNull(oceanId);

        Ocean ocean =
                Iterables.find(oceans, T3Predicates.equalsTopiaEntity(oceanId));

        Preconditions.checkNotNull(ocean);

        Predicate<Trip> tripPredicate =
                T3Predicates.tripUsingOcean(Arrays.asList(ocean));

        Decorator<Trip> tripDecorator =
                decoratorService.getDecorator(locale, Trip.class, DecoratorService.WITH_ID);

        Decorator<Sample> sampleDecorator =
                decoratorService.getDecorator(locale, Sample.class, DecoratorService.WITH_ID);

        String sampleWellEmptyReason =
                l_(locale, "t3.level1.notSelectedSample.sampleWellEmpty");

        String sampleNoMatchingActivityReason =
                l_(locale, "t3.level1.notSelectedSample.sampleNoMatchingActivity");

        Decorator<SampleQuality> sampleQualityDecorator = decoratorService.getDecorator(locale, SampleQuality.class, null);
        Decorator<SampleType> sampleTypeDecorator = decoratorService.getDecorator(locale, SampleType.class, null);
        for (Trip trip : trips) {

            String tripSkipMessagePrefix =
                    "Trip " + tripDecorator.toString(trip) + "' skip due to ";
            if (!fleetIds.contains(
                    trip.getVessel().getFleetCountry().getTopiaId())) {

                // trip's boat fleet not accepted, do NOT keep this trip
                if (log.isDebugEnabled()) {
                    log.debug(tripSkipMessagePrefix + " vessel rejected.");
                }
                continue;
            }

            if (trip.isActivityEmpty()) {

                // no activity, do NOT keep this trip
                if (log.isDebugEnabled()) {
                    log.debug(tripSkipMessagePrefix + " no activity.");
                }
                continue;
            }

            if (trip.isSampleEmpty()) {

                // no sample, do NOT keep this trip
                if (log.isDebugEnabled()) {
                    log.debug(tripSkipMessagePrefix + " no sample.");
                }
                continue;
            }

            if (!tripPredicate.apply(trip)) {

                // no matching ocean, do NOT keep this trip
                if (log.isDebugEnabled()) {
                    log.debug(tripSkipMessagePrefix + " ocean rejected.");
                }
                continue;
            }

            List<String> samplesToKeep = Lists.newArrayList();

            // all rejectedReason (will stored only if the trip is kept)
            Map<String, String> rejectedReason = Maps.newTreeMap();

            for (Sample sample : trip.getSample()) {

                String sampleSkipMessagePrefix =
                        "Trip " + tripDecorator.toString(trip) +
                        ", Sample " + sampleDecorator.toString(sample)
                        + " skip due to ";

                if (!sampleQualityIds.contains(
                        sample.getSampleQuality().getTopiaId())) {

                    // sample quality not accepted, do NOT keep this sample

                    String reason =
                            l_(locale, "t3.level1.notSelectedSample.sampleQualityRejected",
                               sampleQualityDecorator.toString(sample.getSampleQuality()));
                    if (log.isDebugEnabled()) {
                        log.debug(sampleSkipMessagePrefix + " :" +
                                  reason);
                    }
                    rejectedReason.put(sample.getTopiaId(), reason);
                    continue;
                }

                if (!sampleTypeIds.contains(
                        sample.getSampleType().getTopiaId())) {

                    // sample type not accepted, do NOT keep this sample
                    String reason =
                            l_(locale, "t3.level1.notSelectedSample.sampleTypeRejected",
                               sampleTypeDecorator.toString(sample.getSampleType()));
                    if (log.isDebugEnabled()) {


                        log.debug(sampleSkipMessagePrefix + " : " +
                                  reason);
                    }
                    rejectedReason.put(sample.getTopiaId(), reason);
                    continue;
                }

                // ok now must link to an existing set.
                // for the moment just find at least one activity which date
                // in inside the min - max bound

                if (sample.isSampleWellEmpty()) {

                    // no sample set, do NOT keep this sample
                    if (log.isDebugEnabled()) {
                        log.debug(sampleSkipMessagePrefix + " : " +
                                  sampleWellEmptyReason);
                    }
                    rejectedReason.put(sample.getTopiaId(), sampleWellEmptyReason);
                    continue;
                }

                boolean activityFound = false;
                for (SampleWell sampleWell : sample.getSampleWell()) {
                    Activity activity = sampleWell.getActivity();
                    T3Date activityDate = T3Date.newDate(activity.getDate());
                    if (beginDate.beforeOrEquals(activityDate) &&
                        endDate.afterOrEquals(activityDate)) {

                        // ok found a matching activity
                        activityFound = true;
                        break;
                    }
                }

                if (!activityFound) {

                    // no activity match, do NOT keep this sample

                    if (log.isDebugEnabled()) {
                        log.debug(sampleSkipMessagePrefix + " : " +
                                  sampleNoMatchingActivityReason);
                    }
                    rejectedReason.put(sample.getTopiaId(), sampleNoMatchingActivityReason);
                    continue;
                }

                // ok finally this sample can be keep
                samplesToKeep.add(sample.getTopiaId());
            }

            if (samplesToKeep.isEmpty()) {

                // no matching sample, do NOT keep the trip
                if (log.isDebugEnabled()) {
                    log.debug(tripSkipMessagePrefix + " no matching sample.");
                }
                continue;
            }

            if (MapUtils.isNotEmpty(rejectedReason)) {
                notTreatedSampleReason.putAll(rejectedReason);
            }

            // ok found some matching sample for the trip, so keep this trip
            result.putAll(trip.getTopiaId(), samplesToKeep);
        }

        return result;
    }

    public T3Date getMinDate() {
        return minDate;
    }

    public void setMinDate(T3Date minDate) {
        this.minDate = minDate;
    }

    public T3Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(T3Date maxDate) {
        this.maxDate = maxDate;
    }

    public void setStep(Level1Step step) {
        this.step = step;
    }

    @Override
    public String getName(Locale locale) {
        String stepName = l_(locale, step.getI18nKey());
        return l_(locale, "t3.level1.action", stepName);
    }
}
