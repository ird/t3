/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.admin;

import fr.ird.t3.actions.T3ActionConfiguration;

import java.util.Collection;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Configuration of the {@link DeleteTripAction} action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class DeleteTripConfiguration implements T3ActionConfiguration {

    private static final long serialVersionUID = -741229586344924884L;

    /** Ids of trips to treat. */
    protected Collection<String> tripIds;

    /**
     * Flag to delete trips (if sets to {@code false} then just delete trip
     * data.
     */
    protected boolean deleteTrip;

    public Collection<String> getTripIds() {
        return tripIds;
    }

    public void setTripIds(Collection<String> tripIds) {
        this.tripIds = tripIds;
    }

    public boolean isDeleteTrip() {
        return deleteTrip;
    }

    public void setDeleteTrip(boolean deleteTrip) {
        this.deleteTrip = deleteTrip;
    }

    @Override
    public String getName(Locale locale) {
        return l_(locale, "t3.admin.DeleteTrip");
    }
}
