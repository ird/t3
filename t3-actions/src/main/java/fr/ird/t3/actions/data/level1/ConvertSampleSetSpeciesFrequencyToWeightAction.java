package fr.ird.t3.actions.data.level1;

/*
 * #%L
 * T3 :: Actions
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.CountAndWeight;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeight;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeightDAO;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyDAO;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategorySampleDAO;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Converts {@link SampleSetSpeciesFrequency} to {@link SampleSetSpeciesCatWeight}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.2
 */
public class ConvertSampleSetSpeciesFrequencyToWeightAction extends AbstractLevel1Action {

    @InjectDAO(entityType = LengthWeightConversion.class)
    protected LengthWeightConversionDAO lengthWeightConversionDAO;

    @InjectDAO(entityType = SampleSetSpeciesCatWeight.class)
    protected SampleSetSpeciesCatWeightDAO sampleSetSpeciesCatWeightDAO;

    @InjectDAO(entityType = WeightCategorySample.class)
    protected WeightCategorySampleDAO weightCategorySampleDAO;

    protected LengthWeightConversionHelper conversionHelper;

    protected final Map<String, List<WeightCategorySample>> weightCategoryCache;

    protected List<WeightCategorySample> getWeightCategories(SampleWell sampleWell) throws TopiaException {

        Activity activity = sampleWell.getActivity();
        Ocean ocean1 = activity.getOcean();
        SchoolType schoolType = activity.getSchoolType();
        String key = ocean1.getTopiaId() + "-" + schoolType.getTopiaId();
        List<WeightCategorySample> result = weightCategoryCache.get(key);

        if (result == null) {

            result = weightCategorySampleDAO.findAllByOcean(ocean1);
            WeightCategories.sort(result);
            weightCategoryCache.put(key, result);
        }

        Preconditions.checkNotNull(
                result,
                "Could not find any weight categories for ocean " + ocean.getLibelle() );
        return result;
    }

    public ConvertSampleSetSpeciesFrequencyToWeightAction() {
        super(Level1Step.CONVERT_SAMPLE_SET_SPECIES_FREQUENCY_TO_WEIGHT);
        weightCategoryCache = Maps.newTreeMap();
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        conversionHelper = lengthWeightConversionDAO.newConversionHelper();
    }

    @Override
    protected void deletePreviousData() throws TopiaException {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        Set<Trip> trips = samplesByTrip.keySet();
        setNbSteps(samplesByTrip.size() + trips.size());
        for (Trip trip : trips) {

            Collection<Sample> samples = samplesByTrip.get(trip);

            logTreatedAndNotSamplesforATrip(trip, samples);

            long s0 = TimeLog.getTime();

            doExecuteTrip(trip, samples);

            String tripStr = decorate(trip, DecoratorService.WITH_ID);

            getTimeLog().log(s0, "treat trip " + tripStr);

            // flush transaction otherwise too much data in memory
            flushTransaction("Flush transaction for " + tripStr);
        }
        return true;
    }

    protected void doExecuteTrip(Trip trip,
                                 Collection<Sample> samples) throws TopiaException {

        incrementsProgression();

        Set<Activity> tripActivities = Sets.newHashSet();

        // extrapolate for each sample set number to set
        for (Sample sample : samples) {

            long s0 = TimeLog.getTime();

            incrementsProgression();

            String sampleStr = l_(locale, "t3.level1.convertSampleSetSpeciesFrequencyToWeight.sampleStr",
                                  decorate(trip), sample.getSampleNumber());

            for (SampleWell sampleWell : sample.getSampleWell()) {

                long s1 = TimeLog.getTime();

                // found a new activity to convert
                convertFrequenciesToCatWeight(sampleWell);

                getTimeLog().log(s1,
                                 "convertSampleSetSpeciesFrequencyToWeight for " +
                                 tripActivities.size() + " sets.");

            }

            // mark sample as treated for this step of level 1 treatment
            markAsTreated(sample);

            getTimeLog().log(s0, "treat sample " + sampleStr);
        }

        // mar trip as treated for this level 1 step
        markAsTreated(trip);
    }

    protected void convertFrequenciesToCatWeight(SampleWell sampleWell) throws TopiaException {

        Collection<SampleSetSpeciesFrequency> frequencies =
                sampleWell.getSampleSetSpeciesFrequency();

        Activity activity = sampleWell.getActivity();

        Date date = activity.getDate();
        Ocean activityOcean = activity.getOcean();

        // group frequencies by species
        Multimap<Species, SampleSetSpeciesFrequency> frequenciesBySpecies =
                SpeciesDAO.groupBySpecies(frequencies);

        Multimap<Species, Integer> lengthClassesBySpecies =
                ArrayListMultimap.create();

        // collect all length classes by species
        SampleSetSpeciesFrequencyDAO.collectLengthClasses(
                frequenciesBySpecies, lengthClassesBySpecies);

        // get all available weight categories
        List<WeightCategorySample> weightCategories = getWeightCategories(sampleWell);

        for (Species species : frequenciesBySpecies.keySet()) {

            // get conversion for species
            LengthWeightConversion conversion =
                    conversionHelper.getConversions(
                            species,
                            activityOcean,
                            0,
                            date
                    );

            Collection<SampleSetSpeciesFrequency> frequenciesForSpecies =
                    frequenciesBySpecies.get(species);

            List<Integer> lengthClasses =
                    Lists.newArrayList(lengthClassesBySpecies.get(species));
            Collections.sort(lengthClasses);

            Map<WeightCategorySample, CountAndWeight> countAndWeightsByCategories =
                    convertForSpecies(
                            weightCategories,
                            conversion,
                            frequenciesForSpecies,
                            lengthClasses);

            // now we can build for the given activity and species the entries
            // in SetWeight

            fill(sampleWell, species, countAndWeightsByCategories);
        }
    }

    private Map<WeightCategorySample, CountAndWeight> convertForSpecies(List<WeightCategorySample> weightCategories,
                                                                              LengthWeightConversion conversion,
                                                                              Collection<SampleSetSpeciesFrequency> frequencies,
                                                                              List<Integer> lengthClasses) throws TopiaException {

        Map<Integer, WeightCategorySample> weightCategorie =
                conversionHelper.getWeightCategoriesDistribution(
                        conversion,
                        weightCategories,
                        lengthClasses);

        Map<WeightCategorySample, CountAndWeight> result = new HashMap<>();

        T3IOUtil.fillMapWithDefaultValue(result,
                                         weightCategorie.values(),
                                         T3Suppliers.COUNT_AND_WEIGHT_DEFAULT_VALUE);

        for (SampleSetSpeciesFrequency setSize : frequencies) {

            // get length class
            int lfLengthClass = setSize.getLfLengthClass();

            // find the correct category for this length class
            WeightCategorySample weightCategory = weightCategorie.get(lfLengthClass);

            CountAndWeight countAndWeight = result.get(weightCategory);

            Float number = setSize.getNumber();

            countAndWeight.addCount(number);
            // convert to weight from lf length class
            countAndWeight.addWeight(number *
                      conversion.computeWeightFromLFLengthClass(lfLengthClass));

        }

        return result;
    }

    private void fill(SampleWell sampleWell,
                      Species species,
                      Map<WeightCategorySample, CountAndWeight> countAndWeightsByCategories) throws TopiaException {

        for (Map.Entry<WeightCategorySample, CountAndWeight> e2 :
                countAndWeightsByCategories.entrySet()) {
            WeightCategorySample categoryTreatment = e2.getKey();

            CountAndWeight countAndWeight  = e2.getValue();
            float count = countAndWeight.getCount();

            // weight must be stored in tons but are in kilogramms
            Float weight = countAndWeight.getWeight() / 1000;

            SampleSetSpeciesCatWeight setSpeciesCatWeight = sampleSetSpeciesCatWeightDAO.create(
                    SetSpeciesCatWeight.PROPERTY_SPECIES, species,
                    SetSpeciesCatWeight.PROPERTY_WEIGHT_CATEGORY_SAMPLE, categoryTreatment,
                    SetSpeciesCatWeight.PROPERTY_WEIGHT, weight,
                    SetSpeciesCatWeight.PROPERTY_COUNT, count
            );
            sampleWell.addSampleSetSpeciesCatWeight(setSpeciesCatWeight);
        }
    }
}