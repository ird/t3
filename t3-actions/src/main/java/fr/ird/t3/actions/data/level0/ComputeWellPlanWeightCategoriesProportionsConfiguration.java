/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

/**
 * Configuration of a compute well plan weight categories proportions action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see ComputeWellPlanWeightCategoriesProportionsAction
 * @since 1.2
 */
public class ComputeWellPlanWeightCategoriesProportionsConfiguration extends AbstractLevel0Configuration {

    private static final long serialVersionUID = 1L;

    public ComputeWellPlanWeightCategoriesProportionsConfiguration() {
        super(Level0Step.COMPUTE_WELL_PLAN_WEIGHT_CATEGORIES_PROPORTIONS);
    }
}
