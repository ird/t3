/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.RfUsageStatus;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeight;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategorySampleImpl;
import fr.ird.t3.entities.reference.WeightCategoryWellPlan;
import fr.ird.t3.entities.reference.WeightCategoryWellPlanDAO;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Extrapolate weigth from sample to their owing set.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ExtrapolateSampleWeightToSetAction extends AbstractLevel1Action {

    /** Logger. */
    private static final Log log = LogFactory.getLog(
            ExtrapolateSampleWeightToSetAction.class);

    @InjectDAO(entityType = LengthWeightConversion.class)
    protected LengthWeightConversionDAO lengthWeightConversionDAO;

    @InjectDAO(entityType = SetSpeciesFrequency.class)
    protected SetSpeciesFrequencyDAO setSpeciesFrequencyDAO;

    @InjectDAO(entityType = WeightCategoryWellPlan.class)
    protected WeightCategoryWellPlanDAO weightCategoryWellPlanDAO;

    protected WeightCategoryWellPlan categoryM10;

    protected WeightCategoryWellPlan categoryP10;

    protected int nbTreatedSets;

    protected float nbTreatedFishesInSamples;

    protected float nbCreatedFishesInSetSpeciesFrequency;

    protected LengthWeightConversionHelper conversionHelper;

    public ExtrapolateSampleWeightToSetAction() {
        super(Level1Step.EXTRAPOLATE_SAMPLE_WEIGHT_TO_SET);
    }

    public int getNbTreatedSets() {
        return nbTreatedSets;
    }

    public float getNbTreatedFishesInSamples() {
        return nbTreatedFishesInSamples;
    }

    public float getNbCreatedFishesInSetSpeciesFrequency() {
        return nbCreatedFishesInSetSpeciesFrequency;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        // get the -10Kg category
        setCategoryM10(weightCategoryWellPlanDAO.findByCode(1));

        // get the +10Kg category
        setCategoryP10(weightCategoryWellPlanDAO.findByCode(2));

        conversionHelper = lengthWeightConversionDAO.newConversionHelper();
    }

    @Override
    protected void deletePreviousData() throws TopiaException {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        Set<Trip> trips = samplesByTrip.keySet();
        setNbSteps(samplesByTrip.size() + trips.size());
        for (Trip trip : trips) {

            Collection<Sample> samples = samplesByTrip.get(trip);

            logTreatedAndNotSamplesforATrip(trip, samples);

            long s0 = TimeLog.getTime();

            doExecuteTrip(trip, samples);

            String tripStr = decorate(trip, DecoratorService.WITH_ID);

            getTimeLog().log(s0, "treat trip", tripStr);

            // flush transaction otherwise too much data in memory
            flushTransaction("Flush transaction for trip", tripStr);
        }
        return true;
    }

    protected void doExecuteTrip(Trip trip, Collection<Sample> samples) throws TopiaException {

        incrementsProgression();

        // get the length class +10kg limit
        Map<Species, Integer> limitLengthClassBySpecie =
                getThredHoldPlus10ForSpecies(trip, samples, ocean);

        Set<Activity> tripActivities = Sets.newHashSet();

        // extrapolate for each sample set number to set
        for (Sample sample : samples) {

            long s0 = TimeLog.getTime();

            incrementsProgression();

            String sampleStr = l_(locale, "t3.level1.extrapolateSampleWeightToSet.sampleStr",
                    decorate(trip), sample.getSampleNumber());

            float nb = sample.getTotalStandardiseSampleSpeciesFrequencyNumber();

            addInfoMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.sample.nbFishes",
                    sampleStr, nb));
            nbTreatedFishesInSamples += nb;

            for (SampleWell sampleWell : sample.getSampleWell()) {

                Activity activity = sampleWell.getActivity();
                boolean added = tripActivities.add(activity);

                if (added) {

                    // remove all stuff from activity
                    activity.clearSetSpeciesFrequency();
                    activity.clearSetSpeciesCatWeight();
                }

                doExecuteSampleWell(
                        activity,
                        sampleWell,
                        sampleStr,
                        limitLengthClassBySpecie);

            }

            // mark sample as treated for this step of level 1 treatment
            markAsTreated(sample);

            getTimeLog().log(s0, "treat sample ", sampleStr);
        }

        nbTreatedSets += tripActivities.size();

        // mar trip as treated for this level 1 step
        markAsTreated(trip);

    }

    protected void doExecuteSampleWell(Activity activity,
                                       SampleWell sampleWell,
                                       String sampleStr,
                                       Map<Species, Integer> limitLengthClassBySpecie) throws TopiaException {

        String sampleWellStr = l_(locale, "t3.level1.extrapolateSampleWeightToSet.sampleWellStr",
                sampleStr, activity.getDate());

        //compute sample well sample weight
        SampleWellSampleWeight sampleWellSampleWeight = computeSampleWellSampleWeight(sampleWellStr, sampleWell);

        // compute sample well set weight
        SampleWellSetWeight sampleWellSetWeight = computeSampleWellSetWeight(sampleWellStr, sampleWell);

        boolean useRfMins10AndRfPlus10 = getConfiguration().isUseRfMins10AndRfPlus10();

        // compute the rf context
        RFContext rfContext = computeRFContext(sampleWellStr, sampleWellSetWeight, sampleWellSampleWeight, useRfMins10AndRfPlus10);

        // keep rf data + status in the sample well
        sampleWell.setRfMinus10(rfContext.getRfMinus10());
        sampleWell.setRfMinus10UsageStatus(rfContext.getRfMinus10Status());
        sampleWell.setRfPlus10(rfContext.getRfPlus10());
        sampleWell.setRfPlus10UsageStatus(rfContext.getRfPlus10Status());
        /*
        0 : accepté rf-10
        1 : rejeté car non utilisation des rf-10 / rf+10
        2 : rejeté car rf-10 non présent (non défini)
        3 : rejeté car rf-10 vaut 0
        4 : rejeté car rf-10 trop grand (dépasse 500)
        5 : rejeté car rf-10 trop peu de poisson dans l'échantillon

         */
        sampleWell.setRfTot(rfContext.getRfTot());

        // fill the SetSpecieFrequency table
        fillSetSpeciesFrequency(
                activity,
                limitLengthClassBySpecie,
                sampleWell,
                rfContext);

    }

    /**
     * Given a sample well, compute the {@link SampleWellSampleWeight}, says :
     * <ul>
     * <li>his total weight</<li>
     * <li>his weight of -10Kg category</<li>
     * <li>his weight of +10Kg category</<li>
     * </ul>
     *
     * @param sampleWellStr decoration of a sample well
     * @param sampleWell    sampleWell of fishes
     * @return the computed weight of the sample
     * @throws TopiaException if any pb while finding convertors
     * @see SampleWellSampleWeight
     */
    protected SampleWellSampleWeight computeSampleWellSampleWeight(
            String sampleWellStr,
            SampleWell sampleWell) throws TopiaException {

        float pdechm = 0;
        float pdechp = 0;
        float plus10Number = 0;
        float minus10Number = 0;

        for (SampleSetSpeciesCatWeight sampleSetSpeciesCatWeight : sampleWell.getSampleSetSpeciesCatWeight()) {

            WeightCategorySample weightCategorySample = sampleSetSpeciesCatWeight.getWeightCategorySample();
            if (WeightCategories.isMinus10Category(weightCategorySample)) {

                pdechm += sampleSetSpeciesCatWeight.getWeight();
                minus10Number += sampleSetSpeciesCatWeight.getCount();

            } else if (WeightCategories.isPlus10Category(weightCategorySample)) {

                pdechp += sampleSetSpeciesCatWeight.getWeight();
                plus10Number += sampleSetSpeciesCatWeight.getCount();

            } else {
                throw new IllegalStateException("weightCategorySample must plus -10Kg, or +10Kg, but was: " + weightCategorySample);
            }

        }

        SampleWellSampleWeight result = SampleWellSampleWeight.create(
                pdechm,
                pdechp,
                minus10Number,
                plus10Number);

        String message =
                l_(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sampleWell.sampleWellSampleWeight",
                        sampleWellStr,
                        result.getMinus10Weight(),
                        result.getPlus10Weight(),
                        result.getTotalWeight(),
                        result.getMinus10Number(),
                        result.getPlus10Number());
        addInfoMessage(message);
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
        return result;
    }

    /**
     * Given a sample well, compute the {@link SampleWellSetWeight}, says :
     * <ul>
     * <li>his total weight</<li>
     * <li>his weight of -10Kg category</<li>
     * <li>his weight of +10Kg category</<li>
     * </ul>
     *
     * @param sampleWellStr decoration of a sample well
     * @param sampleWell    sampleWell of fishes
     *                      a more +10Kg weight for each species
     * @return the computed weight of the sample
     * @throws TopiaException if any pb while finding convertors
     * @see SampleWellSampleWeight
     */
    protected SampleWellSetWeight computeSampleWellSetWeight(
            String sampleWellStr,
            SampleWell sampleWell) throws TopiaException {

        float pondt = sampleWell.getWeightedWeight();
        Float pondm = sampleWell.getWeightedWeightMinus10();
        Float pondp = sampleWell.getWeightedWeightPlus10();

        SampleWellSetWeight result = SampleWellSetWeight.create(
                pondt,
                pondm,
                pondp);

        String message =
                l_(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sampleWell.sampleWellSetWeight",
                        sampleWellStr,
                        result.getMinus10Weight(),
                        result.getPlus10Weight(),
                        result.getTotalWeight());
        addInfoMessage(message);
        if (log.isDebugEnabled()) {
            log.debug(message);
        }

        return result;
    }

    /**
     * Given the {@code sampleSet} compute his {@code rf context}.
     *
     * @param sampleWellStr           sampleWell decorate value
     * @param setWeight               the sampleSet weight computed
     * @param sampletWeight           the sampleSet weight computed
     * @param useRfMinus10AndRfPlus10 flag to try to use rf-10 and rf+10
     * @return the computed rf context for the given sample set
     * @throws TopiaException if any pb while requesting the database
     */
    protected RFContext computeRFContext(String sampleWellStr,
                                         SampleWellSetWeight setWeight,
                                         SampleWellSampleWeight sampletWeight,
                                         boolean useRfMinus10AndRfPlus10) throws TopiaException {


        float pondt = setWeight.getTotalWeight();
        Float pondp = setWeight.getPlus10Weight();
        Float pondm = setWeight.getMinus10Weight();

        // compute rftot
        float pdecht = sampletWeight.getTotalWeight();
        float rftot = pondt / pdecht;

        // compute rfMinus10
        Float pdechm = sampletWeight.getMinus10Weight();
        Float rfMinus10 = null;
        if (pdechm != null && pdechm > 0 && pondm != null) {
            rfMinus10 = pondm / pdechm;
        }

        // compute rfPlus10
        Float pdechp = sampletWeight.getPlus10Weight();
        Float rfPlus10 = null;
        if (pdechp != null && pdechp > 0 && pondp != null) {
            rfPlus10 = pondp / pdechp;
        }

        if (rftot > getConfiguration().getRfTotMax()) {
            addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rftot.too.high",
                    sampleWellStr, rftot));
        }

        RfUsageStatus rfMinus10Status = RfUsageStatus.ACCEPTED;
        RfUsageStatus rfPlus10Status = RfUsageStatus.ACCEPTED;

        if (!useRfMinus10AndRfPlus10) {

            rfMinus10Status = rfPlus10Status = RfUsageStatus.REJECTED_BY_CONFIGURATION;

        } else {

            if (rfMinus10 == null) {

                addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.notDefined",
                        sampleWellStr, rftot));
                rfMinus10Status = RfUsageStatus.REJECTED_RF_NOT_DEFINED;

            } else {

                int rfMinus10MinNumber = getConfiguration().getRfMinus10MinNumber();
                Float sampletWeightMinus10Number = sampletWeight.getMinus10Number();

                if (Math.abs(rfMinus10) < 0.001f) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.too.low",
                            sampleWellStr, rfMinus10, rftot));
                    rfMinus10Status = RfUsageStatus.REJECTED_RF_IS_ZERO;

                } else if (rfMinus10 > getConfiguration().getRfMinus10Max()) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.too.high",
                            sampleWellStr, rfMinus10, rftot));
                    rfMinus10Status = RfUsageStatus.REJECTED_RF_TOO_HIGH;

                } else if (sampletWeightMinus10Number < rfMinus10MinNumber) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.no.enough.sample.count",
                            sampleWellStr, rfPlus10, rftot, sampletWeightMinus10Number, rfMinus10MinNumber));
                    rfMinus10Status = RfUsageStatus.REJECTED_NOT_ENOUGH_SAMPLE_COUNT;

                }

            }

            if (rfPlus10 == null) {

                addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.notDefined",
                        sampleWellStr, rftot));
                rfPlus10Status = RfUsageStatus.REJECTED_RF_NOT_DEFINED;

            } else {

                int rfPlus10MinNumber = getConfiguration().getRfPlus10MinNumber();
                Float sampletWeightPlus10Number = sampletWeight.getPlus10Number();

                if (Math.abs(rfPlus10) < 0.001f) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.too.low",
                            sampleWellStr, rfPlus10, rftot));
                    rfPlus10Status = RfUsageStatus.REJECTED_RF_IS_ZERO;

                } else if (rfPlus10 > getConfiguration().getRfPlus10Max()) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.too.high",
                            sampleWellStr, rfPlus10, rftot));
                    rfPlus10Status = RfUsageStatus.REJECTED_RF_TOO_HIGH;

                } else if (sampletWeightPlus10Number < rfPlus10MinNumber) {

                    addWarningMessage(l_(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.no.enough.sample.count",
                            sampleWellStr, rfPlus10, rftot, sampletWeightPlus10Number, rfPlus10MinNumber));
                    rfPlus10Status = RfUsageStatus.REJECTED_NOT_ENOUGH_SAMPLE_COUNT;

                }

            }

        }

        RFContext rfContext = RFContext.create(rftot, rfMinus10, rfPlus10, rfMinus10Status, rfPlus10Status);

        String message = l_(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sample.rfContext",
                sampleWellStr, rfContext.getRfTot(), rfContext.getRfMinus10(),
                rfContext.getRfPlus10());
        addInfoMessage(message);
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
        return rfContext;
    }

    public void fillSetSpeciesFrequency(
            Activity activity,
            Map<Species, Integer> limitLengthClassBySpecies,
            SampleWell sampleWell,
            RFContext rfContext) throws TopiaException {

        // group sample set frequencies by species
        Multimap<Species, SampleSetSpeciesFrequency> sampleSetSpeciesFrequencyBySpecies =
                SpeciesDAO.groupBySpecies(sampleWell.getSampleSetSpeciesFrequency());

        for (Species species : sampleSetSpeciesFrequencyBySpecies.keySet()) {

            Integer threshold = limitLengthClassBySpecies.get(species);

            Map<Integer, SetSpeciesFrequency> setSpeciesFrequencyMap =
                    setSpeciesFrequencyDAO.findAllByActivityAndSpeciesOrderdyByLengthClass(activity, species);

            for (SampleSetSpeciesFrequency sampleSetSpeciesFrequency : sampleSetSpeciesFrequencyBySpecies.get(species)) {

                Float number = sampleSetSpeciesFrequency.getNumber();
                int lfLengthClass = sampleSetSpeciesFrequency.getLfLengthClass();

                float rf = rfContext.computeRf(lfLengthClass, threshold);

                float value = rf * number;

                SetSpeciesFrequency setSpeciesFrequency = setSpeciesFrequencyMap.get(lfLengthClass);

                if (setSpeciesFrequency == null) {
                    // new entry
                    setSpeciesFrequency = setSpeciesFrequencyDAO.create(
                            SetSpeciesFrequency.PROPERTY_SPECIES, species,
                            SetSpeciesFrequency.PROPERTY_LF_LENGTH_CLASS, lfLengthClass,
                            SetSpeciesFrequency.PROPERTY_NUMBER, value);
                    activity.addSetSpeciesFrequency(setSpeciesFrequency);
                    setSpeciesFrequencyMap.put(lfLengthClass, setSpeciesFrequency);
                    nbCreatedFishesInSetSpeciesFrequency += value;
                } else {

                    // sum value
                    setSpeciesFrequency.setNumber(setSpeciesFrequency.getNumber() + value);
                }
            }
        }
    }

    /**
     * Obtain for each species used in one of the given sample the first length
     * class which represents a +10Kg weight.
     *
     * @param trip    the trip where samples are done
     * @param samples the sample to scan
     * @param ocean   ocean where trip happens
     * @return the universe of legnth class limits computed indexed by species
     * @throws TopiaException if any db problems while requesting
     */
    public Map<Species, Integer> getThredHoldPlus10ForSpecies(Trip trip,
                                                              Collection<Sample> samples,
                                                              Ocean ocean) throws TopiaException {

        Set<Species> species = SpeciesDAO.getAllSpeciesFromSampleSpecies(samples);

        // for each of those species, found the length class wich matches a
        // weight > 10Kg
        Map<Species, Integer> result = Maps.newHashMap();
        for (Species specie : species) {

            LengthWeightConversion conversion = conversionHelper.getConversions(
                    specie,
                    ocean,
                    0,
                    trip.getLandingDate());

            if (conversion != null) {

                // only add conversion for species if found

                WeightCategorySample fakeCategory = new WeightCategorySampleImpl();
                fakeCategory.setMax(10);

                int lengthClass = conversionHelper.getSpecieHighestLengthClass(conversion, fakeCategory);
                result.put(specie, lengthClass);

            }
        }

        if (log.isDebugEnabled()) {
            for (Map.Entry<Species, Integer> entry : result.entrySet()) {
                log.debug("Species " + entry.getKey().getCode() +
                        " - limit 10Kg length class : " + entry.getValue());

            }
        }
        return result;
    }

    /**
     * This object contains the sample weights (-10,+10 and total) for sample well.
     * <ul>
     * <li>{@link #getMinus10Number()} is legacy {@code pdechm}</li>
     * <li>{@link #getPlus10Number()}} is legacy {@code pdechp}</li>
     * <li>{@link #getTotalWeight()} is legacy {@code pdecht}</li>
     * </ul>
     * We keep also here the number of fishes (-10,+10) for the sample well.
     */
    protected static class SampleWellSampleWeight {

        protected static SampleWellSampleWeight create(
                Float minus10Weight,
                Float plus10Weight,
                Float minus10Number,
                Float plus10Number) {
            SampleWellSampleWeight result = new SampleWellSampleWeight();
            result.minus10Number = minus10Number;
            result.plus10Number = plus10Number;
            result.minus10Weight = minus10Weight;
            result.plus10Weight = plus10Weight;
            return result;
        }

        /** sum of fishes wieght for length class < +10Kg. */
        protected Float minus10Weight;

        /** sum of fishes wieght for length class > +10Kg. */
        protected Float plus10Weight;

        /** Number of fishes for length class > -10Kg. */
        private Float plus10Number;

        /** Number of fishes for length class < -10Kg. */
        private Float minus10Number;

        public Float getTotalWeight() {
            return minus10Weight + plus10Weight;
        }

        public Float getMinus10Weight() {
            return minus10Weight;
        }

        public Float getPlus10Weight() {
            return plus10Weight;
        }

        public Float getPlus10Number() {
            return plus10Number;
        }

        public Float getMinus10Number() {
            return minus10Number;
        }
    }

    /**
     * This object contains the set weights (-10,+10 and total) for
     * sample well.
     * <ul>
     * <li>{@link #getMinus10Weight()} is legacy {@code pondm}</li>
     * <li>{@link #getPlus10Weight()}} is legacy {@code pondp}</li>
     * <li>{@link #getTotalWeight()} is legacy {@code pondt}</li>
     * </ul>
     */
    protected static class SampleWellSetWeight {

        protected static SampleWellSetWeight create(
                Float totalWeight,
                Float minus10Weight,
                Float plus10Weight
        ) {
            SampleWellSetWeight result = new SampleWellSetWeight();
            result.totalWeight = totalWeight;
            result.minus10Weight = minus10Weight;
            result.plus10Weight = plus10Weight;
            return result;
        }

        /** sum of fishes wieght for length class < +10Kg. */
        protected Float totalWeight;

        /** sum of fishes wieght for length class < +10Kg. */
        protected Float minus10Weight;

        /** sum of fishes wieght for length class > +10Kg. */
        protected Float plus10Weight;

        public Float getTotalWeight() {
            return totalWeight;
        }

        public Float getMinus10Weight() {
            return minus10Weight;
        }

        public Float getPlus10Weight() {
            return plus10Weight;
        }
    }

    protected static class RFContext {

        public static RFContext create(Float rfTot,
                                       Float rfm10,
                                       Float rfp10,
                                       RfUsageStatus rfMinus10Status,
                                       RfUsageStatus rrPlus10Status) {
            RFContext result = new RFContext();
            result.rfTot = rfTot;
            result.rfMinus10 = rfm10;
            result.rfPlus10 = rfp10;
            result.rfMinus10Status = rfMinus10Status;
            result.rrPlus10Status = rrPlus10Status;
            result.useRfMinus10OrRfPlus10 =
                    rfMinus10Status == RfUsageStatus.ACCEPTED && rrPlus10Status == RfUsageStatus.ACCEPTED;

            return result;
        }

        /**
         * Rf tot value.
         */
        protected Float rfTot;

        /**
         * Rf minus 10 value.
         */
        protected Float rfMinus10;

        /**
         * Rf plus 10 value.
         */
        protected Float rfPlus10;

        /**
         * Rf minus 10 usage status.
         */
        protected RfUsageStatus rfMinus10Status;

        /**
         * Rf plus 10 usage status.
         */
        protected RfUsageStatus rrPlus10Status;

        /**
         * Try to use rfMinus10/RfPlus10 or always use rfTot
         */
        protected boolean useRfMinus10OrRfPlus10;

        public Float getRfTot() {
            return rfTot;
        }

        public Float getRfMinus10() {
            return rfMinus10;
        }

        public Float getRfPlus10() {
            return rfPlus10;
        }

        public RfUsageStatus getRfMinus10Status() {
            return rfMinus10Status;
        }

        public RfUsageStatus getRfPlus10Status() {
            return rrPlus10Status;
        }

        public Float computeRf(int lfLengthClass, int plus10LengthClass) {

            Float result = null;

            if (useRfMinus10OrRfPlus10) {

                if (lfLengthClass <= plus10LengthClass) {

                    // use rf-10
                    result = rfMinus10;

                } else {

                    // use rf+10
                    result = rfPlus10;

                }

            } else {

                // use only the rfTot
                result = rfTot;

            }

            return result;
        }
    }

    public void setCategoryM10(WeightCategoryWellPlan categoryM10) {
        this.categoryM10 = categoryM10;
    }

    public void setCategoryP10(WeightCategoryWellPlan categoryP10) {
        this.categoryP10 = categoryP10;
    }
}
