/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequency;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequencyDAO;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesCatWeightDAO;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyDAO;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentDAO;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.models.LengthCompositionAggregateModel;
import fr.ird.t3.models.LengthCompositionModel;
import fr.ird.t3.models.LengthCompositionModelHelper;
import fr.ird.t3.models.SpeciesCountAggregateModel;
import fr.ird.t3.models.SpeciesCountModel;
import fr.ird.t3.models.SpeciesCountModelHelper;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ZoneStratumService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import fr.ird.t3.services.ioc.InjectEntityById;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaId;
import org.nuiton.util.TimeLog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;
import static org.nuiton.i18n.I18n.n_;

/**
 * Level 3 treatment action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level3Action extends T3Action<Level3Configuration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Level3Action.class);

    @InjectDAO(entityType = Activity.class)
    protected ActivityDAO activityDAO;

    @InjectDAO(entityType = LengthWeightConversion.class)
    protected LengthWeightConversionDAO lengthWeightConversionDAO;

    @InjectDAO(entityType = WeightCategoryTreatment.class)
    protected WeightCategoryTreatmentDAO weightCategoryTreatmentDAO;

    @InjectDAO(entityType = SetSpeciesFrequency.class)
    protected SetSpeciesFrequencyDAO setSpeciesFrequencyDAO;

    @InjectDAO(entityType = SetSpeciesCatWeight.class)
    protected SetSpeciesCatWeightDAO setSpeciesCatWeightDAO;

    @InjectDAO(entityType = ExtrapolatedAllSetSpeciesFrequency.class)
    protected ExtrapolatedAllSetSpeciesFrequencyDAO extraploatedAllSetSpeciesFrequencyDAO;

    @InjectDAO(entityType = Vessel.class)
    protected VesselDAO vesselDAO;

    protected ZoneStratumAwareMeta zoneMeta;

    protected ZoneVersion zoneVersion;

    @InjectFromDAO(entityType = SchoolType.class, method = "findAllForStratum")
    protected Set<SchoolType> schoolTypes;

    protected Multimap<SchoolType, ZoneStratumAware> zoneBySchoolType;

    @InjectEntityById(entityType = Ocean.class)
    protected Ocean ocean;

    @InjectEntityById(entityType = Country.class)
    protected Country catchFleet;

    @InjectEntitiesById(entityType = Species.class, path = "configuration.speciesIds")
    protected Collection<Species> species;

    @InjectEntitiesById(entityType = Country.class)
    protected Collection<Country> sampleFleets;

    @InjectEntitiesById(entityType = Country.class)
    protected Collection<Country> sampleFlags;

    protected Set<T3Date> startDates;

    protected Multimap<SchoolType, WeightCategoryTreatment>
            weightCategoriesBySchoolType;

    protected Set<Vessel> possibleCatchVessels;

    protected Set<Vessel> possibleSampleVessels;

    protected LengthWeightConversionHelper conversionHelper;

    protected Map<SchoolType, Map<String, Integer>>
            stratumMinimumSampleCountBySchoolType;

    protected int nbStratums;

    protected long nbCatchActivities;

    protected long nbCatchActivitiesWithSample;

    public static final String BEFORE_LEVEL3 = n_("t3.level3.nbFishesBeforeLevel3");

    public static final String AFTER_LEVEL3 = n_("t3.level3.nbFishesAfterLevel3");

    /**
     * Global total fishes count for all stratums.
     * <p/>
     * We store two count here :
     * <ul>
     * <li>{@link #BEFORE_LEVEL3} : total fishes count before level 3</li>
     * <li>{@link #AFTER_LEVEL3} : total fishes count after level 3</li>
     * </ul>
     *
     * @since 1.3.1
     */
    protected SpeciesCountAggregateModel totalFishesCount;

    /**
     * For each stratum gets his result.
     *
     * @since 1.3.1
     */
    protected Collection<L3StratumResult> stratumsResult;

    /**
     * Predicate to filter only species selected in configuration.
     *
     * @since 1.4
     */
    protected Predicate<SpeciesAware> speciesToFixFilter;

    /**
     * Cache of activity (to avoid to reload them for catch and smaple stratum).
     *
     * @since 1.4
     */
    protected final LoadingCache<String, Activity> activityCache;

    protected String topiaCreateDate;

    public static final String INSERT_QUERY =
            "INSERT INTO ExtrapolatedAllSetSpeciesFrequency(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, ACTIVITY, SPECIES, LFLENGTHCLASS, NUMBER) VALUES ('%s', 0, TIMESTAMP '%s 00:00:00.0', '%s','%s', %s, %s);\n";

    public static final String UPDATE_QUERY =
            "UPDATE ExtrapolatedAllSetSpeciesFrequency SET NUMBER = NUMBER + %s WHERE TOPIAID = '%s';\n";

    protected int nbQuery;

    protected StringBuilder queryBuffer = new StringBuilder();

    public Level3Action() {
        activityCache = CacheBuilder.newBuilder().build(
                new CacheLoader<String, Activity>() {
                    @Override
                    public Activity load(String key) throws Exception {
                        return activityDAO.findByTopiaId(key);
                    }
                }
        );
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        totalFishesCount = new SpeciesCountAggregateModel();

        Level3Configuration configuration = getConfiguration();

        configuration.setLocale(getLocale());

        // get zones type meta to use
        setZoneMeta(
                newService(ZoneStratumService.class).getZoneMetaById(configuration.getZoneTypeId()));

        setZoneVersion(zoneMeta.getZoneVersion(
                configuration.getZoneVersionId(), getTransaction()
        ));

        // get all zones by ocean and shcool types
        setZoneBySchoolType(
                zoneMeta.getZones(
                        ocean,
                        schoolTypes,
                        zoneVersion,
                        getTransaction()
                )
        );

        // get start dates to use
        setStartDates(T3Date.getStartDates(
                configuration.getBeginDate(),
                configuration.getEndDate(),
                configuration.getTimeStep()
        ));

        // get weight categories to use (group by school type)
        setWeightCategoriesBySchoolType(
                weightCategoryTreatmentDAO.getWeightCategories(
                        ocean,
                        schoolTypes
                )
        );

        // get possible vessels for catch stratum
        setPossibleCatchVessels(vesselDAO.getPossibleCatchVessels(catchFleet));

        // get possible vessels for sample stratum
        setPossibleSampleVessels(
                vesselDAO.getPossibleSampleVessels(sampleFleets, sampleFlags));

        conversionHelper = lengthWeightConversionDAO.newConversionHelper(
                ocean,
                0,
                configuration.getBeginDate().toBeginDate());

        stratumsResult = Sets.newLinkedHashSet();

        topiaCreateDate = new SimpleDateFormat("yyyy-MM-dd").format(
                serviceContext.getCurrentDate());

        // prepare for each school type all minimum sample count by species
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount =
                configuration.getStratumMinimumSampleCount();

        Map<SchoolType, Map<String, Integer>> minimumSampleCountBySchoolType =
                Maps.newHashMap();

        for (SchoolType schoolType : schoolTypes) {

            Map<String, Integer> bySpecies = Maps.newTreeMap();
            minimumSampleCountBySchoolType.put(schoolType, bySpecies);

            int code = schoolType.getCode();
            switch (code) {
                case 1:

                    // Object school type
                    for (Map.Entry<String, StratumMinimumSampleCount> e :
                            stratumMinimumSampleCount.entrySet()) {
                        bySpecies.put(
                                e.getKey(),
                                e.getValue().getMinimumCountForObjectSchool()
                        );
                    }
                    break;

                case 2:

                    // Free school type
                    for (Map.Entry<String, StratumMinimumSampleCount> e :
                            stratumMinimumSampleCount.entrySet()) {
                        bySpecies.put(
                                e.getKey(),
                                e.getValue().getMinimumCountForFreeSchool()
                        );
                    }
                    break;
                default:
                    throw new IllegalStateException(
                            "Can not use the school fish " + code);

            }
        }
        setStratumMinimumSampleCountBySchoolType(
                minimumSampleCountBySchoolType);
    }

    @Override
    protected void deletePreviousData() {
        // data will be removed from selected activities
    }

    @Override
    protected void finalizeAction() throws TopiaException, IOException {

        activityCache.invalidateAll();
        activityWithMultiZones.clear();

        super.finalizeAction();
    }

    @Override
    protected boolean executeAction() throws Exception {

        speciesToFixFilter = T3Predicates.newSpeciesContainsFilter(species);

        Level3Configuration configuration = getConfiguration();

        // get time step
        int timeStep = configuration.getTimeStep();

        nbStratums = zoneBySchoolType.size() * startDates.size();
        setNbSteps(3 * nbStratums);

        // keep a track of alreay used activity ids (to remove previous level 3 data)
        Set<String> usedActivityIds = Sets.newHashSet();

        int stratumIndex = 0;
        for (SchoolType schoolType : schoolTypes) {

            // get all weight categories for this school type
            List<WeightCategoryTreatment> weightCategories =
                    Lists.newArrayList(weightCategoriesBySchoolType.get(schoolType));
            weightCategoryTreatmentDAO.sort(weightCategories);

            // get all zones for this school type
            Collection<ZoneStratumAware> zones = zoneBySchoolType.get(schoolType);

            // get minimum stratum count by species
            Map<String, Integer> stratumMinimumCountBySpecie =
                    stratumMinimumSampleCountBySchoolType.get(schoolType);

            for (ZoneStratumAware zone : zones) {

                for (T3Date startDate : startDates) {

                    // get end date (only increments on timseStep - 1 to have
                    // exactly timstep month from beginDate.toStartDate() to
                    // endDate.toEndDate()
                    T3Date endDate = startDate.incrementsMonths(timeStep - 1);

                    // clear query buffer
                    queryBuffer = new StringBuilder();
                    nbQuery = 0;

                    StratumConfiguration<Level3Configuration>
                            stratumConfiguration =
                            StratumConfiguration.newStratumConfiguration(
                                    configuration,
                                    zoneMeta,
                                    zone,
                                    schoolType,
                                    startDate,
                                    endDate,
                                    zones,
                                    possibleCatchVessels,
                                    possibleSampleVessels,
                                    stratumMinimumCountBySpecie,
                                    activityCache);

                    try {
                        L3StratumResult result = doExecuteStratum(
                                stratumConfiguration,
                                weightCategories,
                                stratumIndex,
                                usedActivityIds);

                        stratumsResult.add(result);
                    } finally {

                        // first flush delete previous level 3 data
                        flushTransaction("Flush delete level 3 data.");

                        // flush then new data
                        long s0 = TimeLog.getTime();
                        if (nbQuery > 0) {
                            if (log.isInfoEnabled()) {
                                log.info("Will flush " + nbQuery + " queries.");
                            }
                            getTransaction().executeSQL(queryBuffer.toString());
                        }
                        getTimeLog().log(s0, "Execute queries");
                    }

                    stratumIndex++;
                }
            }
        }
        return true;
    }

//    Map<String, Integer> hitActivities = Maps.newTreeMap();

    Map<String, Map<String, String>> activityWithMultiZones = Maps.newTreeMap();

    protected L3StratumResult doExecuteStratum(
            StratumConfiguration<Level3Configuration> conf,
            List<WeightCategoryTreatment> weightCategories,
            int stratumIndex,
            Set<String> usedActivityIds) throws Exception {

        incrementsProgression();

        String stratumPrefix =
                l_(locale, "t3.level3.stratumLibelle",
                        stratumIndex, nbStratums,
                        decorate(conf.getSchoolType()),
                        decorate(conf.getZone()),
                        conf.getBeginDate(),
                        conf.getEndDate()
                );

        L3StratumResult result = new L3StratumResult(conf, stratumPrefix);

        String message =
                l_(locale, "t3.level3.message.start.stratum", stratumPrefix);

        if (log.isInfoEnabled()) {
            log.info(message);
        }

        addInfoMessage("==============================================================================================");
        addInfoMessage(message);
        addInfoMessage("==============================================================================================");

        // get the catch stratum
        L3CatchStratum catchStratum = newCatchStratum(conf, weightCategories);

        incrementsProgression();
        L3SampleStratum sampleStratum = null;

        try {
            if (catchStratum == null) {

                // no catch in this stratum

                // mark as a 0 substitution level
                result.setSubstitutionLevel(0);

                incrementsProgression();
            } else {

                // compute sample stratum

                sampleStratum = newSampleStratum(
                        conf,
                        weightCategories,
                        catchStratum.getTotalCatchWeight());

                // get the substitution level for the sample stratum
                Integer level = sampleStratum.getSubstitutionLevel();

                if (log.isDebugEnabled()) {
                    log.debug("Sample stratum substitution level " + level);
                }

                incrementsProgression();

                if (level == -1) {

                    // sample stratum could not be found

                    // log sample stratum new composition
                    message =
                            l_(locale, "t3.level3.warning.missing.data.for.stratum",
                                    result.getLibelle());

                    if (log.isWarnEnabled()) {
                        log.warn(message);
                    }
                    addWarningMessage(message);

                    // mark it as 999 level to make it appear at top of a reverse list
                    result.setSubstitutionLevel(999);

                } else {

                    // can use this sampleStratum

                    message = l_(locale, "t3.level3.message.sampleStratum.resume",
                            level,
                            sampleStratum.getNbMergedActivities(),
                            sampleStratum.getSampleStratumTotalWeight());

                    message = LengthCompositionModelHelper.decorateModel(
                            getDecoratorService(),
                            message,
                            l_(locale, "t3.level3.fishingCountAndRate"),
                            sampleStratum.getCompositionModel());
                    if (log.isInfoEnabled()) {
                        log.info(message);
                    }
                    addInfoMessage(message);

                    int activityIndex = 1;
                    int nbActivites = catchStratum.getNbActivities();
                    for (Map.Entry<Activity, Integer> e : catchStratum) {

                        Activity activity = e.getKey();

                        int nbZones = e.getValue();

                        // is activity was already treated ?
                        boolean newActivity =
                                usedActivityIds.add(activity.getTopiaId());

                        if (!newActivity) {

                            Preconditions.checkState(nbZones > 1, "Only a multi-zone activity can be seen more tha once!, problem with activity: " + activity.getTopiaId());

//                            // mark it
//                            Integer nb = hitActivities.get(activity.getTopiaId());
//                            if (nb == null) {
//                                nb = 0;
//                            }
//                            hitActivities.put(activity.getTopiaId(), nb + 1);
                        }

                        doExecuteActivityInCatchStratum(
                                catchStratum,
                                sampleStratum,
                                activity,
                                nbZones,
                                activityIndex++,
                                weightCategories,
                                nbActivites,
                                newActivity,
                                result.getTotalFishesCount()
                        );

                        // keep the stratum substitution level in the activity
                        activity.setStratumLevelN3(level);
                        activity.setUseMeanStratumCompositionN3(true);
                    }

                    // --------------------------------------------------
                    // --- Report results
                    // --------------------------------------------------

                    result.setSubstitutionLevel(level);
                    result.addNbActivities(catchStratum.getNbActivities());
                    result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());

                    // merge input and output model from catch stratum models

                    // compute the nb fishes before n3 for all activities of stratum catches
                    // compute the nb fishes after n3 for all activities of stratum catches

                    SpeciesCountAggregateModel totalFishesCountModel = result.getTotalFishesCount();

                    for (Map.Entry<Activity, Integer> e : catchStratum) {
                        mergeActivityTotalFishesCount(e.getKey(), totalFishesCountModel);
                    }

                    // log fishes count of the stratum

                    message = SpeciesCountModelHelper.decorateModel(
                            getDecoratorService(),
                            l_(locale, "t3.level3.catchStratum.nbFishesResume.title"),
                            totalFishesCountModel.extractForSpecies(species),
                            BEFORE_LEVEL3,
                            AFTER_LEVEL3
                    );

                    if (log.isInfoEnabled()) {
                        log.info(message);
                    }

                    addInfoMessage(message);

                    // merge input and output model from catch stratum models

                    totalFishesCount.addValues(totalFishesCountModel);

                    nbCatchActivities += result.getNbActivities();
                    nbCatchActivitiesWithSample += result.getNbActivitiesWithSample();
                }
            }

            return result;
        } finally {
            IOUtils.closeQuietly(catchStratum);
            IOUtils.closeQuietly(sampleStratum);
        }
    }

    public void mergeActivityTotalFishesCount(Activity activity, SpeciesCountAggregateModel totalFishesCountModel) {
        totalFishesCountModel.addValues(BEFORE_LEVEL3, activity.getSetSpeciesFrequency());
        totalFishesCountModel.addValues(AFTER_LEVEL3, activity.getExtrapolatedAllSetSpeciesFrequency());
    }

    protected void doExecuteActivityInCatchStratum(L3CatchStratum catchStratum,
                                                   L3SampleStratum sampleStratum,
                                                   Activity activity,
                                                   int nbZones,
                                                   int activityIndex,
                                                   List<WeightCategoryTreatment> weightCategories,
                                                   int nbActivites,
                                                   boolean deleteOldData,
                                                   SpeciesCountAggregateModel totalFishesCount) throws TopiaException {

        String activityStr = decorate(activity) + " (" + decorate(activity.getTrip(), DecoratorService.WITH_ID) + ")";

        String message = l_(locale, "t3.level3.message.start.activity", activityIndex, nbActivites, activityStr, nbZones);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        // to deal with muli-zone activities (must keep a track of what was
        // already odne)
        Map<String, String> previousValues;

        boolean activityMultiZone = nbZones > 1;

        if (activityMultiZone) {

            // get previous values
            previousValues = activityWithMultiZones.get(activity.getTopiaId());

            if (previousValues == null) {

                // create previous values
                activityWithMultiZones.put(activity.getTopiaId(), previousValues = Maps.<String, String>newHashMap());
            }

        } else {
            previousValues = null;
        }

        if (deleteOldData) {

            // delete old data for this activity
            if (log.isDebugEnabled()) {
                log.debug("Delete previous level3 data of " +
                        activity.getTopiaId() + " :: " +
                        activity.sizeExtrapolatedAllSetSpeciesFrequency());
            }
//            addDeleteQuery(activity);
            activity.deleteComputedDataLevel3();
        }

        boolean activityWithSample = catchStratum.isActivityWithSample(activity);

        boolean useAllSamplesOfStratum = getConfiguration().isUseAllSamplesOfStratum();

        if (activityWithSample && !useAllSamplesOfStratum) {

            // need only to clone back data from SetSpeciesFrequency to new table

            message = l_(locale, "t3.level3.message.activity.with.sample.useOwn.composition");

            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);


            for (SetSpeciesFrequency setSpeciesFrequency : activity.getSetSpeciesFrequency()) {

                Species aSpecies = setSpeciesFrequency.getSpecies();

                SpeciesCountModel speciesCountModel = totalFishesCount.getModel(aSpecies);

                addInsertOrUpdateQuery(
                        activity,
                        previousValues,
                        aSpecies,
                        setSpeciesFrequency.getLfLengthClass(),
                        setSpeciesFrequency.getNumber() / nbZones,
                        speciesCountModel);
            }

        } else {

            if (activityWithSample) {
                message = l_(locale, "t3.level3.message.activity.with.sample.useSampleStratum.composition", activityStr);
            } else {
                message = l_(locale, "t3.level3.message.activity.with.no.sample.useSampleStratum.composition", activityStr);
            }


            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);

            Map<Species, LengthCompositionAggregateModel> compositionModels = sampleStratum.getCompositionModel();

            // get all species found in catches
            Set<Species> catchSpecies = Sets.newHashSet(ActivityDAO.getSpecies(activity.getCorrectedElementaryCatch()));

            // and keep only the one to fix
            catchSpecies.retainAll(species);

            // compute a linear representation of corrected catches (by species and weight category)
            Map<String, CorrectedElementaryCatch> linearCorrectedCatches = Maps.newTreeMap();
            for (CorrectedElementaryCatch aCatch : activity.getCorrectedElementaryCatch()) {
                String key = aCatch.getWeightCategoryTreatment().getTopiaId() + aCatch.getSpecies().getTopiaId();
                linearCorrectedCatches.put(key, aCatch);
            }

            boolean useWeightCategories = getConfiguration().isUseWeightCategories();

            // Generate for each species to fix

            for (Species aSpecies : catchSpecies) {

                // get all compositions for this species
                LengthCompositionAggregateModel compositionModel = compositionModels.get(aSpecies);

                // total count result model
                SpeciesCountModel speciesCountModel = totalFishesCount.getModel(aSpecies);

                // check this species exists in composition

                if (compositionModel == null) {

                    // missing a species in sample, can not fix this species
                    message = l_(locale, "t3.level3.warning.missing.sample.species", activityStr, decorate(aSpecies));
                    if (log.isWarnEnabled()) {
                        log.warn(message);
                    }

                    addWarningMessage(message);
                    continue;
                }

                LengthWeightConversion conversions = conversionHelper.getConversions(aSpecies);

                if (useWeightCategories) {

                    for (WeightCategoryTreatment weightCategory : weightCategories) {

                        // get total catch weight for this species and weight category
                        String key = weightCategory.getTopiaId() + aSpecies.getTopiaId();
                        CorrectedElementaryCatch aCatch = linearCorrectedCatches.get(key);
                        if (aCatch != null) {

                            // get weight to use in this stratum (in kg)
                            float totalWeight = 1000 * aCatch.getCorrectedCatchWeight() / nbZones;

                            // get composition for this weight category
                            LengthCompositionModel model = compositionModel.getModel(weightCategory);

                            generateFrequencies(activity, aSpecies, previousValues, model, totalWeight, speciesCountModel, conversions);

                        }

                    }

                } else {

                    float totalWeight = 0f;

                    for (WeightCategoryTreatment weightCategory : weightCategories) {

                        // get total catch weight for this species and weight category
                        String key = weightCategory.getTopiaId() + aSpecies.getTopiaId();
                        CorrectedElementaryCatch aCatch = linearCorrectedCatches.get(key);
                        if (aCatch != null) {

                            totalWeight += aCatch.getCorrectedCatchWeight();

                        }

                    }

                    totalWeight = 1000 * totalWeight / nbZones;

                    // get composition for all weight categories
                    LengthCompositionModel model = compositionModel.getTotalModel();

                    generateFrequencies(activity, aSpecies, previousValues, model, totalWeight, speciesCountModel, conversions);

                }

            }
        }
    }

    protected void generateFrequencies(Activity activity,
                                       Species aSpecies,
                                       Map<String, String> previousValues,
                                       LengthCompositionModel model,
                                       float totalWeight,
                                       SpeciesCountModel speciesCountModel,
                                       LengthWeightConversion conversions) {

        // get all length classes involved
        Set<Integer> lengthClasses = model.getLengthClasses();

        for (Integer lengthClass : lengthClasses) {

            // rate of this length class beyoung the weight category
            float rate = model.getRate(lengthClass);

            // weight for this length class
            float weight = totalWeight * rate;

            // weight of a fish for this length class
            float unitWeight =
                    conversions.computeWeightFromLFLengthClass(lengthClass);

            // number of fishes for this length class
            float number = weight / unitWeight;

            // create the frequency
            addInsertOrUpdateQuery(activity,
                    previousValues,
                    aSpecies,
                    lengthClass,
                    number,
                    speciesCountModel);

        }

    }

    protected L3CatchStratum newCatchStratum(
            StratumConfiguration<Level3Configuration> stratumConfiguration,
            List<WeightCategoryTreatment> weightCategories) throws Exception {
        L3CatchStratum catchStratum = new L3CatchStratum(
                stratumConfiguration, species, speciesToFixFilter);
        catchStratum.init(serviceContext, weightCategories, this);
        // get the total weight of the catch stratum
        float catchStratumWeight = catchStratum.getTotalCatchWeight();

        if (catchStratumWeight == 0) {

            // no catch in this stratum, skip it
            String message = l_(locale, "t3.level3.message.noCatch.in.stratum");
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);

            // let's nullify the catch straumt (make it no more available)
            catchStratum = null;
        } else {

            // log it
            String message = l_(locale, "t3.level3.message.catchStratum.resume",
                    catchStratum.getNbActivities(),
                    catchStratum.getTotalCatchWeight());
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);
        }
        return catchStratum;
    }

    protected L3SampleStratum newSampleStratum(StratumConfiguration<Level3Configuration> stratumConfiguration,
                                               List<WeightCategoryTreatment> weightCategories,
                                               float totalCatchWeight) throws Exception {
        L3SampleStratum sampleStratum =
                new L3SampleStratum(stratumConfiguration,
                        species,
                        totalCatchWeight,
                        conversionHelper,
                        speciesToFixFilter);
        sampleStratum.init(serviceContext, weightCategories, this);
        return sampleStratum;
    }

    protected void addInsertOrUpdateQuery(Activity activity,
                                          Map<String, String> previousValues,
                                          Species species,
                                          int lengthClass,
                                          float number,
                                          SpeciesCountModel speciesCountModel) {

        String key = species.getTopiaId() + "__" + lengthClass;
        String oldId = previousValues == null ? null : previousValues.get(key);
        String query;
        if (oldId == null) {

            // check unique key
            String uniqueKey = activity.getTopiaId() + "__" + key;

//            if (!insertCache.add(uniqueKey)) {
//
//                // key already used!!!
//                throw new IllegalStateException("Tuple (" + uniqueKey + ") already used!");
//            }
            // insert query
            String newId = TopiaId.create(ExtrapolatedAllSetSpeciesFrequency.class);
            query = String.format(
                    INSERT_QUERY,
                    newId,
                    topiaCreateDate,
                    activity.getTopiaId(),
                    species.getTopiaId(),
                    lengthClass,
                    number);

            if (previousValues != null) {

                // keep it in multi-zone
                previousValues.put(key, newId);
            }
        } else {

            // update query
            query = String.format(UPDATE_QUERY, number, oldId);
        }

        // add query
        queryBuffer.append(query);
        nbQuery++;

        // add to total result count
        speciesCountModel.addValue(AFTER_LEVEL3, lengthClass, number);
    }

    public void setZoneMeta(ZoneStratumAwareMeta zoneMeta) {
        this.zoneMeta = zoneMeta;
    }

    public void setSchoolTypes(Set<SchoolType> schoolTypes) {
        this.schoolTypes = schoolTypes;
    }

    public void setZoneBySchoolType(Multimap<SchoolType, ZoneStratumAware> zoneBySchoolType) {
        this.zoneBySchoolType = zoneBySchoolType;
    }

    public void setSpecies(Set<Species> species) {
        this.species = species;
    }

    public void setStartDates(Set<T3Date> startDates) {
        this.startDates = startDates;
    }

    public void setWeightCategoriesBySchoolType(Multimap<SchoolType, WeightCategoryTreatment> weightCategoriesBySchoolType) {
        this.weightCategoriesBySchoolType = weightCategoriesBySchoolType;
    }

    public void setPossibleCatchVessels(Set<Vessel> possibleCatchVessels) {
        this.possibleCatchVessels = possibleCatchVessels;
    }

    public void setPossibleSampleVessels(Set<Vessel> possibleSampleVessels) {
        this.possibleSampleVessels = possibleSampleVessels;
    }

    public void setStratumMinimumSampleCountBySchoolType(Map<SchoolType, Map<String, Integer>> stratumMinimumSampleCountBySchoolType) {
        this.stratumMinimumSampleCountBySchoolType = stratumMinimumSampleCountBySchoolType;
    }

    public void setZoneVersion(ZoneVersion zoneVersion) {
        this.zoneVersion = zoneVersion;
    }

    // -------------------------------------------------------------------------
    // --- Results -------------------------------------------------------------
    // -------------------------------------------------------------------------

    public long getNbCatchActivities() {
        return nbCatchActivities;
    }

    public long getNbCatchActivitiesWithSample() {
        return nbCatchActivitiesWithSample;
    }

    public int getNbStratums() {
        return nbStratums;
    }

    public int getNbStratumsFixed() {
        return stratumsResult.size();
    }

    public Integer[] getAllSubstitutionLevels() {
        Set<Integer> levels = Sets.newHashSet();
        for (L3StratumResult stratumResult : stratumsResult) {
            levels.add(stratumResult.getSubstitutionLevel());
        }
        List<Integer> result = Lists.newArrayList(levels);
        Collections.reverse(result);
        return result.toArray(new Integer[result.size()]);
    }

    public Collection<L3StratumResult> getStratumResult(int level) {
        Set<L3StratumResult> singleResult = Sets.newLinkedHashSet();
        for (L3StratumResult stratumResult : stratumsResult) {
            if (level == stratumResult.getSubstitutionLevel()) {
                singleResult.add(stratumResult);
            }
        }
        return singleResult;
    }

    public Map<Integer, Collection<L3StratumResult>> getStratumResults() {
        Multimap<Integer, L3StratumResult> result = LinkedHashMultimap.create();
        Integer[] levels = getAllSubstitutionLevels();
        for (Integer level : levels) {
            Set<L3StratumResult> singleResult = Sets.newLinkedHashSet();
            for (L3StratumResult stratumResult : stratumsResult) {
                if (level == stratumResult.getSubstitutionLevel()) {
                    singleResult.add(stratumResult);
                }
            }
            result.putAll(level, singleResult);
        }
        return result.asMap();
    }

    public int getMaximumSizeForStratum() {
        int result = 0;
        for (L3StratumResult stratumResult : stratumsResult) {
            result = Math.max(result, stratumResult.getLibelle().length() + 1);
        }
        return result;
    }

    public String getTotalFishesCountResume() {

        String result = SpeciesCountModelHelper.decorateModel(
                getDecoratorService(),
                l_(locale, "t3.level3.nbFishesResume.title"),
                totalFishesCount.extractForSpecies(species),
                BEFORE_LEVEL3,
                AFTER_LEVEL3
        );

        return result;
    }

}

