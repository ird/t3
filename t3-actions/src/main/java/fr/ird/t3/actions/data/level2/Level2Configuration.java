/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import fr.ird.t3.actions.stratum.LevelConfigurationWithStratum;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Define the global configuration of a level 2 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level2Configuration extends LevelConfigurationWithStratum {

    private static final long serialVersionUID = 1L;

    /** Minimum count of sample usable on a stratum in school types. */
    protected StratumMinimumSampleCount stratumMinimumSampleCount;

    /**
     * To use all samples of the stratum to fix catches with samples (otherwise only use his own samples).
     * @since 1.6.1
     */
    protected boolean useAllSamplesOfStratum;

    public StratumMinimumSampleCount getStratumMinimumSampleCount() {

        if (stratumMinimumSampleCount == null) {
            stratumMinimumSampleCount = new StratumMinimumSampleCount();
        }
        return stratumMinimumSampleCount;
    }

    public Integer getStratumMinimumSampleCountObjectSchoolType() {
        return getStratumMinimumSampleCount().getMinimumCountForObjectSchool();
    }

    public void setStratumMinimumSampleCountObjectSchoolType(Integer stratumMinimumSampleCountObjectSchoolType) {
        getStratumMinimumSampleCount().setMinimumCountForObjectSchool(stratumMinimumSampleCountObjectSchoolType);
    }

    public Integer getStratumMinimumSampleCountFreeSchoolType() {
        return getStratumMinimumSampleCount().getMinimumCountForFreeSchool();
    }

    public void setStratumMinimumSampleCountFreeSchoolType(Integer stratumMinimumSampleCountFreeSchoolType) {
        getStratumMinimumSampleCount().setMinimumCountForFreeSchool(stratumMinimumSampleCountFreeSchoolType);
    }

    public boolean isUseAllSamplesOfStratum() {
        return useAllSamplesOfStratum;
    }

    public void setUseAllSamplesOfStratum(boolean useAllSamplesOfStratum) {
        this.useAllSamplesOfStratum = useAllSamplesOfStratum;
    }

    @Override
    public String getName(Locale locale) {
        return l_(locale, "t3.level2.action");
    }
}
