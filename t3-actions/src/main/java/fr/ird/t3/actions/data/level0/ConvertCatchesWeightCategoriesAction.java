/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConvertor;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConvertorProvider;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.CorrectedElementaryCatchDAO;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.l_;

/**
 * Action to redistribute the elementaries catches using now as weight categories
 * the {@link WeightCategoryTreatment} instead of the
 * {@link WeightCategoryLogBook}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConvertCatchesWeightCategoriesAction extends AbstractLevel0Action<ConvertCatchesWeightCategoriesConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(
            ConvertCatchesWeightCategoriesAction.class);

    @InjectDAO(entityType = CorrectedElementaryCatch.class)
    protected CorrectedElementaryCatchDAO correctedElementaryCatchDAO;

    protected WeightCategoryLogBookConvertorProvider convertorProvider;

    protected Map<Species, CatchWeightResult> specieWeightResult;

    public ConvertCatchesWeightCategoriesAction() {
        super(Level0Step.CONVERT_CATCHES_WEIGHT_CATEGORIES);
    }

    public Map<Species, CatchWeightResult> getSpecieWeightResult() {
        return specieWeightResult;
    }

    public CatchWeightResult getWeightResult(Species species) {
        return specieWeightResult.get(species);
    }

    protected CatchWeightResult getCatchWeithResult(Map<Species, CatchWeightResult> specieWeightResult, Species species) {
        CatchWeightResult result = specieWeightResult.get(species);
        if (result == null) {
            result = new CatchWeightResult();
            specieWeightResult.put(species, result);
        }
        return result;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        setConvertorProvider(
                WeightCategoryLogBookConvertorProvider.newInstance(getTransaction()));

        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);

        specieWeightResult = Maps.newHashMap();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        boolean result = false;

        if (CollectionUtils.isNotEmpty(trips)) {

            setNbSteps(trips.size());

            // do action for each data using the prepared action context

            for (Trip trip : trips) {

                result |= executeForTrip(trip);
            }
        }

        return result;
    }

    protected boolean executeForTrip(Trip trip) throws TopiaException {

        incrementsProgression();

        String tripStr = decorate(trip);

        String message = l_(locale, "t3.level0.convertCatchesWeightCategories.treat.trip",
                            tripStr);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        Map<Species, CatchWeightResult> tripResult = Maps.newHashMap();

        if (!trip.isActivityEmpty()) {

            // must remove previous corrected elementary catches
            for (Activity activity : trip.getActivity()) {

                activity.clearCorrectedElementaryCatch();
            }

            for (Activity activity : trip.getActivity()) {

                if (activity.isElementaryCatchEmpty()) {

                    // no catches
                    continue;
                }

                if (log.isInfoEnabled()) {
                    log.info("Treat activity " + activity.getDate() +
                             " with " + activity.sizeElementaryCatch());
                }
                Ocean currentOcean = activity.getOcean();
                SchoolType schoolType = activity.getSchoolType();

                // get converter for this ocean and school type
                WeightCategoryLogBookConvertor converter =
                        convertorProvider.getConvertor(currentOcean, schoolType);

                if (converter == null) {

                    // can not redistribute if no weight categories found
                    message = l_(locale, "t3.level0.convertCatchesWeightCategories.warning.noconvertor.found",
                                 currentOcean.getLibelle(),
                                 schoolType.getLibelle());
                    if (log.isWarnEnabled()) {
                        log.warn(message);
                    }
                    addWarningMessage(message);
                    continue;
                }

                if (log.isDebugEnabled()) {
                    log.debug("Use converter " + converter);
                }

                // split catches by species
                Multimap<Species, ElementaryCatch> catchesBySpecie =
                        SpeciesDAO.groupBySpecies(activity.getElementaryCatch());

                for (Species species : catchesBySpecie.keySet()) {

                    CatchWeightResult catchWeightResult =
                            getCatchWeithResult(tripResult, species);

                    Collection<ElementaryCatch> catches =
                            catchesBySpecie.get(species);

                    catchWeightResult.addLogBookWeight(catches);

                    if (log.isInfoEnabled()) {
                        log.info("Treat species " + species.getCode() + " with " +
                                 catches.size() + " catches");
                    }

                    // converts to weight category treatment for this species

                    Map<WeightCategoryTreatment, Float> distribution =
                            converter.distribute(species, catches);

                    if (MapUtils.isEmpty(distribution)) {

                        // nothing to create
                        continue;
                    }

                    Collection<CorrectedElementaryCatch> newCatches =
                            Lists.newArrayList();

                    // create new catches
                    for (Map.Entry<WeightCategoryTreatment, Float> e2 :
                            distribution.entrySet()) {
                        WeightCategoryTreatment weightCategoryTreatment = e2.getKey();
                        Float weight = e2.getValue();
                        CorrectedElementaryCatch correctedElementaryCatch =
                                correctedElementaryCatchDAO.create(
                                        CorrectedElementaryCatch.PROPERTY_WEIGHT_CATEGORY_TREATMENT, weightCategoryTreatment,
                                        CorrectedElementaryCatch.PROPERTY_SPECIES, species,
                                        CorrectedElementaryCatch.PROPERTY_CATCH_WEIGHT, weight
                                );

                        newCatches.add(correctedElementaryCatch);
                    }

                    catchWeightResult.addTreatmentWeight(newCatches);

                    activity.addAllCorrectedElementaryCatch(newCatches);

                }
            }
        }

        addInfoMessage(
                l_(locale, "t3.level0.convertCatchesWeightCategories.resume.for.trip",
                   tripStr, tripResult.size()));

        for (Map.Entry<Species, CatchWeightResult> e : tripResult.entrySet()) {
            Species species = e.getKey();

            CatchWeightResult r = e.getValue();
            CatchWeightResult globalCatchWeithResult =
                    getCatchWeithResult(specieWeightResult, species);
            globalCatchWeithResult.addResult(r);

            addInfoMessage(
                    l_(locale, "t3.level0.convertCatchesWeightCategories.resume.for.species",
                       decorate(species),
                       r.getLogBookTotalWeight(),
                       r.getTreatmentTotalWeight()
                    )
            );
        }

        // will need rf1 to be reapplied on the trip
        markTripAsTreated(trip);

        return true;
    }

    public void setConvertorProvider(WeightCategoryLogBookConvertorProvider convertorProvider) {
        this.convertorProvider = convertorProvider;
    }

    public static class CatchWeightResult {

        protected float logBookTotalWeight;

        protected float treatmentTotalWeight;

        public float getLogBookTotalWeight() {
            return logBookTotalWeight;
        }

        public float getTreatmentTotalWeight() {
            return treatmentTotalWeight;
        }

        public void addLogBookWeight(Collection<ElementaryCatch> catches) {
            for (ElementaryCatch aCatch : catches) {
                logBookTotalWeight += aCatch.getCatchWeightRf2();
            }
        }

        public void addTreatmentWeight(Collection<CorrectedElementaryCatch> catches) {
            for (CorrectedElementaryCatch aCatch : catches) {
                treatmentTotalWeight += aCatch.getCatchWeight();
            }
        }

        public void addResult(CatchWeightResult r) {
            logBookTotalWeight += r.getLogBookTotalWeight();
            treatmentTotalWeight += r.getTreatmentTotalWeight();

        }
    }
}
