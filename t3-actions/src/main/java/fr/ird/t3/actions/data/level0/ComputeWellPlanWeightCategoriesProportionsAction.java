/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.data.WellPlan;
import fr.ird.t3.entities.data.WellSetAllSpecies;
import fr.ird.t3.entities.data.WellSetAllSpeciesDAO;
import fr.ird.t3.entities.reference.WeightCategoryWellPlan;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Compute well plan weight categories proportions for all well of selected
 * trips.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class ComputeWellPlanWeightCategoriesProportionsAction extends AbstractLevel0Action<ComputeWellPlanWeightCategoriesProportionsConfiguration> {

    @InjectDAO(entityType = WellSetAllSpecies.class)
    protected WellSetAllSpeciesDAO wellSetAllSpeciesDAO;

    protected float totalWellPlanWeight;

    protected float totalWellSetAllSpeciesWeight;

    protected int nbWell;

    protected int nbWellWithPlan;

    protected int nbWellWithNoPlan;

    public ComputeWellPlanWeightCategoriesProportionsAction() {
        super(Level0Step.COMPUTE_WELL_PLAN_WEIGHT_CATEGORIES_PROPORTIONS);
    }

    public int getNbWell() {
        return nbWell;
    }

    public int getNbWellWithPlan() {
        return nbWellWithPlan;
    }

    public int getNbWellWithNoPlan() {
        return nbWellWithNoPlan;
    }

    public float getTotalWellPlanWeight() {
        return totalWellPlanWeight;
    }

    public float getTotalWellSetAllSpeciesWeight() {
        return totalWellSetAllSpeciesWeight;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
    }

    @Override
    protected void deletePreviousData() {

        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        boolean result = false;

        if (CollectionUtils.isNotEmpty(trips)) {

            setNbSteps(trips.size());

            // do action for each data using the prepared action context

            Set<Well> wellTreated = Sets.newHashSet();

            for (Trip trip : trips) {

                result |= executeForTrip(trip, wellTreated);
            }
        }
        return result;
    }

    protected boolean executeForTrip(Trip trip,
                                     Set<Well> wellTreated) throws TopiaException {

        if (!trip.isWellEmpty()) {

            for (Well well : trip.getWell()) {

                if (wellTreated.add(well)) {

                    // not already treatd well
                    nbWell++;

                    if (well.isWellPlanEmpty()) {

                        // no well plan
                        nbWellWithNoPlan++;
                    } else {

                        nbWellWithPlan++;
                        // with well plan
                        treatWell(trip, well);
                    }
                }
            }
        }

        incrementsProgression();

        markTripAsTreated(trip);
        return true;
    }

    private void treatWell(Trip trip, Well well) throws TopiaException {


        // split well Plan by their activity
        Multimap<Activity, WellPlan> plansByActivity =
                ActivityDAO.groupByActivity(well.getWellPlan());

        // for each activity remove the species information from his wellPlans

        float wellTotalWeight = 0.0f;

        for (Activity activity : plansByActivity.keySet()) {
            Collection<WellPlan> wellPlans = plansByActivity.get(activity);
            Map<WeightCategoryWellPlan, Float> totalWeightByCategory =
                    Maps.newHashMap();

            float totalWeight = 0f;

            // compute total weight for each weight category

            for (WellPlan wellPlan : wellPlans) {
                WeightCategoryWellPlan category =
                        wellPlan.getWeightCategoryWellPlan();
                Float categoryTotalWeight =
                        totalWeightByCategory.get(category);
                if (categoryTotalWeight == null) {
                    categoryTotalWeight = 0f;
                }
                float weight = wellPlan.getWeight();
                categoryTotalWeight += weight;
                totalWeight += weight;
                totalWeightByCategory.put(category, categoryTotalWeight);
            }

            wellTotalWeight += totalWeight;

            // create now all WellSetAllSpecies for the given activity (one
            // for each weight category found for this well plan)

            for (Map.Entry<WeightCategoryWellPlan, Float> e2 :
                    totalWeightByCategory.entrySet()) {
                WeightCategoryWellPlan category = e2.getKey();
                Float weight = e2.getValue();
                Float propWeight = weight / totalWeight;
                WellSetAllSpecies wellSetAllSpecies = wellSetAllSpeciesDAO.create(
                        WellSetAllSpecies.PROPERTY_WEIGHT_CATEGORY_WELL_PLAN, category,
                        WellSetAllSpecies.PROPERTY_ACTIVITY, activity,
                        WellSetAllSpecies.PROPERTY_WEIGHT, weight,
                        WellSetAllSpecies.PROPERTY_PROP_WEIGHT, propWeight
                );
                well.addWellSetAllSpecies(wellSetAllSpecies);

                totalWellSetAllSpeciesWeight += weight;
            }

            totalWeightByCategory.clear();
        }

        totalWellPlanWeight += wellTotalWeight;

        Collection<WellSetAllSpecies> wellSetAllSpecies =
                well.getWellSetAllSpecies();

        String message =
                l_(locale, "t3.level0.computeWellPlanWeightCategoriesProportions.well.withWellPlan",
                   decorate(trip) + " - Well " + decorate(well),
                   wellSetAllSpecies.size(),
                   wellTotalWeight
                );
        addInfoMessage(message);
        for (WellSetAllSpecies wellSetAllSpecy : wellSetAllSpecies) {
            WeightCategoryWellPlan weightCategory =
                    wellSetAllSpecy.getWeightCategoryWellPlan();
            message = l_(locale, "t3.level0.computeWellPlanWeightCategoriesProportions.resume.for.weightCategory",
                         weightCategory.getLibelle(),
                         wellSetAllSpecy.getWeight(),
                         wellSetAllSpecy.getPropWeight(),
                         decorate(wellSetAllSpecy.getActivity())

            );
            addInfoMessage(message);
        }
    }

}