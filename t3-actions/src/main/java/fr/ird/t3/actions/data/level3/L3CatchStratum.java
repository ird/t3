/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.base.Predicate;
import fr.ird.t3.actions.stratum.CatchStratum;
import fr.ird.t3.actions.stratum.CatchStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.T3ServiceContext;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Define a catch stratum for a given stratum of a level 2 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class L3CatchStratum extends CatchStratum<Level3Configuration, Level3Action> {

    /**
     * Keep the total weight of all the catches in the stratum.
     * <p/>
     * Used by sampleStratum for quality tests.
     *
     * @since 1.3.1
     */
    private float totalCatchtWeight;

    /**
     * Predicate to filter only species selected in configuration.
     *
     * @see #getSpeciesToFix()
     * @since 1.4
     */
    private final Predicate<CorrectedElementaryCatch> speciesToFixFilter;

    public L3CatchStratum(StratumConfiguration<Level3Configuration> stratumConfiguration,
                          Collection<Species> speciesToFix,
                          Predicate speciesToFixFilter) {
        super(stratumConfiguration, speciesToFix);
        this.speciesToFixFilter = speciesToFixFilter;
    }

    @Override
    protected CatchStratumLoader<Level3Configuration> newLoader() {
        return new L3CatchStratumLoader();
    }

    @Override
    public void init(T3ServiceContext serviceContext,
                     List<WeightCategoryTreatment> weightCategories,
                     Level3Action messager) throws Exception {

        // load catch stratum
        super.init(serviceContext, weightCategories, messager);

        for (Map.Entry<Activity, Integer> e : this) {

            Activity activity = e.getKey();
            int nbZones = e.getValue();

            // compute total catch weight of the activity (only for species to fix)
            totalCatchtWeight +=
                    T3EntityHelper.getTotal(
                            activity.getCorrectedElementaryCatch(),
                            T3Functions.CORRECTED_ELEMENTARY_CATCH_TO_CORRECTED_CATCH_WEIGHT,
                            speciesToFixFilter,
                            T3Suppliers.newActivityDecorateSupplier(
                                    serviceContext,
                                    activity,
                                    "Found a trip %s - activity %s with a null CorrectedElementaryCatch#correctedCatchWeight")
                    ) / nbZones;
        }
    }

    public int getNbActivitiesWithSample() {
        int result = 0;
        for (Map.Entry<Activity, Integer> e : this) {
            if (isActivityWithSample(e.getKey())) {
                result++;
            }
        }
        return result;
    }

    public boolean isActivityWithSample(Activity activity) {
        return T3Predicates.ACTIVITY_WITH_SET_SAMPLE.apply(activity);
    }

    /**
     * Obtain total catch weight of selected catches for this stratum.
     *
     * @return total catch weight of selected catches for this stratum.
     */
    public float getTotalCatchWeight() {
        return totalCatchtWeight;
    }

}
