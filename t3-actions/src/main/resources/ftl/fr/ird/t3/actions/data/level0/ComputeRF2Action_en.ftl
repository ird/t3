<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

<#if configuration.configurationEmpty>

No landing harbour, simple vessel type and fleet was selected, RF2 will be set to 1 for all trips in date range.

</#if>

Begin date: ${configuration.beginDate}
End date:   ${configuration.endDate}

<#if !configuration.configurationEmpty>
  <#list landingHarbours?values as landingHarbour>
  Selected Landing harbour: ${landingHarbour}
  </#list>

  <#list vesselSimpleTypes?values as vesselSimpleType>
  Selected simple vessel type: ${vesselSimpleType}
  </#list>

  <#list fleets?values as fleet>
  Selected fleet: ${fleet}
  </#list>
</#if>

Indicators
----------

- Number of stratums:                ${action.nbStratums}
- Number of trips:                   ${action.nbTrips}
- Number of trips with computed rf2: ${action.nbTripsWithRF2}

<#include "/ftl/showMessages_en.ftl"/>
