<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#function tableFormat arg0, arg1>
<#return statics['java.lang.String'].format("%1$-50s : %2$-15s ", arg0, arg1)>
</#function>
<#include "/ftl/header.ftl"/>

Date de début : ${configuration.beginDate}
Date de fin   : ${configuration.endDate}

<#list oceans?values as ocean>
Océan sélectionné : ${ocean}
</#list>

<#list fleets?values as fleet>
Flotte sélectionné : ${fleet}
</#list>

<#list sampleQualities?values as sampleQualitity>
Qualité d'échantillon sélectionné : ${sampleQualitity}
</#list>

<#list sampleTypes?values as sampleType>
Type d'échantillon sélectionné : ${sampleType}
</#list>

Indicateurs
-----------

Nombre de marées traitées         : ${action.nbTripsTreated}
Nombre d'échantillons traités     : ${action.nbSamplesTreated}
Nombre d'échantillons non traités : ${action.nbSamplesNotTreated}

<#list action.trips as trip>
Marée ${tripDecorator.toString(trip)} :
<#assign samples = action.getSamples(trip)/>
<#list samples as sample>
 - Echantillon ${sample.sampleNumber} [${sample.topiaId}]
<#assign speciesList = action.getSpeciesOfSample(sample)/>
<#list speciesList as species>
   - Espèce ${speciesDecorator.toString(species)}
<#assign result = action.getResultSpeciesModel(sample, species)/>
<#assign resultMap = result.sampleWellSum/>
     o Somme des individus de ${tableFormat("l'échantillon",result.sampleSum)}
<#list resultMap?keys as sampleWell>
     o Somme des individus de ${tableFormat("la calée " +activityDecorator.toString(sampleWell.activity),result.getSampleWellSum(sampleWell))}
</#list>

</#list>
</#list>
</#list>

<#include "/ftl/showMessages.ftl"/>
