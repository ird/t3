<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

Date de début : ${configuration.beginDate}
Date de fin   : ${configuration.endDate}
Seuil rf1 min : ${configuration.minimumRate}
Seuil rf1 max : ${configuration.maximumRate}

<#list vesselSimpleTypes?values as vesselSimpleType>
Type de navire sélectionné : ${vesselSimpleType}
</#list>

<#list fleets?values as fleet>
Flotte sélectionnée : ${fleet}
</#list>

Indicateurs
-----------

- Nombre de navires                                      : ${action.nbVessels}
- Nombre de marées                                       : ${action.nbTrips}
- Nombre de marées rejetées (sans logbook)               : ${action.nbRejectedTrips}
- Nombre de marées acceptées (avec calcul rf1)           : ${action.nbAcceptedTrips}
- Nombre de marées complêtes                             : ${action.nbCompleteTrips}
- Nombre de marées complêtes rejetées (sans logbook)     : ${action.nbCompleteRejectedTrips}
- Nombre de marées complêtes acceptées (avec calcul rf1) : ${action.nbCompleteAcceptedTrips}
- Nombre de marées complêtes acceptées (rf1 incohérent)  : ${action.nbCompleteAcceptedTripsWithBadRF1}
- Total du poids capturé                                 : ${action.totalCatchWeightRF1}
- Total du poids des lots commerciaux                    : ${action.totalLandingWeight}

<#include "/ftl/showMessages.ftl"/>


