<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

Input Pilot:                 ${configuration.inputProvider.libelle}
Input file:                  ${configuration.inputFile.name}
Use wells:                   ${configuration.useWells?string}
Samples only:                ${configuration.samplesOnly?string}
Authorise to create vessels: ${configuration.canCreateVessel?string}
<#if configuration.canCreateVessel>
Create virtual vessels:      ${configuration.createVirtualVessel?string}
</#if>
Indicators
----------

<#if safeTrips?size &gt; 0>
- Number of importable trips            : ${safeTrips?size}
</#if>
<#if unsafeTrips?size &gt; 0>
- Number of not importable trips        : ${unsafeTrips?size}
</#if>
<#if tripsToReplace?size &gt; 0>
- Number of importable trips to replace : ${tripsToReplace?size}
</#if>

<#if unsafeTrips?size &gt; 0>

Import is not possible since there is some not importable trips (see error messages).

<#list unsafeTrips as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

<#if safeTrips?size &gt; 0>
List of importable trips:

<#list safeTrips as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

<#if tripsToReplace?size &gt; 0>
There is some importable trips which already exist in the database:

<#list tripsToReplace?keys as trip>
  <#assign value = action.getTripToReplace(trip)/>
- ${tripDecorator2.toString(trip)} replaced by ${tripDecorator.toString(value)}
</#list>
</#if>

<#include "/ftl/showMessages_en.ftl"/>


