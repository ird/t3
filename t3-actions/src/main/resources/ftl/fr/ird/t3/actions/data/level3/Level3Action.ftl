<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#assign headerSize  = action.maximumSizeForStratum/>
<#function tableFormat arg0, arg1, arg2>
  <#return statics['java.lang.String'].format("%1$-" + headerSize + "s | %2$-15s | %3$-30s |", arg0, arg1, arg2)>
</#function>
<#include "/ftl/header.ftl"/>

Date de début    : ${configuration.beginDate}
Date de fin      : ${configuration.endDate}
Période de temps : ${configuration.timeStep}

Traitement à appliquer aux calées avec échantillon :
<#if configuration.useAllSamplesOfStratum>
Appliquer les structures de tailles de la strate échantillons
<#else>
Conserver leurs échantillons en l'état
</#if>

<#if configuration.useWeightCategories>
Générer les structures de tailles par catégories de poids
<#else>
Générer les structures de tailles toutes catégories de poids confondues
</#if>

<#list oceans?values as ocean>
Océan sélectionné : ${ocean}
</#list>

<#list zoneTypes?values as zoneType>
Type de zone sélectionnée : ${zoneType}
</#list>

<#list zoneVersions?values as zoneVersion>
Version de la zone sélectionnée : ${zoneVersion}
</#list>

<#list catchFleets?values as catchFleet>
Flotte capture sélectionnée : ${catchFleet}
</#list>

<#list species?values as speciesLabel>
Espèce sélectionnée : ${speciesLabel}
</#list>

<#list sampleFleets?values as sampleFleet>
Flotte d'échantillon sélectionnée : ${sampleFleet}
</#list>

<#list sampleFlags?values as sampleFlag>
Pavillon d'échantillon sélectionnée : ${sampleFlag}
</#list>

Qualité des strates échantillons :

- Ratio minimum entre le poids de la strate capture et échantillon : ${configuration.stratumWeightRatio}

- Nombre minimum d'effectif dans les échantillons (par espèce) :
<#list species?keys as speciesId>
  <#assign speciesLabel = species[speciesId]/>
  <#assign value = configuration.stratumMinimumSampleCount[speciesId]/>
  - Espèce : ${speciesLabel} / banc libre : ${value.minimumCountForFreeSchool} / banc objet : ${value.minimumCountForObjectSchool}
</#list>

Indicateurs
-----------

- Nombre de strates                             : ${action.nbStratums}
- Nombre de strates corrigés                    : ${action.nbStratumsFixed}
- Nombre d'activités traités                    : ${action.nbCatchActivities}
- Nombre d'activités traités (avec échantillon) : ${action.nbCatchActivitiesWithSample}

${action.totalFishesCountResume}

<#assign allSubstitutionLevels = action.allSubstitutionLevels/>

Strates Echantillon (par niveau de substitution)
------------------------------------------------

Niveau 0   est utilisé pour indiquer les strates sans capture
Niveau 999 est utilisé pour indiquer que le dernier niveau de substitution a été atteint sans pour autant passer les tests de qualité

${tableFormat("Strate","Nb de calées","Nb de calées avec échantillon")}
<#list action.allSubstitutionLevels as substitutionLevel>
  <#assign stratums = action.getStratumResult(substitutionLevel)/>

- Niveau de substitution ${substitutionLevel} (${stratums?size} strate(s))
  <#list stratums as stratum>
  ${tableFormat(stratum.libelle,stratum.nbActivities,stratum.nbActivitiesWithSample)}
  </#list>
</#list>

<#include "/ftl/showMessages.ftl"/>
