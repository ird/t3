/*
 * #%L
 * T3 :: Actions
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.FakeT3ServiceContext;
import fr.ird.t3.actions.T3ActionFixtures;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;

import java.util.Map;

/**
 * To test {@link LengthCompositionModelHelper}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthCompositionModelHelperTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(LengthCompositionModelHelperTest.class);

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected T3ActionFixtures fixtures = new T3ActionFixtures();

    @Test
    public void testDecorateModel() throws Exception {

        Species species1 = fixtures.species1();
        Species species2 = fixtures.species2();

        Map<Species, LengthCompositionAggregateModel> models = Maps.newHashMap();

        LengthCompositionAggregateModel model;
        LengthCompositionModel model1;
        LengthCompositionModel model2;
        LengthCompositionModel model3;
        Map<Integer, Float> lengths;

        model = new LengthCompositionAggregateModel();
        model1 = new LengthCompositionModel(fixtures.weightCategoryTreatment1());
        lengths = Maps.newHashMap();
        lengths.put(1, 1.f);
        lengths.put(2, 2.f);
        model1.addValues(lengths);
        model.addModel(model1);

        model2 = new LengthCompositionModel(fixtures.weightCategoryTreatment2());
        lengths = Maps.newHashMap();
        lengths.put(3, 2.f);
        lengths.put(4, 9.f);
        model2.addValues(lengths);
        model.addModel(model2);

        models.put(species1, model);

        model = new LengthCompositionAggregateModel();
        model1 = new LengthCompositionModel(fixtures.weightCategoryTreatment1());
        lengths = Maps.newHashMap();
        lengths.put(1, 11.f);
        lengths.put(2, 2.f);
        model1.addValues(lengths);
        model.addModel(model1);

        model2 = new LengthCompositionModel(fixtures.weightCategoryTreatment2());
        lengths = Maps.newHashMap();
        lengths.put(4, 1.f);
        lengths.put(5, 22.f);
        model2.addValues(lengths);
        model.addModel(model2);

        model3 = new LengthCompositionModel(fixtures.weightCategoryTreatment3());
        lengths = Maps.newHashMap();
        lengths.put(8, 61.f);
        lengths.put(9, 2.f);
        model3.addValues(lengths);
        model.addModel(model3);

        models.put(species2, model);


        String s = LengthCompositionModelHelper.decorateModel(
                serviceContext.newService(DecoratorService.class),
                "Mon titre", "Value titre", models);
        if (log.isInfoEnabled()) {
            log.info("Result :\n" + s);
        }
    }
}
