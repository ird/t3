/*
 * #%L
 * T3 :: Actions
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.FakeT3ServiceContext;
import fr.ird.t3.actions.T3ActionFixtures;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

/**
 * To test {@link WeightCompositionModelHelper}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class WeightCompositionModelHelperTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(WeightCompositionModelHelperTest.class);

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected T3ActionFixtures fixtures = new T3ActionFixtures();

    @Test
    public void testDecorateModel() throws Exception {

        Species species1 = fixtures.species1();
        Species species2 = fixtures.species2();
        Species species3 = fixtures.species3();

        Set<Species> species = Sets.newHashSet(species1, species2, species3);

        WeightCompositionAggregateModel model = new WeightCompositionAggregateModel();
        WeightCompositionModel model1 = new WeightCompositionModel(fixtures.weightCategoryTreatment1());
        Map<Species, Float> weights = Maps.newHashMap();
        weights.put(species1, 1.f);
        weights.put(species2, 2.f);
        model1.addWeights(weights);
        model.addModel(model1);

        WeightCompositionModel model2 = new WeightCompositionModel(fixtures.weightCategoryTreatment2());
        weights = Maps.newHashMap();
        weights.put(species1, 2.f);
        weights.put(species2, 4.f);
        model2.addWeights(weights);
        model.addModel(model2);

        WeightCompositionModel model3 = new WeightCompositionModel(fixtures.weightCategoryTreatment3());
        weights = Maps.newHashMap();
        weights.put(species1, 4.f);
        weights.put(species2, 8.f);
        model3.addWeights(weights);
        model.addModel(model3);

        String s = WeightCompositionModelHelper.decorateModel(
                serviceContext.newService(DecoratorService.class),
                "Mon titre", model);
        if (log.isInfoEnabled()) {
            log.info("Result :\n"+s);
        }
    }
}
