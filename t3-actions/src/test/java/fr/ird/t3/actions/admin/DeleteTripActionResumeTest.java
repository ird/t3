/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.admin;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.data.Trip;
import org.junit.Test;

import java.util.Arrays;

/**
 * Test the resume of action {@link DeleteTripAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class DeleteTripActionResumeTest extends AbstractActionResumeTest<DeleteTripConfiguration, DeleteTripAction> {

    public DeleteTripActionResumeTest() {
        super(DeleteTripConfiguration.class, DeleteTripAction.class);
    }

    @Override
    protected void prepareConfiguration(DeleteTripConfiguration conf) {
        conf.setDeleteTrip(true);
        Trip trip1 = fixtures.trip1();
        Trip trip2 = fixtures.trip2();
        Trip trip3 = fixtures.trip3();
        conf.setTripIds(Arrays.asList(trip1.getTopiaId(),
                                      trip2.getTopiaId(),
                                      trip3.getTopiaId()));
        action.nbTrips = 3;
        action.nbActivities = 30;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }

}
