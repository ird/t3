/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To test the action {@link ComputeRF1Action}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeRF1ActionResumeTest extends AbstractActionResumeTest<ComputeRF1Configuration, ComputeRF1Action> {

    public ComputeRF1ActionResumeTest() {
        super(ComputeRF1Configuration.class, ComputeRF1Action.class);
    }

    @Override
    protected void prepareConfiguration(ComputeRF1Configuration conf) {
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
        configuration.setMinimumRate(0.8f);
        configuration.setMaximumRate(1.5f);
    }

    @Override
    protected void prepareAction(ComputeRF1Action action, Locale locale) {
        super.prepareAction(action, locale);
        action.nbVessels = 10;
        action.nbAcceptedTrips = 5;
        action.nbRejectedTrips = 5;
        action.nbCompleteAcceptedTrips = 2;
        action.nbCompleteAcceptedTripsWithBadRF1 = 1;
        action.nbTrips = 10;
        action.totalCatchWeightRF1 = 100;
        action.totalLandingWeight = 102;
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> vesselSimpleTypes = new TreeMap<String, String>();
        putInMap(vesselSimpleTypes,
                 fixtures.vesselSimpleTypeCanneur(),
                 fixtures.vesselSimpleTypeSenneur());
        parameters.put("vesselSimpleTypes", vesselSimpleTypes);

        Map<String, String> fleets = new TreeMap<String, String>();
        putInMap(fleets, fixtures.frenchCountry());
        parameters.put("fleets", fleets);

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }

}
