/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import org.junit.Test;

import java.util.Locale;
import java.util.Map;

/**
 * To test the resume generation of action
 * {@link ComputeWeightOfCategoriesForSetAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeWeightOfCategoriesForSetActionResumeTest extends AbstractLevel1ActionResumeTest<ComputeWeightOfCategoriesForSetAction> {

    public ComputeWeightOfCategoriesForSetActionResumeTest() {
        super(ComputeWeightOfCategoriesForSetAction.class);
    }

    @Override
    protected void prepareAction(ComputeWeightOfCategoriesForSetAction action, Locale locale) {
        super.prepareAction(action, locale);
        action.nbSampleWithoutWell = 1;
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
