/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;

/**
 * To test the resume generation of action
 * {@link RedistributeSampleNumberToSetAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class RedistributeSampleNumberToSetActionResumeTest extends AbstractLevel1ActionResumeTest<RedistributeSampleNumberToSetAction> {

    public RedistributeSampleNumberToSetActionResumeTest() {
        super(RedistributeSampleNumberToSetAction.class);
    }

    @Override
    protected void prepareAction(RedistributeSampleNumberToSetAction action, Locale locale) {
        super.prepareAction(action, locale);
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();


        parameters.put("speciesDecorator", getDecorator(Species.class));
        parameters.put("tripDecorator", getDecorator(Trip.class));
        parameters.put("activityDecorator", getDecorator(Activity.class));
        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
