/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.FreeMarkerService;
import fr.ird.t3.services.T3ServiceFactory;
import freemarker.ext.beans.BeansWrapper;
import org.junit.Assert;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Rule;
import org.nuiton.util.decorator.Decorator;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Base test for data actions.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractActionResumeTest<C extends T3ActionConfiguration, A extends T3Action<C>> {

    /** Logger. */
    protected final Log log = LogFactory.getLog(getClass());

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected T3ActionContext<C> actionContext;

    protected C configuration;

    protected A action;

    protected final Class<C> configurationClass;

    protected final Class<A> actionClass;

    protected T3ActionFixtures fixtures;

    public AbstractActionResumeTest(Class<C> configurationClass, Class<A> actionClass) {
        this.configurationClass = configurationClass;
        this.actionClass = actionClass;
    }

    @Before
    public void setUp() throws Exception {

        T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();

        configuration = configurationClass.getConstructor().newInstance();

        actionContext = serviceFactory.newT3ActionContext(configuration, serviceContext);

        fixtures = serviceFactory.newService(T3ActionFixtures.class, serviceContext);

        action = serviceFactory.newT3Action(actionClass, actionContext);
    }

    public void doTestRender() throws Exception {

        prepareConfiguration(configuration);

        prepareAction(action, Locale.FRENCH);

        String templateName = action.getRenderTemplateName();

        Map<String, Object> parameters = createTemplateParameters();
        FreeMarkerService freeMarkerService = serviceContext.newService(FreeMarkerService.class);
        String resultForFrenchLocale =
                freeMarkerService.renderTemplate(templateName, serviceContext.getLocale(), parameters);
        Assert.assertNotNull(resultForFrenchLocale);
        if (log.isDebugEnabled()) {
            log.debug(resultForFrenchLocale);
        }

        prepareAction(action, Locale.UK);

        String resultForEnglishLocale =
                freeMarkerService.renderTemplate(templateName, serviceContext.getLocale(), parameters);
        Assert.assertNotNull(resultForEnglishLocale);
        Assert.assertFalse(resultForFrenchLocale.equals(resultForEnglishLocale));
        if (log.isDebugEnabled()) {
            log.debug(resultForEnglishLocale);
        }
    }

    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = new TreeMap<String, Object>();
        parameters.put("startDate", new Date());
        parameters.put("endDate", new Date());
        parameters.put("action", action);
        parameters.put("configuration", action.getConfiguration());
        parameters.put("showError", true);
        parameters.put("showWarning", true);
        parameters.put("showInfo", true);
        parameters.put("statics", BeansWrapper.getDefaultInstance().getStaticModels());
        return parameters;
    }

    protected void prepareConfiguration(C configuration) {
    }

    protected void prepareAction(A action, Locale locale) {
        action.addErrorMessage("Une erreur :(");
        action.addWarningMessage("Un warning -:(");
        action.addInfoMessage("Une info :)");

        serviceContext.setLocale(locale);
        action.locale = serviceContext.getLocale();
    }

    protected <E> Decorator<E> getDecorator(Class<E> beanType) {
        Decorator<E> decorator = getDecorator(beanType, null);
        return decorator;
    }

    protected <E> Decorator<E> getDecorator(Class<E> beanType, String context) {
        DecoratorService decoratorService =
                serviceContext.newService(DecoratorService.class);
        Locale locale = serviceContext.getLocale();
        Decorator<E> decorator = decoratorService.getDecorator(locale,
                                                               beanType,
                                                               context);
        return decorator;
    }

    protected void putInMap(Map<String, String> map, T3ReferenceEntity... entities) {
        for (T3ReferenceEntity entity : entities) {
            map.put(String.valueOf(entity.getCode()), entity.getLibelle());
        }
    }

}
