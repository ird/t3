/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To test the action {@link ComputeSetDurationAndPositiveSetCountAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeSetDurationAndPositiveSetCountActionResumeTest extends AbstractActionResumeTest<ComputeSetDurationAndPositiveSetCountConfiguration, ComputeSetDurationAndPositiveSetCountAction> {

    public ComputeSetDurationAndPositiveSetCountActionResumeTest() {
        super(ComputeSetDurationAndPositiveSetCountConfiguration.class, ComputeSetDurationAndPositiveSetCountAction.class);
    }

    @Override
    protected void prepareConfiguration(ComputeSetDurationAndPositiveSetCountConfiguration conf) {
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
    }

    @Override
    protected void prepareAction(ComputeSetDurationAndPositiveSetCountAction action, Locale locale) {
        super.prepareAction(action, locale);
        action.nbVessels = 5;
        action.nbTrips = 10;
        action.nbActivities = 100;
        action.nbPositiveActivities = 80;
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> vesselSimpleTypes = new TreeMap<String, String>();
        putInMap(vesselSimpleTypes,
                 fixtures.vesselSimpleTypeCanneur(),
                 fixtures.vesselSimpleTypeSenneur());
        parameters.put("vesselSimpleTypes", vesselSimpleTypes);

        Map<String, String> fleets = new TreeMap<String, String>();
        putInMap(fleets, fixtures.frenchCountry());
        parameters.put("fleets", fleets);

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
