/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Test;

import java.util.Collections;
import java.util.Locale;

/**
 * To test the action {@link ComputeRF1Action}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeRF2EmptyConfigurationActionResumeTest extends AbstractActionResumeTest<ComputeRF2Configuration, ComputeRF2Action> {

    public ComputeRF2EmptyConfigurationActionResumeTest() {
        super(ComputeRF2Configuration.class, ComputeRF2Action.class);
    }

    @Override
    protected void prepareConfiguration(ComputeRF2Configuration conf) {
        super.prepareConfiguration(conf);
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
        configuration.setLandingHarbourIds(Collections.<String>emptyList());
        configuration.setVesselSimpleTypeIds(Collections.<String>emptyList());
        configuration.setFleetIds(Collections.<String>emptyList());
    }

    @Override
    protected void prepareAction(ComputeRF2Action action, Locale locale) {
        super.prepareAction(action, locale);

        action.nbStratums = 100;
        action.nbTrips = 10;
        action.nbTripsWithRF2 = 5;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
