/*
 * #%L
 * T3 :: Actions
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityImpl;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequency;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripImpl;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryImpl;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourImpl;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanImpl;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleQualityImpl;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.reference.SampleTypeImpl;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesImpl;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselImpl;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselSimpleTypeImpl;
import fr.ird.t3.entities.reference.VesselType;
import fr.ird.t3.entities.reference.VesselTypeImpl;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentImpl;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.reference.zone.ZoneVersionImpl;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;

/**
 * Fixtures for action tests.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class T3ActionFixtures implements T3Service {

    protected T3ServiceContext serviceContext;

    private Trip trip1;

    private Trip trip2;

    private Trip trip3;

    private Trip trip4;

    private Trip trip5;

    private Trip trip6;

    private Country frenchCountry;

    private Country spanishCountry;

    private Vessel vessel1;

    private Vessel vessel2;

    private VesselType vesselType1;

    private VesselSimpleType vesselSimpleTypeCanneur;

    private VesselSimpleType vesselSimpleTypeSenneur;

    private Ocean oceanAtlantic;

    private Ocean oceanIndian;

    private SampleType sampleType1;

    private SampleType sampleType2;

    private SampleQuality sampleQuality1;

    private SampleQuality sampleQuality2;

    private Species species1;

    private Species species2;

    private Species species3;

    private Harbour harbour1;

    private ZoneVersion zoneVersion1;

    private ZoneVersion zoneVersion2;

    private WeightCategoryTreatment weightCategoryTreatment1;

    private WeightCategoryTreatment weightCategoryTreatment2;

    private WeightCategoryTreatment weightCategoryTreatment3;

    private Activity activityLevel3;


    public Activity activityLevel3() {
        if (activityLevel3 == null) {
            activityLevel3 = new ActivityImpl();
            {
                SetSpeciesFrequency ssf;

                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(10);
                ssf.setNumber(10);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(12);
                ssf.setNumber(20);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(14);
                ssf.setNumber(30);
                activityLevel3.addSetSpeciesFrequency(ssf);

                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(10);
                ssf.setNumber(30);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(12);
                ssf.setNumber(20);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(14);
                ssf.setNumber(10);
                activityLevel3.addSetSpeciesFrequency(ssf);

                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(20);
                ssf.setNumber(10);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(22);
                ssf.setNumber(10);
                activityLevel3.addSetSpeciesFrequency(ssf);
                ssf = new SetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(24);
                ssf.setNumber(10);
                activityLevel3.addSetSpeciesFrequency(ssf);
            }

            {
                ExtrapolatedAllSetSpeciesFrequency ssf;

                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(10);
                ssf.setNumber(10);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(12);
                ssf.setNumber(20);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species1());
                ssf.setLfLengthClass(14);
                ssf.setNumber(30);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);

                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(10);
                ssf.setNumber(30);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(12);
                ssf.setNumber(20);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species2());
                ssf.setLfLengthClass(14);
                ssf.setNumber(10);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);

                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(20);
                ssf.setNumber(10);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(22);
                ssf.setNumber(10);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
                ssf = new ExtrapolatedAllSetSpeciesFrequencyImpl();
                ssf.setSpecies(species3());
                ssf.setLfLengthClass(24);
                ssf.setNumber(10);
                activityLevel3.addExtrapolatedAllSetSpeciesFrequency(ssf);
            }

        }
        return activityLevel3;
    }

    public ZoneVersion zoneVersion1() {
        if (zoneVersion1 == null) {
            zoneVersion1 = new ZoneVersionImpl();
            zoneVersion1.setVersionId("1");
            zoneVersion1.setVersionLibelle("zoneVersion 1");
        }
        return zoneVersion1;
    }

    public ZoneVersion zoneVersion2() {
        if (zoneVersion2 == null) {
            zoneVersion2 = new ZoneVersionImpl();
            zoneVersion2.setVersionId("2");
            zoneVersion2.setVersionLibelle("zoneVersion 2");
        }
        return zoneVersion1;
    }

    public Harbour harbour1() {
        if (harbour1 == null) {
            harbour1 = new HarbourImpl();
            harbour1.setTopiaId("Harbour:1");
            harbour1.setCode(1);
            harbour1.setLibelle("Harbour:1");
        }
        return harbour1;
    }


    public Species species1() {
        if (species1 == null) {
            species1 = new SpeciesImpl();
            species1.setTopiaId("species:1");
            species1.setCode(1);
            species1.setCode3L("YFT");
            species1.setLibelle("species 1");
        }
        return species1;
    }

    public Species species2() {
        if (species2 == null) {
            species2 = new SpeciesImpl();
            species2.setTopiaId("species:2");
            species2.setCode(2);
            species2.setCode3L("SKJ");
            species2.setLibelle("species 2");
        }
        return species2;
    }

    public Species species3() {
        if (species3 == null) {
            species3 = new SpeciesImpl();
            species3.setTopiaId("species:3");
            species3.setCode(3);
            species3.setCode3L("ALB");
            species3.setLibelle("species 3");
        }
        return species3;
    }

    public SampleType sampleType1() {
        if (sampleType1 == null) {
            sampleType1 = new SampleTypeImpl();
            sampleType1.setTopiaId("sampleType:1");
            sampleType1.setCode(1);
            sampleType1.setLibelle("Sample type 1");
        }
        return sampleType1;
    }

    public SampleType sampleType2() {
        if (sampleType2 == null) {
            sampleType2 = new SampleTypeImpl();
            sampleType2.setTopiaId("sampleType:2");
            sampleType2.setCode(2);
            sampleType2.setLibelle("Sample type 2");
        }
        return sampleType2;
    }

    public SampleQuality sampleQuality1() {
        if (sampleQuality1 == null) {
            sampleQuality1 = new SampleQualityImpl();
            sampleQuality1.setTopiaId("sampleQuality:1");
            sampleQuality1.setCode(1);
            sampleQuality1.setLibelle("Sample quality 1");
        }
        return sampleQuality1;
    }

    public SampleQuality sampleQuality2() {
        if (sampleQuality2 == null) {
            sampleQuality2 = new SampleQualityImpl();
            sampleQuality2.setTopiaId("sampleQuality:2");
            sampleQuality2.setCode(2);
            sampleQuality2.setLibelle("Sample quality 2");
        }
        return sampleQuality1;
    }


    public VesselSimpleType vesselSimpleTypeCanneur() {
        if (vesselSimpleTypeCanneur == null) {
            vesselSimpleTypeCanneur = new VesselSimpleTypeImpl();
            vesselSimpleTypeCanneur.setTopiaId("vesselSimpleType:1");
            vesselSimpleTypeCanneur.setCode(1);
            vesselSimpleTypeCanneur.setLibelle("Canneur");
        }
        return vesselSimpleTypeCanneur;
    }

    public VesselSimpleType vesselSimpleTypeSenneur() {
        if (vesselSimpleTypeSenneur == null) {
            vesselSimpleTypeSenneur = new VesselSimpleTypeImpl();
            vesselSimpleTypeSenneur.setTopiaId("vesselSimpleType:2");
            vesselSimpleTypeSenneur.setCode(2);
            vesselSimpleTypeSenneur.setLibelle("Senneur");
        }
        return vesselSimpleTypeCanneur;
    }

    public Ocean oceanAtlantic() {
        if (oceanAtlantic == null) {
            oceanAtlantic = new OceanImpl();
            oceanAtlantic.setTopiaId("ocean:1");
            oceanAtlantic.setCode(1);
            oceanAtlantic.setLibelle("Atlantic");
        }
        return oceanAtlantic;
    }


    public Ocean oceanIndian() {
        if (oceanIndian == null) {
            oceanIndian = new OceanImpl();
            oceanIndian.setTopiaId("ocean:2");
            oceanIndian.setCode(1);
            oceanIndian.setLibelle("Indian");
        }
        return oceanAtlantic;
    }

    public Country spanishCountry() {
        if (spanishCountry == null) {
            spanishCountry = new CountryImpl();
            spanishCountry.setTopiaId("country:1");
            spanishCountry.setCode(2);
            spanishCountry.setLibelle("spanish");
        }
        return spanishCountry;
    }

    public Country frenchCountry() {
        if (frenchCountry == null) {
            frenchCountry = new CountryImpl();
            frenchCountry.setTopiaId("country:1");
            frenchCountry.setCode(1);
            frenchCountry.setLibelle("france");
        }
        return frenchCountry;
    }

    public VesselType vesselType1() {
        if (vesselType1 == null) {
            vesselType1 = new VesselTypeImpl();
            vesselType1.setTopiaId("vesselType:1");
            vesselType1.setCode(1);
            vesselType1.setLibelle("vesselType1");
        }
        return vesselType1;
    }

    public Vessel vessel1() {
        if (vessel1 == null) {
            vessel1 = new VesselImpl();
            vessel1.setTopiaId("vessel:1");
            vessel1.setCode(1);
            vessel1.setLibelle("vessel");
            vessel1.setVesselType(vesselType1());
            vessel1.setFlagCountry(frenchCountry());
            vessel1.setFleetCountry(frenchCountry());
        }
        return vessel1;
    }

    public Vessel vessel2() {
        if (vessel2 == null) {
            vessel2 = new VesselImpl();
            vessel2.setTopiaId("vessel:2");
            vessel2.setCode(2);
            vessel2.setLibelle("vessel2");

            vessel2.setVesselType(vesselType1());
            vessel2.setFlagCountry(frenchCountry());
            vessel2.setFleetCountry(frenchCountry());
        }
        return vessel2;
    }

    public Trip trip1() {
        if (trip1 == null) {
            trip1 = new TripImpl();
            trip1.setTopiaId("topiaId:1");
            trip1.setLandingDate(T3Date.newDate(1, 2012).toBeginDate());
            trip1.setVessel(vessel1());
        }
        return trip1;
    }

    public Trip trip2() {
        if (trip2 == null) {
            trip2 = new TripImpl();
            trip2.setTopiaId("topiaId:2");
            trip2.setLandingDate(T3Date.newDate(2, 2012).toBeginDate());
            trip2.setVessel(vessel2());
        }
        return trip1;
    }

    public Trip trip3() {
        if (trip3 == null) {
            trip3 = new TripImpl();
            trip3.setTopiaId("topiaId:3");
            trip3.setLandingDate(T3Date.newDate(1, 2011).toBeginDate());
            trip3.setVessel(vessel1());
        }
        return trip1;
    }

    public Trip trip4() {
        if (trip4 == null) {
            trip4 = new TripImpl();
            trip4.setTopiaId("topiaId:4");
            trip4.setLandingDate(T3Date.newDate(2, 2011).toBeginDate());
            trip4.setVessel(vessel2());
        }
        return trip4;
    }

    public Trip trip5() {
        if (trip5 == null) {
            trip5 = new TripImpl();
            trip5.setTopiaId("topiaId:5");
            trip5.setLandingDate(T3Date.newDate(1, 2010).toBeginDate());
            trip5.setVessel(vessel2());
        }
        return trip1;
    }

    public Trip trip6() {
        if (trip6 == null) {
            trip6 = new TripImpl();
            trip6.setTopiaId("topiaId:6");
            trip6.setLandingDate(T3Date.newDate(2, 2010).toBeginDate());
            trip6.setVessel(vessel2());
        }
        return trip4;
    }

    public WeightCategoryTreatment weightCategoryTreatment1() {
        if (weightCategoryTreatment1 == null) {
            weightCategoryTreatment1 = new WeightCategoryTreatmentImpl();
            weightCategoryTreatment1.setTopiaId("WeightCategoryTreatment:1");
            weightCategoryTreatment1.setLibelle("WeightCategoryTreatment 1");
            weightCategoryTreatment1.setOcean(oceanAtlantic());
            weightCategoryTreatment1.setMin(0);
            weightCategoryTreatment1.setMax(10);
        }
        return weightCategoryTreatment1;
    }

    public WeightCategoryTreatment weightCategoryTreatment2() {
        if (weightCategoryTreatment2 == null) {
            weightCategoryTreatment2 = new WeightCategoryTreatmentImpl();
            weightCategoryTreatment2.setTopiaId("WeightCategoryTreatment:2");
            weightCategoryTreatment2.setLibelle("WeightCategoryTreatment 2");
            weightCategoryTreatment2.setOcean(oceanAtlantic());
            weightCategoryTreatment2.setMin(10);
            weightCategoryTreatment2.setMax(30);
        }
        return weightCategoryTreatment2;
    }

    public WeightCategoryTreatment weightCategoryTreatment3() {
        if (weightCategoryTreatment3 == null) {
            weightCategoryTreatment3 = new WeightCategoryTreatmentImpl();
            weightCategoryTreatment3.setTopiaId("WeightCategoryTreatment:3");
            weightCategoryTreatment3.setLibelle("WeightCategoryTreatment 3");
            weightCategoryTreatment3.setOcean(oceanAtlantic());
            weightCategoryTreatment3.setMin(30);
        }
        return weightCategoryTreatment3;
    }

    @Override
    public void setServiceContext(T3ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }
}
