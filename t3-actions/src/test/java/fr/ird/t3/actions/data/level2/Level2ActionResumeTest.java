/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.collect.Sets;
import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To test the resume generation of action {@link Level2Action}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level2ActionResumeTest extends AbstractActionResumeTest<Level2Configuration, Level2Action> {

    public Level2ActionResumeTest() {
        super(Level2Configuration.class, Level2Action.class);
    }

    @Override
    protected void prepareConfiguration(Level2Configuration configuration) {
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
        configuration.setStratumMinimumSampleCountObjectSchoolType(10);
        configuration.setStratumMinimumSampleCountFreeSchoolType(20);
        configuration.setTimeStep(4);
        configuration.setStratumWeightRatio(12.5f);
    }

    @Override
    protected void prepareAction(Level2Action action, Locale locale) {
        super.prepareAction(action, locale);

        action.stratumsResult = Sets.newLinkedHashSet();
        L2StratumResult stratumResult = new L2StratumResult(null, "Mon libelle");
        stratumResult.setNbActivities(10);
        stratumResult.setNbActivities(5);

        action.stratumsResult.add(stratumResult);

        action.inputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
        action.outputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> oceans = new TreeMap<String, String>();
        putInMap(oceans, fixtures.oceanAtlantic());
        parameters.put("oceans", oceans);

        Map<String, String> catchFleets = new TreeMap<String, String>();
        putInMap(catchFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
        parameters.put("catchFleets", catchFleets);

        Map<String, String> sampleFleets = new TreeMap<String, String>();
        putInMap(sampleFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
        parameters.put("sampleFleets", sampleFleets);

        Map<String, String> sampleFlags = new TreeMap<String, String>();
        putInMap(sampleFlags, fixtures.frenchCountry(), fixtures.spanishCountry());
        parameters.put("sampleFlags", sampleFlags);

        Map<String, String> species = new TreeMap<String, String>();
        putInMap(species, fixtures.species1(), fixtures.species2());
        parameters.put("species", species);

        Map<String, String> zoneTypes = new TreeMap<String, String>();
        zoneTypes.put("1", "zoneType 1");
        zoneTypes.put("2", "zoneType 2");
        parameters.put("zoneTypes", zoneTypes);

        Map<String, String> zoneVersions = new TreeMap<String, String>();
        ZoneVersion zoneVersion1 = fixtures.zoneVersion1();
        ZoneVersion zoneVersion2 = fixtures.zoneVersion1();
        zoneVersions.put(zoneVersion1.getVersionId(), zoneVersion1.getVersionLibelle());
        zoneVersions.put(zoneVersion2.getVersionId(), zoneVersion2.getVersionLibelle());
        parameters.put("zoneVersions", zoneVersions);

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
