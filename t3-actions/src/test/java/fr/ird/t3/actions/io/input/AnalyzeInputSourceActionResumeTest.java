/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3InputService;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To test render of action {@link AnalyzeInputSourceAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AnalyzeInputSourceActionResumeTest extends AbstractActionResumeTest<AnalyzeInputSourceConfiguration, AnalyzeInputSourceAction> {

    public AnalyzeInputSourceActionResumeTest() {
        super(AnalyzeInputSourceConfiguration.class, AnalyzeInputSourceAction.class);
    }

    @Override
    protected void prepareConfiguration(AnalyzeInputSourceConfiguration conf) {
        configuration.setInputFile(new File("inputSource"));
        configuration.setUseWells(true);
        configuration.setInputProvider(serviceContext.newService(T3InputService.class).getProviders()[0]);
        configuration.setCanCreateVessel(true);
        configuration.setCreateVirtualVessel(true);
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        List<Trip> safeTripList = new ArrayList<Trip>();
        Vessel v2 = fixtures.vessel2();

        safeTripList.add(fixtures.trip1());
        safeTripList.add(fixtures.trip2());

        List<Trip> unsafeTripList = new ArrayList<Trip>();

        unsafeTripList.add(fixtures.trip3());
        unsafeTripList.add(fixtures.trip4());

        Map<Trip, Trip> tripToReplaceList = new HashMap<Trip, Trip>();

        tripToReplaceList.put(safeTripList.get(0), fixtures.trip5());
        tripToReplaceList.put(safeTripList.get(1), fixtures.trip6());

        parameters.put(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, safeTripList);
        parameters.put(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, unsafeTripList);
        parameters.put(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE, tripToReplaceList);

        actionContext.putResult(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE, tripToReplaceList);

        parameters.put("tripDecorator", getDecorator(Trip.class));

        parameters.put("tripDecorator2",
                       getDecorator(Trip.class, DecoratorService.WITH_ID));

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
