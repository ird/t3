/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityImpl;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleImpl;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.SampleWellImpl;
import fr.ird.t3.entities.data.StandardiseSampleSpecies;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesImpl;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripImpl;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesImpl;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.type.T3Date;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Abstract action resume test for all level 1 actions.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AbstractLevel1ActionResumeTest<C extends AbstractLevel1Action> extends AbstractActionResumeTest<Level1Configuration, C> {

    protected AbstractLevel1ActionResumeTest(Class<C> actionClass) {
        super(Level1Configuration.class, actionClass);
    }

    @Override
    protected void prepareConfiguration(Level1Configuration configuration) {
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
    }

    @Override
    protected void prepareAction(C action, Locale locale) {
        super.prepareAction(action, locale);

        // set step in configuration
        action.getConfiguration().setStep(action.getStep());

        Multimap<Trip, Sample> samplesByTrip = ArrayListMultimap.create();
        Vessel v = fixtures.vessel1();
        Vessel v2 = fixtures.vessel2();

        Species sp = new SpeciesImpl();
        sp.setTopiaId("topiaid:1");
        sp.setLibelle("Species 1");
        sp.setCode3L("code3L 1");

        Species sp2 = new SpeciesImpl();
        sp2.setTopiaId("topiaid:2");
        sp2.setLibelle("Species 2");
        sp2.setCode3L("code3L 2");

        Sample s = new SampleImpl();
        s.setTopiaId("topiaid:1");
        s.setSampleNumber(1);

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleWell sw = new SampleWellImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s.addSampleWell(sw);

            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:1");
            sassf.setSpecies(sp);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:2");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleWell sw = new SampleWellImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s.addSampleWell(sw);
            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:3");
            sassf.setSpecies(sp);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:4");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }
        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:1");
            sss.setSpecies(sp);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:1");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:2");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s.addStandardiseSampleSpecies(sss);
        }

        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:3");
            sss.setSpecies(sp2);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:3");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:4");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s.addStandardiseSampleSpecies(sss);
        }

        Sample s2 = new SampleImpl();
        s2.setTopiaId("topiaid:2");
        s2.setSampleNumber(2);

        Sample s3 = new SampleImpl();
        s3.setTopiaId("topiaid:3");
        s3.setSampleNumber(3);
        Sample s4 = new SampleImpl();
        s4.setTopiaId("topiaid:4");
        s4.setSampleNumber(4);

        Trip t = new TripImpl();
        t.setTopiaId("topiaid:1");
        t.setLandingDate(new Date());
        t.setVessel(v);

        t.addSample(s);
        t.addSample(s2);

        samplesByTrip.put(t, s);

        Trip t2 = new TripImpl();
        t2.setTopiaId("topiaid:2");
        t2.setLandingDate(new Date());
        t2.setVessel(v2);
        t2.addSample(s4);
        t2.addSample(s3);

        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:3");
            sss.setSpecies(sp);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:5");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:6");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s4.addStandardiseSampleSpecies(sss);


        }
        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:4");
            sss.setSpecies(sp2);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:7");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:8");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s4.addStandardiseSampleSpecies(sss);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleWell sw = new SampleWellImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s4.addSampleWell(sw);

            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:1");
            sassf.setSpecies(sp2);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:2");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleWell sw = new SampleWellImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s4.addSampleWell(sw);
            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:3");
            sassf.setSpecies(sp2);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:4");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }
        samplesByTrip.put(t2, s4);

        action.setSamplesByTrip(samplesByTrip);
        Multimap<Trip, Sample> notTreatedSamples =
                action.buildSamplesNotTreated(samplesByTrip);
        action.setNotTreatedSamplesByTrip(notTreatedSamples);
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> oceans = new TreeMap<String, String>();
        putInMap(oceans, fixtures.oceanAtlantic());
        parameters.put("oceans", oceans);

        Map<String, String> vesselSimpleTypes = new TreeMap<String, String>();
        putInMap(vesselSimpleTypes,
                 fixtures.vesselSimpleTypeCanneur(),
                 fixtures.vesselSimpleTypeSenneur());
        parameters.put("vesselSimpleTypes", vesselSimpleTypes);

        Map<String, String> fleets = new TreeMap<String, String>();
        putInMap(fleets, fixtures.frenchCountry(), fixtures.spanishCountry());
        parameters.put("fleets", fleets);

        Map<String, String> sampleQualities = new TreeMap<String, String>();
        putInMap(sampleQualities,
                 fixtures.sampleQuality1(),
                 fixtures.sampleQuality2());
        parameters.put("sampleQualities", sampleQualities);

        Map<String, String> sampleTypes = new TreeMap<String, String>();
        putInMap(sampleTypes, fixtures.sampleType1(), fixtures.sampleType2());
        parameters.put("sampleTypes", sampleTypes);

        return parameters;
    }

}
