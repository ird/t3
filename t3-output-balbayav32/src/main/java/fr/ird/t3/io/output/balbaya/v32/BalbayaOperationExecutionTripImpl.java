/*
 * #%L
 * T3 :: Output Balbaya v 32
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import fr.ird.t3.entities.data.ElementaryLanding;
import fr.ird.t3.entities.data.Trip;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Execution of operation {@link T3OutputOperationBalbayaImpl#TRIP_AND_LANDING}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class BalbayaOperationExecutionTripImpl extends AbstractBalbayaOperationExecution {

    public BalbayaOperationExecutionTripImpl(Connection connection) {
        super(connection, T3OutputOperationBalbayaImpl.TRIP_AND_LANDING);
    }

    @Override
    protected boolean checkDatas(T3OutputBalbayaImpl output, List<Trip> trips) throws SQLException {

        // check pkeys before all
        for (Trip trip : trips) {

            int tripVesselCode = trip.getVessel().getCode();
            Date tripLandingDate = trip.getLandingDate();

            boolean exists =
                    checkPKey(CHECK_TRIP, tripVesselCode, tripLandingDate);

            if (exists) {

                // this trip already exists, can not import it, stop operation
                Locale locale = output.getLocale();
                output.getMessager().addErrorMessage(
                        l_(locale, "t3.output.balbaya.error.trip.already.exists",
                           tripVesselCode, tripLandingDate)
                );
                return false;
            }
        }
        return true;
    }

    protected int nbTrips;

    protected int nbElementaryLandings;

    @Override
    protected String getSuccessSummary(T3OutputBalbayaImpl output,
                                       T3OutputBalbayaImpl.TreatmentId id) {
        Locale locale = output.getLocale();
        return l_(locale, "t3.output.balbabya.operation.tripAndLanding.success",
                  id.getNumber(), nbTrips, nbElementaryLandings);
    }

    @Override
    protected void buildRequests(T3OutputBalbayaImpl output,
                                 T3OutputBalbayaImpl.TreatmentId id,
                                 List<Trip> trips) {

        nbTrips = nbElementaryLandings = 0;

        int countryCode = id.getCountryCode();
        int treatmentNumber = id.getNumber();

        for (Trip trip : trips) {

            int tripVesselCode = trip.getVessel().getCode();
            Date tripLandingDate = trip.getLandingDate();

            // add trip

            nbTrips++;
            addRequest(INSERT_MAREE,
                       requests,
                       tripVesselCode,
                       tripLandingDate,
                       trip.getDepartureDate(),
                       trip.getDepartureHarbour().getCode(),
                       trip.getLandingHarbour().getCode(),
                       trip.getTimeAtSea(),
                       trip.getFishingTime(),
                       trip.getLandingTotalWeight(),
                       trip.getFalseFishesWeight(),
                       trip.getLogBookAvailability(),
                       trip.getFishHoldEmpty(),
                       trip.getLoch(),
                       treatmentNumber,
                       countryCode
            );
            if (!trip.isElementaryLandingEmpty()) {

                // add elementary landing
                for (ElementaryLanding elementaryLanding : trip.getElementaryLanding()) {

                    nbElementaryLandings++;

                    addRequest(INSERT_LOT_COM,
                               requests,
                               tripVesselCode,
                               tripLandingDate,
                               elementaryLanding.getNumber(),
                               elementaryLanding.getWeightCategoryLanding().getSpecies().getCode(),
                               elementaryLanding.getWeightCategoryLanding().getCode(),
                               elementaryLanding.getWeight()
                    );
                }
            }
        }
    }

    public static final String CHECK_TRIP = "SELECT count(*) FROM maree where " +
                                            "c_bat = %1$s AND d_dbq = '%2$s'::date;";

    /**
     * To insert a new line into maree table (from Trip entity).
     * <p/>
     * c_bat       | numeric(4,0) | not null
     * d_dbq       | date         | not null
     * d_depart    | date         |
     * c_port_dep  | numeric(3,0) | not null
     * c_port_dbq  | numeric(3,0) | not null
     * c_zone_geo  | numeric(4,0) |
     * v_temps_m   | numeric(4,0) | not null
     * v_temps_p   | numeric(4,0) | not null
     * v_poids_dbq | numeric(7,3) | not null
     * v_poids_fp  | numeric(6,3) | not null
     * f_enq       | numeric(1,0) | not null
     * f_cal_vid   | numeric(1,0) | not null
     * v_loch      | numeric(5,0) |
     * id_jeu_d    | numeric(4,0) | not null
     * c_pays_d    | numeric(3,0) | not null
     */
    public static final String INSERT_MAREE = "INSERT INTO maree (" +
                                              "c_bat," +
                                              "d_dbq," +
                                              "d_depart," +
                                              "c_port_dep, " +
                                              "c_port_dbq," +
                                              "v_temps_m," +
                                              "v_temps_p," +
                                              "v_poids_dbq," +
                                              "v_poids_fp," +
                                              "f_enq," +
                                              "f_cal_vid," +
                                              "v_loch," +
                                              "id_jeu_d," +
                                              "c_pays_d" +
                                              ") VALUES (" +
                                              "%1$s," +
                                              "'%2$s'::date," +
                                              "'%3$s'::date," +
                                              "%4$s," +
                                              "%5$s," +
                                              "%6$s," +
                                              "%7$s," +
                                              "%8$s," +
                                              "%9$s," +
                                              "%10$s," +
                                              "%11$s," +
                                              "%12$s," +
                                              "%13$s," +
                                              "%14$s" +
                                              ");";


    /**
     * To insert a new line into lot_com table (from ElementaryLanding entity).
     * <p/>
     * c_bat      | numeric(4,0) | not null
     * d_dbq      | date         | not null
     * n_lot      | numeric(4,0) | not null
     * c_esp      | numeric(3,0) | not null
     * c_cat_c    | numeric(2,0) | not null
     * v_poids_lc | numeric(7,3) | not null
     */
    public static final String INSERT_LOT_COM = "INSERT INTO lot_com (" +
                                                "c_bat, " +
                                                "d_dbq," +
                                                "n_lot, " +
                                                "c_esp," +
                                                "c_cat_c," +
                                                "v_poids_lc" +
                                                ") VALUES(" +
                                                "%1$s," +
                                                "'%2$s'::date," +
                                                "%3$s," +
                                                "%4$s," +
                                                "%5$s," +
                                                "%6$s" +
                                                ");";
}
