/*
 * #%L
 * T3 :: Output Balbaya v 32
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import com.google.common.collect.Lists;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.data.Trip;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Abstract operation execution.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractBalbayaOperationExecution {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractBalbayaOperationExecution.class);

    protected final Connection connection;

    protected final T3OutputOperationBalbayaImpl operation;

    protected final List<String> requests;

    protected AbstractBalbayaOperationExecution(Connection connection,
                                                T3OutputOperationBalbayaImpl operation) {
        this.connection = connection;
        this.operation = operation;
        this.requests = Lists.newArrayList();
    }

    public String execute(List<Trip> trips,
                          T3OutputBalbayaImpl output) throws Exception {

        String summary;

        boolean check = checkDatas(output, trips);

        Locale locale = output.getLocale();
        if (!check) {

            // stop operation
            summary = l_(locale, "t3.output.balbaya.error.export.checkData");
        } else {
            T3Messager messager = output.getMessager();

            // create new treatment id
            T3OutputBalbayaImpl.TreatmentId treatmentId =
                    createNewTreatmentEntry(output);

            messager.addInfoMessage(l_(locale,"t3.output.balbaya.created_id", treatmentId.getType(), treatmentId.getNumber()));

            buildRequests(output, treatmentId, trips);

            try {

                for (String request : requests) {
                    PreparedStatement sta = connection.prepareStatement(request);
                    try {
                        if (log.isInfoEnabled()) {
                            log.info("Execute request " + request);
                        }
                        messager.addInfoMessage(request);
                        sta.executeUpdate();
                    } finally {
                        sta.close();
                    }
                }

                // commit statements
                connection.commit();

                summary = getSuccessSummary(output, treatmentId);
            } catch (Exception e) {
                try {

                    // rollback statements
                    connection.rollback();
                } finally {
                    if (log.isErrorEnabled()) {
                        log.error("Could not execute operation " + operation +
                                  " for reason " + e.getMessage());
                    }
                    messager.addErrorMessage(e.getMessage());
                }
                summary =  l_(locale, "t3.output.balbaya.error.export.exception", e.getMessage());
            }
        }


        return summary;
    }

    protected abstract boolean checkDatas(T3OutputBalbayaImpl output,
                                          List<Trip> trips) throws SQLException;

    protected abstract void buildRequests(T3OutputBalbayaImpl output,
                                          T3OutputBalbayaImpl.TreatmentId treatmentId,
                                          List<Trip> trips) throws TopiaException;

    protected abstract String getSuccessSummary(T3OutputBalbayaImpl output,
                                                T3OutputBalbayaImpl.TreatmentId id);

    /**
     * @param output
     * @return the treatment request with new freshly id.
     * @throws SQLException if any sql error while asking a treatment id
     */
    protected T3OutputBalbayaImpl.TreatmentId createNewTreatmentEntry(T3OutputBalbayaImpl output) throws SQLException {

        T3OutputBalbayaImpl.TreatmentId id = newTreatmentId(output);
        addRequest(INSERT_A_JEU_D,
                   requests,
                   id.getNumber(),
                   id.getType(),
                   id.getComment(),
                   output.getFleetCode(),
                   output.getOceanCode(),
                   output.getConfiguration().getBeginDate().toBeginSqlDate(),
                   output.getConfiguration().getEndDate().toEndSqlDate(),
                   id.getOrigin(),
                   id.getComment(),
                   new java.sql.Date(new Date().getTime())
        );
        return id;
    }

    protected T3OutputBalbayaImpl.TreatmentId newTreatmentId(T3OutputBalbayaImpl output) throws SQLException {

        // must obtain a new treatment id from the sql connection

        T3OutputBalbayaImpl.TreatmentId result;

        //fixme Use the currentValue + 1 and then only pass sequence to nextval when everything is ok
        PreparedStatement ps = connection.prepareStatement("select nextval('a_jeu_seq');");
        try {
            int treatmentNumber;
            ResultSet resultSet = ps.executeQuery();
            try {
                resultSet.next();
                treatmentNumber = resultSet.getInt(1);
            } finally {
                resultSet.close();
            }
            result = new T3OutputBalbayaImpl.TreatmentId(
                    treatmentNumber,
                    operation.getTreatmentId(),
                    "Export T3+  " + operation.getLibelle(output.getLocale()),
                    "T3+ v" + output.getApplicationConfiguration().getApplicationVersion(),
                    output.getFleetCode(),
                    output.getOceanCode()
            );
        } finally {
            ps.close();
        }
        return result;
    }

    protected void addRequest(String format,
                              List<String> requests,
                              Object... parameters) {
        String request = String.format(format, parameters);
        requests.add(request);
        if (log.isDebugEnabled()) {
            log.debug(request);
        }
    }

    protected boolean checkPKey(String checkPattern,
                                Object... params) throws SQLException {

        String sql = String.format(checkPattern, params);
        PreparedStatement sta = connection.prepareStatement(sql);
        try {
            if (log.isInfoEnabled()) {
                log.info("Execute request " + sql);
            }
            sta.execute();
            ResultSet resultSet = sta.getResultSet();
            try {
                resultSet.next();
                int nbResult = resultSet.getInt(1);
                return nbResult > 0;
            } finally {
                resultSet.close();
            }
        } finally {
            sta.close();
        }
    }

    /**
     * To insert a new line into a_jeu_d table .
     * <p/>
     * id_jeu_d    | numeric(4,0)            | not null
     * c_pays_d    | numeric(3,0)            | not null
     * c_ocea      | numeric(2,0)            | not null
     * c_typ_d     | numeric(2,0)            | not null
     * d_deb_d     | date                    | not null
     * d_fin_d     | date                    | not null
     * l_fic_d     | character varying(128)  | not null
     * l_org_fic_d | character varying(256)  | not null
     * d_dispo     | date                    | not null
     * l_com_ins   | character varying(1024) | not null
     * d_suppr     | date                    |
     * l_com_suppr | character varying(1024) |
     */
    public static final String INSERT_A_JEU_D = "INSERT INTO a_jeu_d (" +
                                                "id_jeu_d, " +
                                                "c_typ_d," +
                                                "l_com_ins," +
                                                "c_pays_d, " +
                                                "c_ocea," +
                                                "d_deb_d," +
                                                "d_fin_d," +
                                                "l_fic_d," +
                                                "l_org_fic_d," +
                                                "d_dispo" +
                                                ") VALUES(" +
                                                "%1$s," +
                                                "%2$s," +
                                                "'%3$s'," +
                                                "%4$s," +
                                                "%5$s," +
                                                "'%6$s'::date," +
                                                "'%7$s'::date," +
                                                "'%8$s'," +
                                                "'%9$s'," +
                                                "'%10$s'::date" +
                                                ");";
}
