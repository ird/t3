fsunrise=

function(jour,mois,an,latitude,longitude,localOffset=4) {
#sunset - sunrise Algorithm
#Source: 	Almanac for Computers, 1990, published by Nautical Almanac Office, United States Naval Observatory,Washington, DC 20392
# passablement transform� par DGaertner
# zenith:                Sun's zenith for sunrise/sunset,   offical  = 90 degrees 50',   civil  = 96 degrees,  nautical  = 102 degrees,  astronomical = 108 degrees
#zenith<-90.5
zenith<-90.833
# zenith officiel !!!!
#Attention: 90.5 transform� en ENT(90.5)+((90.5-ENT(90.5))*(5/3))=90.83311 et COS(RADIANS(90.83311)), ou COS(90.83311*pi/180)= -0.01454
zenith<-trunc(zenith)+((zenith-trunc(zenith))*(5/3))

#Inputs: 	jour, mois, an:      date of sunrise/sunset
	# lat, lon:   location for sunrise/sunset (en degr� d�cimal)
	# NOTE: longitude is positive for East and negative for West
	# localOffset (d�calage horaire)

#1. first calculate the day of the year
	N1 <-floor(275 * mois / 9)
	N2 <-floor((mois + 9) / 12)
	N3<- (1 + floor((an - 4 * floor(an / 4) + 2) / 3))
	N<- N1 - (N2 * N3) + jour - 30

#2. convert the longitude to hour value and calculate an approximate time

	lngHour <-longitude / 15
	#if rising time is desired:
	t.r <- N + ((6 - lngHour) / 24)
	#if setting time is desired:
	t.s <-N + ((18 - lngHour) / 24)

#3. calculate the Sun's mean anomaly

	M.r <- (0.9856 * t.r) - 3.289
	M.s <- (0.9856 * t.s) - 3.289

#4. calculate the Sun's true longitude
#NOTE: L potentially needs to be adjusted into the range [0,360) by adding/subtracting 360

	L.r <- M.r + (1.916 * sin(M.r*pi/180)) + (0.020 * sin(2 * M.r*pi/180)) + 282.634
	if (L.r<0) L.r<-L.r+360
	if (L.r>360) L.r<-L.r-360

	L.s <- M.s + (1.916 * sin(M.s*pi/180)) + (0.020 * sin(2 * M.s*pi/180)) + 282.634
	if (L.s<0) L.s<-L.s+360
	if (L.s>360) L.s<-L.s-360

#5a. calculate the Sun's right ascension
#	NOTE: RA potentially needs to be adjusted into the range [0,360) by adding/subtracting 360

	RA.r <- atan(0.91764 * tan(L.r*pi/180))*180/pi
	if (RA.r<0) RA.r<-RA.r+360
	if (RA.r>360) RA.r<-RA.r-360

	RA.s <- atan(0.91764 * tan(L.s*pi/180))*180/pi
	if (RA.s<0) RA.s<-RA.s+360
	if (RA.s>360) RA.s<-RA.s-360
#5b. right ascension value needs to be in the same quadrant as L

	Lquadrant.r  <- (floor( L.r/90)) * 90
	RAquadrant.r <- (floor(RA.r/90)) * 90
	RA.r <- RA.r + (Lquadrant.r - RAquadrant.r)

	Lquadrant.s  <- (floor( L.s/90)) * 90
	RAquadrant.s <- (floor(RA.s/90)) * 90
	RA.s <- RA.s + (Lquadrant.s - RAquadrant.s)

#5c. right ascension value needs to be converted into hours

	RA.r<- RA.r / 15
	RA.s<- RA.s / 15

#6. calculate the Sun's declination

	sinDec.r <- 0.39782 * sin(L.r*pi/180)
  cosDec.r <- cos(asin(sinDec.r))
	sinDec.s <- 0.39782 * sin(L.s*pi/180)
  cosDec.s <- cos(asin(sinDec.s))

#7a. calculate the Sun's local hour angle

	cosH.r <- (cos(zenith*pi/180) - (sinDec.r * sin(latitude*pi/180))) / (cosDec.r * cos(latitude*pi/180))
	if (cosH.r >  1) { 	  cat(" the sun never rises on this location (on the specified date)","\n")}

	cosH.s <- (cos(zenith*pi/180) - (sinDec.s * sin(latitude*pi/180))) / (cosDec.s * cos(latitude*pi/180))
	if (cosH.s < -1) {	  cat(" the sun never sets on this location (on the specified date)","\n")}

#7b. finish calculating H and convert into hours

	#if rising time is desired:
	  H.r <- 360 - (acos(cosH.r)*180/pi)
	#if setting time is desired:
	  H.s <- (acos(cosH.s)*180/pi)+360

	H.r <- H.r / 15
  H.s <- H.s / 15

#8. calculate local mean time of rising/setting

	Tt.r <- H.r + RA.r - (0.06571 * t.r) - 6.622
	Tt.s <- H.s + RA.s - (0.06571 * t.s) - 6.622

#9. adjust back to UTC
#NOTE: UT potentially needs to be adjusted into the range [0,24) by adding/subtracting 24

	UT.r <- Tt.r - lngHour
	UT.s <- Tt.s - lngHour
	#if rising time is desired:
	if (UT.r<0) UT.r<-UT.r+24
	if (UT.r>24) UT.r<-UT.r-24

#	10. convert UT value to local time zone of latitude/longitude

	localT.r <- UT.r + localOffset
	localT.s <- UT.s + localOffset
LocalT.r<-round(trunc(localT.r)+((localT.r-trunc(localT.r))*(3/5)),2)
LocalT.s<-round(trunc(localT.s)+((localT.s-trunc(localT.s))*(3/5)),2)


return (data.frame(UT.r,UT.s,LocalT.r,LocalT.s)) }