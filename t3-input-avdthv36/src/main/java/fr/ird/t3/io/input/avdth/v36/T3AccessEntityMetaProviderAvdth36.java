/*
 * #%L
 * T3 :: Input AVDTH v 36
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v36;

import com.google.common.collect.Sets;
import fr.ird.msaccess.type.IntToCoordonne;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityFishingContext;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.data.ElementaryLanding;
import fr.ird.t3.entities.data.LocalMarketBatch;
import fr.ird.t3.entities.data.LocalMarketSample;
import fr.ird.t3.entities.data.LocalMarketSampleSpecies;
import fr.ird.t3.entities.data.LocalMarketSampleSpeciesFrequency;
import fr.ird.t3.entities.data.LocalMarketSampleWell;
import fr.ird.t3.entities.data.LocalMarketSurvey;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSpecies;
import fr.ird.t3.entities.data.SampleSpeciesFrequency;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.data.WellPlan;
import fr.ird.t3.entities.reference.Company;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.ElementaryCatchFate;
import fr.ird.t3.entities.reference.ElementaryLandingFate;
import fr.ird.t3.entities.reference.FishingContext;
import fr.ird.t3.entities.reference.FpaZone;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.LocalMarketPackaging;
import fr.ird.t3.entities.reference.LocalMarketPackagingType;
import fr.ird.t3.entities.reference.ObjectType;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.TransmittingBuoyType;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselActivity;
import fr.ird.t3.entities.reference.VesselSizeCategory;
import fr.ird.t3.entities.reference.VesselType;
import fr.ird.t3.entities.reference.WeightCategoryLanding;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.entities.reference.WeightCategoryWellPlan;
import fr.ird.t3.entities.reference.WellDestiny;
import fr.ird.t3.io.input.access.T3AccessDataEntityMeta;
import fr.ird.t3.io.input.access.T3AccessEntityMeta;
import fr.ird.t3.io.input.access.T3AccessEntityMetaProvider;
import fr.ird.t3.io.input.access.T3AccessReferentielEntityMeta;
import fr.ird.t3.io.input.access.type.IntToBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Set;

/**
 * Provider of {@link T3AccessEntityMeta}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 2.0
 */
public class T3AccessEntityMetaProviderAvdth36 implements T3AccessEntityMetaProvider {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3AccessEntityMetaProviderAvdth36.class);

    @Override
    public Set<T3AccessEntityMeta> getMetas() {
        Set<T3AccessEntityMeta> result = Sets.newLinkedHashSet();

        // ACTIVITE : [C_BAT, C_OCEA, C_OPERA, C_TBANC, D_ACT, D_DBQ, F_CUVE_C, F_DON_ORG, F_EXPERT, F_OBS, F_POS_COR, F_POS_VMS_D, H_ACT, N_ACT, Q_ACT, V_COUR_DIR, V_COUR_VIT, V_LAT, V_LON, V_NB_OP, V_POIDS_CAP, V_TEMP_S, V_TMER, V_TPEC]
        registerDataMeta(
                result,
                T3EntityEnum.Activity,
                "ACTIVITE",
                new String[]{"C_BAT", "D_DBQ", "D_ACT", "N_ACT"},
                new Object[]{
                        Activity.PROPERTY_ACTIVITY_FISHING_CONTEXT, ActivityFishingContext.class,
                        Activity.PROPERTY_ELEMENTARY_CATCH, ElementaryCatch.class
                },
                new Object[]{
                        Activity.PROPERTY_TRIP, Trip.class
                },
                Activity.PROPERTY_DATE, Date.class, "D_ACT",
                Activity.PROPERTY_NUMBER, Integer.class, "N_ACT",
                Activity.PROPERTY_OCEAN, Ocean.class, "C_OCEA",
                Activity.PROPERTY_SCHOOL_TYPE, SchoolType.class, "C_TBANC",
                Activity.PROPERTY_OBSERVED_FLAG, Integer.class, "F_OBS",
                Activity.PROPERTY_EXPERT_FLAG, Integer.class, "F_EXPERT",
                Activity.PROPERTY_LATITUDE, IntToCoordonne.class, "V_LAT",
                Activity.PROPERTY_LONGITUDE, IntToCoordonne.class, "V_LON",
                Activity.PROPERTY_SET_COUNT, Integer.class, "V_NB_OP",
                Activity.PROPERTY_TOTAL_CATCH_WEIGHT, Float.class, "V_POIDS_CAP",
                Activity.PROPERTY_VESSEL_ACTIVITY, VesselActivity.class, "C_OPERA",
                Activity.PROPERTY_CURRENT_DIRECTION, Float.class, "V_COUR_DIR",
                Activity.PROPERTY_CURRENT_VELOCITY, Float.class, "V_COUR_VIT",
                Activity.PROPERTY_DIVERGENT_VMSPOSITION, Integer.class, "F_POS_VMS_D",
                Activity.PROPERTY_QUADRANT, Integer.class, "Q_ACT",

                Activity.PROPERTY_FIXED_POSITION_FLAG, Integer.class, "F_POS_COR",
                Activity.PROPERTY_WELL_COMPATIBILITY, Integer.class, "F_CUVE_C",

                Activity.PROPERTY_SURFACE_TEMPERATURE, Float.class, "V_TEMP_S",
                Activity.PROPERTY_FISHING_TIME, Float.class, "V_TPEC",
                Activity.PROPERTY_TIME_AT_SEA, Float.class, "V_TMER",
                Activity.PROPERTY_ORIGINAL_DATA_FLAG, Integer.class, "F_DON_ORG",
                Activity.PROPERTY_WIND_DIRECTION, Float.class, "V_VENT_DIR",
                Activity.PROPERTY_WIND_SPEED, Float.class, "V_VENT_VIT",
                Activity.PROPERTY_OBJECT_TYPE, ObjectType.class, "C_TYP_OBJET",
                Activity.PROPERTY_ECOLOGICAL_OBJECT, Integer.class, "F_DCP_ECO",
                Activity.PROPERTY_BUOY_OWNERSHIP, Integer.class, "F_PROP_BALISE",
                Activity.PROPERTY_TRANSMITTING_BUOY_TYPE, TransmittingBuoyType.class, "C_TYP_BALISE",
                Activity.PROPERTY_BUOY_ID, String.class, "V_ID_BALISE",
                Activity.PROPERTY_BUOY_RELATED_WEIGHT, Float.class, "V_POIDS_ESTIM_DCP",
                Activity.PROPERTY_COMMENT, String.class, "L_COM_A",
                Activity.PROPERTY_FPA_ZONE, FpaZone.class, "C_Z_FPA"

        );

        // CAPT_ELEM : [C_BAT, C_CAT_T, C_ESP, D_ACT, D_DBQ, N_ACT, N_CAPT, V_POIDS_CAPT]
        registerDataMeta(
                result,
                T3EntityEnum.ElementaryCatch,
                "CAPT_ELEM",
                new String[]{"C_BAT", "D_DBQ", "D_ACT", "N_ACT", "N_CAPT"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                ElementaryCatch.PROPERTY_WEIGHT_CATEGORY_LOG_BOOK, WeightCategoryLogBook.class, "C_CAT_T",
                ElementaryCatch.PROPERTY_ELEMENTARY_CATCH_FATE, ElementaryCatchFate.class, "C_DEST",
                ElementaryCatch.PROPERTY_CATCH_WEIGHT, Float.class, "V_POIDS_CAPT"
        );

        // LOT_COM : [C_BAT, C_CAT_C, C_ESP, D_DBQ, N_LOT, V_POIDS_LC]
        registerDataMeta(
                result,
                T3EntityEnum.ElementaryLanding,
                "LOT_COM",
                new String[]{"C_BAT", "D_DBQ", "N_LOT"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                ElementaryLanding.PROPERTY_NUMBER, Integer.class, "N_LOT",
                ElementaryLanding.PROPERTY_WEIGHT_CATEGORY_LANDING, WeightCategoryLanding.class, "C_CAT_C",
                ElementaryLanding.PROPERTY_ELEMENTARY_LANDING_FATE, ElementaryLandingFate.class, "C_DEST",
                ElementaryLanding.PROPERTY_WEIGHT, Float.class, "V_POIDS_LC"
        );

        // ACT_ASSOC : [C_ASSOC, C_BAT, D_ACT, D_DBQ, N_ACT, N_ASSOC]
        registerDataMeta(
                result,
                T3EntityEnum.ActivityFishingContext,
                "ACT_ASSOC",
                new String[]{"C_BAT", "D_DBQ", "D_ACT", "N_ACT", "N_ASSOC"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                ActivityFishingContext.PROPERTY_STEP, Integer.class, "N_ASSOC",
                ActivityFishingContext.PROPERTY_FISHING_CONTEXT, FishingContext.class, "C_ASSOC"
        );

        // ECHANTILLON : [C_BAT, C_PORT_DBQ, C_QUAL_ECH, C_TYP_ECH, D_DBQ, F_POS_CUVE, F_S_ECH, N_CUVE, N_ECH, V_POIDS_ECH, V_POIDS_M10, V_POIDS_P10]
        // ECHANTILLON : [F_POS_CUVE, F_S_ECH, N_CUVE]
        registerDataMeta(
                result,
                T3EntityEnum.Sample,
                "ECHANTILLON",
                new String[]{"C_BAT", "D_DBQ", "N_ECH"},
                new Object[]{
                        Sample.PROPERTY_SAMPLE_WELL, SampleWell.class,
                        Sample.PROPERTY_SAMPLE_SPECIES, SampleSpecies.class
                },
                new Object[]{
                        Sample.PROPERTY_WELL, Well.class,
                },
                Sample.PROPERTY_SAMPLE_TYPE, SampleType.class, "C_TYP_ECH",
                Sample.PROPERTY_SAMPLE_QUALITY, SampleQuality.class, "C_QUAL_ECH",
                Sample.PROPERTY_LANDING_HARBOUR, Harbour.class, "C_PORT_DBQ",
                Sample.PROPERTY_SAMPLE_NUMBER, Integer.class, "N_ECH",
                Sample.PROPERTY_GLOBAL_WEIGHT, Float.class, "V_POIDS_ECH",
                Sample.PROPERTY_MINUS10_WEIGHT, Float.class, "V_POIDS_M10",
                Sample.PROPERTY_PLUS10_WEIGHT, Float.class, "V_POIDS_P10",
                Sample.PROPERTY_SUPER_SAMPLE_FLAG, IntToBoolean.class, "F_S_ECH",
                Sample.PROPERTY_WELL_NUMBER, Integer.class, "N_CUVE",
                Sample.PROPERTY_WELL_POSITION, Integer.class, "F_POS_CUVE"
        );

        // ECH_CALEE : [C_BAT, C_TBANC, C_ZONE_GEO, D_ACT, D_DBQ, N_ACT, N_ECH, Q_ACT, V_LAT, V_LON, V_POND]
        registerDataMeta(
                result,
                T3EntityEnum.SampleWell,
                "ECH_CALEE",
                new String[]{"C_BAT", "D_DBQ", "D_ACT", "N_ACT", "N_ECH"},
                EMPTY_OBJECT_ARRAY,
                new Object[]{
                        SampleWell.PROPERTY_ACTIVITY, Activity.class,
                },
                SampleWell.PROPERTY_WEIGHTED_WEIGHT, Float.class, "V_POND"
        );

        // ECH_ESP : [C_BAT, C_ESP, D_DBQ, F_LDLF, N_ECH, N_S_ECH, V_NB_MES, V_NB_TOT]
        registerDataMeta(
                result,
                T3EntityEnum.SampleSpecies,
                "ECH_ESP",
                new String[]{"C_BAT", "D_DBQ", "N_ECH", "F_LDLF", "N_S_ECH", "C_ESP"},
                new Object[]{
                        SampleSpecies.PROPERTY_SAMPLE_SPECIES_FREQUENCY, SampleSpeciesFrequency.class
                },
                EMPTY_OBJECT_ARRAY,
                SampleSpecies.PROPERTY_LDLF_FLAG, Integer.class, "F_LDLF",
                SampleSpecies.PROPERTY_SPECIES, Species.class, "C_ESP",
                SampleSpecies.PROPERTY_SUPER_SAMPLE_NUMBER, Integer.class, "N_S_ECH",
                SampleSpecies.PROPERTY_TOTAL_COUNT, Float.class, "V_NB_TOT",
                SampleSpecies.PROPERTY_MEASURED_COUNT, Float.class, "V_NB_MES"
        );

        // ECH_FREQT : [C_BAT, C_ESP, D_DBQ, F_LDLF, N_ECH, N_S_ECH, V_EFF, V_LONG]
        registerDataMeta(
                result,
                T3EntityEnum.SampleSpeciesFrequency,
                "ECH_FREQT",
                new String[]{"C_BAT", "D_DBQ", "N_ECH", "N_S_ECH", "C_ESP", "F_LDLF"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                SampleSpeciesFrequency.PROPERTY_LENGTH_CLASS, Integer.class, "V_LONG",
                SampleSpeciesFrequency.PROPERTY_NUMBER, Integer.class, "V_EFF"
        );

        // CUVE : [C_BAT, C_DEST, D_DBQ, F_POS_CUVE, N_CUVE]
        registerDataMeta(
                result,
                T3EntityEnum.Well,
                "CUVE",
                new String[]{"C_BAT", "D_DBQ", "N_CUVE", "F_POS_CUVE"},
                new Object[]{
                        Well.PROPERTY_WELL_PLAN, WellPlan.class
                },
                EMPTY_OBJECT_ARRAY,
                Well.PROPERTY_WELL_NUMBER, Integer.class, "N_CUVE",
                Well.PROPERTY_WELL_POSITION, Integer.class, "F_POS_CUVE",
                Well.PROPERTY_WELL_DESTINY, WellDestiny.class, "C_DEST"
        );

        // CUVE_CALEE : [C_BAT, C_CAT_POIDS, C_ESP, D_ACT, D_DBQ, F_POS_CUVE, N_ACT, N_CALESP, N_CUVE, V_NB, V_POIDS]
        registerDataMeta(
                result,
                T3EntityEnum.WellPlan,
                "CUVE_CALEE",
                new String[]{"C_BAT", "D_DBQ", "N_CUVE", "F_POS_CUVE", "N_CALESP"},
                EMPTY_OBJECT_ARRAY,
                new Object[]{
                        WellPlan.PROPERTY_ACTIVITY, Activity.class
                },
                WellPlan.PROPERTY_SPECIES, Species.class, "C_ESP",
                WellPlan.PROPERTY_WEIGHT_CATEGORY_WELL_PLAN, WeightCategoryWellPlan.class, "C_CAT_POIDS",
                WellPlan.PROPERTY_WEIGHT, Float.class, "V_POIDS",
                WellPlan.PROPERTY_NUMBER, Integer.class, "V_NB"
        );

        // MAREE : [C_BAT, C_PORT_DBQ, C_PORT_DEP, C_ZONE_GEO, D_DBQ, D_DEPART, F_CAL_VID, F_ENQ, L_COM_M, V_LOCH, V_POIDS_DBQ, V_POIDS_FP, V_TEMPS_M, V_TEMPS_P]
        registerDataMeta(
                result,
                T3EntityEnum.Trip,
                "MAREE",
                new String[]{"C_BAT", "D_DBQ"},
                new Object[]{
                        Trip.PROPERTY_ACTIVITY, Activity.class,
                        Trip.PROPERTY_ELEMENTARY_LANDING, ElementaryLanding.class,
                        Trip.PROPERTY_WELL, Well.class,
                        Trip.PROPERTY_SAMPLE, Sample.class,
                        Trip.PROPERTY_LOCAL_MARKET_BATCH, LocalMarketBatch.class,
                        Trip.PROPERTY_LOCAL_MARKET_SAMPLE, LocalMarketSample.class,
                        Trip.PROPERTY_LOCAL_MARKET_SURVEY, LocalMarketSurvey.class
                },
                EMPTY_OBJECT_ARRAY,
                Trip.PROPERTY_VESSEL, Vessel.class, "C_BAT",
                Trip.PROPERTY_LANDING_HARBOUR, Harbour.class, "C_PORT_DBQ",
                Trip.PROPERTY_DEPARTURE_HARBOUR, Harbour.class, "C_PORT_DEP",
                Trip.PROPERTY_LANDING_DATE, Date.class, "D_DBQ",
                Trip.PROPERTY_DEPARTURE_DATE, Date.class, "D_DEPART",
                Trip.PROPERTY_COMMENT, String.class, "L_COM_M",
                Trip.PROPERTY_LOCH, Integer.class, "V_LOCH",
                Trip.PROPERTY_TIME_AT_SEA, Float.class, "V_TEMPS_M",
                Trip.PROPERTY_FISHING_TIME, Float.class, "V_TEMPS_P",
                Trip.PROPERTY_FALSE_FISHES_WEIGHT, Float.class, "V_POIDS_FP",
                Trip.PROPERTY_LANDING_TOTAL_WEIGHT, Float.class, "V_POIDS_DBQ",
                Trip.PROPERTY_FISH_HOLD_EMPTY, Integer.class, "F_CAL_VID",
                Trip.PROPERTY_LOG_BOOK_AVAILABILITY, Integer.class, "F_ENQ"
        );

        // FP_LOT : [C_BAT, D_DBQ, N_LOT, C_ESP, C_COND, , V_POIDS_PESE, L_ORIGINE, L_COM]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketBatch,
                "FP_LOT",
                new String[]{"C_BAT", "D_DBQ", "N_LOT"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                LocalMarketBatch.PROPERTY_BATCH_ID, String.class, "N_LOT",
                LocalMarketBatch.PROPERTY_SPECIES, Species.class, "C_ESP",
                LocalMarketBatch.PROPERTY_LOCAL_MARKET_PACKAGING, LocalMarketPackaging.class, "C_COND",
                LocalMarketBatch.PROPERTY_COUNT, Float.class, "N_UNIT",
                LocalMarketBatch.PROPERTY_WEIGHT, Float.class, "V_POIDS_PESE",
                LocalMarketBatch.PROPERTY_ORIGIN, String.class, "L_ORIGINE",
                LocalMarketBatch.PROPERTY_COMMENT, String.class, "L_COM"
        );

        // FP_ECHANTILLON : [C_BAT, D_DBQ, N_ECH, D_ECH, C_BAT_REEL]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketSample,
                "FP_ECHANTILLON",
                new String[]{"C_BAT", "D_DBQ", "N_ECH"},
                new Object[]{
                        LocalMarketSample.PROPERTY_LOCAL_MARKET_SAMPLE_SPECIES, LocalMarketSampleSpecies.class,
                        LocalMarketSample.PROPERTY_LOCAL_MARKET_SAMPLE_WELL, LocalMarketSampleWell.class
                },
                EMPTY_OBJECT_ARRAY,
                LocalMarketSample.PROPERTY_SAMPLE_ID, String.class, "N_ECH",
                LocalMarketSample.PROPERTY_DATE, Date.class, "D_ECH",
                LocalMarketSample.PROPERTY_REAL_VESSEL, Vessel.class, "C_BAT_REEL"
        );

        // FP_ECH_ESP : [C_BAT, D_DBQ, N_ECH, C_ESP, F_LDLF, V_NB_MES]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketSampleSpecies,
                "FP_ECH_ESP",
                new String[]{"C_BAT", "D_DBQ", "N_ECH", "C_ESP", "F_LDLF"},
                new Object[]{
                        LocalMarketSampleSpecies.PROPERTY_LOCAL_MARKET_SAMPLE_SPECIES_FREQUENCY, LocalMarketSampleSpeciesFrequency.class
                },
                EMPTY_OBJECT_ARRAY,
                LocalMarketSampleSpecies.PROPERTY_SPECIES, Species.class, "C_ESP",
                LocalMarketSampleSpecies.PROPERTY_LDLF_FLAG, int.class, "F_LDLF",
                LocalMarketSampleSpecies.PROPERTY_MEASURED_COUNT, Float.class, "V_NB_MES"
        );

        // FP_ECH_FREQT : [C_BAT, D_DBQ, N_ECH, C_ESP, F_LDLF, V_LONG, V_EFF]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketSampleSpeciesFrequency,
                "FP_ECH_FREQT",
                new String[]{"C_BAT", "D_DBQ", "N_ECH", "C_ESP", "F_LDLF", "V_LONG"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                LocalMarketSampleSpeciesFrequency.PROPERTY_LENGTH_CLASS, float.class, "V_LONG",
                LocalMarketSampleSpeciesFrequency.PROPERTY_NUMBER, int.class, "V_EFF"
        );

        // FP_ECH_CUVE : [C_BAT, D_DBQ, N_ECH, N_CUVE, C_POSITION]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketSampleWell,
                "FP_ECH_CUVE",
                new String[]{"C_BAT", "D_DBQ", "N_ECH", "N_CUVE", "C_POSITION"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                LocalMarketSampleWell.PROPERTY_WELL_NUMBER, int.class, "N_CUVE",
                LocalMarketSampleWell.PROPERTY_WELL_POSITION, int.class, "C_POSITION"
        );

        // FP_SONDAGE : [C_BAT, D_DBQ, N_SOND, C_ESP, V_PROP]
        registerDataMeta(
                result,
                T3EntityEnum.LocalMarketSurvey,
                "FP_SONDAGE",
                new String[]{"C_BAT", "D_DBQ", "N_SOND", "C_ESP"},
                EMPTY_OBJECT_ARRAY,
                EMPTY_OBJECT_ARRAY,
                LocalMarketSurvey.PROPERTY_SURVEY_ID, String.class, "N_SOND",
                LocalMarketSurvey.PROPERTY_SPECIES, Species.class, "C_ESP",
                LocalMarketSurvey.PROPERTY_PROPORTION, float.class, "V_PROP"
        );

        // OPERA : [C_OPERA, L_OP8L, L_OPERA]
        registerReferentielMeta(
                result,
                T3EntityEnum.VesselActivity,
                "OPERA",
                new String[]{"C_OPERA"},
                VesselActivity.PROPERTY_CODE, Integer.class, "C_OPERA",
                VesselActivity.PROPERTY_LIBELLE, String.class, "L_OPERA",
                VesselActivity.PROPERTY_LIBELLE8, String.class, "L_OP8L"
        );

        // CAT_BATEAU : [C_CAT_B, L_CAPAC, L_JAUGE]
        registerReferentielMeta(
                result,
                T3EntityEnum.VesselSizeCategory,
                "CAT_BATEAU",
                new String[]{"C_CAT_B"},
                VesselSizeCategory.PROPERTY_CODE, Integer.class, "C_CAT_B",
                VesselSizeCategory.PROPERTY_CAPACITY_LIBELLE, String.class, "L_CAPAC",
                VesselSizeCategory.PROPERTY_GAUGE_LIBELLE, String.class, "L_JAUGE"
        );

        // BATEAU : [AN_SERV, C_BAT, C_CAT_B, C_FLOTTE, C_PAYS, C_QUILLE, C_TYP_B, D_CHGT_PAV, L_BAT, L_COM_B, V_CT_M3, V_L_HT, V_MAX_RECH, V_P_CV]
        registerReferentielMeta(
                result,
                T3EntityEnum.Vessel,
                "BATEAU",
                new String[]{"C_BAT"},
                Vessel.PROPERTY_CODE, Integer.class, "C_BAT",
                Vessel.PROPERTY_VESSEL_TYPE, VesselType.class, "C_TYP_B",
                Vessel.PROPERTY_VESSEL_SIZE_CATEGORY, VesselSizeCategory.class, "C_CAT_B",
                Vessel.PROPERTY_COMPANY, Company.class, "C_ARMEMENT",
                Vessel.PROPERTY_FLAG_COUNTRY, Country.class, "C_PAYS",
                Vessel.PROPERTY_FLEET_COUNTRY, Country.class, "C_FLOTTE",
                Vessel.PROPERTY_COMMENT, String.class, "L_COM_B",
                Vessel.PROPERTY_LIBELLE, String.class, "L_BAT",
                Vessel.PROPERTY_YEAR_SERVICE, Integer.class, "AN_SERV",
                Vessel.PROPERTY_KEEL_CODE, Integer.class, "C_QUILLE",
                Vessel.PROPERTY_POWER, Float.class, "V_P_CV",
                Vessel.PROPERTY_FLAG_CHANGE_DATE, Date.class, "D_CHGT_PAV",
                Vessel.PROPERTY_LENGTH, Float.class, "V_L_HT",
                Vessel.PROPERTY_SEARCH_MAXIMUM, Float.class, "V_MAX_RECH",
                Vessel.PROPERTY_CAPACITY, Float.class, "V_CT_M3",
                Vessel.PROPERTY_SPEED_MAXIMUM, Float.class, "V_MAX",
                Vessel.PROPERTY_VALIDITY_START_DATE, Date.class, "D_DEBUT",
                Vessel.PROPERTY_VALIDITY_END_DATE, Float.class, "D_FIN",
                Vessel.PROPERTY_FAO_CODE, String.class, "C_IMMAT_FAO",
                Vessel.PROPERTY_CTOI_CODE, String.class, "C_IMMAT_CTOI",
                Vessel.PROPERTY_IATTC_CODE, String.class, "C_IMMAT_IATTC"
        );

        // TYPE_BATEAU : [C_TYP_B, L_TYP_B]
        registerReferentielMeta(
                result,
                T3EntityEnum.VesselType,
                "TYPE_BATEAU",
                new String[]{"C_TYP_B"},
                VesselType.PROPERTY_CODE, Integer.class, "C_TYP_B",
                VesselType.PROPERTY_LIBELLE, String.class, "L_TYP_B"
        );

        // CAT_COM : [C_CAT_C, C_ESP, L_CC_SOV, L_CC_STAR]
        registerReferentielMeta(
                result,
                T3EntityEnum.WeightCategoryLanding,
                "CAT_COM",
                new String[]{"C_CAT_C", "C_ESP"},
                WeightCategoryLanding.PROPERTY_CODE, Integer.class, "C_CAT_C",
                WeightCategoryLanding.PROPERTY_SPECIES, Species.class, "C_ESP",
                WeightCategoryLanding.PROPERTY_SOV_LIBELLE, String.class, "L_CC_SOV",
                WeightCategoryLanding.PROPERTY_STAR_LIBELLE, String.class, "L_CC_STAR"
        );

        // PAYS : [C_PAYS, L_PAYS]
        registerReferentielMeta(
                result,
                T3EntityEnum.Country,
                "PAYS",
                new String[]{"C_PAYS"},
                Country.PROPERTY_CODE, Integer.class, "C_PAYS",
                Country.PROPERTY_LIBELLE, String.class, "L_PAYS"
        );

        // ASSOC : [C_ASSOC, C_ASSOC_G, C_ASSOC_R, L_ASSOC]
        registerReferentielMeta(
                result,
                T3EntityEnum.FishingContext,
                "ASSOC",
                new String[]{"C_ASSOC"},
                FishingContext.PROPERTY_CODE, Integer.class, "C_ASSOC",
                FishingContext.PROPERTY_SCHOOL_TYPE, SchoolType.class, "C_ASSOC_G",
                FishingContext.PROPERTY_REDUCED_CODE, Integer.class, "C_ASSOC_R",
                FishingContext.PROPERTY_LIBELLE, String.class, "L_ASSOC"
        );

        // TYPE_BANC : [C_TBANC, L_TBANC, L_TBANC4L]
        registerReferentielMeta(
                result,
                T3EntityEnum.SchoolType,
                "TYPE_BANC",
                new String[]{"C_TBANC"},
                SchoolType.PROPERTY_CODE, Integer.class, "C_TBANC",
                SchoolType.PROPERTY_LIBELLE, String.class, "L_TBANC",
                SchoolType.PROPERTY_LIBELLE4, String.class, "L_TBANC4L"
        );

        // OCEAN : [C_OCEA, L_OCEA]
        registerReferentielMeta(
                result,
                T3EntityEnum.Ocean,
                "OCEAN",
                new String[]{"C_OCEA"},
                Ocean.PROPERTY_CODE, Integer.class, "C_OCEA",
                Ocean.PROPERTY_LIBELLE, String.class, "L_OCEA"
        );

        // PORT : [C_PORT, L_COM_P, L_PORT, V_LAT_P, V_LON_P]
        registerReferentielMeta(
                result,
                T3EntityEnum.Harbour,
                "PORT",
                new String[]{"C_PORT"},
                Harbour.PROPERTY_CODE, Integer.class, "C_PORT",
                Harbour.PROPERTY_COMMENT, String.class, "L_COM_P",
                Harbour.PROPERTY_LIBELLE, String.class, "L_PORT",
                Harbour.PROPERTY_LATITUDE, Float.class, "V_LAT_P",
                Harbour.PROPERTY_LONGITUDE, Float.class, "V_LON_P",
                Harbour.PROPERTY_OCEAN, Ocean.class, "C_OCEA"
        );

        // QUAL_ECH : [C_QUAL_ECH, L_QUAL_ECH]
        registerReferentielMeta(
                result,
                T3EntityEnum.SampleQuality,
                "QUAL_ECH",
                new String[]{"C_QUAL_ECH"},
                SampleQuality.PROPERTY_CODE, Integer.class, "C_QUAL_ECH",
                SampleQuality.PROPERTY_LIBELLE, String.class, "L_QUAL_ECH"
        );

        // TYPE_ECHANT : [C_TYP_ECH, L_TYP_ECH]
        registerReferentielMeta(
                result,
                T3EntityEnum.SampleType,
                "TYPE_ECHANT",
                new String[]{"C_TYP_ECH"},
                SampleType.PROPERTY_CODE, Integer.class, "C_TYP_ECH",
                SampleType.PROPERTY_LIBELLE, String.class, "L_TYP_ECH"
        );

        // CAT_TAILLE : [C_CAT_T, C_ESP, L_CAT_T, V_POIDS_M]
        registerReferentielMeta(
                result,
                T3EntityEnum.WeightCategoryLogBook,
                "CAT_TAILLE",
                new String[]{"C_CAT_T", "C_ESP"},
                WeightCategoryLogBook.PROPERTY_CODE, Integer.class, "C_CAT_T",
                WeightCategoryLogBook.PROPERTY_LIBELLE, String.class, "L_CAT_T",
                WeightCategoryLogBook.PROPERTY_MAXIMUM_WEIGHT, Float.class, "V_POIDS_M",
                WeightCategoryLogBook.PROPERTY_SPECIES, Species.class, "C_ESP"
        );

        // ESPECE : [C_ESP, C_ESP_3L, L_ESP, L_ESP_S]
        registerReferentielMeta(
                result,
                T3EntityEnum.Species,
                "ESPECE",
                new String[]{"C_ESP"},
                Species.PROPERTY_CODE, Integer.class, "C_ESP",
                Species.PROPERTY_CODE3_L, String.class, "C_ESP_3L",
                Species.PROPERTY_LIBELLE, String.class, "L_ESP",
                Species.PROPERTY_SCIENTIFIC_LIBELLE, String.class, "L_ESP_S"
        );

        // DESTIN : [C_DEST, L_DEST]
        registerReferentielMeta(
                result,
                T3EntityEnum.WellDestiny,
                "DESTIN",
                new String[]{"C_DEST"},
                WellDestiny.PROPERTY_CODE, Integer.class, "C_DEST",
                WellDestiny.PROPERTY_LIBELLE, String.class, "L_DEST"
        );

        // CAT_POIDS : [C_CAT_POIDS, L_CAT_POIDS]
        registerReferentielMeta(
                result,
                T3EntityEnum.WeightCategoryWellPlan,
                "CAT_POIDS",
                new String[]{"C_CAT_POIDS"},
                WeightCategoryWellPlan.PROPERTY_CODE, Integer.class, "C_CAT_POIDS",
                WeightCategoryWellPlan.PROPERTY_LIBELLE, String.class, "L_CAT_POIDS"
        );

        // FP_TYPE_COND : [C_TYP_COND, L_TYP_COND]
        registerReferentielMeta(
                result,
                T3EntityEnum.LocalMarketPackagingType,
                "FP_TYPE_COND",
                new String[]{"C_TYP_COND"},
                LocalMarketPackagingType.PROPERTY_CODE, Integer.class, "C_TYP_COND",
                LocalMarketPackagingType.PROPERTY_LIBELLE, String.class, "L_TYP_COND"
        );

        // FP_CONDITIONNEMENT : [C_COND, C_PORT, D_DEBUT, D_FIN, L_COND, C_TYP_COND, V_POIDS]
        registerReferentielMeta(
                result,
                T3EntityEnum.LocalMarketPackaging,
                "FP_CONDITIONNEMENT",
                new String[]{"C_COND"},
                LocalMarketPackaging.PROPERTY_CODE, Integer.class, "C_COND",
                LocalMarketPackaging.PROPERTY_LIBELLE, String.class, "L_COND",
                LocalMarketPackaging.PROPERTY_START_DATE, Date.class, "D_DEBUT",
                LocalMarketPackaging.PROPERTY_END_DATE, Date.class, "D_FIN",
                LocalMarketPackaging.PROPERTY_LOCAL_MARKET_PACKAGING_TYPE, LocalMarketPackagingType.class, "C_TYP_COND",
                LocalMarketPackaging.PROPERTY_WEIGHT, Float.class, "V_POIDS"
        );

        // TYPE_OBJET : [C_TYP_OBJET, C_RFMOS, L_TYP_OBJET, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.ObjectType,
                "TYPE_OBJET",
                new String[]{"C_TYP_OBJET"},
                ObjectType.PROPERTY_CODE, Integer.class, "C_TYP_OBJET",
                ObjectType.PROPERTY_LIBELLE, String.class, "L_TYP_OBJET",
                ObjectType.PROPERTY_RFMOS_CODE, String.class, "C_RFMOS",
                ObjectType.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        // TYPE_BALISE: [C_TYP_BALISE, L_TYP_BALISE, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.TransmittingBuoyType,
                "TYPE_BALISE",
                new String[]{"C_TYP_BALISE"},
                TransmittingBuoyType.PROPERTY_CODE, Integer.class, "C_TYP_BALISE",
                TransmittingBuoyType.PROPERTY_LIBELLE, String.class, "L_TYP_BALISE",
                TransmittingBuoyType.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        // ZONE_FPA : [C_Z_FPA, L_Z_FPA, C_PAYS, C_SUBDIV, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.FpaZone,
                "ZONE_FPA",
                new String[]{"C_Z_FPA"},
                FpaZone.PROPERTY_CODE, Integer.class, "C_Z_FPA",
                FpaZone.PROPERTY_LIBELLE, String.class, "L_Z_FPA",
                FpaZone.PROPERTY_SUB_CODE, String.class, "C_SUBDIV",
                FpaZone.PROPERTY_COUNTRY, Country.class, "C_PAYS",
                FpaZone.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        // LOT_COM_DESTIN : [C_DEST , L_DEST, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.ElementaryLandingFate,
                "LOT_COM_DESTIN",
                new String[]{"LOT_COM_DESTIN"},
                TransmittingBuoyType.PROPERTY_CODE, Integer.class, "C_DEST",
                TransmittingBuoyType.PROPERTY_LIBELLE, String.class, "L_DEST",
                TransmittingBuoyType.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        // CAPT_ELEM_DESTIN : [C_DEST, L_DEST, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.ElementaryCatchFate,
                "CAPT_ELEM_DESTIN",
                new String[]{"C_DEST"},
                TransmittingBuoyType.PROPERTY_CODE, Integer.class, "C_DEST",
                TransmittingBuoyType.PROPERTY_LIBELLE, String.class, "L_DEST",
                TransmittingBuoyType.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        // ARMEMENT : [C_ARMEMENT, L_ARMEMENT, STATUT]
        registerReferentielMeta(
                result,
                T3EntityEnum.Company,
                "ARMEMENT",
                new String[]{"C_ARMEMENT"},
                TransmittingBuoyType.PROPERTY_CODE, Integer.class, "C_ARMEMENT",
                TransmittingBuoyType.PROPERTY_LIBELLE, String.class, "L_ARMEMENT",
                TransmittingBuoyType.PROPERTY_STATUS, boolean.class, "STATUT"
        );

        return result;
    }

    protected void registerReferentielMeta(Set<T3AccessEntityMeta> universe,
                                           T3EntityEnum type,
                                           String tableName,
                                           String[] pkeys,
                                           Object... properties) {
        T3AccessEntityMeta meta = new T3AccessReferentielEntityMeta(
                type,
                tableName,
                pkeys,
                properties
        );
        registerMeta(universe, meta);

    }

    protected void registerDataMeta(Set<T3AccessEntityMeta> universe,
                                    T3EntityEnum type,
                                    String tableName,
                                    String[] pkeys,
                                    Object[] association,
                                    Object[] reverseProperties,
                                    Object... properties) {
        T3AccessEntityMeta meta = new T3AccessDataEntityMeta(
                type,
                tableName,
                pkeys,
                association,
                reverseProperties,
                properties
        );
        registerMeta(universe, meta);
    }


    protected void registerMeta(Set<T3AccessEntityMeta> universe,
                                T3AccessEntityMeta meta) {
        if (log.isDebugEnabled()) {
            log.debug("add " + meta.getType() + ":" + meta.getTableName() + " to universe");
        }
        universe.add(meta);
    }
}
