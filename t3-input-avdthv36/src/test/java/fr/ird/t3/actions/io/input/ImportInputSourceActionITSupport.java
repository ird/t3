/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.FakeT3ServiceContext;
import fr.ird.t3.actions.MSAccessTestConfiguration;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.io.input.avdth.v36.T3InputProviderAvdth36;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import java.io.File;
import java.util.Set;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class ImportInputSourceActionITSupport {

    /** Logger */
    private static final Log log = LogFactory.getLog(ImportInputSourceActionITSupport.class);

    protected static MSAccessTestConfiguration msConfig =
            new MSAccessTestConfiguration("testExecute_([^_]*)_(.*)");

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected T3InputProvider inputProvider;

    protected File target;

    @BeforeClass
    public static void beforeClass() throws Exception {

        boolean b = msConfig.beforeClass();

        if (!b) {
            Assume.assumeTrue(false);
        }
    }

    @Before
    public void setUp() throws Exception {

        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);

        boolean doIt = msConfig.setup(serviceContext.getTestName());

        if (doIt) {

            String dbName = msConfig.dbName;

            if (log.isDebugEnabled()) {
                log.debug("Do test for db " + dbName);
            }

            File workingDirectory = serviceContext.getApplicationConfiguration().getTreatmentWorkingDirectory(
                    dbName,
                    true
            );

            // push in treatment directory the base to import

            target = new File(workingDirectory, dbName);
            if (log.isDebugEnabled()) {
                log.debug("Will copy msaccess from " + msConfig.accessFile +
                        " to " + target);
            }
            FileUtils.copyFile(msConfig.accessFile, target);

            inputProvider = serviceContext.newService(T3InputService.class).getProvider(T3InputProviderAvdth36.ID);
        }
    }

    @After
    public void tearDown() throws Exception {

        serviceContext.close();
    }

    public void testExecute(int nbSafe, int nbUnsafe) throws Exception {
        testExecute(nbSafe, nbUnsafe, false, false, false);
    }

    public void testExecute(int nbSafe, int nbUnsafe,
                            boolean sampleOnly,
                            boolean canCreateVessel,
                            boolean createVirtualVessel) throws Exception {

        if (msConfig.doTest(serviceContext.getTestName())) {

            AnalyzeInputSourceConfiguration analyzeActionConfiguration = AnalyzeInputSourceConfiguration.newConfiguration(
                    inputProvider, target, true, sampleOnly, canCreateVessel, createVirtualVessel
            );

            T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();

            T3ActionContext<AnalyzeInputSourceConfiguration> analyzeContext =
                    serviceFactory.newT3ActionContext(analyzeActionConfiguration, serviceContext);

            AnalyzeInputSourceAction analyzeAction;

            analyzeAction = serviceFactory.newT3Action(AnalyzeInputSourceAction.class, analyzeContext);

            Assert.assertNotNull(analyzeAction);
            analyzeAction.run();

            Set<Trip> safeTrips = analyzeAction.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_SAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(safeTrips);
            Assert.assertEquals(nbSafe, safeTrips.size());

            Set<Trip> unsafeTrips = analyzeAction.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(unsafeTrips);
            Assert.assertEquals(nbUnsafe, unsafeTrips.size());

            if (log.isInfoEnabled()) {
                log.info("[" + msConfig.dbName + "] safe : " + safeTrips.size() +
                        " - unsafe : " + unsafeTrips.size());
            }

            ImportInputSourceConfiguration importActionConfiguration = ImportInputSourceConfiguration.newConfiguration(
                    analyzeActionConfiguration
            );
            importActionConfiguration.setTripsToImport(safeTrips);

            T3ActionContext<ImportInputSourceConfiguration> importContext =
                    serviceFactory.newT3ActionContext(importActionConfiguration, serviceContext);

            // input db is safe, import it in h2 db

            long oldNbTrips;
            long newNbTrips;

            importActionConfiguration.setTripsToImport(safeTrips);

            oldNbTrips = T3DAOHelper.getTripDAO(serviceContext.getTransaction()).count();

            ImportInputSourceAction importAction =
                    serviceFactory.newT3Action(ImportInputSourceAction.class, importContext);

            importAction.run();

            newNbTrips = T3DAOHelper.getTripDAO(serviceContext.getTransaction()).count();

            Assert.assertEquals(oldNbTrips + safeTrips.size(), newNbTrips);

        }


    }

}
