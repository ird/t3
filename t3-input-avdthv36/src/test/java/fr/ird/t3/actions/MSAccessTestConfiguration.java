/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configuration of a msaccess test.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class MSAccessTestConfiguration {

    /** Le chemin où trouver les bases access. */
    public static final String DB_PATH = "src:test:access";

    public static final String DB_PATTERN = "%s_%s_V36.mdb";

    /** Logger. */
    private static final Log log = LogFactory.getLog(MSAccessTestConfiguration.class);

    boolean executeOnce;

    Integer exactYear;

    Integer lastYear;

    Boolean executeAll;

    Pattern TEST_NAME_PATTERN;

    File dbBasedir;

    public File accessFile;

    public boolean execute;

    public String dbName;

    private static File basedir;

    public MSAccessTestConfiguration(String TEST_NAME_PATTERN) {
        this.TEST_NAME_PATTERN = Pattern.compile(TEST_NAME_PATTERN);
    }

    public static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);
        }
        return basedir;
    }


    public boolean beforeClass() {

        String[] split = DB_PATH.split(":");

        dbBasedir = getBasedir();
        for (String s1 : split) {
            dbBasedir = new File(dbBasedir, s1);
        }

        if (!dbBasedir.exists()) {
            log.warn("Db directory [" + dbBasedir + "] does not exist, will skip test.");

            return false;
        }

        log.info("Db directory [" + basedir + "] detected, will do tests.");

        String s = System.getenv("executeAll");
        if (!StringUtils.isEmpty(s)) {
            executeAll = Boolean.valueOf(s);
        } else {
            executeAll = false;
        }
        executeAll = true;

        s = System.getenv("lastYear");
        if (!StringUtils.isEmpty(s) && !"null".equals(s)) {
            lastYear = Integer.valueOf(s);
        }

        s = System.getenv("exactYear");
        if (!StringUtils.isEmpty(s) && !"null".equals(s)) {
            exactYear = Integer.valueOf(s);
        }
        return true;
    }

    public boolean setup(String testName) {

        execute = false;

        Matcher matcher = TEST_NAME_PATTERN.matcher(testName);
        if (!matcher.find()) {
            throw new IllegalStateException(
                    "Test name [" + testName +
                    "] is not valid and should respect pattern " +
                    TEST_NAME_PATTERN
            );
        }
        String ocean = matcher.group(1);
        String group = matcher.group(2);

        dbName = String.format(DB_PATTERN, ocean, group);

        accessFile = new File(new File(dbBasedir, ocean), dbName);

        boolean exists = accessFile.exists();

        if (!exists) {
            log.warn("Could not find access file " + accessFile);
            return false;
        }

        if (log.isDebugEnabled()) {
            log.debug("DbName = " + dbName);
        }

        execute = true;

        return true;
    }

    public boolean doTest(String testName) {
        if (!execute) {
            log.info("will not execute test [" + testName + "]");
            return false;
        }

        executeOnce = true;
        return true;
    }
}
