/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.T3AVDTHV35TestAtlanticOcean;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 * TODO Faire repasser ces tests
 */
@Ignore
public class ImportInputSourceActionAtlanticOceanIT extends ImportInputSourceActionITSupport implements T3AVDTHV35TestAtlanticOcean {

    @Override
    @Test
    public void testExecute_OA_2000() throws Exception {
        testExecute(347, 7);
    }

    @Override
    @Test
    public void testExecute_OA_2001() throws Exception {
        testExecute(351, 10);
    }

    @Override
    @Test
    public void testExecute_OA_2002() throws Exception {
        testExecute(310, 7);
    }

    @Override
    @Test
    public void testExecute_OA_2003() throws Exception {
        testExecute(373, 2);
    }

    @Override
    @Test
    public void testExecute_OA_2004() throws Exception {
        testExecute(179, 3);
    }

    @Override
    @Test
    public void testExecute_OA_2005() throws Exception {
        testExecute(123, 1);
    }

    @Override
    @Test
    public void testExecute_OA_2006() throws Exception {
        testExecute(95, 2);
    }

    @Override
    @Test
    public void testExecute_OA_2007() throws Exception {
        testExecute(80, 0);
    }

    @Override
    @Test
    public void testExecute_OA_2008() throws Exception {
        testExecute(59, 1);
    }

    @Override
    @Test
    public void testExecute_OA_2009() throws Exception {
        testExecute(81, 4);
    }

    @Override
    @Test
    public void testExecute_OA_2010() throws Exception {
        testExecute(80, 8);
    }

    @Override
    @Test
    public void testExecute_OA_2011() throws Exception {
        testExecute(80, 8);
    }

    @Override
    @Test
    public void testExecute_OA_2012() throws Exception {
        testExecute(80, 8);
    }

    @Override
    @Test
    public void testExecute_OA_2013() throws Exception {
        testExecute(80, 8);
    }

    @Override
    @Test
    public void testExecute_OA_2014() throws Exception {
        testExecute(80, 8);
    }

    @Override
    @Test
    public void testExecute_OA_2015() throws Exception {
        testExecute(80, 8);
    }


}
