/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.T3AVDTHV35TestIndianOcean;
import org.junit.Test;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AnalyzeInputSourceActionIndianOceanIT extends AnalyzeInputSourceActionITSupport implements T3AVDTHV35TestIndianOcean {

    @Test
    @Override
    public void testExecute_OI_2000() throws Exception {
        testExecute(155, 65, 220, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2001() throws Exception {
        testExecute(70, 118, 188, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2002() throws Exception {
        testExecute(103, 125, 228, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2003() throws Exception {
        testExecute(83, 136, 219, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2004() throws Exception {
        testExecute(78, 118, 196, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2005() throws Exception {
        testExecute(78, 110, 188, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2006() throws Exception {
        testExecute(181, 12, 193, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2007() throws Exception {
        testExecute(155, 12, 167, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2008() throws Exception {
        testExecute(146, 34, 180, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2009() throws Exception {
        testExecute(122, 19, 141, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2010() throws Exception {
        testExecute(112, 0, 112, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2011() throws Exception {
        testExecute(141, 0, 136, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2012() throws Exception {
        testExecute(145, 0, 145, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2013() throws Exception {
        testExecute(124, 0, 124, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2014() throws Exception {
        testExecute(149, 0, 149, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2015() throws Exception {
        testExecute(111, 1, 110, 1);
    }

}
