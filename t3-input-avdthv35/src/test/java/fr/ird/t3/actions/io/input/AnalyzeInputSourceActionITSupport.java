/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.FakeT3ServiceContext;
import fr.ird.t3.actions.MSAccessTestConfiguration;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.io.input.avdth.v35.T3InputProviderAvdth35;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Set;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AnalyzeInputSourceActionITSupport {

    /** Logger */
    private static final Log log = LogFactory.getLog(AnalyzeInputSourceActionITSupport.class);

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected static MSAccessTestConfiguration msConfig =
            new MSAccessTestConfiguration("testExecute_([^_]*)_(.*)");

    protected static boolean useWells;

    protected BufferedWriter outputWriter;

    protected File target;

    protected T3InputProvider inputProvider;

    @BeforeClass
    public static void beforeClass() throws Exception {

        boolean b = msConfig.beforeClass();

        if (!b) {
            Assume.assumeTrue(false);
        }

        String s = System.getenv("useWells");
        if (!StringUtils.isEmpty(s)) {
            useWells = Boolean.valueOf(s);
        } else {
            useWells = false;
        }
    }

    @Before
    public void setUp() throws Exception {

        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);

        boolean doIt = msConfig.setup(serviceContext.getTestName());

        if (doIt) {

            String dbName = msConfig.dbName;

            if (log.isDebugEnabled()) {
                log.debug("Do test for db " + dbName);
            }

            File outputFile = new File(serviceContext.getTestDir(), "result.txt");

            if (log.isInfoEnabled()) {
                log.info("Will save result in file : " + outputFile);
            }

            outputWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outputFile)));

            File workingDirectory =
                    serviceContext.getApplicationConfiguration().getTreatmentWorkingDirectory(
                            "yo",
                            true
                    );

            // push in treatment directory the base to import

            target = new File(workingDirectory, dbName);
            if (log.isDebugEnabled()) {
                log.debug("Will copy msaccess from " + msConfig.accessFile +
                        " to " + target);
            }
            FileUtils.copyFile(msConfig.accessFile, target);
            T3InputService inputService =
                    serviceContext.newService(T3InputService.class);

            inputProvider = inputService.getProvider(T3InputProviderAvdth35.ID);
        }

    }

    @After
    public void tearDown() throws Exception {

        serviceContext.close();

        if (outputWriter != null) {

            outputWriter.flush();
            outputWriter.close();

        }
    }

    public void testExecute(int nbSafe,
                            int nbUnsafe,
                            int nbSafeWithoutwell,
                            int nbUnsafeWithoutWell) throws Exception {
        testExecute(
                nbSafe,
                nbUnsafe,
                nbSafeWithoutwell,
                nbUnsafeWithoutWell,
                false,
                false,
                false
        );
    }

    public void testExecute(int nbSafe,
                            int nbUnsafe,
                            int nbSafeWithoutwell,
                            int nbUnsafeWithoutWell,
                            boolean sampleOnly,
                            boolean canCreateVessel,
                            boolean createVirtualVessel) throws Exception {

        if (msConfig.doTest(serviceContext.getTestName())) {

            T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();

            AnalyzeInputSourceConfiguration actionConfiguration = AnalyzeInputSourceConfiguration.newConfiguration(
                    inputProvider, target, useWells, sampleOnly, canCreateVessel, createVirtualVessel
            );
            T3ActionContext<AnalyzeInputSourceConfiguration> context =
                    serviceFactory.newT3ActionContext(actionConfiguration, serviceContext);


            outputWriter.write("----------------------------------------------------\n");
            outputWriter.write(msConfig.accessFile + "\n");
            T3Action<AnalyzeInputSourceConfiguration> action;

            action = serviceFactory.newT3Action(AnalyzeInputSourceAction.class, context);

            Assert.assertNotNull(action);

            action.run();

            Set<Trip> safeTrips = action.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_SAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(safeTrips);
            outputWriter.write("found " + safeTrips.size() + "   safe trip(s).\n");
            Set<Trip> unsafeTrips = action.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(unsafeTrips);
            outputWriter.write("found " + unsafeTrips.size() + " unsafe trip(s).\n");
            List<String> messages;

            if (log.isWarnEnabled()) {
                messages = action.getWarnMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write("[WARNING] " + message + "\n");
                    }
                }
            }

            if (log.isErrorEnabled()) {
                messages = action.getErrorMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write("[ERROR] " + message + "\n");
                    }
                }
            }

            if (log.isInfoEnabled()) {
                log.info("\n[" + msConfig.dbName + "] safe : " + safeTrips.size() +
                        " - unsafe : " + unsafeTrips.size() + "\n");
            }

            if (useWells) {
                Assert.assertEquals(nbSafe, safeTrips.size());
                Assert.assertEquals(nbUnsafe, unsafeTrips.size());
            } else {
                Assert.assertEquals(nbSafeWithoutwell, safeTrips.size());
                Assert.assertEquals(nbUnsafeWithoutWell, unsafeTrips.size());
            }

        }


    }

    public void testExecute(int nbSafe, int nbUnsafe) throws Exception {
        testExecute(nbSafe, nbUnsafe, nbSafe, nbUnsafe);
    }

}
