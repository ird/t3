/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v35;

import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.data.ElementaryLanding;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.data.WellPlan;
import fr.ird.t3.entities.reference.WeightCategoryLanding;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.io.input.MissingForeignKeyInT3;
import fr.ird.t3.io.input.access.T3AccessDataSource;
import fr.ird.t3.io.input.access.T3AccessEntity;
import fr.ird.t3.io.input.access.T3AccessEntityMeta;
import fr.ird.t3.io.input.access.T3DataEntityVisitor;
import fr.ird.type.CoordinateHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.Serializable;

/**
 * Implementation of {@link T3DataEntityVisitor} for the avdth v35 db.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3DataEntityVisitorAvdth35 extends T3DataEntityVisitor {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3DataEntityVisitorAvdth35.class);

    protected final ReverseAssociationGetter<Trip, WellPlan, Activity> wellPlanReverse;

    protected final ReverseAssociationGetter<Trip, Sample, Well> sampleReverse;

    protected final ReverseAssociationGetter<Trip, SampleWell, Activity> sampleSetReverse;

    public T3DataEntityVisitorAvdth35(T3AccessDataSource dataSource,
                                      ReferenceEntityMap referentiel) {
        super(dataSource, referentiel);

        wellPlanReverse = newReverseAssociationGetter(
                Trip.class,
                WellPlan.class,
                Activity.class,
                Trip.PROPERTY_ACTIVITY,
                WellPlan.PROPERTY_ACTIVITY
        );

        sampleReverse = newReverseAssociationGetter(
                Trip.class,
                Sample.class,
                Well.class,
                Trip.PROPERTY_WELL,
                Sample.PROPERTY_WELL
        );

        sampleSetReverse = newReverseAssociationGetter(
                Trip.class,
                SampleWell.class,
                Activity.class,
                Trip.PROPERTY_ACTIVITY,
                SampleWell.PROPERTY_ACTIVITY
        );
    }

    @Override
    public void onEnd(T3AccessEntity entity, T3AccessEntityMeta meta) {

        super.onEnd(entity, meta);

        if (!deepVisit) {
            return;
        }

        T3EntityEnum type = meta.getType();

        if (type == T3EntityEnum.Activity) {

            Activity a = (Activity) entity;

            Integer quadrant = a.getQuadrant();
            Float latitude = CoordinateHelper.getSignedLatitude(
                    quadrant, a.getLatitude());
            Float longitude = CoordinateHelper.getSignedLongitude(
                    quadrant, a.getLongitude());
            a.setLatitude(latitude);
            a.setLongitude(longitude);

            if (log.isDebugEnabled()) {
                log.debug("Will use for activity " + entity +
                          " coordinates <" + a.getLongitude() + "," +
                          a.getLatitude() + ">");
            }
        }
    }

    @Override
    public void onVisitReverseAssociation(String propertyName,
                                          T3AccessEntity entity,
                                          T3AccessEntityMeta meta) {

        if (!deepVisit) {
            return;
        }

        if (SampleWell.PROPERTY_ACTIVITY.equals(propertyName) &&
            entity instanceof SampleWell) {

            // special case, must obtain the activity from the trip

            sampleSetReverse.attachReverseAssocation(entity);
            return;
        }

        if (WellPlan.PROPERTY_ACTIVITY.equals(propertyName) &&
            entity instanceof WellPlan) {

            // special case, must obtain the activity from the trip

            wellPlanReverse.attachReverseAssocation(entity);
            return;
        }

        if (Sample.PROPERTY_WELL.equals(propertyName) &&
            entity instanceof Sample) {

            // special case, must obtain the well from the trip

            sampleReverse.attachReverseAssocation(entity);
            return;
        }

        super.onVisitReverseAssociation(propertyName, entity, meta);
    }

    @Override
    public void onVisitComposition(String propertyName,
                                   Class<?> type,
                                   T3AccessEntity entity,
                                   T3AccessEntityMeta meta) {

        if (ElementaryLanding.PROPERTY_WEIGHT_CATEGORY_LANDING.equals(propertyName)
            && entity instanceof ElementaryLanding) {

            Serializable codeEspece = getProperty("C_ESP", row);
            Serializable codeCate = getProperty("C_CAT_C", row);

            // must found the species
            T3AccessEntityMeta specieMeta =
                    dataSource.getMeta(T3EntityEnum.Species);

            TopiaEntity specie = getReferenceEntity(specieMeta, codeEspece);

            T3AccessEntityMeta categoryMeta =
                    dataSource.getMeta(T3EntityEnum.WeightCategoryLanding);

            // special case : need to find a reference with two primary keys...
            TopiaEntity ref = getReferenceEntity(
                    categoryMeta,
                    new String[]{WeightCategoryLanding.PROPERTY_CODE, WeightCategoryLanding.PROPERTY_SPECIES},
                    codeCate, specie
            );

            if (ref == null) {

                if (log.isWarnEnabled()) {
                    log.warn("Can't find WeightCategoryLanding for speciesCode / categoryCode: " + codeEspece + "/" + codeCate);
                }
                getMissingForeignKeys().add(new MissingForeignKeyInT3(
                        T3EntityEnum.ElementaryLanding,
                        T3EntityEnum.WeightCategoryLanding,
                        getPKey(T3EntityEnum.ElementaryLanding),
                        new Object[]{codeEspece, codeCate}
                ));

            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Detected landing catch weight category : " + ref);
                }
                entity.setProperty(propertyName, ref);

            }

            return;
        }

        if (ElementaryCatch.PROPERTY_WEIGHT_CATEGORY_LOG_BOOK.equals(propertyName)
            && entity instanceof ElementaryCatch) {

            // special case : need to find a reference with two primary keys...

            Serializable codeEspece = getProperty("C_ESP", row);
            Serializable codeCate = getProperty("C_CAT_T", row);

            // must found the species
            T3AccessEntityMeta specieMeta =
                    dataSource.getMeta(T3EntityEnum.Species);

            TopiaEntity specie = getReferenceEntity(specieMeta, codeEspece);

            T3AccessEntityMeta categoryMeta =
                    dataSource.getMeta(T3EntityEnum.WeightCategoryLogBook);

            // special case : need to find a reference with two primary keys...
            TopiaEntity ref = getReferenceEntity(
                    categoryMeta,
                    new String[]{WeightCategoryLogBook.PROPERTY_CODE, WeightCategoryLogBook.PROPERTY_SPECIES},
                    codeCate, specie
            );

            if (ref == null) {

                if (log.isWarnEnabled()) {
                    log.warn("Can't find weightCategoryLogBook for speciesCode / categoryCode: " + codeEspece + "/" + codeCate);
                }
                getMissingForeignKeys().add(new MissingForeignKeyInT3(
                        T3EntityEnum.ElementaryCatch,
                        T3EntityEnum.WeightCategoryLogBook,
                        getPKey(T3EntityEnum.ElementaryCatch),
                        new Object[]{codeEspece, codeCate}
                ));

            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Detected logBook catch weight category : " + ref);
                }
                entity.setProperty(propertyName, ref);

            }

            return;
        }

        super.onVisitComposition(propertyName, type, entity, meta);
    }
}
