<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:form>
  <fieldset>

    <legend>
      <s:text name="t3.label.data.level1.configuration.resume"/>
    </legend>

      <%-- selected sample quality --%>
    <s:checkboxlist key="configuration.sampleQualityIds" disabled="true"
                    list="sampleQualities" template="mycheckboxlist"
                    label='%{getText("t3.common.sampleQuality")}'/>

      <%-- selected sample type --%>
    <s:checkboxlist key="configuration.sampleTypeIds" disabled="true"
                    list="sampleTypes" template="mycheckboxlist"
                    label='%{getText("t3.common.sampleType")}'/>

      <%-- selected fleet country --%>
    <s:checkboxlist key="configuration.fleetIds" disabled="true"
                    list="fleets" template="mycheckboxlist"
                    label='%{getText("t3.common.fleetCountry")}'/>

      <%-- selected ocean --%>
    <s:select key="configuration.oceanId" disabled="true" list="oceans"
              label='%{getText("t3.common.ocean")}'/>

      <%-- selected begin date --%>
    <s:textfield key="configuration.beginDate" disabled="true"
                 label='%{getText("t3.common.beginDate")}'/>

      <%-- selected end date --%>
    <s:textfield key="configuration.endDate" disabled="true"
                 label='%{getText("t3.common.endDate")}'/>

      <%-- rfTotMax --%>
    <s:textfield key="configuration.rfTotMax" disabled="true"
                 label='%{getText("t3.common.rfTotMax")}'/>

      <%-- rfMinus10Max --%>
    <s:textfield key="configuration.rfMinus10Max" disabled="true"
                 label='%{getText("t3.common.rfMinus10Max")}'/>

      <%-- rfPlus10Max --%>
    <s:textfield key="configuration.rfPlus10Max" disabled="true"
                 label='%{getText("t3.common.rfPlus10Max")}'/>

      <%-- use or not rf-10 and rf+10 --%>
    <s:radio key="configuration.useRfMins10AndRfPlus10" disabled="true" list="useRfMinus10AndRfPlus10OrNot"
             label='%{getText("t3.label.data.level1.configuration.useRfMinus10AndRfPlus10OrNot")}'/>

      <%-- selected matching trips and samples count --%>
    <s:label name="matchingTripCount"
             key="t3.level1.matching.tripAndSample.count"
             value="%{matchingTripCount} / %{matchingSampleCount} / %{notMatchingSampleCount}"/>
  </fieldset>
</s:form>
