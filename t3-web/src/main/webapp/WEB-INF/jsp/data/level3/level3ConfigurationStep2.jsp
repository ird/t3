<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<title><s:text name="t3.label.data.treatment.level3"/></title>

<h2><s:text name="t3.label.data.treatment.level3"/></h2>

<s:form method="post" validate="true" namespace="/level3">

  <fieldset>

    <legend>
      <s:text name="t3.label.configuration.step2"/>
    </legend>

    <%--<s:hidden name="validate" value="true"/>--%>

      <%-- selected sample fleet countries --%>
    <s:checkboxlist key="configuration.sampleFleetIds" list="sampleFleets"
                    label='%{getText("t3.common.sampleFleetCountries")}'
                    requiredLabel="true" template="mycheckboxlist"/>


      <%-- selected sample flag countries --%>
    <s:checkboxlist key="configuration.sampleFlagIds" list="sampleFlags"
                    label='%{getText("t3.common.sampleFlagCountries")}'
                    requiredLabel="true" template="mycheckboxlist"/>

      <%-- selected stratumWeightRatio  --%>
    <s:textfield key="configuration.stratumWeightRatio"
                 label='%{getText("t3.common.stratumWeightRatio")}'
                 requiredLabel="true"/>

      <%-- use all samples --%>
    <s:radio key="configuration.useAllSamplesOfStratum" requiredLabel="true" list="useSamplesOrNot"
             label='%{getText("t3.label.data.level3.configuration.samplesToUse")}'/>

      <%-- use weight categories --%>
    <s:radio key="configuration.useWeightCategories" requiredLabel="true" list="useWeightCategoriesOrNot"
             label='%{getText("t3.label.data.level3.configuration.useWeightCategoriesOrNot")}'/>
    <br/>
    <table class="cleanBoth">
      <tr>
        <th class="wwlbl"><s:text name="t3.common.species"/></th>
        <th><s:text
          name="t3.common.stratumMinimumSampleCountObjectSchoolType"/></th>
        <th><s:text
          name="t3.common.stratumMinimumSampleCountFreeSchoolType"/></th>
      </tr>
      <s:set var="speciesData" value="%{stratumMinimumSampleCount}"/>
      <s:iterator value="species" status="status" var="entry">
        <s:set var="speciesId">${entry.key}</s:set>
        <s:set var="boId">BO:${speciesId}</s:set>
        <s:set var="blId">BL:${speciesId}</s:set>
        <tr>
          <td class="wwlbl nowrap"><s:property value="value"/></td>
          <td class="nowrap">
            <s:set
              var="v">${speciesData[speciesId].minimumCountForObjectSchool}</s:set>
            <s:textfield name="%{boId}" theme="simple" requiredLabel="true"
                         value="%{v}"/>
          </td>
          <td class="nowrap">
            <s:set
              var="v">${speciesData[speciesId].minimumCountForFreeSchool}</s:set>
            <s:textfield name="%{blId}" theme="simple" requiredLabel="true"
                         value="%{v}"/>
          </td>
        </tr>
      </s:iterator>
    </table>

  </fieldset>

  <s:if test="!missingDatas">

    <br/>
    <%-- back to step 1 --%>
    <s:submit action="configureLevel3Step1!input"
              key="t3.action.back.to.configuration.step1" align="right"/>

    <%--validate + go to resume --%>
    <s:submit action="configureLevel3Step2"
              key="t3.action.validate.configuration" align="right"/>
  </s:if>
</s:form>
