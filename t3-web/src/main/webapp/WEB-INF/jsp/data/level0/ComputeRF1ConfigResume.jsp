<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<fieldset xmlns="http://www.w3.org/1999/xhtml">

  <legend>
    <s:text name="t3.label.config.resume"/>
  </legend>

  <%--s:select key="configuration.oceanId" disabled="true" list="oceans"
            label='%{getText("t3.common.ocean")}'/--%>

  <s:textfield key="configuration.beginDate" disabled="true"
               label='%{getText("t3.common.beginDate")}'/>

  <s:textfield key="configuration.endDate" disabled="true"
               label='%{getText("t3.common.endDate")}'/>

  <s:textfield key="configuration.minimumRate" disabled="true"
               label='%{getText("t3.common.rf1.minimumRate.acceptable")}'/>

  <s:textfield key="configuration.maximumRate" disabled="true"
               label='%{getText("t3.common.rf1.maximumRate.acceptable")}'/>

  <s:checkboxlist key="configuration.vesselSimpleTypeIds" disabled="true"
                  list="vesselSimpleTypes" template="mycheckboxlist"
                  label='%{getText("t3.common.vesselSimpleType")}'/>

  <s:checkboxlist key="configuration.fleetIds" disabled="true"
                  list="fleets" template="mycheckboxlist"
                  label='%{getText("t3.common.fleetCountry")}'/>

</fieldset>
