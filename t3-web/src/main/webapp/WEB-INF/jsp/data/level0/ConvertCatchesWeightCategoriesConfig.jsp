<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" charset="UTF-8"
        src="<s:url value='/js/monthpicker.js' />"></script>
<style type="text/css">
  .ui-datepicker-calendar {
    display: none;
  }
  .wwlbl {
    width: 500px;
  }
</style>
<title><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.convertCatchesWeightCategories"/></title>

<h2><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.convertCatchesWeightCategories"/></h2>
<s:if test="confirm">

  <s:form method="post" validate="false" namespace="/level0">

    <jsp:include page="ConvertCatchesWeightCategoriesConfigResume.jsp"/>

    <br/>
    <s:submit action="configureConvertCatchesWeightCategories"
              key="t3.action.backToConfiguration" align="right"/>
    <s:submit action="run-ConvertCatchesWeightCategories"
              key="t3.action.runAction" align="right"/>

  </s:form>
</s:if>

<s:else>

  <s:form method="post" validate="true" namespace="/level0">

    <fieldset>

      <legend>
        <s:text name="t3.label.configure"/>
      </legend>
      <%--s:select key="configuration.oceanId" list="oceans"
                label='%{getText("t3.common.ocean")}' requiredLabel="true"/--%>

        <%-- begin date --%>
      <sj:datepicker key="configuration.beginDate" requiredLabel="true"
                     label='%{getText("t3.common.beginDate")}'
                     appendText=" (mm-yyyy)"/>

        <%-- end date --%>
      <sj:datepicker key="configuration.endDate" requiredLabel="true"
                     label='%{getText("t3.common.endDate")}'
                     appendText=" (mm-yyyy)"/>

      <s:checkboxlist key="configuration.vesselSimpleTypeIds"
                      list="vesselSimpleTypes"
                      label='%{getText("t3.common.vesselSimpleType")}'
                      requiredLabel="true" template="mycheckboxlist"/>

      <s:checkboxlist key="configuration.fleetIds" list="fleets"
                      label='%{getText("t3.common.fleetCountry")}'
                      requiredLabel="true" template="mycheckboxlist"/>

      <s:hidden name="confirm" value="true"/>
    </fieldset>
    <br/>
    <s:submit action="validateConvertCatchesWeightCategories"
              key="t3.action.validateConfiguration" align="right"/>
  </s:form>

  <script type="text/javascript">

    jQuery(document).ready(function () {

      $.prepareMonthPickers(
        {
          minDateAsMonth:'<s:property value="configuration.minDate"/>',
          maxDateAsMonth:'<s:property value="configuration.maxDate"/>'
        });

    });
  </script>
</s:else>
