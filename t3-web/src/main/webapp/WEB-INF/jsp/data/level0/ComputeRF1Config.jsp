<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" charset="UTF-8"
        src="<s:url value='/js/monthpicker.js' />"></script>
<style type="text/css">
  .ui-datepicker-calendar {
    display: none;
  }
  .wwlbl {
    width: 500px;
  }
</style>

<title><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.computeRF1"/></title>

<h2><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.computeRF1"/></h2>

<s:if test="confirm">

  <s:form method="post" validate="false" namespace="/level0">

    <jsp:include page="ComputeRF1ConfigResume.jsp"/>

    <br/>
    <s:submit action="configureComputeRF1"
              key="t3.action.backToConfiguration" align="right"/>
    <s:submit action="run-ComputeRF1" key="t3.action.runAction" align="right"/>

  </s:form>
</s:if>

<s:else>

  <s:form method="post" validate="true">

    <fieldset>

      <legend>
        <s:text name="t3.label.configure"/>
      </legend>

      <%-- begin date --%>
      <sj:datepicker key="configuration.beginDate" requiredLabel="true"
                     label='%{getText("t3.common.beginDate")}'
                     appendText=" (mm-yyyy)"/>

        <%-- end date --%>
      <sj:datepicker key="configuration.endDate" requiredLabel="true"
                     label='%{getText("t3.common.endDate")}'
                     appendText=" (mm-yyyy)"/>

      <s:textfield key="configuration.minimumRate"
                   label='%{getText("t3.common.rf1.minimumRate.acceptable")}'
                   requiredLabel="true"/>

      <s:textfield key="configuration.maximumRate"
                   label='%{getText("t3.common.rf1.maximumRate.acceptable")}'
                   requiredLabel="true"/>

      <s:checkboxlist key="configuration.vesselSimpleTypeIds"
                      list="vesselSimpleTypes"
                      label='%{getText("t3.common.vesselSimpleType")}'
                      requiredLabel="true" template="mycheckboxlist"/>

      <s:checkboxlist key="configuration.fleetIds" list="fleets"
                      label='%{getText("t3.common.fleetCountry")}'
                      requiredLabel="true" template="mycheckboxlist"/>

    </fieldset>

    <br/>
    <s:submit action="validateComputeRF1"
              key="t3.action.validateConfiguration" align="right"/>
  </s:form>

</s:else>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $.prepareMonthPickers(
      {
        minDateAsMonth:'<s:property value="configuration.minDate"/>',
        maxDateAsMonth:'<s:property value="configuration.maxDate"/>'
      });

  });
</script>
