<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<fieldset>

  <legend>
    <s:text name="t3.label.config.resume"/>
  </legend>

  <%-- selected ocean --%>
  <s:select key="configuration.oceanId" disabled="true" list="oceans"
            label='%{getText("t3.common.ocean")}'/>

  <%-- selected zone type--%>
  <s:select key="configuration.zoneTypeId" disabled="true" list="zoneTypes"
            label='%{getText("t3.common.zoneType")}'/>

  <%-- selected zone version--%>
  <s:select key="configuration.zoneVersionId" disabled="true"
            list="zoneVersions" label='%{getText("t3.common.zoneVersion")}'/>

  <%-- selected catch fleet --%>
  <s:select key="configuration.catchFleetId" list="catchFleets" disabled="true"
            label='%{getText("t3.common.catchFleet")}'/>

  <%-- selected time step  --%>
  <s:select key="configuration.timeStep" list="timeSteps" disabled="true"
            label='%{getText("t3.common.timeStep")}'/>

  <%-- selected begin date --%>
  <s:textfield key="configuration.beginDate" disabled="true"
               label='%{getText("t3.common.beginDate")}'/>

  <%-- selected end date --%>
  <s:textfield key="configuration.endDate" disabled="true"
               label='%{getText("t3.common.endDate")}'/>

  <div class="formSeparator"></div>
  <%-- selected species --%>
  <s:checkboxlist key="configuration.speciesIds" disabled="true"
                  list="species" template="mycheckboxlist"
                  label='%{getText("t3.common.species")}'/>

  <div class="formSeparator"></div>
  <%-- selected sample fleet countries --%>
  <s:checkboxlist key="configuration.sampleFleetIds" disabled="true"
                  list="sampleFleets" template="mycheckboxlist"
                  label='%{getText("t3.common.sampleFleetCountries")}'/>

  <div class="formSeparator"></div>
  <%-- selected sample flag countries --%>
  <s:checkboxlist key="configuration.sampleFlagIds" disabled="true"
                  list="sampleFlags" template="mycheckboxlist"
                  label='%{getText("t3.common.sampleFlagCountries")}'/>

  <div class="formSeparator"></div>
  <%-- selected stratumWeightRatio  --%>
  <s:textfield key="configuration.stratumWeightRatio" disabled="true"
               label='%{getText("t3.common.stratumWeightRatio")}'/>

  <%-- selected stratumMinimumSampleCountObjectSchoolType  --%>
  <s:textfield key="configuration.stratumMinimumSampleCountObjectSchoolType"
               disabled="true"
               label="%{getText('t3.common.stratumMinimumSampleCountObjectSchoolType')}"/>

  <%-- selected stratumMinimumSampleCountFreeSchoolType  --%>
  <s:textfield key="configuration.stratumMinimumSampleCountFreeSchoolType"
               disabled="true"
               label="%{getText('t3.common.stratumMinimumSampleCountFreeSchoolType')}"/>

  <div class="formSeparator"></div>
  <%-- use all samples --%>
  <s:radio key="configuration.useAllSamplesOfStratum" list="useSamplesOrNot" disabled="true"
           label='%{getText("t3.label.data.level2.configuration.samplesToUse")}'/>


</fieldset>
