<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<title><s:text name="t3.label.data.importData"/></title>

<h2><s:text name="t3.label.data.importData"/> - <s:text
  name="t3.label.result"/></h2>

<jsp:include page="importDataConfigResume.jsp"/>

<s:if test="nbImportedTrips == 0">
  <p>
    <s:text name="t3.label.message.no.trips.imported"/>
  </p>
</s:if>
<s:else>

  <s:if test="nbDeletedTrips > 0">
    <p>
      <s:text name="t3.label.message.trips.deleted">
        <s:param>
          <s:property value="%{nbDeletedTrips}"/>
        </s:param>
      </s:text>
    </p>
  </s:if>
  <p>
    <s:text name="t3.label.message.trips.imported">
      <s:param>
        <s:property value="%{nbImportedTrips}"/>
      </s:param>
    </s:text>
  </p>

</s:else>

<s:a action="configureImportData!input" namespace="/io">
  <s:text name="t3.menu.io.importData.again"/>
</s:a>
