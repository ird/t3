<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:set name="title"><s:text name="t3.label.admin.t3.database.detail"/></s:set>

<title><s:property value="#title"/></title>

<h2><s:property value="#title"/></h2>

<fieldset>
  <legend>
    <s:text name="t3.common.database.general.caracteristics"/>
  </legend>

  <s:label value='%{nbOceans}'
           label="%{getText('t3.common.nbOceans')}"/>
  <s:label value='%{nbTrips}'
           label="%{getText('t3.common.nbTrips')}"/>
  <s:label value='%{nbActivities}'
           label="%{getText('t3.common.nbActivities')}"/>
</fieldset>

<br/>

<sj:tabbedpanel id="oceanTabs">

  <s:iterator value="oceans" var="ocean" status="itrStatus">

    <sj:tab id="%{topiaid}" target="tab_%{#itrStatus.index}"
            label="%{getText('t3.common.ocean')} - %{decorate(#ocean)}"/>

  </s:iterator>

  <s:iterator value="oceans" var="ocean" status="itrStatus">

    <s:set id="nbTrips" value="%{#action.getNbTrips(#ocean)}"/>
    <s:set id="tripsWithNoDataComputed"
           value="%{#action.getTripsWithNoDataComputed(#ocean)}"/>
    <s:set id="tripsWithSomeDataComputed"
           value="%{#action.getTripsWithSomeDataComputed(#ocean)}"/>
    <s:set id="tripsWithAllDataComputed"
           value="%{#action.getTripsWithAllDataComputed(#ocean)}"/>

    <div id="tab_${itrStatus.index}">
      <fieldset>
        <legend>
          <s:text name="t3.common.database.ocean.caracteristics"/>
        </legend>

        <s:label value='%{getNbTrips(#ocean)}'
                 label="%{getText('t3.common.nbTrips')}"/>
        <s:label value='%{getFirstTripDate(#ocean)}'
                 label="%{getText('t3.common.firstTrip')}"/>
        <s:label value='%{getLastTripDate(#ocean)}'
                 label="%{getText('t3.common.lastTrip')}"/>
        <s:label value='%{getNbActivities(#ocean)}'
                 label="%{getText('t3.common.nbActivities')}"/>
      </fieldset>

      <br/>

      <sj:tabbedpanel id="oceanTabs_%{#itrStatus.index}">

        <s:if test="#tripsWithNoDataComputed.size() > 0">

          <s:set var="titleNoData">
            <s:text name="t3.common.database.tripsWithNoData">
              <s:param value="%{#tripsWithNoDataComputed.size()}"/>
              <s:param value="%{#nbTrips}"/>
            </s:text>
          </s:set>

          <sj:tab id="%{topiaid}" target="oceanTab_%{#itrStatus.index}_noData"
                  label="%{#titleNoData}"/>

          <div id="oceanTab_${itrStatus.index}_noData">
            <ul>
              <s:iterator value="tripsWithNoDataComputed" var="e">
                <li>
                  <s:a namespace="/trip" action="tripDetail">
                    <s:param name="tripIds" value="%{#e.key}"/>
                    <s:param name="back">stats</s:param>
                    <s:property value="%{#e.value}"/>
                  </s:a>
                </li>
              </s:iterator>
            </ul>
          </div>

        </s:if>

        <s:if test="#tripsWithSomeDataComputed.size() > 0">

          <s:set var="titleSomeData">
            <s:text name="t3.common.database.tripsWithSomeData">
              <s:param value="%{#tripsWithSomeDataComputed.size()}"/>
              <s:param value="%{#nbTrips}"/>
            </s:text>
          </s:set>

          <sj:tab id="%{topiaid}" target="oceanTab_%{#itrStatus.index}_someData"
                  label="%{#titleSomeData}"/>

          <div id="oceanTab_${itrStatus.index}_someData">
            <ul>
              <s:iterator value="tripsWithSomeDataComputed" var="e">
                <li>
                  <s:a namespace="/trip" action="tripDetail">
                    <s:param name="tripIds" value="%{#e.key}"/>
                    <s:param name="back">stats</s:param>
                    <s:property value="%{#e.value}"/>
                  </s:a>
                </li>
              </s:iterator>
            </ul>
          </div>

        </s:if>

        <s:if test="#tripsWithAllDataComputed.size() > 0">

          <s:set var="titleAllData">
            <s:text name="t3.common.database.tripsWithAllData">
              <s:param value="%{#tripsWithAllDataComputed.size()}"/>
              <s:param value="%{#nbTrips}"/>
            </s:text>
          </s:set>

          <sj:tab id="%{topiaid}" target="oceanTab_%{#itrStatus.index}_allData"
                  label="%{#titleAllData}"/>

          <div id="oceanTab_${itrStatus.index}_allData">
            <ul>
              <s:iterator value="tripsWithAllDataComputed" var="e">
                <li>
                  <s:a namespace="/trip" action="tripDetail">
                    <s:param name="tripIds" value="%{#e.key}"/>
                    <s:param name="back">stats</s:param>
                    <s:property value="%{#e.value}"/>
                  </s:a>
                </li>
              </s:iterator>
            </ul>
          </div>

        </s:if>

      </sj:tabbedpanel>

    </div>

  </s:iterator>

</sj:tabbedpanel>

