<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:set name="title"><s:text name="t3.label.admin.trip.detail"/></s:set>

<style type="text/css">
  .wwlbl {
    width: 600px;
  }
</style>
<title><s:property value="#title"/></title>

<h2><s:property value="#title"/></h2>

<s:iterator value="trips" var="trip">
  <s:set name="catchSpecies" value="%{#trip.getElementaryCatchSpecies()}"/>
  <s:set name="landingSpecies" value="%{#trip.getElementaryLandingSpecies()}"/>
  <s:set name="sampleTotalLD1Number"
         value="%{#trip.getSampleTotalLD1Number()}"/>
  <s:set name="sampleTotalLFNumber" value="%{#trip.getSampleTotalLFNumber()}"/>
  <s:set name="sampleTotalNumber"
         value="%{sampleTotalLD1Number+sampleTotalLFNumber}"/>

  <fieldset>
    <legend>
      <s:text name="t3.common.trip.detail">
        <s:param><s:property value="%{topiaId}"/> </s:param>
      </s:text>
    </legend>
    <s:label
      value='%{vessel.libelle} (%{vessel.code}) - %{vessel.flagCountry.libelle} - %{vessel.vesselType.libelle}'
      label="%{getText('t3.common.vessel')}"/>
    <s:label value='%{formatDate(departureDate)}'
             label="%{getText('t3.common.departureDate')}"/>
    <s:label value='%{departureHarbour.libelle}'
             label="%{getText('t3.common.departureHarbour')}"/>
    <s:label value='%{formatDate(landingDate)}'
             label="%{getText('t3.common.landingDate')}"/>
    <s:label value='%{landingHarbour.libelle}'
             label="%{getText('t3.common.landingHarbour')}"/>
    <s:label value='%{comment}'
             label="%{getText('t3.common.comment')}"/>
    <s:label value='%{landingTotalWeight}'
             label="%{getText('t3.common.landingTotalWeight')}"/>
    <s:label value='%{falseFishesWeight}'
             label="%{getText('t3.common.falseFishesWeight')}"/>
    <s:label value='%{fishingTime}'
             label="%{getText('t3.common.fishingTime')}"/>
    <s:label value='%{timeAtSea}'
             label="%{getText('t3.common.timeAtSea')}"/>
    <s:label value='%{loch}'
             label="%{getText('t3.common.loch')}"/>

    <s:label value='%{getBoolean(logBookAvailability)}'
             label="%{getText('t3.common.logBookAvailability')}"/>
    <s:label value='%{getBoolean(samplesOnly)}'
             label="%{getText('t3.common.samplesOnly')}"/>
    <s:label value='%{getBoolean(fishHoldEmpty)}'
             label="%{getText('t3.common.fishHoldEmpty')}"/>
    <s:label value='%{completionStatus}'
             label="%{getText('t3.common.completionStatus')}"/>
    <hr style="margin-bottom: 10px"/>
      <%--Level 0 --%>
    <strong><s:text name="t3.common.level0"/></strong>
    <s:label value='%{getBoolean(rf1Computed)}'
             label="%{getText('t3.common.rf1Computed')}"/>
    <s:if test="rf1Computed">
      <s:label value='%{rf1}'
               label="%{getText('t3.common.rf1')}"/>
    </s:if>
    <s:label value='%{getBoolean(rf2Computed)}'
             label="%{getText('t3.common.rf2Computed')}"/>
    <s:if test="rf2Computed">
      <s:label value='%{rf2}'
               label="%{getText('t3.common.rf2')}"/>
    </s:if>
    <s:label value='%{getBoolean(catchesWeightCategorieConverted)}'
             label="%{getText('t3.common.catchesWeightCategorieConverted')}"/>
    <s:label value='%{getBoolean(setDurationAndPositiveCountComputed)}'
             label="%{getText('t3.common.setDurationAndPositiveCountComputed')}"/>
    <s:label value='%{getBoolean(effortComputed)}'
             label="%{getText('t3.common.effortComputed')}"/>
    <s:if test="effortComputed">
      <s:label value='%{computedFishingTimeN0}'
               label="%{getText('t3.common.computedFishingTimeN0')}"/>
      <s:label value='%{computedSearchTimeN0}'
               label="%{getText('t3.common.computedSearchTimeN0')}"/>
      <s:label value='%{computedTimeAtSeaN0}'
               label="%{getText('t3.common.computedTimeAtSeaN0')}"/>
      <s:label value='%{getBoolean(wellPlanWeightCategoriesComputed)}'
               label="%{getText('t3.common.wellPlanWeightCategoriesComputed')}"/>
    </s:if>
    <hr style="margin-bottom: 10px"/>
      <%--Level 1--%>
    <strong><s:text name="t3.common.level1"/></strong>
    <s:label value='%{getBoolean(extrapolateSampleCountedAndMeasured)}'
             label="%{getText('t3.level1.step.extrapolateSampleCountedAndMeasured')}"/>

    <s:label value='%{getBoolean(standardizeSampleMeasures)}'
             label="%{getText('t3.level1.step.standardizeSampleMeasure')}"/>

    <s:label value='%{getBoolean(computeWeightOfCategoriesForSet)}'
             label="%{getText('t3.level1.step.computeWeightOfCategoriesForSet')}"/>

    <s:label value='%{getBoolean(redistributeSampleNumberToSet)}'
             label="%{getText('t3.level1.step.redistributeSampleNumberToSet')}"/>

    <s:label value='%{getBoolean(extrapolateSampleWeightToSet)}'
             label="%{getText('t3.level1.step.extrapolateSampleWeightToSet')}"/>

    <s:label value='%{getBoolean(convertSetSpeciesFrequencyToWeight)}'
             label="%{getText('t3.level1.step.convertSetSpeciesFrequencyToWeight')}"/>

    <s:label value='%{getBoolean(convertSampleSetSpeciesFrequencyToWeight)}'
             label="%{getText('t3.level1.step.convertSampleSetSpeciesFrequencyToWeight')}"/>

    <hr style="margin-bottom: 10px"/>
      <%--Level 2--%>
    <strong><s:text name="t3.common.level2"/></strong>
    <s:label value='%{getBoolean(level2Computed)}'
             label="%{getText('t3.common.level2Computed')}"/>

    <hr style="margin-bottom: 10px"/>
      <%--Level 3--%>
    <strong><s:text name="t3.common.level3"/></strong>

    <s:label value='%{getBoolean(level3Computed)}'
             label="%{getText('t3.common.level3Computed')}"/>

    <hr style="margin-bottom: 10px"/>

    <s:label value='%{#trip.getElementaryCatchTotalWeight(#catchSpecies)}'
             label="%{getText('t3.common.elementaryCatchTotalWeight')}"/>
    <s:if test="rf1Computed">
      <s:label value='%{#trip.getElementaryCatchTotalWeightRf1(#catchSpecies)}'
               label="%{getText('t3.common.elementaryCatchTotalWeightRf1')}"/>
    </s:if>
    <s:if test="rf2Computed">
      <s:label value='%{#trip.getElementaryCatchTotalWeightRf2(#catchSpecies)}'
               label="%{getText('t3.common.elementaryCatchTotalWeightRf2')}"/>
    </s:if>
    <s:label value='%{#trip.getElementaryLandingTotalWeight(#landingSpecies)}'
             label="%{getText('t3.common.elementaryLandingTotalWeight')}"/>

    <s:label value='%{sampleTotalLD1Number}'
             label="%{getText('t3.common.sampleTotalLD1Number')}"/>
    <s:label value='%{sampleTotalLFNumber}'
             label="%{getText('t3.common.sampleTotalLFNumber')}"/>
    <s:label value='%{sampleTotalNumber}'
             label="%{getText('t3.common.sampleTotalNumber')}"/>

  </fieldset>
  <br/>
</s:iterator>

<%--<s:a action="backToTripList" namespace="/trip">--%>
<s:a href="#" onclick="history.back(); return false;">
  <s:if test="back == 'stats'">
    <s:text name="t3.label.admin.backToDatabaseStatistics"/>
  </s:if>
  <s:else>
    <s:text name="t3.label.admin.backToTripList"/>
  </s:else>
</s:a>

