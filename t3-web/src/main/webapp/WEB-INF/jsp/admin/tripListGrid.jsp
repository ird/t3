<%--
  #%L
  T3 :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>
<title><s:text name="t3.label.admin.user.list"/></title>
<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<s:url id="loadUrl" action="getTrips" namespace="/json" escapeAmp="false"/>
<s:url id="showUrl" action="tripDetail" namespace="/trip" escapeAmp="false"/>

<script type="text/javascript">

  var gridTitle = "<s:text name='t3.common.trips'/>";
  var nbMax = "<s:property value='tripListModel.nbTrips'/>";
  jQuery(document).ready(function () {
    var confirmTripDelete = $('#confirmTripDelete');
    confirmTripDelete.hide();
    var tripDetail = $('#tripDetail');
    tripDetail.hide();
    $.addMultiRowTopic('trips', 'Show', tripDetail, "tripIds");
    $.addMultiRowTopic('trips', 'DeleteTrips', confirmTripDelete, "tripIds");

    $.subscribe('trips-completed', function (event) {

      $('[class="ui-jqgrid-title"]').text(gridTitle + " (" + $('#trips')[0].p.records + " / " + nbMax + ")");
    });
  });
</script>

<s:set id="deleteKey" value='%{getText("t3.action.deleteTrips")}'/>
<s:set id="detailKey" value='%{getText("t3.action.showDetails")}'/>
<s:form method="post" namespace="/trip">

  <sjg:grid id="trips" caption="%{getText('t3.common.trips.loading')}"
            dataType="json" href="%{loadUrl}" gridModel="trips"
            pager="true" pagerButtons="true" pagerInput="true"
            navigator="true" autowidth="true" rownumbers="false"
            onBeforeTopics="trips-before"
            onSelectRowTopics='trips-rowSelect'
            onCompleteTopics="trips-completed,trips-cleanSelect"
            navigatorEdit="false"
            navigatorDelete="false"
            navigatorSearch="false"
            navigatorRefresh="false"
            navigatorAdd="false"
            resizable="true"
            rowList="10,15,20,50,100,250,500,1000,2000,4000" rowNum="10"
            viewrecords="true"
            multiselect="true"
            navigatorExtraButtons="{
                show: { title : '%{detailKey}', icon: 'ui-icon-pencil', topic: 'trips-rowShow' },
                deleteTrips: { title : '%{deleteKey}', icon: 'ui-icon-trash', topic: 'trips-rowDeleteTrips' }
        }">
    <sjg:gridColumn name="id" title="id" hidden="true"/>
    <sjg:gridColumn name="vessel.libelle" sortable="false"
                    title='%{getText("t3.common.vessel")}'/>

    <sjg:gridColumn name="vessel.fleetCountry.libelle" sortable="false"
                    title='%{getText("t3.common.fleetCountry")}'/>

    <sjg:gridColumn name="vessel.flagCountry.libelle" sortable="false"
                    title='%{getText("t3.common.flagCountry")}'/>

    <sjg:gridColumn name="departureDate" formatter="date" sortable="false"
                    title='%{getText("t3.common.departureDate")}'/>

    <sjg:gridColumn name="landingDate" formatter="date" sortable="false"
                    title='%{getText("t3.common.landingDate")}'/>

  </sjg:grid>



  <s:submit id='confirmTripDelete' action="confirmTripDelete" method="input"/>
  <s:submit id='tripDetail' action="tripDetail"/>

</s:form>
