<%--
  #%L
  T3 :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<fieldset>

  <legend>
    <s:text name="t3.label.config.resume"/>
  </legend>

  <s:if test="configuration.deleteTrip">
    <s:set name="title"><s:text
      name="t3.label.admin.trip.delete.trips"/></s:set>
  </s:if>
  <s:else>
    <s:set name="title"><s:text
      name="t3.label.admin.trip.delete.data"/></s:set>
  </s:else>
  <s:label key="matchingTripCount"
           label='%{getText("t3.common.number.delete.trip.mode")}'
           requiredLabel="true" value="%{title}"/>
  <br/>
  <s:label key="nbTrips"
           label='%{getText("t3.common.number.of.trip.delete")}'
           requiredLabel="true" value="%{configuration.tripIds.size}"/>

  <%--<ul>--%>
    <%--<s:iterator value="trips" status="status">--%>
      <%--<li>--%>
        <%--<s:property value="value"/>--%>
      <%--</li>--%>
    <%--</s:iterator>--%>
  <%--</ul>--%>

</fieldset>
