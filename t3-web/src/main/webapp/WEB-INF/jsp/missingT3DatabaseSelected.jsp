<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<title><s:text name="t3.label.missingT3DatabaseSelected"/></title>

<h2><s:text name="t3.label.missingT3DatabaseSelected"/></h2>

<hr/>

<p>
  <s:text name="t3.message.missingT3DatabaseSelected"/>
</p>

<ul>
  <li>
    <s:a action="selectUserT3Database!input" namespace="/user">
      <s:text name="t3.menu.select.t3Database"/>
    </s:a>
  </li>
  <li>
    <s:a action="userForm" namespace="/user">
      <s:text name="t3.menu.admin.editUser"/>
      <s:param name="user.topiaId" value="#session.t3Session.user.topiaId"/>
      <s:param name="userEditAction" value="'edit'"/>
    </s:a>
  </li>
  <s:if test="userIsAdmin">
    <li>
      <s:a action="userList" namespace="/user">
        <s:text name="t3.menu.admin.user"/>
      </s:a>
    </li>
  </s:if>
</ul>
<br/>
