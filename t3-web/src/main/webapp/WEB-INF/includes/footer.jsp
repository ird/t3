<%@ page import="fr.ird.t3.web.actions.T3ActionSupport" %>
<%--
#%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<hr/>
<div id='footer'>
  <ul class="clearfix">
    <li>
      <a href="http://maven-site.forge.codelutin.com/t3/"
         title="Documentation de l'application" target="_blank">
        T3+
      </a>
    </li>
    <li>
      <a href="http://forge.codelutin.com/projects/t3/roadmap?completed=1"
         title="Modifications faites pour cette version" target="_blank">
        Roadmap <%=T3ActionSupport.getApplicationVersion()%>
      </a>
    </li>
    <li>
      <a href="http://www.gnu.org/licenses/agpl.html" title="Licence AGPL v3"
         target="_blank">
        AGPLv3
      </a>
    </li>
    <li>
      Copyright 2010 - 2012
      <a href="http://www.ird.fr">IRD</a>
      <a href="http://www.codelutin.com" title="Code Lutin" target="_blank">Code
        Lutin</a>
    </li>
  </ul>
</div>
