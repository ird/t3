/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

( function ($) {

    $.fn.extend(
            {
                addRowSelectTopic:function (gridId) {
                    $.subscribe(gridId + '-rowSelect', function (event) {
                        var gridId = event.data.id;
                        var opts = jQuery.struts2_jquery[gridId] = {};
                        var id = event.originalEvent.id;
                        if (id) {
                            opts['selectedRow'] = id;
                        }
                    }, {id:gridId});
                },

                addClearSelectTopic:function (gridId) {
                    $.subscribe(gridId + '-cleanSelect', function (event) {
                        var gridId = event.data.id;
                        jQuery.struts2_jquery[gridId] = {};
                    }, {id:gridId});
                },

                addAddRowTopic:function (gridId, url) {
                    $.subscribe(gridId + '-rowAdd', function (event) {
                        var url = event.data.url;
                        window.location = url;
                    }, {id:gridId, url:url});
                },

                addSingleRowTopic:function (gridId, action, url, parameterName) {
                    $.subscribe(gridId + '-row' + action, function (event) {
                        var gridId = event.data.id;
                        var opts = jQuery.struts2_jquery[gridId];
                        if (opts && opts['selectedRow']) {
                            var selectedId = opts['selectedRow'];
                            var parameterName = event.data.parameterName;
                            var params = {}
                            params[parameterName] = selectedId;
                            var url = $.prepareUrl(event.data.url, params);
                            window.location = url;
                        }
                    }, {id:gridId, url:url, parameterName:parameterName});
                },
                addMultiRowTopic:function (gridId, action, target, checkboxName, callback) {
                    $.subscribe(gridId + '-row' + action, function (event) {
                        var gridId = event.data.id;
                        var prefix = 'jqg_' + gridId + '_';
                        var prefixLength = prefix.length;

                        // get all selected ids
                        var inputs = $('table#' + gridId + ' :checked[id^="' + prefix + '"]');

                        if (inputs && inputs.length) {
                            var checkboxName = event.data.checkboxName;
                            inputs.each(function () {
                                var id = this.id;
                                var newId = id.substring(prefixLength);
                                $.attr(this, "name", checkboxName);
                                $.attr(this, "value", newId);
                                $.attr(this, "checked", "checked");
                            });
                            var callback = event.data.callback;
                            if (callback) {
                                callback();
                            }
                            event.data.target.click();
                        }
                    }, {id:gridId,
                        target:target,
                        checkboxName:checkboxName,
                        callback:callback});
                },

                serializeCheckboxs:function (useFilterId, id, params, newId) {
                    var useFilterIdValue = $(':checkbox[name="' + useFilterId + '"]')[0].checked;

                    if (!useFilterIdValue) {

                        // not using this filter
                        return;
                    }

                    var all = $(':checkbox[name="' + id + '"]');
                    var selected = $(':checked[name="' + id + '"]');
                    if (all.length == selected.length) {

                        // use all available data, so this it a none filter
                        return;
                    }

                    params[useFilterId] = true;

                    // apply a year filter
                    var list = [];
                    selected.each(function () {
                        list.push(this.value);
                    });
                    if (!newId) {
                        newId = id;
                    }
                    params[newId] = list;
                },

                useFilter:function (useFilterCheckBoxId, noUserId, useId) {

                    var checkBox = $('#' + useFilterCheckBoxId);

                    checkBox.change({notUseId:$('#' + noUserId), useId:$('#wwgrp_' + useId)}, function (event) {
                        var value = this.checked;
                        if (value) {

                            // use filter
                            event.data.notUseId.hide();
                            event.data.useId.show();
                        } else {
                            event.data.useId.hide();
                            event.data.notUseId.show();
                        }
                    });
                    checkBox.change();
                },

                prepareUrl:function (url, params) {
                    var result = url;
                    if (url.indexOf("?") > -1) {
                        result += "&";
                    } else {
                        result += "?";
                    }
                    result += $.param(params);
                    return result;
                }
            });

    $.extend({
                 addRowSelectTopic:function (gridId) {
                     return $(document).addRowSelectTopic(gridId);
                 },

                 addClearSelectTopic:function (gridId) {
                     return $(document).addClearSelectTopic(gridId);
                 },

                 addAddRowTopic:function (gridId, url) {
                     return $(document).addAddRowTopic(gridId, url);
                 },

                 addSingleRowTopic:function (gridId, action, url, parameterName) {
                     return $(document).addSingleRowTopic(gridId, action, url, parameterName);
                 },

                 addMultiRowTopic:function (gridId, action, target, checkboxName, calbback) {
                     return $(document).addMultiRowTopic(gridId, action, target, checkboxName, calbback);
                 },
                 serializeCheckboxs:function (useFilterId, id, params, newId) {
                     return $(document).serializeCheckboxs(useFilterId, id, params, newId);
                 },
                 useFilter:function (useFilterCheckBoxId, noUserId, useId) {
                     return $(document).useFilter(useFilterCheckBoxId, noUserId, useId);
                 },
                 prepareUrl:function (url, params) {
                     return $(document).prepareUrl(url, params);
                 }
             });
})(jQuery);

