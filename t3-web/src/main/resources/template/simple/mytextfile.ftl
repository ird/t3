<#--
 #%L
 T3 :: Web
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/${parameters.templateDir}/${parameters.theme}/controlheader.ftl" />
<#include "/${parameters.templateDir}/simple/text.ftl" />
<input type="submit"<#rt/>
<#if parameters.id??>
 id="${parameters.id?html}_delete"<#rt/>
</#if>
 name="action:${parameters.dynamicAttributes["deleteAction"]}"<#rt/>
 value="${parameters.dynamicAttributes["deleteLabel"]}"<#rt/>
 onclick="dojo.byId('${parameters.dynamicAttributes["deleteWidgetId"]}').value='${parameters.dynamicAttributes["deleteId"]}'; return true;"<#rt/>
<#include "/${parameters.templateDir}/simple/css.ftl" />
        />
<#--<a href="${parameters.dynamicAttributes["action"]}">Supprimer</a>-->
<#include "/${parameters.templateDir}/xhtml/controlfooter.ftl" />
