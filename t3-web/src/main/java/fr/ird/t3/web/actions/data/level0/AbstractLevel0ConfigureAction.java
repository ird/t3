/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.data.level0.AbstractLevel0Configuration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselSimpleTypeDAO;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Map;

/**
 * Abstract level 0 configuration action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractLevel0ConfigureAction<C extends AbstractLevel0Configuration> extends AbstractConfigureAction<C> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractLevel0ConfigureAction.class);

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    @InjectDAO(entityType = Vessel.class)
    protected transient VesselDAO vesselDAO;

    @InjectDAO(entityType = Country.class)
    protected transient CountryDAO countryDAO;

    @InjectDAO(entityType = VesselSimpleType.class)
    protected transient VesselSimpleTypeDAO vesselSimpleTypeDAO;

    @InjectDecoratedBeans(beanType = VesselSimpleType.class)
    protected Map<String, String> vesselSimpleTypes;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> fleets;

    protected AbstractLevel0ConfigureAction(Class<C> configurationType) {
        super(configurationType);
    }

    @Override
    public final void prepare() throws Exception {

        // always invalidate configuration status
        setConfirm(false);

        // whatever is the current state of form, remove action context from session
        // will need to confirm configuration to enable again the action to start
        removeActionContextFromSession();

        // make sur configuration is created before all
        C config = getConfiguration();

        // inject every thing needed (daos, ...)
        injectExcept(InjectDecoratedBeans.class);

        if (!isConfigurationInSession()) {

            // load default configuration
            loadDefaultConfiguration(config);
        }

        injectOnly(InjectDecoratedBeans.class);
    }

    public final String prepareConfiguration() throws Exception {

        if (!isConfigurationInSession() && !hasFieldErrors()) {

            // store configuration in session
            storeActionConfiguration(configuration);
        }
        return INPUT;
    }

    @Override
    public final String execute() throws Exception {

        // init action context in session
        prepareActionContext();

        // no error on configuration : can confirm action
        setConfirm(true);
        return SUCCESS;
    }

    public Map<String, String> getVesselSimpleTypes() {
        return vesselSimpleTypes;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    /**
     * Loads the default configuration.
     *
     * @param config the configuration to fill
     * @throws TopiaException if any error while loading data from T3 database
     */
    protected void loadDefaultConfiguration(C config) throws TopiaException {

        T3Date minDate = tripDAO.getFirstLandingDate();
        config.setMinDate(minDate);
        config.setBeginDate(minDate);
        T3Date maxDate = tripDAO.getLastLandingDate();
        config.setMaxDate(maxDate);
        config.setEndDate(maxDate);
        config.setVesselSimpleTypes(sortToList(vesselSimpleTypeDAO.findAllUsedInTrip()));
        config.setFleets(sortToList(countryDAO.findAllFleetUsedInTrip()));

        if (log.isInfoEnabled()) {
            log.info("beginDate : " + config.getBeginDate());
            log.info("endDate : " + config.getEndDate());
        }
    }

    @Override
    public final String input() throws Exception {
        throw new NoSuchMethodException("DO NOT COME HERE!!!");
    }

}
