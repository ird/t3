/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level2;

import fr.ird.t3.actions.data.level2.Level2Configuration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * To manager the step 2 of a level 2 treatment configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConfigureLevel2Step2Action extends AbstractConfigureAction<Level2Configuration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ConfigureLevel2Step2Action.class);

    @InjectDAO(entityType = Country.class)
    protected transient CountryDAO countryDAO;

    @InjectDAO(entityType = SchoolType.class)
    protected transient SchoolTypeDAO schoolTypeDAO;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> sampleFleets;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> sampleFlags;

    protected final Map<String, String> timeSteps = createTimeSteps();

    protected Map<String, String> useSamplesOrNot;

    /**
     * Flag to know if some data are missings.
     * <p/>
     * This flag is setted in the {@link #prepare()} method while
     * loading possibles data.
     */
    protected boolean missingDatas;

    public ConfigureLevel2Step2Action() {
        super(Level2Configuration.class);
    }

    @Override
    public void prepare() throws Exception {

        useSamplesOrNot = createLevel2UseSamplesOrNotMap();

        Level2Configuration conf = getConfiguration();

        // let's inject everything except decorated values
        injectExcept(InjectDecoratedBeans.class);

        missingDatas = false;

        String oceanId = conf.getOceanId();

        conf.setSampleFlags(sortToList(countryDAO.findAllFlagUsedInSample(oceanId)));
        conf.setSampleFleets(sortToList(countryDAO.findAllFleetUsedInSample(oceanId)));

        if (CollectionUtils.isEmpty(conf.getSampleFlags())) {
            addFieldError("configuration.sampleFleetIds",
                          _("t3.error.no.sample.fleet.found"));
            // need some data
            missingDatas = true;
        }
        if (CollectionUtils.isEmpty(conf.getSampleFleets())) {
            addFieldError("configuration.sampleFlagIds",
                          _("t3.error.no.sample.flag.found"));
            // need some data
            missingDatas = true;
        }

        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);

        // prepapre stratum minimum sample count for free school type value
        Integer stratumMinimumSampleCountFreeSchoolType =
                conf.getStratumMinimumSampleCountFreeSchoolType();
        if (stratumMinimumSampleCountFreeSchoolType == null) {

            // get value from db
            SchoolType schoolType = schoolTypeDAO.findByCode(2);
            stratumMinimumSampleCountFreeSchoolType =
                    schoolType.getThresholdNumberLevel2();

            // store it back in configuration
            conf.setStratumMinimumSampleCountFreeSchoolType(
                    stratumMinimumSampleCountFreeSchoolType);
        }

        // prepapre stratum minimum sample count for object school type value
        Integer stratumMinimumSampleCountObjectSchoolType =
                conf.getStratumMinimumSampleCountObjectSchoolType();
        if (stratumMinimumSampleCountObjectSchoolType == null) {

            SchoolType schoolType = schoolTypeDAO.findByCode(1);
            stratumMinimumSampleCountObjectSchoolType =
                    schoolType.getThresholdNumberLevel2();

            // store it back in configuration
            conf.setStratumMinimumSampleCountObjectSchoolType(
                    stratumMinimumSampleCountObjectSchoolType);
        }

        // prepapre stratum weight ratio value
        float stratumWeightRatio = conf.getStratumWeightRatio();
        if (stratumWeightRatio == 0) {

            // get the default value from application configuration
            stratumWeightRatio = getApplicationConfig().getStratumWeightRatio();

            // store it back in configuration
            conf.setStratumWeightRatio(stratumWeightRatio);
        }

        if (log.isInfoEnabled()) {
            log.info("Selected sample fleet countries  : " + conf.getSampleFleetIds());
            log.info("Selected sample fleet countries  : " + conf.getSampleFlagIds());
            log.info("Selected min sample count BL     : " + conf.getStratumMinimumSampleCountFreeSchoolType());
            log.info("Selected min sample count BO     : " + conf.getStratumMinimumSampleCountObjectSchoolType());
            log.info("Selected weight ratio            : " + conf.getStratumWeightRatio());
        }

    }

    @Override
    public void validate() {

        Level2Configuration config = getConfiguration();

        if (CollectionUtils.isEmpty(config.getSampleFlagIds())) {
            addFieldError("configuration.sampleFlagIds",
                          _("t3.error.no.sample.flag.selected"));
        }

        if (CollectionUtils.isEmpty(config.getSampleFleetIds())) {
            addFieldError("configuration.sampleFleetIds",
                          _("t3.error.no.sample.fleet.selected"));
        }
    }

    @Override
    public String execute() throws Exception {

        Level2Configuration config = getConfiguration();

        // as the level 2 configuration is now fully valid, store it
        config.setValidStep2(true);

        return SUCCESS;
    }

    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    public boolean isMissingDatas() {
        return missingDatas;
    }

    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

}
