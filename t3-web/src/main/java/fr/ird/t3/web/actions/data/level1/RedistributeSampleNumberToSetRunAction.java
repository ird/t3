/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import fr.ird.t3.actions.data.level1.Level1Step;
import fr.ird.t3.actions.data.level1.RedistributeSampleNumberToSetAction;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;

import java.util.Date;
import java.util.Map;

/**
 * Launch the level 1 action {@link RedistributeSampleNumberToSetAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class RedistributeSampleNumberToSetRunAction extends AbstractLevel1RunAction<RedistributeSampleNumberToSetAction> {

    private static final long serialVersionUID = 1L;

    public RedistributeSampleNumberToSetRunAction() {
        super(RedistributeSampleNumberToSetAction.class,
              Level1Step.REDISTRIBUTE_SAMPLE_SET_TO_SET);
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(
            RedistributeSampleNumberToSetAction action,
            Exception error,
            Date startDate,
            Date endDate) {
        Map<String, Object> parameters = super.prepareResumeParameters(
                action,
                error,
                startDate,
                endDate
        );

        parameters.put("speciesDecorator", getDecorator(Species.class));
        parameters.put("tripDecorator", getDecorator(Trip.class, DecoratorService.WITH_ID));
        parameters.put("activityDecorator", getDecorator(Activity.class));
        return parameters;
    }
}
