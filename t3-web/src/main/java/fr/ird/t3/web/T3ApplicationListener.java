/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import fr.ird.converter.FloatConverter;
import fr.ird.t3.T3Configuration;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaRootContextFactory;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserImpl;
import fr.ird.t3.services.DefaultT3ServiceContext;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import fr.ird.t3.services.UserService;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.util.converter.ConverterUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.beans.Introspector;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * To listen start or end of the application.
 * <p/>
 * On start we will load the configuration and check connection to internal
 * database, creates schema and create an admin user in none found in database.
 * <p/>
 * On stop, just release the application configuration.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3ApplicationListener implements ServletContextListener {

    /** Logger. */
    protected static final Log log =
            LogFactory.getLog(T3ApplicationListener.class);

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find pg driver", e);
            }
        }
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find h2 driver", e);
            }
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (log.isInfoEnabled()) {
            log.info("Application starting at " + new Date() + "...");
        }

        // initialize configuration
        T3Configuration configuration = new T3Configuration();
        configuration.init();

        T3ApplicationContext applicationContext = new T3ApplicationContext();
        T3ApplicationContext.setT3ApplicationContext(
                sce.getServletContext(), applicationContext
        );

        // initialize configuration
        applicationContext.setConfiguration(configuration);

        // init I18n
        DefaultI18nInitializer i18nInitializer =
                new DefaultI18nInitializer("t3-i18n");
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, Locale.getDefault());

        // register our not locale dependant converter
        Converter converter = ConverterUtil.getConverter(Float.class);
        if (converter != null) {
            ConvertUtils.deregister(Float.class);
        }
        ConvertUtils.register(new FloatConverter(), Float.class);

        T3ServiceContext serviceContext = DefaultT3ServiceContext.newContext(
                Locale.getDefault(),
                null,
                null,
                configuration,
                new T3ServiceFactory()
        );

        // init database (and create minimal admin user if required)
        initInternalDatabase(applicationContext,
                             serviceContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (log.isInfoEnabled()) {
            log.info("Application is ending at " + new Date() + "...");
        }

        ServletContext servletContext = sce.getServletContext();

        // get application context
        T3ApplicationContext applicationContext =
                T3ApplicationContext.getT3ApplicationContext(servletContext);

        // remove application context from container
        T3ApplicationContext.remove3ApplicationContext(servletContext);

        // release all user sessions
        releaseSessions(applicationContext);

        // release internal db
        TopiaContext rootContext = applicationContext.getInternalRootContext();
        try {
            T3EntityHelper.releaseH2RootContext(rootContext);
        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error("Error when closing internal db", e);
            }
        }

        // see http://wiki.apache.org/commons/Logging/FrequentlyAskedQuestions#A_memory_leak_occurs_when_undeploying.2Fredeploying_a_webapp_that_uses_Commons_Logging._How_do_I_fix_this.3F
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        LogFactory.release(contextClassLoader);

        Introspector.flushCaches();
    }

    protected void releaseSessions(T3ApplicationContext applicationContext) {
        Set<T3Session> t3Sessions = applicationContext.getT3Sessions();

        if (CollectionUtils.isNotEmpty(t3Sessions)) {
            for (T3Session t3Session : t3Sessions) {
                try {
                    applicationContext.destroyT3Session(t3Session);
                } catch (Exception e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not close session " + t3Session, e);
                    }
                }
            }
        }

    }

    /**
     * Init the internal database, says :
     * <ul>
     * <li>If no schema found or if asked to updateSchema using the
     * {@code updateSchema} configuration option is on.</li>
     * <li>If no user found is db, create a administrator user
     * {@code admin/admin}</li>
     * </ul>
     *
     * @param applicationContext t3 application context where to store global internal db root context
     * @param serviceContext     service context to use
     */
    protected void initInternalDatabase(T3ApplicationContext applicationContext,
                                        T3ServiceContext serviceContext) {

        try {
            T3Configuration configuration =
                    serviceContext.getApplicationConfiguration();

            T3TopiaRootContextFactory factory = new T3TopiaRootContextFactory();
            TopiaContext internalRootContext =
                    factory.newInternalDb(configuration);
            applicationContext.setInternalRootContext(internalRootContext);


            boolean schemaNeedUpdate =
                    !isInternalSchemaCreated(internalRootContext) ||
                    configuration.isUpdateSchema();

            if (schemaNeedUpdate) {

                internalRootContext.updateSchema();
//                updateInternalSchema(internalRootContext);
            }
        } catch (Exception e) {
            throw new IllegalStateException("could not start db", e);
        }

        try {
            createAdminUser(serviceContext,
                            applicationContext.getInternalRootContext());

        } catch (Exception e) {
            throw new IllegalStateException(
                    "could not create default admin user", e);
        }
    }

//    /**
//     * Updates the schema of the internal database.
//     *
//     * @param rootContext internal db root context
//     * @throws TopiaException if something was wrong while schema update
//     */
//    protected void updateInternalSchema(TopiaContext rootContext) throws TopiaException {
//        if (log.isInfoEnabled()) {
//            log.info("Will create or update schema for db.");
//        }
//        // must create the schema
//
//        // start a connexion to load schema
//        TopiaContext tx = rootContext.beginTransaction();
//        try {
//            tx.updateSchema();
//
//        } finally {
//
//            T3EntityHelper.releaseContext(tx);
//        }
//    }

    /**
     * Creates the adminsitrator ({@code admin/admin}) on the internal
     * database.
     *
     * @param serviceContext service context
     * @param rootContext    internal database root context
     * @throws Exception if could not create the user.
     */
    protected void createAdminUser(T3ServiceContext serviceContext,
                                   TopiaContext rootContext) throws Exception {

        TopiaContext tx = rootContext.beginTransaction();
        try {

            serviceContext.setInternalTransaction(tx);
            UserService service = serviceContext.newService(UserService.class);

            List<T3User> users = service.getUsers();

            if (CollectionUtils.isEmpty(users)) {

                // no users in database create the admin user
                if (log.isInfoEnabled()) {
                    log.info("No user in database, will create default " +
                             "admin user (password admin).");
                }

                T3User u = new T3UserImpl();
                u.setLogin("admin");
                u.setPassword("admin");
                u.setAdmin(true);

                // store it in db
                service.createUser(u);
            }
        } finally {
            serviceContext.setInternalTransaction(null);
            T3EntityHelper.releaseContext(tx);
        }
    }

    /**
     * Tests if the internal database schema created.
     *
     * @param rootContext internal root context
     * @return {@code true} if the schema is already created, {@code false}
     * otherwise
     * @throws TopiaException if something was wrong while requesting database
     */
    protected boolean isInternalSchemaCreated(TopiaContext rootContext) throws TopiaException {

//        TopiaContextImplementor tx =
//                (TopiaContextImplementor)
//                        rootContext.beginTransaction();
//        try {
        boolean schemaFound =
                ((TopiaContextImplementor) rootContext).isSchemaExist(T3UserImpl.class);

        return schemaFound;

//        } finally {
//            T3EntityHelper.releaseContext(tx);
//        }
    }
}
