/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.decorator.Decorator;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * To obtain statistics of the selected T3 database.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3DatabaseDetailAction extends T3ActionSupport {

    private static final long serialVersionUID = -8408617621456216934L;

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    /** All trips of the database group by their owing ocean. */
    protected transient Multimap<Ocean, Trip> tripsByOcean;

    /** All oceans used in any trip. */
    protected List<Ocean> oceans;

    /** Count of trips in database. */
    protected long nbTrips;

    /** Count of activities in database. */
    @InjectFromDAO(entityType = Activity.class, method = "count")
    protected long nbActivities;

    protected TripListModel tripListModel;

    public TripListModel getTripListModel() {
        if (tripListModel == null) {
            tripListModel = getT3Session().getTripListModel();

            if (tripListModel == null) {
                tripListModel = new TripListModel();
            }
        }
        return tripListModel;
    }

    @Override
    public String input() throws Exception {

        injectOnly(InjectDAO.class, InjectFromDAO.class);

        if (getT3Session().getTripListModel() == null) {

            // must load it
            tripListModel = loadTripListModel(tripDAO);
        } else {

            // use existing one
            tripListModel = getT3Session().getTripListModel();
        }

        oceans = tripListModel.getOceans();

        tripsByOcean = ArrayListMultimap.create();
        for (Ocean ocean : oceans) {
            List<String> ids = (List<String>) tripListModel.getTripIds(ocean);
            List<Trip> trips = tripDAO.findAllByIds(ids);
            tripsByOcean.putAll(ocean, trips);
        }
        nbTrips = tripListModel.getTripIdsByOcean().values().size();

        return INPUT;
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public long getNbTrips() {
        return nbTrips;
    }

    public long getNbActivities() {
        return nbActivities;
    }

    public long getNbActivities(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        long result = 0;
        for (Trip trip : trips) {
            result += trip.sizeActivity();
        }
        return result;
    }

    public int getNbOceans() {
        return getOceans().size();
    }

    public int getNbTrips(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        return trips.size();
    }

    public String getFirstTripDate(Ocean ocean) {
        List<Trip> trips = (List<Trip>) getTrips(ocean);
        Trip trip = trips.get(0);
        return formatDate(trip.getLandingDate());
    }

    public String getLastTripDate(Ocean ocean) {
        List<Trip> trips = (List<Trip>) getTrips(ocean);
        Trip trip = trips.get(trips.size() - 1);
        return formatDate(trip.getLandingDate());
    }

    public Map<String, String> getTripsWithNoDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filtredTrips = TripDAO.getAllTripsWithNoDataComputed(trips);
        Map<String, String> result = getTripMap(filtredTrips);
        return result;
    }

    public Map<String, String> getTripsWithSomeDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filtredTrips = TripDAO.getAllTripsWithSomeDataComputed(trips);
        Map<String, String> result = getTripMap(filtredTrips);
        return result;
    }

    public Map<String, String> getTripsWithAllDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filtredTrips = TripDAO.getAllTripsWithAllDataComputed(trips);
        Map<String, String> result = getTripMap(filtredTrips);
        return result;
    }

    protected Collection<Trip> getTrips(Ocean ocean) {
        Collection<Trip> trips = tripsByOcean.get(ocean);
        return trips;
    }

    protected Map<String, String> getTripMap(Collection<Trip> trips) {
        Map<String, String> result = Maps.newLinkedHashMap();
        Decorator<Trip> decorator = getDecorator(Trip.class);
        Function<TopiaEntity, String> toTopiaId = T3Functions.TO_TOPIA_ID;
        for (Trip filtredTrip : trips) {
            result.put(toTopiaId.apply(filtredTrip),
                       decorator.toString(filtredTrip));
        }
        return result;
    }

}
