/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Map;

/**
 * To obtain the list of trips of the database.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class TripListAction extends T3ActionSupport {

    protected static final Log log = LogFactory.getLog(TripListAction.class);

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    @InjectFromDAO(entityType = Trip.class, method = "findAllYearsUsedInTrip")
    protected Collection<Integer> allYears;

    @InjectFromDAO(entityType = Country.class, method = "findAllFleetUsedInTrip")
    protected Collection<Country> allFleets;

    @InjectFromDAO(entityType = Country.class, method = "findAllFlagUsedInTrip")
    protected Collection<Country> allFlags;

    @InjectFromDAO(entityType = Vessel.class, method = "findAllUsedInTrip")
    protected Collection<Vessel> allVessels;

    protected Map<String, String> years;

    protected Map<String, String> oceans;

    protected Map<String, String> fleets;

    protected Map<String, String> flags;

    protected Map<String, String> vessels;

    protected TripListModel tripListModel;

    protected boolean back;

    public Map<String, String> getYears() {
        return years;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getFlags() {
        return flags;
    }

    public Map<String, String> getVessels() {
        return vessels;
    }

    public TripListModel getTripListModel() {
        if (tripListModel == null) {
            tripListModel = getT3Session().getTripListModel();

            if (tripListModel == null) {
                tripListModel = new TripListModel();
            }
        }
        return tripListModel;
    }

    @Override
    public String input() throws Exception {

        injectOnly(InjectDAO.class, InjectFromDAO.class);

        if (getT3Session().getTripListModel() == null) {

            // must load it
            tripListModel = loadTripListModel(tripDAO);
        } else {

            // use existing one
            tripListModel = getT3Session().getTripListModel();
        }

        years = Maps.newLinkedHashMap();
        for (Integer year : allYears) {
            years.put(year + "", year + "");
        }
        oceans = sortAndDecorate(tripListModel.getOceans());
        fleets = sortAndDecorate(allFleets);
        flags = sortAndDecorate(allFlags);
        vessels = sortAndDecorate(allVessels);

        return INPUT;
    }

    public String loadGrid() throws Exception {

        tripListModel = getT3Session().getTripListModel();

        return INPUT;
    }

    public String backToTrip() throws Exception {
        back = true;
        return input();
    }
}
