/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Abstract check interceptor.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractCheckInterceptor extends AbstractInterceptor {

    private static final long serialVersionUID = -7169251953113201351L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractCheckInterceptor.class);

    /** Where to redirect where user is loggued */
    protected String redirectAction;

    public void setRedirectAction(String redirectAction) {
        this.redirectAction = redirectAction;
    }

    protected abstract boolean doCheck(ActionInvocation invocation);

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        boolean check = doCheck(invocation);

        if (!check) {

            String redirectUrl = getRedirectUrl();
            if (log.isInfoEnabled()) {
                log.info("Will redirect to " + redirectUrl);
            }
            redirect(redirectUrl);

            return null;
        }

        String result = invocation.invoke();
        return result;
    }

    protected String getRedirectUrl() {
        return redirectAction;
    }

    protected void redirect(String url) throws Exception {

        HttpServletResponse response = ServletActionContext.getResponse();
        HttpServletRequest request = ServletActionContext.getRequest();

        String path = request.getContextPath();
        if (!url.startsWith("/")) {
            path += "/";
        }
        response.sendRedirect(path + url);
    }

}
