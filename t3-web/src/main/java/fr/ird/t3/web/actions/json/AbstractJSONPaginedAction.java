/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ird.t3.web.actions.json;

import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;
import org.nuiton.topia.persistence.pager.TopiaPagerBeanBuilder;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract JSON action with pagination support.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 0.1
 */
public abstract class AbstractJSONPaginedAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    public abstract Integer getRows();

    public abstract Integer getPage();

    public abstract Long getTotal();

    public abstract Long getRecords();

    protected TopiaPagerBean pager = new TopiaPagerBean();

    // sorting order - asc or desc
    protected String sord;

    // get index row - i.e. user click to sort.
    protected String sidx;

    protected String filters;

    public void setRows(Integer rows) {
        pager.setPageSize(rows);
    }

    public void setPage(Integer page) {
        pager.setPageIndex(page);
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    protected String getSortColumn() {
        String result = null;
        if (useSort()) {
            result = sidx;
        }
        return result;
    }

    protected Boolean isSortAscendant() {
        Boolean result = null;
        if (useSort()) {
            result = "asc".equals(sord);
        }
        return result;
    }

    protected boolean useSort() {
        return StringUtils.isNotEmpty(sidx);
    }

    protected Object getFilterObject() throws JSONException {
        Object filterObject = null;
        if (StringUtils.isNotEmpty(filters)) {
            filterObject = JSONUtil.deserialize(filters);
        }
        return filterObject;
    }

    protected void initFilter() throws JSONException {

        TopiaPagerBeanBuilder builder = new TopiaPagerBeanBuilder(pager);

        if (useSort()) {
            builder.setSortAscendant(isSortAscendant())
                    .setSortcolumn(getSortColumn());
        }

        if (StringUtils.isNotEmpty(filters)) {
            Map<String, Object> filterObject =
                    (Map<String, Object>) JSONUtil.deserialize(filters);

            String groupOp = (String) filterObject.get("groupOp");
            Collection<Map<String, String>> rules =
                    (Collection<Map<String, String>>) filterObject.get("rules");

            pager = builder.
                    setFilterOperationGroup(groupOp).
                    addRules(rules).
                    toBean();
        }
    }
}
