/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.data.TripDTO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanDAO;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.web.actions.admin.TripListModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.PagerBeanUtil;
import org.nuiton.util.TimeLog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Obtains a list of trips.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class GetTripsAction extends AbstractJSONPaginedAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(GetTripsAction.class);

    private static final TimeLog TIME_LOG = new TimeLog(GetTripsAction.class);

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    @InjectDAO(entityType = Ocean.class)
    protected transient OceanDAO oceanDAO;

    @InjectDAO(entityType = Country.class)
    protected transient CountryDAO countryDAO;

    @InjectDAO(entityType = Vessel.class)
    protected transient VesselDAO vesselDAO;

    protected List<TripDTO> trips;

    public List<TripDTO> getTrips() {
        return trips;
    }

    protected TripListModel model;

    @Override
    public String execute() throws Exception {

        injectOnly(InjectDAO.class);

        List<String> allTripIds;
        List<Trip> allTrips;

        long t0 = TimeLog.getTime();

        model = getT3Session().getTripListModel();

        TIME_LOG.log(t0, "getTrips (ocean filter)");

        Multimap<Ocean, String> tripsByOcean = model.getTripIdsByOcean();

        if (model.isOceanFilter()) {

            // filtrage par ocean

            List<String> oceanIds = model.getOceanIds();

            allTripIds = Lists.newArrayList();

            if (CollectionUtils.isNotEmpty(oceanIds)) {
                for (String oceanId : oceanIds) {

                    Ocean ocean;

                    if ("null".equals(oceanId)) {
                        ocean = null;
                    } else {
                        ocean = oceanDAO.findByTopiaId(oceanId);
                    }
                    Collection<String> allByOcean = tripsByOcean.get(ocean);
                    allTripIds.addAll(allByOcean);
                }
            }
        } else {

            allTripIds = Lists.newArrayList(tripsByOcean.values());
        }

        if (log.isInfoEnabled()) {
            log.info("Nb total trips : " + allTripIds.size());
        }

        t0 = TimeLog.getTime();

        if (CollectionUtils.isEmpty(allTripIds)) {
            allTrips = Lists.newArrayList();
        } else {
            allTrips = tripDAO.findAllByIds(allTripIds);
        }

        TIME_LOG.log(t0, "load from ids");
        t0 = TimeLog.getTime();

        if (CollectionUtils.isNotEmpty(allTrips) && model.isYearFilter()) {
            filterYears(allTrips);
        }
        if (log.isInfoEnabled()) {
            log.info("Nb total trips (after year filter) " + allTrips.size());
        }

        if (CollectionUtils.isNotEmpty(allTrips) &&
            model.isVesselFilter() ||
            model.isFlagFilter() ||
            model.isFleetFilter()) {

            filterVessels(allTrips);
        }

        if (log.isInfoEnabled()) {
            log.info("Nb total trips (after vessel / flag / fleet filter) " +
                     allTrips.size());
        }

        TIME_LOG.log(t0, "all filter");

        initFilter();

        pager.setRecords(allTrips.size());

        List<Trip> filteredTrips = getPageFromList(allTrips);

        if (log.isInfoEnabled()) {
            log.info("pager pageSize         : " + pager.getPageSize());
            log.info("pager pagesNumber      : " + pager.getPagesNumber());
            log.info("pager pageIndex        : " + pager.getPageIndex());
            log.info("pager records          : " + pager.getRecords());
            log.info("pager recordStartIndex : " + pager.getRecordStartIndex());
            log.info("pager recordEndIndex   : " + pager.getRecordEndIndex());
        }

        trips = TripDAO.toDTO(filteredTrips);
        return SUCCESS;
    }

    private void filterYears(List<Trip> allTrips) {
        List<Integer> years = model.getYears();

        if (CollectionUtils.isEmpty(years)) {

            // no year, so nothing in return
            allTrips.clear();
        } else {

            TripDAO.retainsDepartureYears(allTrips, years);
        }
    }

    private void filterVessels(List<Trip> allTrips) throws TopiaException {


        Collection<Vessel> vessels = VesselDAO.getAllVessels(allTrips);

        if (CollectionUtils.isNotEmpty(vessels) && model.isVesselFilter()) {

            List<String> vesselIds = model.getVesselIds();

            if (CollectionUtils.isEmpty(vesselIds)) {

                // no vessel
                vessels.clear();
            } else {

                Collection<Vessel> vesselsToFilter =
                        loadEntities(vesselDAO, vesselIds);
                vessels.retainAll(vesselsToFilter);
            }
        }

        if (CollectionUtils.isNotEmpty(vessels) && model.isFlagFilter()) {

            List<String> flagIds = model.getFlagIds();

            if (CollectionUtils.isEmpty(flagIds)) {

                // no flag, so no vessel
                vessels.clear();
            } else {
                Collection<Country> flags = loadEntities(countryDAO, flagIds);
                VesselDAO.retainsFlagCountries(vessels, flags);
            }
        }

        if (CollectionUtils.isNotEmpty(vessels) && model.isFleetFilter()) {

            List<String> fleetIds = model.getFleetIds();
            if (CollectionUtils.isEmpty(fleetIds)) {

                // no fleet, so no vessel
                vessels.clear();
            } else {
                Collection<Country> fleets = loadEntities(countryDAO, fleetIds);
                VesselDAO.retainsFleetCountries(vessels, fleets);
            }
        }

        if (CollectionUtils.isEmpty(vessels)) {

            // no vessel, so nothing in return
            allTrips.clear();
        } else {

            TripDAO.retainsVessels(allTrips, vessels);
        }
    }

    @Override
    public Integer getRows() {
        return pager.getPageSize();
    }

    @Override
    public Integer getPage() {
        return pager.getPageIndex();
    }

    @Override
    public Long getTotal() {
        return pager.getPagesNumber();
    }

    @Override
    public Long getRecords() {
        return pager.getRecords();
    }

    protected <E extends TopiaEntity> Collection<E> loadEntities(TopiaDAO<E> dao,
                                                                 Iterable<String> ids) throws TopiaException {
        Collection<E> result = Lists.newArrayList();
        for (String id : ids) {
            E e = dao.findByTopiaId(id);
            result.add(e);
        }
        return result;
    }

    protected <E> List<E> getPageFromList(List<E> elements) {

        PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);
        List<E> subList = PagerBeanUtil.getPage(elements, pager.getPageIndex(), pager.getPageSize());
        List<E> result = new ArrayList<E>(subList);

        return result;
    }
}
