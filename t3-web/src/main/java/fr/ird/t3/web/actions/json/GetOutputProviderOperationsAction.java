/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import com.google.common.collect.Maps;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;


/**
 * Json action to obtain list of operation for the a outputProvider given his id.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class GetOutputProviderOperationsAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(GetOutputProviderOperationsAction.class);

    protected String outputProviderId;

    protected Map<String, String> operations;

    public void setOutputProviderId(String outputProviderId) {
        this.outputProviderId = outputProviderId;
    }

    public Map<String, String> getOperations() {
        return operations;
    }

    @Override
    public String execute() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("outputProviderId = " + outputProviderId);
        }

        operations = Maps.newLinkedHashMap();

        for (T3OutputOperation operation :
                getT3OutputService().getOperations(outputProviderId)) {
            operations.put(operation.getId(), operation.getLibelle(getLocale()));
        }

        return SUCCESS;
    }
}
