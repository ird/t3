/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.entities.user.UserT3Database;
import fr.ird.t3.entities.user.UserT3DatabaseImpl;
import org.nuiton.topia.TopiaException;

import java.util.Collection;

/**
 * Operations of {@link UserDatabase}
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class UserT3DatabaseAction extends AbstractUserDatabaseAction<UserT3Database> {

    private static final long serialVersionUID = -2894789101651139566L;

    @Override
    public String getDatabaseType() {
        return T3_DATABASE;
    }

    @Override
    protected UserT3Database getDatabase(String id) throws TopiaException {
        return getUserService().getUserT3Database(id);
    }

    @Override
    protected void create(String userId, UserT3Database database) throws Exception {
        getUserService().addUserT3Database(userId, database);
    }

    @Override
    protected void update(UserT3Database database) throws Exception {
        getUserService().updateUserT3Database(database);
    }

    @Override
    protected void delete(String userId, UserT3Database database) throws Exception {
        getUserService().removeUserT3Database(userId, database.getTopiaId());
    }

    @Override
    public UserT3Database getDatabaseConfiguration() {
        if (databaseConfiguration == null) {
            databaseConfiguration = new UserT3DatabaseImpl();
        }
        return databaseConfiguration;
    }

    @Override
    protected Collection<UserT3Database> getDatabases(T3User user) {
        return user.getUserT3Database();
    }
}
