/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.opensymphony.xwork2.ActionContext;
import fr.ird.t3.T3Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import javax.servlet.ServletContext;
import java.util.Map;
import java.util.Set;

/**
 * Application context loaded once for all when application starts and
 * is destroyed at the end of it.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3ApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3ApplicationContext.class);

    /** Key to store the single instance of the application context */
    private static final String APPLICATION_CONTEXT_PARAMETER =
            "t3ApplicationContext";

    /** Application configuration. */
    protected T3Configuration configuration;

    /** Root context for the internal database. */
    protected TopiaContext internalRootContext;

    /** Set of all loggued user sessions to be close at shutdown time. */
    protected Set<T3Session> t3Sessions;

    public static T3ApplicationContext getT3ApplicationContext(ActionContext actionContext) {
        Map<String, Object> application = actionContext.getApplication();
        T3ApplicationContext result =
                (T3ApplicationContext) application.get(
                        APPLICATION_CONTEXT_PARAMETER);
        return result;
    }

    public static T3ApplicationContext getT3ApplicationContext(ServletContext servletContext) {
        T3ApplicationContext result =
                (T3ApplicationContext) servletContext.getAttribute(
                        APPLICATION_CONTEXT_PARAMETER);
        return result;
    }

    public static void setT3ApplicationContext(ServletContext servletContext,
                                               T3ApplicationContext applicationContext) {
        servletContext.setAttribute(APPLICATION_CONTEXT_PARAMETER,
                                    applicationContext);
    }

    public static void remove3ApplicationContext(ServletContext servletContext) {
        servletContext.removeAttribute(APPLICATION_CONTEXT_PARAMETER);
    }

    public Set<T3Session> getT3Sessions() {
        return t3Sessions;
    }

    public synchronized void registerT3Session(T3Session session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        if (t3Sessions == null) {
            t3Sessions = Sets.newHashSet();
        }
        if (log.isInfoEnabled()) {
            log.info("Register user session for [" +
                     session.getUser().getLogin() + "]");
        }
        t3Sessions.add(session);
    }

    public synchronized void destroyT3Session(T3Session session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        Preconditions.checkNotNull(t3Sessions);
        if (log.isInfoEnabled()) {
            log.info("Destroy user session for [" +
                     session.getUser().getLogin() + "]");
        }
        // remove session from active ones
        t3Sessions.remove(session);
        // close session
        session.close();
    }

    public T3Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(T3Configuration configuration) {
        this.configuration = configuration;
    }

    public TopiaContext getInternalRootContext() {
        return internalRootContext;
    }

    public void setInternalRootContext(TopiaContext internalRootContext) {
        this.internalRootContext = internalRootContext;
    }

}
