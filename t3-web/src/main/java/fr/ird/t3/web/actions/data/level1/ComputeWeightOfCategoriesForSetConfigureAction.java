/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import fr.ird.t3.actions.data.level1.ComputeWeightOfCategoriesForSetAction;
import fr.ird.t3.actions.data.level1.Level1Step;

/**
 * Configure the level 1 action {@link ComputeWeightOfCategoriesForSetAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeWeightOfCategoriesForSetConfigureAction extends AbstractLevel1ConfigureAction {

    private static final long serialVersionUID = 1L;

    public ComputeWeightOfCategoriesForSetConfigureAction() {
        super(Level1Step.COMPUTE_WEIGHT_OF_CATEGORIES_FOR_SET);
    }

}
