/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.data.level0.ComputeRF2Action;
import fr.ird.t3.actions.data.level0.ComputeRF2Configuration;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;

import java.util.Date;
import java.util.Map;

/**
 * Compute Raising factor 2.
 *
 * @author chemit <chemit@codelutin.com>
 * @see ComputeRF2Action
 * @since 1.0
 */
public class ComputeRF2RunAction extends AbstractLevel0RunAction<ComputeRF2Configuration, ComputeRF2Action> {

    private static final long serialVersionUID = 1L;

    @InjectDecoratedBeans(beanType = Harbour.class, filterById = true)
    protected Map<String, String> landingHarbours;

    public ComputeRF2RunAction() {
        super(ComputeRF2Action.class);
    }

    public Map<String, String> getLandingHarbours() {
        return landingHarbours;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(ComputeRF2Action action,
                                                          Exception error,
                                                          Date startDate,
                                                          Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action,
                                                                error,
                                                                startDate,
                                                                endDate
        );
        map.put("landingHarbours", landingHarbours);
        return map;
    }

}
