/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import com.opensymphony.xwork2.conversion.TypeConversionException;
import fr.ird.t3.entities.type.T3Date;
import org.apache.struts2.util.StrutsTypeConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Converter of {@link T3Date}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3DateConverter extends StrutsTypeConverter {

    protected final SimpleDateFormat dateFormat =
            new SimpleDateFormat("MM-yyyy");

    @Override
    public Object convertFromString(Map context,
                                    String[] values,
                                    Class toClass) {
        Object result = null;

        if (T3Date.class.equals(toClass)) {


        }
        if (values != null && values.length != 0) {

            // no value to convert

            try {
                Date date = dateFormat.parse(values[0]);
                result = T3Date.newDate(date);
            } catch (ParseException e) {
                throw new TypeConversionException(
                        "Could not convert " + values[0] + " to T3Date", e);
            }
        }
        return result;
    }

    @Override
    public String convertToString(Map context, Object o) {
        String result = null;
        if (o instanceof T3Date) {
            result = o.toString();
        }
        return result;
    }
}
