/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.reference.Ocean;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * A model to keep in session trip list filter model.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1
 */
public class TripListModel implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient Multimap<Ocean, String> tripIdsByOcean;

    protected List<Integer> years;

    protected List<String> oceanIds;

    protected List<String> fleetIds;

    protected List<String> flagIds;

    protected List<String> vesselIds;

    protected boolean yearFilter;

    protected boolean oceanFilter;

    protected boolean fleetFilter;

    protected boolean flagFilter;

    protected boolean vesselFilter;

    private List<Ocean> oceans;

    public void setYears(List<Integer> years) {
        this.years = years;
    }

    public void setOceanIds(List<String> oceanIds) {
        this.oceanIds = oceanIds;
    }

    public void setFleetIds(List<String> fleetIds) {
        this.fleetIds = fleetIds;
    }

    public void setFlagIds(List<String> flagIds) {
        this.flagIds = flagIds;
    }

    public void setVesselIds(List<String> vesselIds) {
        this.vesselIds = vesselIds;
    }

    public void setYearFilter(boolean yearFilter) {
        this.yearFilter = yearFilter;
    }

    public void setOceanFilter(boolean oceanFilter) {
        this.oceanFilter = oceanFilter;
    }

    public void setFleetFilter(boolean fleetFilter) {
        this.fleetFilter = fleetFilter;
    }

    public void setFlagFilter(boolean flagFilter) {
        this.flagFilter = flagFilter;
    }

    public void setVesselFilter(boolean vesselFilter) {
        this.vesselFilter = vesselFilter;
    }

    public Multimap<Ocean, String> getTripIdsByOcean() {
        return tripIdsByOcean;
    }

    public void setTripIdsByOcean(Multimap<Ocean, String> tripIdsByOcean) {
        this.tripIdsByOcean = tripIdsByOcean;
    }

    public boolean isOceanFilter() {
        return oceanFilter;
    }

    public boolean isYearFilter() {
        return yearFilter;
    }

    public boolean isFleetFilter() {
        return fleetFilter;
    }

    public boolean isFlagFilter() {
        return flagFilter;
    }

    public boolean isVesselFilter() {
        return vesselFilter;
    }

    public List<Integer> getYears() {
        return years;
    }

    public List<String> getOceanIds() {
        return oceanIds;
    }

    public List<String> getFleetIds() {
        return fleetIds;
    }

    public List<String> getFlagIds() {
        return flagIds;
    }

    public List<String> getVesselIds() {
        return vesselIds;
    }

    public long getNbTrips() {
        return tripIdsByOcean == null ? 0 : tripIdsByOcean.values().size();
    }

    public Collection<String> getTripIds(Ocean ocean) {
        return tripIdsByOcean == null ? null : tripIdsByOcean.get(ocean);
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public void setOceans(List<Ocean> oceans) {
        this.oceans = oceans;
    }
}
