/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import fr.ird.t3.entities.user.UserDatabaseDTO;
import fr.ird.t3.entities.user.UserOutputDatabase;
import org.nuiton.topia.TopiaException;

/**
 * Obtains a database configuration from his id.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class GetUserOutputDatabaseAction extends AbstractGetUserDatabaseAction<UserOutputDatabase> {

    private static final long serialVersionUID = 1L;

    /**
     * <strong>Note:</strong> As this is a json action, we need to have at
     * least a property on this very class...
     *
     * @return the selected database.
     */
    @Override
    public UserDatabaseDTO getDatabase() {
        return database;
    }

    @Override
    protected UserOutputDatabase getUserDatabase(String databaseId) throws TopiaException {
        UserOutputDatabase result = getUserService().getUserOutputDatabase(databaseId);
        return result;
    }

}
