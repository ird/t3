/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.validators;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.validator.ValidationException;
import fr.ird.t3.actions.data.level0.Level0Step;
import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.util.decorator.Decorator;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Validate that a level 1 configuration have all his trips ok for a
 * level 1 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level1ConfigurationFieldValidator extends T3BaseFieldValidatorSupport {

    @Override
    protected void validateWhenNotSkip(Object object) throws ValidationException {

        if (getValidatorContext().hasFieldErrors()) {

            // do not continue
            return;
        }

        Level1Configuration configuration =
                (Level1Configuration) getFieldValue("configuration", object);

        Map<String, Trip> tripByTopiaIds = (Map<String, Trip>)
                getFieldValue("tripByTopiaIds", object);

        // check that every selected trip has his level 0 done.
        Set<String> tripIds = configuration.getSampleIdsByTripId().keySet();

        Decorator<Trip> decoratorByType =
                (Decorator<Trip>) getFieldValue("tripDecorator", object);

        Level0Step[] level0Steps = Level0Step.values();
        Multimap<String, Level0Step> m = ArrayListMultimap.create();
        for (String tripId : tripIds) {

            Trip trip = tripByTopiaIds.get(tripId);

            if (!trip.isSamplesOnly()) {

                // only check level0Step done only if Trip#samplesOnly is not setted to true
                String key = decoratorByType.toString(trip);
                for (Level0Step level0Step : level0Steps) {

                    boolean tripState = level0Step.getTripState(trip);
                    if (!tripState) {
                        m.put(key, level0Step);
                    }
                }
            }
        }

        if (!m.isEmpty()) {

            // there is some trips without full level0 done

            for (String tripStr : m.keySet()) {
                Collection<Level0Step> steps = m.get(tripStr);
                addFieldError("matchingTripCount",
                              _("t3.error.missing.level0.steps.for.trip", tripStr, steps));
            }
        }
    }

    @Override
    public String getValidatorType() {
        return "level1Configuration";
    }
}
