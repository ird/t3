/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.data.level0.ComputeRF1Action;
import fr.ird.t3.actions.data.level0.ComputeRF2Configuration;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import org.nuiton.topia.TopiaException;

import java.util.Map;

/**
 * To configure the Compute Raising factor 1 action.
 *
 * @author chemit <chemit@codelutin.com>
 * @see ComputeRF1Action
 * @since 1.0
 */
public class ComputeRF2ConfigureAction extends AbstractLevel0ConfigureAction<ComputeRF2Configuration> {

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = Harbour.class)
    protected transient HarbourDAO harbourDAO;

    @InjectDecoratedBeans(beanType = Harbour.class)
    protected Map<String, String> landingHarbours;

    public ComputeRF2ConfigureAction() {
        super(ComputeRF2Configuration.class);
    }

    @Override
    protected void loadDefaultConfiguration(ComputeRF2Configuration config) throws TopiaException {

        super.loadDefaultConfiguration(config);

        config.setLandingHarbours(sortToList(harbourDAO.findAllUsedInLandingTrip()));
    }

    public Map<String, String> getLandingHarbours() {
        return landingHarbours;
    }

}
