/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.web.filter.TopiaTransactionFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Deliver for each http request an t3 db transaction (if user has it in
 * his session).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3UserTransactionFilter extends TopiaTransactionFilter {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3UserTransactionFilter.class);

    public static final String USER_TRANSACTION = "userTransaction";

    public static TopiaContext getTransaction(ServletRequest request) {
        TopiaContext topiaContext = (TopiaContext)
                request.getAttribute(USER_TRANSACTION);
        return topiaContext;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        setRequestAttributeName(USER_TRANSACTION);
    }

    @Override
    public TopiaContext beginTransaction(ServletRequest request) {

        HttpSession session = ((HttpServletRequest) request).getSession();
        T3Session t3Session = T3Session.getT3Session(session);
        TopiaContext rootContext = t3Session.getRootContext();
        try {
            TopiaContext transaction = rootContext.beginTransaction();
            if (log.isDebugEnabled()) {
                log.debug("Starts a new t3 transaction " + transaction);
            }
            return transaction;
        } catch (TopiaException eee) {
            throw new TopiaRuntimeException("Could not start transaction", eee);
        }
    }

}
