/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.actions.T3ActionContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ObjectUtil;

/**
 * Abstract run action.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractConfigureAction<C extends T3ActionConfiguration> extends T3ActionSupport implements Preparable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractConfigureAction.class);

    protected transient T3ActionContext<C> t3ActionContext;

    protected transient C configuration;

    /** To confirm the configuration. */
    protected boolean confirm;

    protected final Class<C> configurationType;
//
//    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("mm-yyyy");

    public AbstractConfigureAction(Class<C> configurationType) {
        this.configurationType = configurationType;
    }

    public final T3ActionContext<C> getT3ActionContext() {
        return t3ActionContext;
    }

    public final C getConfiguration() {
        if (configuration == null) {

            // try in session
            configuration = getConfigurationFromSession();

            if (configuration == null) {

                // really a new configuration
                configuration = ObjectUtil.newInstance(configurationType);
            }
        }
        return configuration;
    }

    public final boolean isConfigurationInSession() {
        boolean b = getConfigurationFromSession() != null;
        return b;
    }

    public boolean isConfirm() {
        return confirm;
    }

    protected final T3ActionContext<C> prepareActionContext() {

        t3ActionContext = getServiceFactory().newT3ActionContext(
                getConfiguration(), getServiceContext());

        if (log.isInfoEnabled()) {
            log.info("Created action context " + t3ActionContext);
        }
        getT3Session().setActionContext(t3ActionContext);
        return t3ActionContext;
    }

    protected final C getConfigurationFromSession() {
        return getActionConfiguration(configurationType);
    }

    protected final boolean isConfigurationValid() {
        return configuration != null && !hasFieldErrors();
    }

    protected final void storeActionConfiguration(C configuration) {
        getT3Session().setActionConfiguration(configuration);
    }

    protected final void removeConfigurationFromSession() {
        getT3Session().removeActionConfiguration(configurationType);
    }

    protected final void removeActionContextFromSession() {
        getT3Session().removeActionContext();
    }

    protected void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }
}
