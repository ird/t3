/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.data.level0.AbstractLevel0Configuration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractRunAction;

import java.util.Date;
import java.util.Map;

/**
 * Abstract level 0 run action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractLevel0RunAction<C extends AbstractLevel0Configuration, A extends T3Action<C>> extends AbstractRunAction<C, A> {

    private static final long serialVersionUID = 1L;

    @InjectDecoratedBeans(beanType = VesselSimpleType.class, filterById = true)
    private Map<String, String> vesselSimpleTypes;

    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> fleets;

    protected AbstractLevel0RunAction(Class<A> actionType) {
        super(actionType);
    }

    public Map<String, String> getVesselSimpleTypes() {
        return vesselSimpleTypes;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(A action,
                                                          Exception error,
                                                          Date startDate,
                                                          Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action,
                                                                error,
                                                                startDate,
                                                                endDate
        );
        map.put("vesselSimpleTypes", vesselSimpleTypes);
        map.put("fleets", fleets);
        return map;
    }
}
