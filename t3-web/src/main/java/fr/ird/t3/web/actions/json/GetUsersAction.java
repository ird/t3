/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserDAO;
import fr.ird.t3.entities.user.T3UserDTO;
import fr.ird.t3.web.actions.T3ActionSupport;

import java.util.List;

/**
 * Obtains all users of the t3 internal database.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class GetUsersAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    protected List<T3UserDTO> users;

    public List<T3UserDTO> getUsers() {
        return users;
    }

    @Override
    public String execute() throws Exception {

        List<T3User> t3Users = getUserService().getUsers();

        users = T3UserDAO.toDTO(t3Users);

        return SUCCESS;
    }

}
