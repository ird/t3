/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserImpl;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Action to manage user (create - update - change password,...)
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class UserAction extends T3ActionSupport implements Preparable {

    protected static final Log log = LogFactory.getLog(UserAction.class);

    private static final long serialVersionUID = 1L;

    public static final String BACK_TO_LIST = "backToList";

    protected T3User user;

    protected String userEditAction;

    public String getUserEditAction() {
        return userEditAction;
    }

    public void setUserEditAction(String userEditAction) {
        this.userEditAction = userEditAction;
    }

    @Override
    public void prepare() throws Exception {

        String userId = getUser().getTopiaId();
        if (!StringUtils.isEmpty(userId)) {

            // load user
            user = getUserService().getUserById(userId);
        }
    }

    public String doCreate() throws Exception {
        T3User t3User = getUser();
        String userLogin = t3User.getLogin();

        if (log.isInfoEnabled()) {
            log.info("will create user  " + userLogin);
        }

        // create user
        getUserService().createUser(t3User);
        return BACK_TO_LIST;
    }

    public String doUpdate() throws Exception {
        T3User t3User = getUser();

        if (log.isInfoEnabled()) {
            log.info("will update user  " + t3User.getLogin());
        }

        // update user
        getUserService().updateUser(t3User);
        return SUCCESS;
    }

    public String doDelete() throws Exception {
        T3User t3User = getUser();
        if (log.isInfoEnabled()) {
            log.info("will delete user " + t3User.getLogin());
        }
        getUserService().deleteUser(t3User.getTopiaId());
        return BACK_TO_LIST;
    }

    @Override
    public void validate() {

        EditActionEnum action = getEditActionEnum();

        log.info("Edit action : " + action);

        if (action != null) {

            // validate only if a action is setted

            T3User t3User = getUser();
            String userLogin = t3User.getLogin();

            switch (action) {

                case CREATE:

                    // login + password required

                    if (StringUtils.isEmpty(userLogin)) {

                        // empty user login
                        addFieldError("user.login",
                                      _("t3.error.required.login"));
                    } else {

                        // check login not already used
                        T3User login;
                        try {
                            login = getUserService().getUserByLogin(userLogin);
                        } catch (Exception e) {

                            // could not get user
                            throw new IllegalStateException(
                                    "Could not obtain user " + userLogin, e);
                        }
                        if (login != null) {
                            addFieldError("user.login",
                                          _("t3.error.login.already.used"));
                        }
                    }

                    String userPassword = t3User.getPassword();
                    if (StringUtils.isEmpty(userPassword)) {

                        // empty user password
                        addFieldError("user.password",
                                      _("t3.error.required.password"));
                    }

                    break;
                case EDIT:

                    // at the moment nothing to validate
                    break;
                case DELETE:

                    // check user has no profile associated
//                ParametersProfileService service = getParametersProfileService();
//                List<ParametersProfile> profiles = service.getParametersProfilesByUser(userLogin);
//                log.info("profiles form user " + userLogin + " : " + profiles.size());
//                if (!CollectionUtils.isEmpty(profiles)) {
//                    addFieldError("user.login", _("t3.error.user.associated.with.profiles"));
//                }
                default:
                    // nothing to validate
            }
        }

    }

    public T3User getUser() {
        if (user == null) {
            user = new T3UserImpl();
        }
        return user;
    }

    protected EditActionEnum getEditActionEnum() {
        EditActionEnum result = null;
        if (userEditAction != null) {
            result = EditActionEnum.valueOf(userEditAction.toUpperCase());
        }
        return result;
    }
}
