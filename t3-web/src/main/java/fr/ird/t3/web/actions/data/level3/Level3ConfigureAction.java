/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * To configure level 3 action, mainly print the level 3 configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level3ConfigureAction extends AbstractConfigureAction<Level3Configuration> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(Level3ConfigureAction.class);

    private static final long serialVersionUID = 1L;

    private final Map<String, String> timeSteps = createTimeSteps();

    /** all fleet Countries. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true, filterBySingleId = true)
    protected Map<String, String> catchFleets;

    /** all zoneTypes. */
    @InjectDecoratedBeans(beanType = ZoneStratumAwareMeta.class)
    protected Map<String, String> zoneTypes;

    /** all zoneVersions. */
    @InjectDecoratedBeans(beanType = ZoneVersion.class)
    protected Map<String, String> zoneVersions;

    /** all oceans. */
    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true, filterBySingleId = true)
    protected Map<String, String> oceans;

    /** all species. */
    @InjectDecoratedBeans(beanType = Species.class, filterById = true, pathIds = "speciesIds")
    protected Map<String, String> species;

    /** all sample fleets. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    protected Map<String, String> sampleFleets;

    /** all sample flags. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    protected Map<String, String> sampleFlags;

    protected Map<String, String> useSamplesOrNot;

    protected Map<String, String> useWeightCategoriesOrNot;

    public Level3ConfigureAction() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Prepare with configuration " + getConfiguration());
        }

        useSamplesOrNot = createLevel3UseSamplesOrNotMap();
        useWeightCategoriesOrNot = createLevel3UseWeightCategoriesOrNotMap();

        // load configuration
        getConfiguration();

        // load decorated beans
        injectOnly(InjectDecoratedBeans.class);
    }

    @Override
    public String execute() throws Exception {

        prepareActionContext();

        return SUCCESS;
    }

    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    public Map<String, String> getCatchFleets() {
        return catchFleets;
    }

    public Map<String, String> getZoneTypes() {
        return zoneTypes;
    }

    public Map<String, String> getZoneVersions() {
        return zoneVersions;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getSpecies() {
        return species;
    }

    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    public Map<String, StratumMinimumSampleCount> getStratumMinimumSampleCount() {
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount =
                getConfiguration().getStratumMinimumSampleCount();
        if (stratumMinimumSampleCount == null) {
            stratumMinimumSampleCount = Maps.newTreeMap();
            getConfiguration().setStratumMinimumSampleCount(
                    stratumMinimumSampleCount);
        }
        return stratumMinimumSampleCount;
    }

    public StratumMinimumSampleCount getStratumMinimumSampleCount(String specie) {
        return getStratumMinimumSampleCount().get(specie);
    }

    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    public Map<String, String> getUseWeightCategoriesOrNot() {
        return useWeightCategoriesOrNot;
    }
}
