/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.data.level0.ComputeWellPlanWeightCategoriesProportionsAction;
import fr.ird.t3.actions.data.level0.ComputeWellPlanWeightCategoriesProportionsConfiguration;

/**
 * Computes well plan weight categories proportions.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class ComputeWellPlanWeightCategoriesProportionsRunAction extends AbstractLevel0RunAction<ComputeWellPlanWeightCategoriesProportionsConfiguration, ComputeWellPlanWeightCategoriesProportionsAction> {

    private static final long serialVersionUID = 1L;

    public ComputeWellPlanWeightCategoriesProportionsRunAction() {
        super(ComputeWellPlanWeightCategoriesProportionsAction.class);
    }
}
