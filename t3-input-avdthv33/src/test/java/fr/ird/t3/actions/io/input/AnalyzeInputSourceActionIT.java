/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.FakeT3ServiceContext;
import fr.ird.t3.actions.MSAccessTestConfiguration;
import fr.ird.t3.actions.T3AVDTHV33Test;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.io.input.avdth.v33.T3InputProviderAvdth33;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Set;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AnalyzeInputSourceActionIT implements T3AVDTHV33Test {

    /** Logger */
    private static final Log log = LogFactory.getLog(AnalyzeInputSourceActionIT.class);

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected static MSAccessTestConfiguration msConfig =
            new MSAccessTestConfiguration("testExecute_([^_]*)_(.*)");

    protected static boolean useWells;

    protected BufferedWriter outputWriter;

    private File target;

    private T3InputProvider inputProvider;

    @BeforeClass
    public static void beforeClass() throws Exception {

        boolean b = msConfig.beforeClass();

        if (!b) {
            Assume.assumeTrue(false);
        }

        String s = System.getenv("useWells");
        if (!StringUtils.isEmpty(s)) {
            useWells = Boolean.valueOf(s);
        } else {
            useWells = false;
        }
    }

    @Before
    public void setUp() throws Exception {

        boolean initOk = serviceContext.initIsOk();
        Assume.assumeTrue("Could not init db", initOk);

        boolean doIt = msConfig.setup(serviceContext.getTestName());

        if (doIt) {

            String dbName = msConfig.dbName;

            if (log.isDebugEnabled()) {
                log.debug("Do test for db " + dbName);
            }

            File outputFile = new File(serviceContext.getTestDir(), "result.txt");

            if (log.isInfoEnabled()) {
                log.info("Will save result in file : " + outputFile);
            }

            outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

            File workingDirectory =
                    serviceContext.getApplicationConfiguration().getTreatmentWorkingDirectory("yo", true);

            // push in treatment directory the base to import

            target = new File(workingDirectory, dbName);
            if (log.isDebugEnabled()) {
                log.debug("Will copy msaccess from " + msConfig.accessFile + " to " + target);
            }
            FileUtils.copyFile(msConfig.accessFile, target);

            inputProvider = serviceContext.newService(T3InputService.class).getProvider(T3InputProviderAvdth33.ID);
        }

    }

    @After
    public void tearDown() throws Exception {

        serviceContext.close();

        if (outputWriter != null) {

            outputWriter.flush();
            outputWriter.close();

        }
    }

    public void testExecute(int nbSafe,
                            int nbUnsafe,
                            int nbSafeWithoutwell,
                            int nbUnsafeWithoutWell) throws Exception {
        testExecute(
                nbSafe,
                nbUnsafe,
                nbSafeWithoutwell,
                nbUnsafeWithoutWell,
                false,
                false,
                false
        );
    }

    public void testExecute(int nbSafe,
                            int nbUnsafe,
                            int nbSafeWithoutwell,
                            int nbUnsafeWithoutWell,
                            boolean sampleOnly,
                            boolean canCreateVessel,
                            boolean createVirtualVessel) throws Exception {

        if (msConfig.doTest(serviceContext.getTestName())) {

            T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();

            AnalyzeInputSourceConfiguration actionConfiguration = AnalyzeInputSourceConfiguration.newConfiguration(
                    inputProvider, target, useWells, sampleOnly, canCreateVessel, createVirtualVessel
            );
            T3ActionContext<AnalyzeInputSourceConfiguration> context =
                    serviceFactory.newT3ActionContext(actionConfiguration, serviceContext);


            outputWriter.write("----------------------------------------------------\n");
            outputWriter.write(msConfig.accessFile + "\n");
            T3Action<AnalyzeInputSourceConfiguration> action;

            action = serviceFactory.newT3Action(AnalyzeInputSourceAction.class, context);

            Assert.assertNotNull(action);

            action.run();

            Set<Trip> safeTrips = action.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_SAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(safeTrips);
            outputWriter.write("found " + safeTrips.size() + "   safe trip(s).\n");
            Set<Trip> unsafeTrips = action.getResultAsSet(
                    AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS,
                    Trip.class
            );
            Assert.assertNotNull(unsafeTrips);
            outputWriter.write("found " + unsafeTrips.size() + " unsafe trip(s).\n");
            List<String> messages;

            if (log.isWarnEnabled()) {
                messages = action.getWarnMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write("[WARNING] " + message + "\n");
                    }
                }
            }

            if (log.isErrorEnabled()) {
                messages = action.getErrorMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write("[ERROR] " + message + "\n");
                    }
                }
            }

            if (log.isInfoEnabled()) {
                log.info("\n[" + msConfig.dbName + "] safe : " + safeTrips.size() +
                         " - unsafe : " + unsafeTrips.size() + "\n");
            }

            if (useWells) {
                Assert.assertEquals(nbSafe, safeTrips.size());
                Assert.assertEquals(nbUnsafe, unsafeTrips.size());
            } else {
                Assert.assertEquals(nbSafeWithoutwell, safeTrips.size());
                Assert.assertEquals(nbUnsafeWithoutWell, unsafeTrips.size());
            }

        }


    }

    public void testExecute(int nbSafe, int nbUnsafe) throws Exception {
        testExecute(nbSafe, nbUnsafe, nbSafe, nbUnsafe);
    }

    @Test
    @Override
    public void testExecute_OI_2000() throws Exception {
        testExecute(155, 65, 220, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2001() throws Exception {
        testExecute(70, 118, 188, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2002() throws Exception {
        testExecute(103, 125, 227, 1);
    }

    @Test
    @Override
    public void testExecute_OI_2003() throws Exception {
        testExecute(83, 136, 219, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2004() throws Exception {
        testExecute(78, 118, 196, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2005() throws Exception {
        testExecute(78, 110, 186, 2);
    }

    @Test
    @Override
    public void testExecute_OI_2006() throws Exception {
        testExecute(181, 12, 188, 5);
    }

    @Test
    @Override
    public void testExecute_OI_2007() throws Exception {
        testExecute(155, 12, 162, 5);
    }

    @Test
    @Override
    public void testExecute_OI_2008() throws Exception {
        testExecute(146, 34, 174, 6);
    }

    @Test
    @Override
    public void testExecute_OI_2009() throws Exception {
        testExecute(122, 19, 141, 0);
    }

    @Test
    @Override
    public void testExecute_OI_2011() throws Exception {
        testExecute(141, 0, 126, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2000() throws Exception {
        testExecute(318, 36, 354, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2001() throws Exception {
        testExecute(298, 63, 361, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2002() throws Exception {
        testExecute(184, 133, 317, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2003() throws Exception {
        testExecute(218, 157, 375, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2004() throws Exception {
        testExecute(121, 61, 182, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2005() throws Exception {
        testExecute(93, 31, 124, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2006() throws Exception {
        testExecute(70, 27, 97, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2007() throws Exception {
        testExecute(53, 27, 80, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2008() throws Exception {
        testExecute(46, 14, 60, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2009() throws Exception {
        testExecute(64, 21, 85, 0);
    }

    @Test
    @Override
    public void testExecute_ATL_2010() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_ESATL_2006() throws Exception {
        testExecute(70, 27, 97, 0, true, true, true);
    }

    @Test
    @Override
    public void testExecute_ESATL_2007() throws Exception {
        testExecute(273, 27, 238, 0, true, true, true);
    }

    @Test
    @Override
    public void testExecute_ESATL_2008() throws Exception {
        testExecute(46, 14, 60, 0, true, true, true);
    }

    @Test
    @Override
    public void testExecute_ESATL_2009() throws Exception {
        testExecute(64, 21, 85, 0, true, true, true);
    }

    @Test
    @Override
    public void testExecute_ESATL_2010() throws Exception {
        testExecute(75, 13, 80, 8, true, true, true);
    }
}
