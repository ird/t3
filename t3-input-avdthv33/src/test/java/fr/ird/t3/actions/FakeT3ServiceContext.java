/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import fr.ird.t3.T3Configuration;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.T3SqlScriptsImporter;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaRootContextFactory;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.mockito.Mockito;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.util.FileUtil;
import org.nuiton.util.Version;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class FakeT3ServiceContext extends TestWatcher implements T3ServiceContext, Closeable {

    /** Le chemin où trouver les bases access. */
    public static final String DB_PATH = "src:test:access";

    public static final String DB_PATTERN = "%s_%s_V33.mdb";

    private static final Log log = LogFactory.getLog(FakeT3ServiceContext.class);

    protected final T3ServiceFactory serviceFactory = new T3ServiceFactory();

    protected TopiaContext rootContext;

    protected TopiaContext transaction;

    protected File testDir;

    protected T3Configuration applicationConfiguration;

    private Locale locale;

    private String testName;

    private boolean initOk;

    @Override
    protected void starting(Description description) {
        super.starting(description);
        testName = description.getMethodName();

        testDir = T3IOUtil.getTestSpecificDirectory(description.getTestClass(),
                description.getMethodName());
        if (log.isInfoEnabled()) {
            log.info("Test dir = " + testDir);
        }

        File treatmentDirectory = new File(testDir, "treatment");
        try {
            FileUtil.createDirectoryIfNecessary(treatmentDirectory);
        } catch (IOException e) {
            throw new IllegalStateException("Could not create directory " + treatmentDirectory, e);
        }

        T3Configuration realConfiguration = new T3Configuration() {
            @Override
            public void init() {
                parse();
            }
        };
        realConfiguration.init();
        Version t3DataVersion = realConfiguration.getT3DataVersion();
        applicationConfiguration = Mockito.mock(T3Configuration.class);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory()).thenReturn(treatmentDirectory);
        Mockito.when(applicationConfiguration.getT3DataVersion()).thenReturn(t3DataVersion);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory(Mockito.anyString(), Mockito.anyBoolean())).thenCallRealMethod();

        try {

            // where to put h2 database
            File dbDirectory = new File(testDir, "h2-database");

            rootContext = new T3TopiaRootContextFactory().newEmbeddedDb(dbDirectory);

            // inject in db the referentiel

            T3SqlScriptsImporter.importReferential(new File(""), this, T3SqlScriptsImporter.SKIP_SPECIES_LENGTH_STEP_FILE);

            initOk = true;
        } catch (Exception e) {
            initOk = false;
            if (log.isErrorEnabled()) {
                log.error("Could not start db", e);
            }
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected void finished(Description description) {
        super.finished(description);

        if (!rootContext.isClosed()) {
            try {
                rootContext.closeContext();
            } catch (TopiaException e) {
                throw new TopiaRuntimeException(e);
            }
        }
    }

    /** May be used in test to get a fresh transaction. */
    @Override
    public TopiaContext getTransaction() {
        if (transaction == null) {
            try {
                transaction = rootContext.beginTransaction();
            } catch (TopiaException e) {
                throw new TopiaRuntimeException(e);
            }
        }
        return transaction;
    }

    @Override
    public TopiaContext getInternalTransaction() {
        return null;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public T3Configuration getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setInternalTransaction(TopiaContext transaction) {
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        this.transaction = transaction;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }

    public File getTestDir() {
        return testDir;
    }

    public String getTestName() {
        return testName;
    }

    @Override
    public void close() throws IOException {

        if (transaction != null) {
            T3EntityHelper.releaseRootContext(((TopiaContextImplementor) transaction).getRootContext());
        }
    }

    public boolean initIsOk() {
        return initOk;
    }
}
