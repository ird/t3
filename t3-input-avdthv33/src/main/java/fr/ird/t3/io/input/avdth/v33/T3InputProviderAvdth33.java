/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v33;

import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.T3InputProvider;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.io.File;

/**
 * Provides input for avdth.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3InputProviderAvdth33 implements T3InputProvider {

    private static final long serialVersionUID = 1L;

    public static final Version VERSION = VersionUtil.valueOf("3.3");

    public static final String NAME = "AVDTH";

    public static final String ID = NAME + "__" + VERSION;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getName() {
        return "AVDTH";
    }

    @Override
    public Version getVersion() {
        return VERSION;
    }

    @Override
    public String getInputType() {
        return "MS-ACCESS";
    }

    @Override
    public String getLibelle() {
        return getName() + " - v." + getVersion() + " (" + getInputType() + ")";
    }

    @Override
    public T3Input newInstance(File inputFile) {
        return new T3InputAvdth33();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof T3InputProvider)) {
            return false;
        }

        T3InputProvider that = (T3InputProvider) o;

        return ID.equals(that.getId());
    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }
}
