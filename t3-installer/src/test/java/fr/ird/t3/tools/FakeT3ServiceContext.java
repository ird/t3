/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools;

import fr.ird.t3.T3Configuration;
import fr.ird.t3.T3ConfigurationOption;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.T3SqlScriptsImporter;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaRootContextFactory;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;

import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

/**
 * Service context for tests.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class FakeT3ServiceContext extends TestWatcher implements T3ServiceContext {

    private static final Log log =
            LogFactory.getLog(FakeT3ServiceContext.class);

    /** A time-stamp, allow to make multiple build and keep the tests data. */
    protected static final String TIMESTAMP = String.valueOf(System.nanoTime());

    protected T3ServiceFactory serviceFactory;

    protected TopiaContext rootContext;

    protected TopiaContext transaction;

    protected Date fakeCurrentTime;

    protected File testDir;

    protected T3Configuration applicationConfiguration;

    private Locale locale;
    private boolean initOk;

    @Override
    protected void starting(Description description) {
        super.starting(description);

        testDir = T3IOUtil.getTestSpecificDirectory(
                description.getTestClass(),
                description.getMethodName());

        if (log.isInfoEnabled()) {
            log.info("Test dir = " + testDir);
        }

        Properties defaultProps = new Properties();
        defaultProps.put(T3ConfigurationOption.DATA_DIRECTORY.getKey(), testDir);
        applicationConfiguration = new T3Configuration(defaultProps) {
            @Override
            public void init() {
                parse();
            }
        };
        applicationConfiguration.init();
        try {

            // where to put h2 database
            File dbDirectory = new File(testDir, "h2-database");

            rootContext = new T3TopiaRootContextFactory().newEmbeddedDb(dbDirectory);

            // inject in db the referentiel
            T3SqlScriptsImporter.importReferential(new File(""), this, T3SqlScriptsImporter.SKIP_SPECIES_LENGTH_STEP_FILE);
            initOk = true;
        } catch (Exception e) {
            initOk = false;
            throw new IllegalStateException("Could not start db", e);
        }
    }

    @Override
    protected void finished(Description description) {
        super.finished(description);

        T3EntityHelper.releaseRootContext(rootContext);
    }

    /** May be used in test to get a fresh transaction. */
    @Override
    public TopiaContext getTransaction() {
        if (transaction == null) {
            try {
                transaction = newTransaction();
            } catch (TopiaException e) {
                throw new TopiaRuntimeException(e);
            }
        }
        return transaction;
    }

    @Override
    public TopiaContext getInternalTransaction() {
        return null;
    }

    public TopiaContext newTransaction() throws TopiaException {
        return rootContext.beginTransaction();
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public T3Configuration getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setInternalTransaction(TopiaContext transaction) {
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        this.transaction = transaction;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }

    public void setServiceFactory(T3ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public File getTestDir() {
        return testDir;
    }

    public boolean isInitOk() {
        return initOk;
    }

}
