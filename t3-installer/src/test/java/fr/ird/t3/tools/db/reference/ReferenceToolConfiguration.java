/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Configuration to execute a reference tool test.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ReferenceToolConfiguration<E extends TopiaEntity, T extends AbstractReferenceImporter<E>> {

    protected final String resourcePath;

    protected final String inputFileName;

    protected final Class<E> entityClass;

    protected final Class<T> toolClass;

    protected final int expectedCount;

    protected final boolean removeReferences;

    public ReferenceToolConfiguration(Class<E> entityClass,
                                      Class<T> toolClass,
                                      String resourcePath,
                                      String inputFileName,
                                      boolean removeReferences,
                                      int expectedCount) {
        this.entityClass = entityClass;
        this.toolClass = toolClass;
        this.resourcePath = resourcePath;
        this.inputFileName = inputFileName;
        this.removeReferences = removeReferences;
        this.expectedCount = expectedCount;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    public Class<T> getToolClass() {
        return toolClass;
    }

    public int getExpectedCount() {
        return expectedCount;
    }

    public boolean isRemoveReferences() {
        return removeReferences;
    }
}
