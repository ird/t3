/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.reference.Species;

/**
 * To test the tool {@link SpeciesExtraDataImporter}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SpeciesExtraDataImporterIT extends AbstractReferenceImporterIT<Species, SpeciesExtraDataImporter> {

    @Override
    protected ReferenceToolConfiguration<Species, SpeciesExtraDataImporter> newConfiguration() {
        return new ReferenceToolConfiguration<Species, SpeciesExtraDataImporter>(
                Species.class,
                SpeciesExtraDataImporter.class,
                "/reference/speciesExtraData.csv",
                "speciesExtraData.csv",
                false,
                81
        );
    }

}

