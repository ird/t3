/*
 * #%L
 * T3 :: Business
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.reference.zone.ZoneFAO;
import fr.ird.t3.entities.type.T3Date;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Test tools {@link ZoneFAOImporter}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
@RunWith(value = Parameterized.class)
public class ZoneFAOImporterIT extends AbstractZoneImporterIt<ZoneFAO, ZoneFAOImporter> {

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {

        Collection<Object[]> result = new ArrayList<Object[]>();
        result.add(new Object[]{"/zone/zones_fao.csv.zip", "zones-fao.sql", "0", "Zone FAO 2011", T3Date.newDate(1, 2011).toBeginDate(), null, 19});
        return result;
    }

    public ZoneFAOImporterIT(String dataResourcePath,
                             String outputName,
                             String versionId,
                             String versionLibelle,
                             Date versionStartDate,
                             Date versionEndDate,
                             int expected) {
        super(ZoneFAO.class,
              dataResourcePath,
              outputName,
              versionId,
              versionLibelle,
              versionStartDate,
              versionEndDate,
              expected
        );
    }

    @Override
    protected ZoneFAOImporter createTool() {
        ZoneFAOImporter tool = new ZoneFAOImporter(
                versionId,
                versionLibelle,
                versionStartDate,
                versionEndDate
        );
        return tool;
    }

}
