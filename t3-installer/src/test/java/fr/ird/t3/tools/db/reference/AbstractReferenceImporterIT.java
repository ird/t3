/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3ScriptHelper;
import fr.ird.t3.tools.AbstracToolTest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.File;
import java.util.List;

/**
 * abstract test for reference importel tools.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractReferenceImporterIT<E extends TopiaEntity, T extends AbstractReferenceImporter<E>> extends AbstracToolTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractReferenceImporterIT.class);

    // csv data file to use for the test
    protected File inputFile;

    protected ReferenceToolConfiguration<E, T> configuration;

    protected abstract ReferenceToolConfiguration<E, T> newConfiguration();

    @Before
    public void setUp() throws Exception {

        super.setUp();

        if (log.isDebugEnabled()) {
            log.debug("Do test for db " + serviceContext.getTestDir());
        }

        configuration = newConfiguration();

        inputFile = T3IOUtil.copyResourceToFile(configuration.getResourcePath(),
                                                serviceContext.getTestDir(),
                                                configuration.getInputFileName()
        );
    }

    @Test
    public void runTool() throws Exception {

        // instanciate a new tool
        T tool = serviceContext.newService(configuration.getToolClass());

        Class<E> entityClass = configuration.getEntityClass();

        TopiaContext transaction = serviceContext.getTransaction();
        if (configuration.isRemoveReferences()) {

            // remove exisiting references
            deleteEntities(transaction, entityClass);
        }

        // launch tools
        tool.execute(inputFile);

        // count result
        assertCountEntities(transaction, entityClass,
                            configuration.getExpectedCount());

        // export referentiel
        File outputFile = new File(inputFile.getParentFile(),
                                   "newReferentiel.sql");
        T3ScriptHelper.exportDatabase(transaction, outputFile);
    }

    public <T extends TopiaEntity> void deleteEntities(TopiaContext tx,
                                                       Class<T> entityType) throws TopiaException {
        TopiaContext tx2;
        tx2 = tx.beginTransaction();
        try {

            TopiaDAO<T> dao = T3DAOHelper.<T, TopiaDAO<T>>getDAO(tx2,
                                                                 entityType);

            List<T> all = dao.findAll();

            if (CollectionUtils.isNotEmpty(all)) {
                if (log.isInfoEnabled()) {
                    log.info("Will delete " + all.size() +
                             " entities of type " + entityType.getName());
                }
                for (T t : all) {
                    dao.delete(t);
                }
            }
            tx2.commitTransaction();
        } finally {
            tx2.closeContext();
        }
    }

}
