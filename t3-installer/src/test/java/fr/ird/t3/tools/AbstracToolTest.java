/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools;

import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.services.T3ServiceFactory;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class AbstracToolTest {

    @Rule
    public FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    @Before
    public void setUp() throws Exception {

        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);

        serviceContext.setServiceFactory(new T3ServiceFactory());
    }

    public <T extends TopiaEntity> void assertCountEntities(TopiaContext tx,
                                                            Class<T> entityType,
                                                            int expectedCount) throws TopiaException {
        TopiaContext tx2;
        tx2 = tx.beginTransaction();
        try {
            TopiaDAO<T> dao = T3DAOHelper.<T, TopiaDAO<T>>getDAO(tx2,
                                                                 entityType);

            long count = dao.count();
            org.junit.Assert.assertEquals("Should have found " + expectedCount +
                                          " entities of type " + entityType.getName() +
                                          ", but found " + count, expectedCount, count);
        } finally {
            tx2.closeContext();
        }
    }

}
