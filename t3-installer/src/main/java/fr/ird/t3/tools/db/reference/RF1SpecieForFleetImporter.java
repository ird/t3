/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.RF1SpeciesForFleet;
import fr.ird.t3.entities.reference.RF1SpeciesForFleetDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A tool to import content of table {@link RF1SpeciesForFleet} from a incoming
 * file named {@code rf1Species.txt}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class RF1SpecieForFleetImporter extends AbstractReferenceImporter<RF1SpeciesForFleet> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RF1SpecieForFleetImporter.class);

    @InjectDAO(entityType = RF1SpeciesForFleet.class)
    protected RF1SpeciesForFleetDAO dao;

    @InjectFromDAO(entityType = Country.class)
    protected List<Country> countries;

    @InjectFromDAO(entityType = Species.class)
    protected List<Species> species;

    protected Map<Integer, Country> countryByCode;

    protected Map<Integer, Species> specieByCode;

    @Override
    protected void before() throws TopiaException {

        if (CollectionUtils.isEmpty(countries)) {
            throw new IllegalStateException("No country found in db.");
        }

        if (CollectionUtils.isEmpty(species)) {
            throw new IllegalStateException("No species found in db.");
        }

        long nb = dao.count();

        if (nb > 0) {
            throw new IllegalStateException(
                    "Some RF1SpeciesForFleet found in db.");
        }

        countryByCode = T3EntityHelper.splitBycode(countries);
        specieByCode = T3EntityHelper.splitBycode(species);
    }

    @Override
    protected List<RF1SpeciesForFleet> loadFile(File inputFile) throws TopiaException, IOException {

        List<RF1SpeciesForFleet> result = new ArrayList<RF1SpeciesForFleet>();

        LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
        try {

            String line;

            while ((line = reader.readLine()) != null) {

                if (line.trim().startsWith("#")) {
                    // this is a comment, do not treate it
                    continue;
                }
                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.split(":");

                // always check we have 2 cells
                if (cells.length != 2) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have 2 cells but had here " +
                            cells.length);
                }

                loadLine(result,
                         lineNumber,
                         cells,
                         countryByCode,
                         specieByCode,
                         dao
                );
            }
        } finally {
            reader.close();
        }

        return result;
    }

    protected void loadLine(List<RF1SpeciesForFleet> result,
                            int lineNumber,
                            String[] cells,
                            Map<Integer, Country> countryByCode,
                            Map<Integer, Species> specieByCode,
                            RF1SpeciesForFleetDAO dao) throws TopiaException {

        // first cell is ocean code
        int countryCode = convertToInt(cells, lineNumber, 0);

        Country country = getCountry(countryByCode, countryCode, lineNumber);

        // second cell is species code
        int specieCode = convertToInt(cells, lineNumber, 1);
        Species species = getSpecie(specieByCode, specieCode, lineNumber);

        RF1SpeciesForFleet rf1SpeciesForFleet = dao.create(
                RF1SpeciesForFleet.PROPERTY_COUNTRY, country,
                RF1SpeciesForFleet.PROPERTY_SPECIES, species
        );
        result.add(rf1SpeciesForFleet);

    }

}
