/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.zone.ZoneCWP;
import fr.ird.t3.entities.reference.zone.ZoneCWPDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;

/**
 * Tool to import zone cwp from a csv file and produce an output sql file with
 * update statement to inject postgis data.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ZoneCWPImporter extends AbstractZoneImporter<ZoneCWP, ZoneCWPDAO> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneCWPImporter.class);

    public static final String INSERT_LINE_PATTERN =
            "INSERT INTO ZONECWP(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, " +
            "VERSIONID, VERSIONLIBELLE, VERSIONSTARTDATE,  VERSIONENDDATE, " +
            "QUADRANT, SQUARELATITUDE, SQUARELONGITUDE, " +
            "MINLATITUDE, MAXLATITUDE, MINLONGITUDE, MAXLONGITUDE, " +
            "THE_GEOM) VALUES ('%s', %s, %s," +
            " '%s', '%s', %s, %s, " +
            "%s, %s, %s, " +
            "%s, %s, %s, %s," +
            "'%s');";

    public ZoneCWPImporter(String versionId,
                           String versionLibelle,
                           Date versionStartDate,
                           Date versionEndDate) {
        super(versionId,
              versionLibelle,
              versionStartDate,
              versionEndDate,
              8);
    }

    @Override
    protected ZoneCWPDAO getDAO() throws TopiaException {
        return T3DAOHelper.getZoneCWPDAO(getTransaction());
    }

    @Override
    protected String loadLine(List<ZoneCWP> result,
                              int lineNumber,
                              String[] cells) throws TopiaException {

        // quadrant
        int quadrant = convertToInt(cells, lineNumber, 0);

        // squareLatitude
        int squareLatitude = convertToInt(cells, lineNumber, 1);

        // squareLongitude
        int squareLongitude = convertToInt(cells, lineNumber, 2);

        // minLatitude
        int minLatitude = convertToInt(cells, lineNumber, 3);

        // maxLatitude
        int maxLatitude = convertToInt(cells, lineNumber, 4);

        // minLongitude
        int minLongitude = convertToInt(cells, lineNumber, 5);

        // maxLongitude
        int maxLongitude = convertToInt(cells, lineNumber, 6);

        // postgis field
        String postgis = convertToString(cells, lineNumber, 7);

        ZoneCWP zone = dao.create(
                ZoneCWP.PROPERTY_QUADRANT, quadrant,
                ZoneCWP.PROPERTY_SQUARE_LATITUDE, squareLatitude,
                ZoneCWP.PROPERTY_SQUARE_LONGITUDE, squareLongitude,
                ZoneCWP.PROPERTY_MIN_LATITUDE, minLatitude,
                ZoneCWP.PROPERTY_MAX_LATITUDE, maxLatitude,
                ZoneCWP.PROPERTY_MIN_LONGITUDE, minLongitude,
                ZoneCWP.PROPERTY_MAX_LONGITUDE, maxLongitude
        );
        result.add(zone);
        String insertLine = String.format(INSERT_LINE_PATTERN,
                                          zone.getTopiaId(),
                                          zone.getTopiaVersion(),
                                          formatDate(zone.getTopiaCreateDate()),
                                          versionId,
                                          versionLibelle,
                                          versionStartDate,
                                          versionEndDate,
                                          zone.getQuadrant(),
                                          zone.getSquareLatitude(),
                                          zone.getSquareLongitude(),
                                          zone.getMinLatitude(),
                                          zone.getMaxLatitude(),
                                          zone.getMinLongitude(),
                                          zone.getMaxLongitude(),
                                          postgis
        );
        if (log.isDebugEnabled()) {
            log.debug(insertLine);
        }
        return insertLine;
    }
}
