/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesLengthStep;
import fr.ird.t3.entities.reference.SpeciesLengthStepDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Tool to creates content of {@link SpeciesLengthStep} reference from a csv file
 * and store it in a given database.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SpeciesLengthStepImporter extends AbstractReferenceImporter<SpeciesLengthStep> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SpeciesLengthStepImporter.class);

    @InjectDAO(entityType = SpeciesLengthStep.class)
    protected SpeciesLengthStepDAO dao;

    @InjectFromDAO(entityType = Ocean.class)
    protected List<Ocean> oceans;

    @InjectFromDAO(entityType = Species.class)
    protected List<Species> species;

    protected Map<Integer, Ocean> oceanByCode;

    protected Map<Integer, Species> specieByCode;

    @Override
    protected void before() throws TopiaException {

        if (CollectionUtils.isEmpty(oceans)) {
            throw new IllegalStateException("No ocean found in db.");
        }

        if (CollectionUtils.isEmpty(species)) {
            throw new IllegalStateException("No species found in db.");
        }

        long nb = dao.count();

        if (nb > 0) {
            throw new IllegalStateException(
                    "Some SpeciesLengthStep found in db.");
        }

        oceanByCode = T3EntityHelper.splitBycode(oceans);
        specieByCode = T3EntityHelper.splitBycode(species);
    }

    @Override
    protected List<SpeciesLengthStep> loadFile(File inputFile) throws TopiaException, IOException {

        List<SpeciesLengthStep> result = new ArrayList<SpeciesLengthStep>();

        LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
        try {

            String line;

            while ((line = reader.readLine()) != null) {

                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }
                if (line.startsWith(",,") || line.startsWith("OCEAN")) {
                    // this is not a data line
                    continue;
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.split(",");

                // always check we have 103 cells
                if (cells.length != 103) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have 103 cells but had here " +
                            cells.length);
                }
                loadLine(result, lineNumber, cells);
            }
        } finally {
            reader.close();
        }
        return result;
    }

    protected void loadLine(List<SpeciesLengthStep> result,
                            int lineNumber,
                            String[] cells) throws TopiaException {

        // first cell is ocean code
        int oceanCode = convertToInt(cells, lineNumber, 0);

        Ocean ocean = getOcean(oceanByCode, oceanCode, lineNumber);

        // second cell is species code
        int specieCode = convertToInt(cells, lineNumber, 1);
        Species species = getSpecie(specieByCode, specieCode, lineNumber);

        // third code is ld1 length class
        int ld1Class = convertToInt(cells, lineNumber, 2);

        // total ratio for this ld1 class
        int totalRatio = 0;

        // all the rest of cells are ld length class distribution
        for (int i = 3, j = cells.length; i < j; i++) {
            int ratio = convertToIntTenTimes(cells, lineNumber, i);
            if (ratio > 0) {

                // can create a new line lf class line

                int lfClass = 20 + 2 * (i - 3);

                totalRatio += ratio;
                SpeciesLengthStep speciesLengthStep = dao.create(
                        SpeciesLengthStep.PROPERTY_OCEAN, ocean,
                        SpeciesLengthStep.PROPERTY_SPECIES, species,
                        SpeciesLengthStep.PROPERTY_LD1_CLASS, ld1Class,
                        SpeciesLengthStep.PROPERTY_LF_CLASS, lfClass,
                        SpeciesLengthStep.PROPERTY_RATIO, ratio / 10f
                );
                result.add(speciesLengthStep);
            }
        }

        // totalRatio can be 0 or 1000, nothing else :)
        if (totalRatio == 0) {
            if (log.isDebugEnabled()) {
                log.debug("At line [" + lineNumber + "], [species " +
                          species.getLibelle() +
                          "], distribution of ld1 class " + ld1Class +
                          " is null.");
            }
        } else if (totalRatio != 1000) {
            if (log.isWarnEnabled()) {
                log.warn("At line [" + lineNumber + "], [species " +
                         species.getLibelle() +
                         "], distribution of ld1 class " + ld1Class +
                         " sum not to 1000 but to " + totalRatio);
            }
        }
    }
}
