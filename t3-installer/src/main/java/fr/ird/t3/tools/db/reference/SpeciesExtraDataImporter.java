/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A tool to import in species table extra data such as {@code measuredRatio} or
 * {@code lfLengthClassStep}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SpeciesExtraDataImporter extends AbstractReferenceImporter<Species> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SpeciesExtraDataImporter.class);

    @InjectDAO(entityType = Species.class)
    protected SpeciesDAO dao;

    @InjectFromDAO(entityType = Species.class)
    protected List<Species> species;

    protected Map<Integer, Species> specieByCode;

    @Override
    protected void before() throws TopiaException {

        if (CollectionUtils.isEmpty(species)) {
            throw new IllegalStateException("No species found in db.");
        }
        specieByCode = T3EntityHelper.splitBycode(species);

    }

    @Override
    protected List<Species> loadFile(File inputFile) throws TopiaException, IOException {

        List<Species> result = new ArrayList<Species>();

        LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
        try {

            String line;

            while ((line = reader.readLine()) != null) {

                if (line.trim().startsWith("#")) {
                    // this is a comment, do not treate it
                    continue;
                }
                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.split(":");

                // always check we have 4 cells
                if (cells.length != 5) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have 5 cells but had here " +
                            cells.length);
                }

                loadLine(result,
                         lineNumber,
                         cells
                );
            }
        } finally {
            reader.close();
        }

        return result;
    }

    protected void loadLine(List<Species> result,
                            int lineNumber,
                            String[] cells) throws TopiaException {

        // first cell is species code
        int specieCode = convertToInt(cells, lineNumber, 0);
        Species species = getSpecie(specieByCode, specieCode, lineNumber);

        // second cell is measuredRatio
        float measuredRatio = convertToFloat(cells, lineNumber, 1);

        // third cell is lfLengthClassStep
        int lfLengthClassStep = convertToInt(cells, lineNumber, 2);

        // fourth cell is thresholdNumberLevel3FreeSchoolType
        int thresholdNumberLevel3FreeSchoolType = convertToInt(cells, lineNumber, 3);

        // fith cell is thresholdNumberLevel3ObjectSchoolType
        int thresholdNumberLevel3ObjectSchoolType = convertToInt(cells, lineNumber, 4);

        species.setMeasuredRatio(measuredRatio);
        species.setLfLengthClassStep(lfLengthClassStep);
        species.setThresholdNumberLevel3FreeSchoolType(thresholdNumberLevel3FreeSchoolType);
        species.setThresholdNumberLevel3ObjectSchoolType(thresholdNumberLevel3ObjectSchoolType);

        dao.update(species);
        result.add(species);

    }
}
