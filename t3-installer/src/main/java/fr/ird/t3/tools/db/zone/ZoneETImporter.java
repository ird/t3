/*
 * #%L
 * T3 :: Business
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanDAO;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeDAO;
import fr.ird.t3.entities.reference.zone.ZoneEE;
import fr.ird.t3.entities.reference.zone.ZoneET;
import fr.ird.t3.entities.reference.zone.ZoneETDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Tool to import zone ET from a csv file and produce an output sql file with
 * update statement to inject postgis data.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ZoneETImporter extends AbstractZoneImporter<ZoneET, ZoneETDAO> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneETImporter.class);

    public static final String INSERT_LINE_PATTERN =
            "INSERT INTO ZONEET(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, VERSIONID, " +
            "VERSIONLIBELLE, VERSIONSTARTDATE, VERSIONENDDATE, GID, LIBELLE, SCHOOLTYPE, OCEAN, THE_GEOM) VALUES ('%s', %s, %s, '%s', '%s', %s, %s, %s, '%s', '%s', '%s', '%s');";

    Map<Integer, Ocean> oceanByCode;

    Map<Integer, SchoolType> schoolTypeByCode;

    public ZoneETImporter(String versionId,
                          String versionLibelle,
                          Date versionStartDate,
                          Date versionEndDate) {
        super(versionId,
              versionLibelle,
              versionStartDate,
              versionEndDate,
              5);
    }

    @Override
    protected ZoneETDAO getDAO() throws TopiaException {
        return T3DAOHelper.getZoneETDAO(getTransaction());
    }

    @Override
    protected void prepareExecute() throws TopiaException {
        OceanDAO oceanDAO = T3DAOHelper.getOceanDAO(getTransaction());
        List<Ocean> oceans = oceanDAO.findAll();

        if (CollectionUtils.isEmpty(oceans)) {
            throw new IllegalStateException("No ocean found in db.");
        }

        SchoolTypeDAO schoolTypeDAO = T3DAOHelper.getSchoolTypeDAO(getTransaction());

        List<SchoolType> schoolTypes = schoolTypeDAO.findAll();
        if (CollectionUtils.isEmpty(schoolTypes)) {
            throw new IllegalStateException("No school types found in db.");
        }

        oceanByCode = new TreeMap<Integer, Ocean>();
        for (Ocean ocean : oceans) {
            oceanByCode.put(ocean.getCode(), ocean);
        }


        schoolTypeByCode = new TreeMap<Integer, SchoolType>();
        for (SchoolType schoolType : schoolTypes) {
            schoolTypeByCode.put(schoolType.getCode(), schoolType);
        }
    }

    @Override
    protected String loadLine(List<ZoneET> result,
                              int lineNumber,
                              String[] cells) throws TopiaException {

        // first cell is gid code
        int gid = convertToInt(cells, lineNumber, 0);

        // second cell is libelle
        String libelle = convertToString(cells, lineNumber, 1);

        // third cell is ocean code
        int oceanCode = convertToInt(cells, lineNumber, 2);
        Ocean ocean = getOcean(oceanCode,
                               lineNumber
        );

        // fourth cell is school type  code
        int schoolTypeCode = convertToInt(cells, lineNumber, 3);
        SchoolType schoolType = getSchoolType(schoolTypeCode,
                                              lineNumber);

        // fiveth cell is postgis field
        String postgis = convertToString(cells, lineNumber, 4);


        ZoneET zone = dao.create(
                ZoneEE.PROPERTY_GID, gid,
                ZoneET.PROPERTY_OCEAN, ocean,
                ZoneET.PROPERTY_SCHOOL_TYPE, schoolType,
                ZoneEE.PROPERTY_LIBELLE, libelle
        );

        result.add(zone);
        String insertLine = String.format(INSERT_LINE_PATTERN,
                                          zone.getTopiaId(),
                                          zone.getTopiaVersion(),
                                          formatDate(zone.getTopiaCreateDate()),
                                          versionId,
                                          versionLibelle,
                                          versionStartDate,
                                          versionEndDate,
                                          zone.getGid(),
                                          zone.getLibelle().replaceAll("'", "''"),
                                          zone.getSchoolType().getTopiaId(),
                                          zone.getOcean().getTopiaId(),
                                          postgis
        );
        if (log.isDebugEnabled()) {
            log.debug(insertLine);
        }
        return insertLine;
    }

    protected SchoolType getSchoolType(int specieCode,
                                       int lineNumber) {

        SchoolType specie = schoolTypeByCode.get(specieCode);
        if (specie == null) {
            throw new NullPointerException(
                    "At line [" + lineNumber + "], school type with code [" +
                    specieCode + "] does not exists in db.");
        }
        return specie;
    }

    protected Ocean getOcean(int oceanCode,
                             int lineNumber) {

        Ocean ocean = oceanByCode.get(oceanCode);
        if (ocean == null) {
            throw new NullPointerException(
                    "At line [" + lineNumber + "], ocean with code [" +
                    oceanCode + "] does not exists in db.");
        }
        return ocean;
    }
}
